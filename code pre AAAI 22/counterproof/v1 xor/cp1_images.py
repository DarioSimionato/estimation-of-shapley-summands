import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import matplotlib.pyplot as plt
from matplotlib.transforms import offset_copy

with open('cp1_count_multiple_bounds_results_on_100.pickle', 'rb') as handle:
    results = pickle.load(handle)
# results

n_trs_els  = [10,25,50,100,500,1000,5000,10000]
n_val_els  = [1, 2, 5, 10, 100]
n_val_sets = [1, 10, 100, 1000, 10000, 100000]
n_tests    = 100

n = ["A", "B", "C", "S"]
fig, ax = plt.subplots(3,len(n), figsize = (15,15), sharey = "row")
pad = 5 # in points

ax[0][0].annotate("stats changing n_training_points", xy=(0, 0.5), xytext=(-ax[0][0].yaxis.labelpad - pad, 0),
                xycoords=ax[0][0].yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation = 90)
for i in range(len(n)):
    to_show_naive        = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"][n[i]]      for el_tr in n_trs_els]
    # to_show_prior_kn     = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["prior_kn"][n[i]]   for el_tr in n_trs_els]
    to_show_rademacher   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"][n[i]] for el_tr in n_trs_els]
    # to_show_MVS_naive      = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["MVS_naive"][n[i]]      for el_tr in n_trs_els]
    # to_show_MVS_prior_kn   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["MVS_prior_kn"][n[i]] for el_tr in n_trs_els]
    # to_show_MVS_rademacher = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["MVS_rademacher"][n[i]] for el_tr in n_trs_els]
    ax[0][i].plot([str(nn) for nn in n_trs_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[0][i].plot([str(nn) for nn in n_trs_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    ax[0][i].plot([str(nn) for nn in n_trs_els], to_show_rademacher, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[0][i].plot([str(nn) for nn in n_trs_els], to_show_MVS_naive, label = "MVS_naive", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[0][i].plot([str(nn) for nn in n_trs_els], to_show_MVS_prior_kn, label = "MVS_prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    # ax[0][i].plot([str(nn) for nn in n_trs_els], to_show_MVS_rademacher, label = "MVS_rademacher", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[0][i].set_title(n[i])
    ax[0][i].set_ylim(-5,105)
    ax[0][i].legend()


ax[1][0].annotate("stats changing n_validation_points", xy=(0, 0.5), xytext=(-ax[1][0].yaxis.labelpad - pad, 0),
                xycoords=ax[1][0].yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation = 90)
for i in range(len(n)):
    to_show_naive        = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"][n[i]]      for val_el in n_val_els]
    # to_show_prior_kn     = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["prior_kn"][n[i]]   for val_el in n_val_els]
    to_show_rademacher   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"][n[i]] for val_el in n_val_els]
    # to_show_MVS_naive      = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["MVS_naive"][n[i]]      for val_el in n_val_els]
    # to_show_MVS_prior_kn   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["MVS_prior_kn"][n[i]] for val_el in n_val_els]
    # to_show_MVS_rademacher = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["MVS_rademacher"][n[i]] for val_el in n_val_els]
    ax[1][i].plot([str(nn) for nn in n_val_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[1][i].plot([str(nn) for nn in n_val_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    ax[1][i].plot([str(nn) for nn in n_val_els], to_show_rademacher, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[1][i].plot([str(nn) for nn in n_val_els], to_show_MVS_naive, label = "MVS_naive", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[1][i].plot([str(nn) for nn in n_val_els], to_show_MVS_prior_kn, label = "MVS_prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    # ax[1][i].plot([str(nn) for nn in n_val_els], to_show_MVS_rademacher, label = "MVS_rademacher", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[1][i].set_ylim(-5,105)
    ax[1][i].legend()


ax[2][0].annotate("stats changing n_validation_sets", xy=(0, 0.5), xytext=(-ax[2][0].yaxis.labelpad - pad, 0),
                xycoords=ax[2][0].yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation = 90)
for i in range(len(n)):
    to_show_naive        = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"][n[i]]      for val_set in n_val_sets]
    # to_show_prior_kn     = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["prior_kn"][n[i]]   for val_set in n_val_sets]
    to_show_rademacher   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"][n[i]] for val_set in n_val_sets]
    # to_show_MVS_naive      = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["MVS_naive"][n[i]]      for val_set in n_val_sets]
    # to_show_MVS_prior_kn   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["MVS_prior_kn"][n[i]] for val_set in n_val_sets]
    # to_show_MVS_rademacher = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["MVS_rademacher"][n[i]] for val_set in n_val_sets]
    ax[2][i].plot([str(nn) for nn in n_val_sets], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[2][i].plot([str(nn) for nn in n_val_sets], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    ax[2][i].plot([str(nn) for nn in n_val_sets], to_show_rademacher, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[2][i].plot([str(nn) for nn in n_val_sets], to_show_MVS_naive, label = "MVS_naive", alpha = 0.5, marker='o', linestyle = 'solid')
    # ax[2][i].plot([str(nn) for nn in n_val_sets], to_show_MVS_prior_kn, label = "MVS_prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    # ax[2][i].plot([str(nn) for nn in n_val_sets], to_show_MVS_rademacher, label = "MVS_rademacher", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[2][i].set_ylim(-5,105)
    ax[2][i].legend()

fig.tight_layout()
fig.subplots_adjust(left=0.05, top=0.95)
#fig.show()
fig.savefig('cp1_plots.pdf')