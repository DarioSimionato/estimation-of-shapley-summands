class dependencyInfos:

    def __init__(self, df, method="p-value", independence_method="cor", data_file="", indep_file="", save_every_update = True, independence_language = "R", calculus_type = "all"):
        self.method = method
        self.independence_method = independence_method
        self.data_file = data_file
        self.indep_file = indep_file
        self.df = df
        self.save_every_update = save_every_update
        self.save_counter = 0
        self.independence_language = independence_language
        self.calculus_type = calculus_type   # "all" "only_>=5" "all_>=5"

        self.lookout_PCD = {}
        self.lookout_PC = {}
        self.lookout_MB = {False: {}, True:{}}
        self.lookout_Mod_PCD = {}
        self.lookout_Mod_PC = {}
        self.lookout_MB_MOD = {False: {}, True:{}}
        self.lookout_PC_def = {}
        self.lookout_MB_algo = {}
        self.lookout_independence = {}
        self.summands_initialized = False


    def initialize_dag_struc(name):
        if name == "alarm":
            return """[HIST|LVF][CVP|LVV][PCWP|LVV][HYP][LVV|HYP:LVF][LVF][STKV|HYP:LVF][ERLO][HRBP|ERLO:HR][HREK|ERCA:HR][ERCA][HRSA|ERCA:HR][ANES][APL][TPR|APL][ECO2|ACO2:VLNG][KINK][MINV|INT:VLNG][FIO2][PVS|FIO2:VALV][SAO2|PVS:SHNT][PAP|PMB][PMB][SHNT|INT:PMB][INT][PRSS|INT:KINK:VTUB][DISC][MVS][VMCH|MVS][VTUB|DISC:VMCH][VLNG|INT:KINK:VTUB][VALV|INT:VLNG][ACO2|VALV][CCHL|ACO2:ANES:SAO2:TPR][HR|CCHL][CO|HR:STKV][BP|CO:TPR]"""
        if name == "insurance":
            return "[Age][Mileage][SocioEcon|Age][GoodStudent|Age:SocioEcon][RiskAversion|Age:SocioEcon][OtherCar|SocioEcon][VehicleYear|SocioEcon:RiskAversion][MakeModel|SocioEcon:RiskAversion][SeniorTrain|Age:RiskAversion][HomeBase|SocioEcon:RiskAversion][AntiTheft|SocioEcon:RiskAversion][RuggedAuto|VehicleYear:MakeModel][Antilock|VehicleYear:MakeModel][DrivingSkill|Age:SeniorTrain][CarValue|VehicleYear:MakeModel:Mileage][Airbag|VehicleYear:MakeModel][DrivQuality|RiskAversion:DrivingSkill][Theft|CarValue:HomeBase:AntiTheft][Cushioning|RuggedAuto:Airbag][DrivHist|RiskAversion:DrivingSkill][Accident|DrivQuality:Mileage:Antilock][ThisCarDam|RuggedAuto:Accident][OtherCarCost|RuggedAuto:Accident][MedCost|Age:Accident:Cushioning][ILiCost|Accident][ThisCarCost|ThisCarDam:Theft:CarValue][PropCost|ThisCarCost:OtherCarCost]"
        if name == "ecoli":
            return "[b1191][cspG][eutG][cspA|cspG][fixC|b1191][sucA|eutG][yecO|cspG][yedE|cspG][atpG|sucA][cchB|fixC][dnaJ|sucA][flgD|sucA][gltA|sucA][lpdA|yedE][pspB|cspG:yedE][sucD|sucA][tnaA|b1191:fixC:sucA][yceP|eutG:fixC][yfiA|cspA][ygbD|fixC][ygcE|b1191:sucA][yhdM|sucA][yjbO|fixC][asnA|ygcE][atpD|sucA:ygcE][hupB|cspA:yfiA][ibpB|eutG:yceP][pspA|cspG:pspB:yedE][yfaD|eutG:sucA:yceP][icdA|asnA:ygcE][lacA|asnA:cspG][nmpC|pspA][yheI|atpD:yedE][aceB|icdA][b1963|yheI][dnaK|yheI][folK|yheI][lacY|asnA:cspG:eutG:lacA][ycgX|fixC:yheI][dnaG|ycgX:yheI][lacZ|asnA:lacA:lacY][nuoM|lacY][b1583|lacA:lacZ:yceP][mopB|dnaK:lacZ][yaeM|cspG:lacA:lacZ][ftsJ|mopB]"
    