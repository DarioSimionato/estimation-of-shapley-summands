# contains functions to perform the evaluation of R^2 values

import numpy as np
from sklearn import linear_model
import math
from itertools import chain, combinations
import pickle as pickle
import os
import shutil
import multiprocess as mp
import itertools

def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def compose_dataset(X, col_names, selection):
    # given a selection of variables, create a dataset using only the respective variables
    X_ret = []
    for letter in selection:
        X_ret.append(X[:,col_names.index(letter)])
    return np.array(X_ret).transpose()

def get_R2_score(y_mean, y_real, y_pred):
    return 1-sum((y_real - y_pred)*(y_real - y_pred))/sum((y_real - y_mean)*(y_real - y_mean))

def get_scores_from_training(n_elements_tr, n_elements_va, n_valids, variable_set):
    # creates a dataset and multiple validation sets calculating R^2 considering each subset of variables.
    # Returns a dictionary with the subset as a key and an array of R^2 values of each validation set
    R2_values  = dict()
    for subset in powerset(variable_set):
        R2_values[subset] = []
               
    train_size = n_elements_tr
    valid_size = n_elements_va
    np.warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning) 
    
    # creation of the training set
    a1 = np.random.normal(scale=2.0, size=train_size)
    b1 = np.random.normal(scale=2.0, size=train_size)
    c1 = np.random.normal(scale=2.0, size=train_size)
    s1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=train_size)
    t1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=train_size)
    extra = [np.random.normal(scale=2.0, size=train_size) for v in variable_set if v not in "ABCS"]
    extra = [item for sublist in extra for item in sublist]
    X_tot = [item for sublist in np.array([a1,b1,c1,s1,extra]) for item in sublist]

    X_train_tot = np.reshape(np.array(X_tot).flatten(), (len(variable_set),train_size)).transpose()
        
    # X_train_tot = np.array([a1,b1,c1,s1]).transpose()
    Y_train = np.array(t1)
    Y_train_mean = np.mean(Y_train)

    lin_reg = {}
    for subset in powerset(variable_set):
        # create and train one linear regression per variable subset
        X_train = compose_dataset(X = X_train_tot, col_names = variable_set, selection = subset)
        lin_reg[subset] = linear_model.LinearRegression() 
        lin_reg[subset].fit(X_train, Y_train)  
    
    for valid_number in range(n_valids):   
        
        # create a validation set
        a2 = np.random.normal(scale=2.0, size=valid_size)
        b2 = np.random.normal(scale=2.0, size=valid_size)
        c2 = np.random.normal(scale=2.0, size=valid_size)
        s2 = a2 + b2 + c2 + np.random.normal(scale=2.0, size=valid_size)
        t2 = a2 + b2 + c2 + np.random.normal(scale=2.0, size=valid_size)
        extra = [np.random.normal(scale=2.0, size=valid_size) for v in variable_set if v not in "ABCS"]
        extra = [item for sublist in extra for item in sublist]
        X_tot = [item for sublist in np.array([a2,b2,c2,s2,extra]) for item in sublist]

        X_valid_tot = np.reshape(np.array(X_tot).flatten(), (len(variable_set),valid_size)).transpose()

        # X_valid_tot = np.array([a2,b2,c2,s2]).transpose()
        Y_valid = np.array(t2)           
        
        for subset in powerset(variable_set):
            # evaluate the performance of each validation set considering every subset of variables
            X_valid = compose_dataset(X = X_valid_tot,  col_names = variable_set, selection = subset)
            # calculate R2 using training set mean
            v = get_R2_score(y_mean = Y_train_mean, y_real = Y_valid, y_pred = lin_reg[subset].predict(X_valid))
            R2_values[subset].append(v)  

    return R2_values

def calculate_and_save_R2_values(n_trs_els, n_val_els, n_val_sets, i, var_set):
    # parallelizes the calculation of R^2 values

    path = os.path.join(os.getcwd(), "data_bound",'test'+str(i)+"_"+str(n_trs_els)+"ntels"+str(n_val_els)+"nvels"+str(n_val_sets)+'sets')
    if os.path.exists(path):
        shutil.rmtree(path)  # clear previous data (if present)
    os.mkdir(path)

    R2_values = get_scores_from_training(n_elements_tr = n_trs_els, n_elements_va = n_val_els, n_valids = n_val_sets,
        variable_set = var_set)

    with open(os.path.join('.',"data_bound",'test'+str(i)+"_"+str(n_trs_els)+"ntels"+str(n_val_els)+"nvels"+str(n_val_sets)+'sets','R2_values_TRel'+str(n_trs_els)+'_VALel'+str(n_val_els)+'_on'+str(n_val_sets)+'sets.pickle'), 'wb') as handle:
        pickle.dump(R2_values, handle, protocol=pickle.HIGHEST_PROTOCOL)
    