# import numpy as np
# import math
# from itertools import chain, combinations
# import networkx as nx
# import matplotlib.pyplot as plt
# import math
# import pickle5 as pickle
# # import heapq


# def powerset(iterable):
#     "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
#     s = list(iterable)
#     return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

# ps = [str(t) for t in powerset("ABCS")]
# ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]
# # for w in ps:
# #     print(w,len(w),"AS" in w)
# # for p in ps:
# #     print([p.issubset(pp) for pp in ps])

# dg = nx.DiGraph()
# # dg.add_node(str(set()))
# for el in ps:
#     dg.add_node(el)
# prev_len = 0
# prev_layer = []
# # prev_layer = [set()]
# curr_layer = []
# for el in ps:
#     if len(el) != prev_len +1:
#         prev_layer = curr_layer
#         curr_layer = []
#         prev_len = prev_len + 1
#         print(el, curr_layer)
#     for pr_el in prev_layer:
#         if set([x for x in pr_el]).issubset(set([x for x in el])):
#             dg.add_edge(el, pr_el)
#             # print(str(pr_el), "->", str(el))
#     curr_layer.append(el)

# pos = {}
# # pos = {str(set()) : (2.5,0)}
# curr_len = 0
# addition = [1,0,1,2.5]
# for el in ps:
#     if curr_len != len(el):
#         curr_len = len(el)
#         shift = 0
#     pos[el] = (shift+addition[len(el)-1], len(el))
#     shift = shift + 1

# plt.subplot()
# nx.draw(dg, pos, with_labels=True, font_weight='bold')
# plt.show()
# # img = Image.open(plt)
# # img.show()
# def minimals():
#     return ["ABCS"]
    
# def get_children(graph, node):
#     return [n for n in dg.adj[node]]

# dg2 = nx.DiGraph()
# pos = {}
# sums = []
# for e in ps:
#     for c in get_children(dg,e):
#         if str(c) =="":
#             c = ()
#         if str(c) != str(e):
#             if str(e)+"-"+str(c) not in sums:
#                 dg2.add_node(str(e)+"-"+str(c))
#                 sums.append(str(e)+"-"+str(c))
# sums = sorted(sums, reverse= True, key = lambda x: len(x))
# dg2.nodes()
# row = 0
# col = 0
# curr_l = len(sums[0])
# for s in sums:
#     if curr_l > len(s):
#         curr_l = len(s)
#         col = 0
#     for v in "ABCS":
#         if s.count(v) == 2:
#             # print(s,s.replace(v,""),s.count(v))
#             second = s.replace(v,"")
#             if str(second) =="":
#                 second = "()"
#             dg2.add_edge(s,second)
#             if s not in pos:
#                 pos[s] = (len(s),col)
#                 col += 1
# dg2.nodes()

# dg2 = nx.DiGraph()
# pos = {}
# new_ps = []
# for var in "ABCS":
#     for s1 in ps:
#         if var in s1:
#             s2 = s1.replace(var, "")                
#             if s2 == "":
#                 s2 = "()" 
#             new_ps.append(s1+" - "+s2)
#             dg2.add_node(s1+" - "+s2)
# edgs = []
# for s1 in new_ps:
#     for s2 in new_ps:
#         if len(s1) == len(s2) + 2:
#             if any([s1.replace(v,"") == s2 for v in "ABCS"]) and (s1,s2) not in edgs:
#                 dg2.add_edge(s1,s2)
#                 edgs.append((s1,s2))



# plt.subplot()
# nx.draw(dg2,  with_labels=True, font_weight='bold')
# plt.show()


# # plt.show()
# # fig.show()
# # fig.savefig('plots_annotation.pdf')