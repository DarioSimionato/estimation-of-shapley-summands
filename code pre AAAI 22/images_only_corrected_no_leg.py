
import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import matplotlib.pyplot as plt
from matplotlib.transforms import offset_copy

# results

n_trs_els  = [10,25,50,100,500,1000,5000,10000]
n_val_els  = [1, 2, 5, 10, 100]
n_val_sets = [1, 10, 100, 1000, 10000, 100000]
n_tests    = 100

fig, ax = plt.subplots(2,3, figsize = (25,9))
pad = 5 # in points

for a in ax.ravel():
    a.set_box_aspect(1)

# ax[0][0].annotate("By changing training set size", xy=(0, 0.5), xytext=(-ax[0][0].yaxis.labelpad - pad, 0),
#                 xycoords=ax[0][0].yaxis.label, textcoords='offset points',
#                 size='large', ha='right', va='center', rotation = 90)
# ax[0][1].annotate("By changing test set size", xy=(0, 0.5), xytext=(-ax[0][1].yaxis.labelpad - pad, 0),
#                 xycoords=ax[0][1].yaxis.label, textcoords='offset points',
#                 size='large', ha='right', va='center', rotation = 90)
# ax[0][2].annotate("By changing number of test sets", xy=(0, 0.5), xytext=(-ax[0][2].yaxis.labelpad - pad, 0),
#                 xycoords=ax[0][2].yaxis.label, textcoords='offset points',
#                 size='large', ha='right', va='center', rotation = 90)
ax[0][0].set_title('By changing training set size', multialignment='center')
ax[0][1].set_title('By changing test set size', multialignment='center')
ax[0][2].set_title('By changing number of test sets', multialignment='center')
with open('count_results_on100.pickle', 'rb') as handle:
    results = pickle.load(handle)

    to_show_naive        = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"]      for el_tr in n_trs_els]
    to_show_prior_kn     = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["prior_kn"]["correct"]   for el_tr in n_trs_els]
    to_show_multiple_bounds   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
    to_show_single_bound   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
    
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='^', linestyle = 'solid', markersize=8)
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='v', linestyle = 'dashed', markersize=8)
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_multiple_bounds, color = "darkgreen", label = "multiple_bounds", alpha = 0.5, marker='<', linestyle = 'solid', markersize=8)
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='>', linestyle = 'solid', markersize=8)
    
    # ax[0][0].set_title("First test")
    ax[0][0].annotate("First experiment", xy=(0, 0.5), xytext=(-ax[0][0].yaxis.labelpad - pad, 0),
                xycoords=ax[0][0].yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation = 90)
    ax[0][0].set_ylim(-5,105)
    ax[0][0].set_xlabel("Training set size")
    ax[0][0].set_ylabel("Percentage of correct results")
    # ax[0][0].legend()

    to_show_naive        = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"]      for val_el in n_val_els]
    to_show_prior_kn     = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["prior_kn"]["correct"]   for val_el in n_val_els]
    to_show_multiple_bounds   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
    to_show_single_bound   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
    
    ax[0][1].plot([str(nn) for nn in n_val_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='^', linestyle = 'solid', markersize=8)
    ax[0][1].plot([str(nn) for nn in n_val_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='v', linestyle = 'dashed', markersize=8)
    ax[0][1].plot([str(nn) for nn in n_val_els], to_show_multiple_bounds, color = "darkgreen", label = "multiple_bounds", alpha = 0.5, marker='<', linestyle = 'solid', markersize=8)
    ax[0][1].plot([str(nn) for nn in n_val_els], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='>', linestyle = 'solid', markersize=8)
    
    ax[0][1].set_ylim(-5,105)
    ax[0][1].set_xlabel("Test set size")
    ax[0][1].set_ylabel("Percentage of correct results")
    # ax[1][0].legend()

    to_show_naive        = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"]      for val_set in n_val_sets]
    to_show_prior_kn     = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["prior_kn"]["correct"]   for val_set in n_val_sets]
    to_show_multiple_bounds   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
    to_show_single_bound   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
    
    ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='^', linestyle = 'solid', markersize=8)
    ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='v', linestyle = 'dashed', markersize=8)
    ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_multiple_bounds, color = "darkgreen", label = "multiple_bounds", alpha = 0.5, marker='<', linestyle = 'solid', markersize=8)
    ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='>', linestyle = 'solid', markersize=8)
    
    ax[0][2].set_ylim(-5,105)
    ax[0][2].set_xlabel("Number of test sets")
    ax[0][2].set_ylabel("Percentage of correct results")
    # ax[2][0].legend()


with open(os.path.join(os.getcwd(),'counterproof','v4 square function','cp4_count_LRresults_on100.pickle'), 'rb') as handle:
    resultsLR = pickle.load(handle)
    with open(os.path.join(os.getcwd(),'counterproof','v4 square function','cp4_count_NNresults_on100.pickle'), 'rb') as handle:
        resultsNN = pickle.load(handle)

        to_show_naiveLR        = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"] for el_tr in n_trs_els]
        to_show_multiple_boundsLR   = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
        to_show_single_boundLR   = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
        to_show_naiveNN        = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"] for el_tr in n_trs_els]
        to_show_multiple_boundsNN   = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
        to_show_single_boundNN   = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
        
        ax[1][0].plot([str(nn) for nn in n_trs_els], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='^', linestyle = 'solid', markersize=8)
        ax[1][0].plot([str(nn) for nn in n_trs_els], to_show_multiple_boundsLR, color = "darkgreen", label = "multiple_bounds_LR", alpha = 0.5, marker='v', linestyle = 'solid', markersize=8)
        ax[1][0].plot([str(nn) for nn in n_trs_els], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='<', linestyle = 'solid', markersize=8)
        ax[1][0].plot([str(nn) for nn in n_trs_els], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='*', linestyle = 'solid', markersize=10)
        ax[1][0].plot([str(nn) for nn in n_trs_els], to_show_multiple_boundsNN, color = "magenta", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid', markersize=6)
        ax[1][0].plot([str(nn) for nn in n_trs_els], to_show_single_boundNN, color = "orange", label = "single_bound_NN", alpha = 0.5, marker='d', linestyle = 'solid', markersize=8)
        
        # ax[1][0].set_title("Second test")
        ax[1][0].annotate("Second experiment", xy=(0, 0.5), xytext=(-ax[1][0].yaxis.labelpad - pad, 0),
                xycoords=ax[1][0].yaxis.label, textcoords='offset points',
                size='large', ha='right', va='center', rotation = 90)
        ax[1][0].set_xlabel("Training set size")
        ax[1][0].set_ylabel("Percentage of correct results")
        ax[1][0].set_ylim(-5,105)
        # ax[0][1].legend()

        to_show_naiveLR        = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"] for val_el in n_val_els]
        to_show_multiple_boundsLR   = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
        to_show_single_boundLR   = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
        to_show_naiveNN        = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"] for val_el in n_val_els]
        to_show_multiple_boundsNN   = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
        to_show_single_boundNN   = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
        
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='^', linestyle = 'solid', markersize=8)
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_multiple_boundsLR, color = "darkgreen", label = "multiple_bounds_LR", alpha = 0.5, marker='v', linestyle = 'solid', markersize=8)
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='<', linestyle = 'solid', markersize=8)
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='*', linestyle = 'solid', markersize=10)
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_multiple_boundsNN, color = "magenta", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid', markersize=6)
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_single_boundNN, color = "orange", label = "single_bound_NN", alpha = 0.5, marker='d', linestyle = 'solid', markersize=8)

        ax[1][1].set_ylim(-5,105)
        ax[1][1].set_xlabel("Test set size")
        ax[1][1].set_ylabel("Percentage of correct results")
        # ax[1][1].legend()

        to_show_naiveLR        = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"] for val_set in n_val_sets]
        to_show_multiple_boundsLR   = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
        to_show_single_boundLR   = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
        to_show_naiveNN        = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"] for val_set in n_val_sets]
        to_show_multiple_boundsNN   = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
        to_show_single_boundNN   = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
        
        ax[1][2].plot([str(nn) for nn in n_val_sets], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='^', linestyle = 'solid', markersize=8)
        ax[1][2].plot([str(nn) for nn in n_val_sets], to_show_multiple_boundsLR, color = "darkgreen", label = "multiple_bounds_LR", alpha = 0.5, marker='v', linestyle = 'solid', markersize=8)
        ax[1][2].plot([str(nn) for nn in n_val_sets], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='<', linestyle = 'solid', markersize=8)
        ax[1][2].plot([str(nn) for nn in n_val_sets], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='*', linestyle = 'solid', markersize=10)
        ax[1][2].plot([str(nn) for nn in n_val_sets], to_show_multiple_boundsNN, color = "magenta", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid', markersize=6)
        ax[1][2].plot([str(nn) for nn in n_val_sets], to_show_single_boundNN, color = "orange", label = "single_bound_NN", alpha = 0.5, marker='d', linestyle = 'solid', markersize=8)
        
        ax[1][2].set_ylim(-5,105)
        ax[1][2].set_xlabel("Number of test sets")
        ax[1][2].set_ylabel("Percentage of correct results")
        # ax[2][1].legend()

# ax[0][2].legend(loc="center left", bbox_to_anchor=(1.05, 0.5))
# ax[1][2].legend(loc="center left", bbox_to_anchor=(1.05, 0.5))
for i in range(2):
    for j in range(3):
        plt.setp(ax[i][j].xaxis.get_majorticklabels(), rotation=45)


fig.tight_layout()
# fig.subplots_adjust(left=0.1, top=0.95, right = 0.8, wspace = 0.25, hspace = 0.025)
fig.subplots_adjust(left=0.1, top=0.95, right = 0.8, wspace = 0.25, hspace = 0.1)
# fig.set_size_inches(fig.get_size_inches() * np.array([1.8, 1]), forward=True)
fig.suptitle("Correct answers out of 100 runs")
fig.show()
fig.savefig('plots_only_correct_100.pdf')