import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import multiprocess as mp
import itertools
from itertools import chain, combinations
from cp1_calculate_bounds import create_S_matrix, calculate_bound_per_variable, calculate_bound_total, create_summand_S
from cp1_ds_creator import calculate_and_save_R2_values

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits

raise Exception("XOR counterproof to discard since it is an ill posed problem")

class ResultDict():
    # dictionary container of the results on each trial

    def __init__(self, n_trs_els, n_val_els, n_val_sets):
        self.results = dict()
        for n_el_tr in n_trs_els:
            self.results[n_el_tr] = dict()
            for n_el_val in n_val_els:
                self.results[n_el_tr][n_el_val] = dict()
                for n_v_sets in n_val_sets:    
                    self.results[n_el_tr][n_el_val][n_v_sets] = dict()
                            
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["S"] = 0
                    
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["S"] = 0

    def add_values(self, n_el_tr, n_el_val, n_v_sets, res_naive, res_prior_kn, res_rademacher, res_MVS_naive, res_MVS_prior_kn, res_MVS_rademacher):
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["A"]        += 1*res_naive["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["B"]        += 1*res_naive["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["C"]      += 1*res_naive["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["S"]      += 1*res_naive["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["A"]     += 1*res_prior_kn["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["B"]     += 1*res_prior_kn["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["C"]   += 1*res_prior_kn["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["S"]   += 1*res_prior_kn["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["A"]   += 1*res_rademacher["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["B"]   += 1*res_rademacher["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["C"] += 1*res_rademacher["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["S"] += 1*res_rademacher["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["A"]       += 1*res_MVS_naive["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["B"]       += 1*res_MVS_naive["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["C"]     += 1*res_MVS_naive["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_naive"]["S"]     += 1*res_MVS_naive["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["A"]    += 1*res_MVS_prior_kn["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["B"]    += 1*res_MVS_prior_kn["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["C"]  += 1*res_MVS_prior_kn["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_prior_kn"]["S"]  += 1*res_MVS_prior_kn["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["A"]  += 1*res_MVS_rademacher["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["B"]  += 1*res_MVS_rademacher["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["C"]+= 1*res_MVS_rademacher["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["MVS_rademacher"]["S"]+= 1*res_MVS_rademacher["S"]


def calculate_variable_informativeness(variable_set, emp_means, sd_bounds, summands_name, thr_prior_kn, thr_MVS_prior_kn, method):
    # given the empirical means and the supreme deviation bounds, it calculates if each variable is informative.
    # The calculation is performed in different ways:
    # 1 - naive: the variable is informative if all its summands are > 0
    # 2 - prior_kn: the variable is informative if all its summands are > threshold (thr_prior_kn)
    # 3 - rademacher: the variable is informative if for every summand if the confidence interval given by the bound
    #     on the supremum deviation is > 0
    # 4 - MVS_rademacher: for each variable v, the variable is informative only if the summand all-vars - all-but-v > 0
    # 5 - MVS_prior_kn: for each variable v, the variable is informative only if the summand all-vars - all-but-v > threshold (thr_MVS_prior_kn)
    # For each approach, it returns if the method spotted any false positive, any false negative and if it spots all
    # and only important variables as such.

    inf_naive = dict()
    inf_MVS_naive = dict()
    inf_prior_kn = dict()
    inf_MVS_prior_kn = dict()
    inf_rademacher = dict()
    inf_MVS_rademacher = dict()
    
    res_naive = dict()
    res_MVS_naive = dict()
    res_prior_kn = dict()
    res_MVS_prior_kn = dict()
    res_rademacher = dict()
    res_MVS_rademacher = dict()

    for v in variable_set:
        selected_summands = [i for i in range(len(summands_name)) if summands_name[i].count(v) == 1]
        inf_naive[v]      = all(emp_means[selected_summands]>0)
        inf_prior_kn[v]   = all(emp_means[selected_summands]>thr_prior_kn)
        if method == "multiple_bounds":
            inf_rademacher[v] = all(emp_means[selected_summands] - sd_bounds[selected_summands]>0)
        else:
            inf_rademacher[v] = all(emp_means[selected_summands] - sd_bounds>0)
        biggest_summ = [i for i in selected_summands if variable_set in summands_name[i]][0]   
        inf_MVS_naive[v]      = emp_means[biggest_summ] > 0 
        inf_MVS_prior_kn[v]   = emp_means[biggest_summ] > thr_MVS_prior_kn
        if method == "multiple_bounds":
            inf_MVS_rademacher[v] = (emp_means[biggest_summ] - sd_bounds[biggest_summ])>0
        else:
            inf_MVS_rademacher[v] = (emp_means[biggest_summ] - sd_bounds)>0 

    
    return inf_naive, inf_prior_kn, inf_rademacher, inf_MVS_naive, inf_MVS_prior_kn, inf_MVS_rademacher

def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def loop_calculation(*args):
    # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable
    params = args[0]
    n_el_tr = params[0]
    n_el_val = params[1]
    n_v_sets = params[2]
    ps = params[3]
    min_val = params[4]
    thr_prior_kn = params[5]
    thr_MVS_prior_kn = params[6]
    method = params[7]

    delta = 0.05
    n = 1000
    m = n_v_sets
    sigma = np.random.rand(n,m) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 

    with open(os.path.join('.','data','R2_values_TRel'+str(n_el_tr)+'_VALel'+str(n_el_val)+'_on'+str(n_v_sets)+'sets.pickle'), 'rb') as handle:
        R2_dict = pickle.load(handle)

    if method == "multiple_bounds":
        naive_val = 0
    S, new_ps = create_S_matrix(R2_dict =  R2_dict, ps = ps, min_val = min_val, n_v_sets = n_v_sets, variable_set = "ABCS", naive_score = naive_val)
        corrected_delta = delta / len(new_ps)  # correction for multiple hypotesis testing
        z = max(1,abs(1-min_val))   # since the minimum value of the summand V1-V2 may be -2 in the case of V1 = 1 and V2 = -1
        c = abs(1 - abs(1-min_val)) # for the same reason of above
        sd_bounds = calculate_bound_per_variable(S, n=n, sigma = sigma, delta=corrected_delta, z = z, c = c, get_splitted_values = False)
        emp_means  = np.mean(S, axis = 0)
    elif method == "single_bound":
        naive_val = 0
    S, new_ps = create_S_matrix(R2_dict =  R2_dict, ps = ps, min_val = min_val, n_v_sets = n_v_sets, variable_set = "ABCS", naive_score = naive_val, keep_metrics_divided= True)
        z = max(1,min_val)   
        c = abs(1 - min_val) 
        sd_bounds = calculate_bound_total(n=n, variable_set = "ABCS", S = S, S_names=new_ps, sigma = sigma, delta = delta, z=z, c=c)
        sd_bounds = 2 * sd_bounds # errors are summed since I make difference of values
        emp_means  = np.mean(S, axis = 0).reshape(1,-1)
        emp_means, new_ps  = create_summand_S(emp_means,ps, variable_set = "ABCS")
        emp_means = emp_means[0]

    
    res_naive, res_prior_kn, res_rademacher, res_MVS_naive, res_MVS_prior_kn, res_MVS_rademacher = calculate_variable_informativeness(variable_set = "ABCS", emp_means = emp_means, sd_bounds = sd_bounds,
                     summands_name = new_ps, thr_prior_kn = thr_prior_kn, thr_MVS_prior_kn = thr_MVS_prior_kn, method = method)

    return n_el_tr, n_el_val, n_v_sets, res_naive, res_prior_kn, res_rademacher, res_MVS_naive, res_MVS_prior_kn, res_MVS_rademacher, method

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits


if __name__ == '__main__':
    ps = [str(t) for t in powerset("ABCS")]
    ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]

    parser = argparse.ArgumentParser('CP4')
    parser.add_argument('-p', '--processors', type=int, default= -1)
    parser.add_argument('-r', '--runs', type=int, default= 100)
    parser.add_argument('-t', '--time', type=bool, default= True)
    
    args = parser.parse_args()
    
    process_number = args.processors
    n_tests = args.runs
    output_time = args.time

    n_trs_els  = [10,25,50,100,500,1000,5000,10000]
    n_val_els  = [1, 2, 5, 10, 100]
    n_val_sets = [1, 10, 100, 1000, 10000, 100000]

    min_val = -1  
    thr_prior_kn   = (7/12-9/16)/2   # for this experiment scope, it's half the value of one of the lowest summands (AS - S)
    thr_MVS_prior_kn = (3/4-5/8)/2     # half the value of the lowest positive summunds that uses all - all-but-one params
    seed = 120395
    np.random.seed(seed=seed)
    if process_number < 1:
        process_number = len(n_trs_els)*len(n_val_els)*len(n_val_sets)
    if process_number > 72:
        print("Limiting process number to 72")
        process_number = 72

    # initialize result dict structure
    res_multiple_bounds = ResultDict(n_trs_els, n_val_els, n_val_sets)
    res_single_bound = ResultDict(n_trs_els, n_val_els, n_val_sets)

    print("process_number",process_number)
    print("n_tests",n_tests)
    print("output_time",output_time)

    if output_time:
        t1 = time.time()
        
    with threadpool_limits(limits=1, user_api='blas'):
        for test in range(n_tests):
            calculate_and_save_R2_values(n_trs_els = n_trs_els, n_val_els = n_val_els, n_val_sets = n_val_sets, process_number = process_number)

            p = mp.Pool(process_number)

            input = ((n_el_tr, n_el_val, n_v_sets, ps, min_val, thr_prior_kn, thr_MVS_prior_kn, method) 
                        for n_el_tr, n_el_val, n_v_sets, ps, min_val, thr_prior_kn, thr_MVS_prior_kn, method
                        in list(itertools.product(n_trs_els, n_val_els, n_val_sets, [ps], [min_val], [thr_prior_kn], [thr_MVS_prior_kn], ["multiple_bounds","single_bound"])))
            results = p.map(loop_calculation, input)   

            p.close()
            p.join()
            
            for n_el_tr, n_el_val, n_v_sets, res_naive, res_prior_kn, res_rademacher, res_MVS_naive, res_MVS_prior_kn, res_MVS_rademacher, method in results:
                if method == "multiple_bounds":
                    res_multiple_bounds.add_values(n_el_tr, n_el_val, n_v_sets, res_naive, res_prior_kn, res_rademacher, res_MVS_naive, res_MVS_prior_kn, res_MVS_rademacher)
                elif method == "single_bound":
                    res_single_bound.add_values(n_el_tr, n_el_val, n_v_sets, res_naive, res_prior_kn, res_rademacher, res_MVS_naive, res_MVS_prior_kn, res_MVS_rademacher)
            
            if output_time:
                print((time.time()-t1)/60,"End of test",test)
            else:
                print("End test",test)

        with open('count_multiple_bounds_results_on_'+str(n_tests)+'.pickle', 'wb') as handle:
            pickle.dump(res_multiple_bounds.results, handle, protocol=pickle.HIGHEST_PROTOCOL)
        with open('count_single_bound_results_on_'+str(n_tests)+'.pickle', 'wb') as handle:
            pickle.dump(res_single_bound.results, handle, protocol=pickle.HIGHEST_PROTOCOL)

    if output_time:
        print((time.time()-t1)/60,"minutes elapsed for",n_tests,"runs")