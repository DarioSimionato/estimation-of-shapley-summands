import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import multiprocess as mp
import itertools
from itertools import chain, combinations
from calculate_bounds import create_S_matrix, calculate_bound_per_variable, calculate_bound_total, create_summand_S
from ds_creator import calculate_and_save_R2_values

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits


class ResultDict():
    # dictionary container of the results on each trial

    def __init__(self, n_trs_els, n_val_els, n_val_sets):
        self.results = dict()
        for n_el_tr in n_trs_els:
            self.results[n_el_tr] = dict()
            for n_el_val in n_val_els:
                self.results[n_el_tr][n_el_val] = dict()
                for n_v_sets in n_val_sets:    
                    self.results[n_el_tr][n_el_val][n_v_sets] = dict()
                            
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["no FP"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["no FN"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["correct"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["no FP"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["no FN"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["correct"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["no FP"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["no FN"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["correct"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["S"] = 0

                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"] = dict()
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["no FP"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["no FN"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["correct"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["A"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["B"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["C"] = 0
                    self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["S"] = 0

    def add_values(self, n_el_tr, n_el_val, n_v_sets, inf_naive, inf_prior_kn, inf_rademacher_sb, inf_rademacher_mb, res_naive, res_prior_kn, res_rademacher_sb, res_rademacher_mb):
    
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["no FP"]        += 1*res_naive["no FP"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["no FN"]        += 1*res_naive["no FN"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["correct"]      += 1*res_naive["correct"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["no FP"]     += 1*res_prior_kn["no FP"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["no FN"]     += 1*res_prior_kn["no FN"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["correct"]   += 1*res_prior_kn["correct"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["no FP"]  += 1*res_rademacher_sb["no FP"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["no FN"]  += 1*res_rademacher_sb["no FN"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["correct"]+= 1*res_rademacher_sb["correct"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["no FP"]  += 1*res_rademacher_mb["no FP"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["no FN"]  += 1*res_rademacher_mb["no FN"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["correct"]+= 1*res_rademacher_mb["correct"]

        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["A"]        += 1*inf_naive["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["B"]        += 1*inf_naive["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["C"]        += 1*inf_naive["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["naive"]["S"]        += 1*inf_naive["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["A"]     += 1*inf_prior_kn["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["B"]     += 1*inf_prior_kn["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["C"]     += 1*inf_prior_kn["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["prior_kn"]["S"]     += 1*inf_prior_kn["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["A"]   += 1*inf_rademacher_sb["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["B"]   += 1*inf_rademacher_sb["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["C"]   += 1*inf_rademacher_sb["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["single_bound"]["S"]   += 1*inf_rademacher_sb["S"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["A"]   += 1*inf_rademacher_mb["A"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["B"]   += 1*inf_rademacher_mb["B"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["C"]   += 1*inf_rademacher_mb["C"]
        self.results[n_el_tr][n_el_val][n_v_sets]["multiple_bounds"]["S"]   += 1*inf_rademacher_mb["S"]
        



def calculate_variable_informativeness(variable_set, emp_means, sd_bounds_sb, sd_bounds_mb, summands_name, thr_prior_kn):
    # given the empirical means and the supreme deviation bounds, it calculates if each variable is informative.
    # The calculation is performed in different ways:
    # 1 - naive: the variable is informative if all its summands are > 0
    # 2 - prior_kn: the variable is informative if all its summands are > threshold (thr_prior_kn)
    # 3 - rademacher: the variable is informative if for every summand if the confidence interval given by the bound
    #     on the supremum deviation is > 0. 
    # For each approach, it returns if the method spotted any false positive, any false negative and if it spots all
    # and only important variables as such.
    
    inf_naive = dict()
    inf_prior_kn = dict()
    inf_rademacher_sb = dict()
    inf_rademacher_mb = dict()
    
    res_naive = dict()
    res_prior_kn = dict()
    res_rademacher_sb = dict()
    res_rademacher_mb = dict()
    
    for v in variable_set:
        selected_summands = [i for i in range(len(summands_name)) if summands_name[i].count(v) == 1]
        inf_naive[v]      = all(emp_means[selected_summands]>0)
        inf_prior_kn[v]   = all(emp_means[selected_summands]>thr_prior_kn)
        inf_rademacher_sb[v] = all(emp_means[selected_summands] - sd_bounds_sb>0)  
        inf_rademacher_mb[v] = all(emp_means[selected_summands] - sd_bounds_mb[selected_summands]>0)

    res_naive["no FP"] = not inf_naive["S"]
    res_naive["no FN"] = inf_naive["A"] and inf_naive["B"] and inf_naive["C"]
    res_naive["correct"] = res_naive["no FP"] and res_naive["no FN"]
    
    res_prior_kn["no FP"] = not inf_prior_kn["S"]
    res_prior_kn["no FN"] = inf_prior_kn["A"] and inf_prior_kn["B"] and inf_prior_kn["C"]
    res_prior_kn["correct"] = res_prior_kn["no FP"] and res_prior_kn["no FN"]
    
    res_rademacher_sb["no FP"] = not inf_rademacher_sb["S"]
    res_rademacher_sb["no FN"] = inf_rademacher_sb["A"] and inf_rademacher_sb["B"] and inf_rademacher_sb["C"]
    res_rademacher_sb["correct"] = res_rademacher_sb["no FP"] and res_rademacher_sb["no FN"]

    res_rademacher_mb["no FP"] = not inf_rademacher_mb["S"]
    res_rademacher_mb["no FN"] = inf_rademacher_mb["A"] and inf_rademacher_mb["B"] and inf_rademacher_mb["C"]
    res_rademacher_mb["correct"] = res_rademacher_mb["no FP"] and res_rademacher_mb["no FN"]
    
    return inf_naive, inf_prior_kn, inf_rademacher_sb, inf_rademacher_mb, res_naive, res_prior_kn, res_rademacher_sb, res_rademacher_mb

def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def loop_calculation(*args):
    # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable
    params = args[0]
    n_el_tr = params[0]
    n_el_val = params[1]
    n_v_sets = params[2]
    ps = params[3]
    min_val = params[4]
    thr_prior_kn = params[5]

    delta = 0.05
    n = 1000
    m = n_v_sets
    sigma = np.random.rand(n,m) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 

    with open(os.path.join('.','data','R2_values_TRel'+str(n_el_tr)+'_VALel'+str(n_el_val)+'_on'+str(n_v_sets)+'sets.pickle'), 'rb') as handle:
        R2_dict = pickle.load(handle)
    
    naive_val = 0
    S, new_ps = create_S_matrix(R2_dict =  R2_dict, ps = ps, min_val = min_val, n_v_sets = n_v_sets, variable_set = "ABCS", naive_score = naive_val)
    corrected_delta = delta / len(new_ps)  # correction for multiple hypotesis testing
    z = max(1,abs(1-min_val))   # since the minimum value of the summand V1-V2 may be -2 in the case of V1 = 1 and V2 = -1
    c = abs(1 - abs(1-min_val)) # for the same reason of above
    emp_means  = np.mean(S, axis = 0)

    sd_bounds_mb = calculate_bound_per_variable(S, n=n, sigma = sigma, delta=corrected_delta, z = z, c = c, get_splitted_values = False)
    
    sd_bounds_sb = calculate_bound_total(n=n, variable_set = "ABCS", S = S, S_names=new_ps, sigma = sigma, delta = delta, z=z, c=c, get_splitted_values = False)
    

    inf_naive, inf_prior_kn, inf_rademacher_sb, inf_rademacher_mb, res_naive, res_prior_kn, res_rademacher_sb, res_rademacher_mb = calculate_variable_informativeness(variable_set = "ABCS", emp_means = emp_means, sd_bounds_sb = sd_bounds_sb, sd_bounds_mb = sd_bounds_mb,
                     summands_name = new_ps, thr_prior_kn = thr_prior_kn)
    return n_el_tr, n_el_val, n_v_sets, inf_naive, inf_prior_kn, inf_rademacher_sb, inf_rademacher_mb, res_naive, res_prior_kn, res_rademacher_sb, res_rademacher_mb



if __name__ == '__main__':
    ps = [str(t) for t in powerset("ABCS")]
    ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]

    parser = argparse.ArgumentParser('main')
    parser.add_argument('-p', '--processors', type=int, default= -1)
    parser.add_argument('-r', '--runs', type=int, default= 100)
    parser.add_argument('-t', '--time', type=bool, default= True)
    
    args = parser.parse_args()
    
    process_number = args.processors
    n_tests = args.runs
    output_time = args.time

    n_trs_els  = [10,25,50,100,500,1000,5000,10000]
    n_val_els  = [1, 2, 5, 10, 100]
    n_val_sets = [1, 10, 100, 1000, 10000, 100000]

    min_val = -1  
    thr_prior_kn   = (7/12-9/16)/2   # for this experiment scope, it's half the value of one of the lowest summands (AS - S)
    seed = 120395
    np.random.seed(seed=seed)
    if process_number < 1:
        process_number = len(n_trs_els)*len(n_val_els)*len(n_val_sets)
    if process_number > 72:
        print("Limiting process number to 72")
        process_number = 72

    # initialize result dict structure
    res_container = ResultDict(n_trs_els, n_val_els, n_val_sets)

    print("process_number",process_number)
    print("n_tests",n_tests)
    print("output_time",output_time)

    if output_time:
        t1 = time.time()
        
    with threadpool_limits(limits=1, user_api='blas'):
        for test in range(n_tests):
            calculate_and_save_R2_values(n_trs_els = n_trs_els, n_val_els = n_val_els, n_val_sets = n_val_sets, process_number = process_number)

            p = mp.Pool(process_number)

            input = ((n_el_tr, n_el_val, n_v_sets, ps, min_val, thr_prior_kn) 
                        for n_el_tr, n_el_val, n_v_sets, ps, min_val, thr_prior_kn
                        in list(itertools.product(n_trs_els, n_val_els, n_val_sets, [ps], [min_val], [thr_prior_kn])))
            results = p.map(loop_calculation, input)   

            p.close()
            p.join()
            
            for n_el_tr, n_el_val, n_v_sets, inf_naive, inf_prior_kn, inf_rademacher_sb, inf_rademacher_mb, res_naive, res_prior_kn, res_rademacher_sb, res_rademacher_mb in results:
                res_container.add_values(n_el_tr, n_el_val, n_v_sets, inf_naive, inf_prior_kn, inf_rademacher_sb, inf_rademacher_mb, res_naive, res_prior_kn, res_rademacher_sb, res_rademacher_mb)
                
            if output_time:
                print((time.time()-t1)/60,"End of test",test)
            else:
                print("End test",test)


        with open('count_results_on'+str(n_tests)+'.pickle', 'wb') as handle:
            pickle.dump(res_container.results, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
    if output_time:
        print((time.time()-t1)/60,"minutes elapsed for",n_tests,"runs")