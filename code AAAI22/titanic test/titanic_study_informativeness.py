import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import multiprocess as mp
import itertools
from itertools import chain, combinations
from titanic_calculate_bounds import create_S_matrix, calculate_bound_per_variable, calculate_bound_total, create_summand_S


def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set


def calculate_variable_informativeness(variable_set, emp_means, sd_bounds, summands_name, method):
    # given the empirical means and the supreme deviation bounds, it calculates if each variable is informative.
    # The calculation is performed in different ways:
    # 1 - naive: the variable is informative if all its summands are > 0
    # 2 - prior_kn: the variable is informative if all its summands are > threshold (thr_prior_kn)
    # 3 - rademacher: the variable is informative if for every summand if the confidence interval given by the bound
    #     on the supremum deviation is > 0
    # 4 - MVS_rademacher: for each variable v, the variable is informative only if the summand all-vars - all-but-v > 0
    # 5 - MVS_prior_kn: for each variable v, the variable is informative only if the summand all-vars - all-but-v > threshold (thr_MVS_prior_kn)
    # For each approach, it returns if the method spotted any false positive, any false negative and if it spots all
    # and only important variables as such.
    
    inf_rademacher = dict()

    for v in variable_set:
        selected_summands = [i for i in range(len(summands_name)) if summands_name[i].count(v) == 1]
        if method == "multiple_bounds":
            try:
                inf_rademacher[v] = all(emp_means[selected_summands] - sd_bounds[selected_summands]>0)
            except:
                inf_rademacher[v] = (emp_means[selected_summands] - sd_bounds[selected_summands]>0).all()
        else:
            try:
                inf_rademacher[v] = all(emp_means[selected_summands] - sd_bounds>0)
            except:
                inf_rademacher[v] = (emp_means[selected_summands] - sd_bounds>0).all()
               
    
    return inf_rademacher


def calculate_bounds(filename, ps, variable_set, method):# Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable

    delta = 0.05
    n = 1000

    with open(os.path.join(filename), 'rb') as handle:
        R2_dict = pickle.load(handle)
        
    for k in R2_dict:
        R2_dict[k] = 1 - np.array(R2_dict[k])  #passing from brier ERROR to brier SCORE. NB: on the empty set I put the score so this consideration does not hold

    
    m = len(R2_dict[('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L')])
    sigma = np.random.rand(n,m) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 

    if "shuffle" in filename:
        naive_val = R2_dict["()"]  # I calculated the naive value only for the shuffled version
    else:
        naive_val = 0

    min_val = -1
    naive_val = 0
    S, new_ps = create_S_matrix(R2_dict =  R2_dict, ps = ps, min_val = min_val, n_v_sets = m, variable_set = variable_set, naive_score = naive_val)
    corrected_delta = delta / len(new_ps)  # correction for multiple hypotesis testing
    z = max(1,abs(1-min_val))   # since the minimum value of the summand V1-V2 may be -2 in the case of V1 = 1 and V2 = -1
    c = abs(1 - abs(1-min_val)) # for the same reason of above
    emp_means  = np.mean(S, axis = 0)

    if method == "multiple_bounds":
        sd_bounds = calculate_bound_per_variable(S, n=n, sigma = sigma, delta=corrected_delta, z = z, c = c, get_splitted_values = False)
    elif method == "single_bound":
        sd_bounds = calculate_bound_total(n=n, variable_set = variable_set, S = S, S_names=new_ps, sigma = sigma, delta = delta, z=z, c=c, get_splitted_values = False)

    # print(emp_means)
    # print(emp_means[0].shape)
    # print(sd_bounds)
    res_rademacher = calculate_variable_informativeness(variable_set = variable_set, emp_means = emp_means, sd_bounds = sd_bounds,
                     summands_name = new_ps, method = method)
    return S, new_ps, res_rademacher


variable_set = "ABCDEFGHIJKL"
ps = [str(t) for t in powerset(variable_set)]
ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]

# n_sets = sys.argv[1]
for n_sets in [1000, 2500, 5000]:
    print(n_sets)
    if n_sets == "single":
        S, new_ps, res_rademacher_single = calculate_bounds('data/score_titanic_single_els.pickle', ps, variable_set, method = "single_bound")
        S, new_ps, res_rademacher_mult = calculate_bounds('data/score_titanic_single_els.pickle', ps, variable_set, method = "multiple_bounds")
        print("RESULTS WITH",n_sets, "SETS")
        print()
        print("single_els")        
        for var in "ABCDEFGHIJKL":
            sum_m_vals = [(np.mean(S[:,i]), new_ps[i]) for i in range(S.shape[1]) if new_ps[i].count(var) == 1]
            sum_m_vals = sorted(sum_m_vals)
            print(var, "multiple_bounds:", res_rademacher_mult[var],"   single_bound:", res_rademacher_single[var])
            print(" ", sum_m_vals[0], sum_m_vals[1])
            print(" ... ")
            print(" ", sum_m_vals[-2], sum_m_vals[-1])

    else:
        S, new_ps, res_rademacher_single = calculate_bounds('data/score_titanic_w_shuffle_on'+str(n_sets)+'tests.pickle', ps, variable_set, method = "single_bound")
        S, new_ps, res_rademacher_mult = calculate_bounds('data/score_titanic_w_shuffle_on'+str(n_sets)+'tests.pickle', ps, variable_set, method = "multiple_bounds")

        print("w_shuffle")        
        for var in "ABCDEFGHIJKL":
            sum_m_vals = [(np.mean(S[:,i]), new_ps[i]) for i in range(S.shape[1]) if new_ps[i].count(var) == 1]
            sum_m_vals = sorted(sum_m_vals)
            print(var, "multiple_bounds:", res_rademacher_mult[var],"   single_bound:", res_rademacher_single[var])
            print(" ", sum_m_vals[0], sum_m_vals[1])
            print(" ... ")
            print(" ", sum_m_vals[-2], sum_m_vals[-1])
    print()
    print()
