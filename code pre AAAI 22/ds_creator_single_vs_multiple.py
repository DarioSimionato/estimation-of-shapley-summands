# contains functions to perform the evaluation of R^2 values

import numpy as np
from sklearn import linear_model
import math
from itertools import chain, combinations
import pickle as pickle
import os
import shutil
import multiprocess as mp
import itertools
import time

def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def compose_dataset(X, col_names, selection):
    # given a selection of variables, create a dataset using only the respective variables
    X_ret = []
    for letter in selection:
        X_ret.append(X[:,col_names.index(letter)])
    return np.array(X_ret).transpose()

def get_R2_score(y_mean, y_real, y_pred):
    return 1-sum((y_real - y_pred)*(y_real - y_pred))/sum((y_real - y_mean)*(y_real - y_mean))

def get_scores_from_training(n_elements_tr, n_elements_va, n_valids, variable_set):
    # creates a dataset and multiple validation sets calculating R^2 considering each subset of variables.
    # Returns a dictionary with the subset as a key and an array of R^2 values of each validation set
    R2_values  = dict()
    ps = powerset(variable_set)

    for subset in ps:
        R2_values[subset] = []
               
    train_size = n_elements_tr
    valid_size = n_elements_va
    
    # creation of the training set
    a1 = np.random.normal(scale=2.0, size=train_size)
    b1 = np.random.normal(scale=2.0, size=train_size)
    c1 = np.random.normal(scale=2.0, size=train_size)
    s1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=train_size)
    t1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=train_size)
    extra = [np.random.normal(scale=2.0, size=train_size) for v in variable_set if v not in "ABCS"]

    X_train_tot = np.reshape(np.array([a1,b1,c1,s1,extra]).flatten(), (len(variable_set),train_size)).transpose()
    Y_train = np.array(t1)
    Y_train_mean = np.mean(Y_train)

    lin_reg = {}
    for subset in ps:
        # create and train one linear regression per variable subset
        lin_reg[subset] = linear_model.LinearRegression() 
    
    p = mp.Pool(72)

    input = ((X_tot, Y, Y_train_mean, variable_set, subset, lr, op_type) 
                for X_tot, Y, Y_train_mean, variable_set, subset, lr, op_type
                in list(itertools.product([X_train_tot], [Y_train], [Y_train_mean], [variable_set], ps, [lin_reg], ["train"])))
    results = p.map(elementary_step_fit_or_evaluate, input)   

    p.close()
    p.join()
    
    for lr, sub in results:
        lin_reg[sub] = lr

    for valid_number in range(n_valids):   
        
        # create a validation set
        a2 = np.random.normal(scale=2.0, size=valid_size)
        b2 = np.random.normal(scale=2.0, size=valid_size)
        c2 = np.random.normal(scale=2.0, size=valid_size)
        s2 = a2 + b2 + c2 + np.random.normal(scale=2.0, size=valid_size)
        t2 = a2 + b2 + c2 + np.random.normal(scale=2.0, size=valid_size)
        extra = [np.random.normal(scale=2.0, size=valid_size) for v in variable_set if v not in "ABCS"]

        X_valid_tot = np.reshape(np.array([a2,b2,c2,s2,extra]).flatten(), (len(variable_set),valid_size)).transpose()
        # X_valid_tot = np.array([a2,b2,c2,s2]).transpose()
        Y_valid = np.array(t2)           
        
        p = mp.Pool(72)

        input = ((X_tot, Y, Y_train_mean, variable_set, subset, lr, op_type) 
                    for X_tot, Y, Y_train_mean, variable_set, subset, lr, op_type
                    in list(itertools.product([X_valid_tot], [Y_valid], [Y_train_mean], [variable_set], ps, [lin_reg], ["test"])))
        results = p.map(elementary_step_fit_or_evaluate, input)   

        p.close()
        p.join()

        for r2, sub in results:
            R2_values[sub].append(r2)

    return R2_values

# X_tot, Y, Y_train_mean, variable_set, subset, lr, op_type
def elementary_step_fit_or_evaluate(*args):
    # performs the calculation of R^2 values of a linear regression trained on a training set of n_el_tr elements
    # with respect to n_v_sets validation sets each one of n_el_val and then saves the results

    params = args[0]
    X_tot = params[0]
    Y = params[1]
    Y_train_mean = params[2]
    variable_set = params[3]
    subset = params[4]
    lr = params[5]
    op_type = params[6]
    
    X = compose_dataset(X = X_tot,  col_names = variable_set, selection = subset)
    if op_type == "train":
        lr[subset].fit(X, Y) 
        return lr[subset],subset
    else:
        R2_values = get_R2_score(y_mean = Y_train_mean, y_real = Y, y_pred = lr[subset].predict(X))
        return R2_values,subset

def calculate_and_save_R2_values(n_trs_els, n_val_els, n_val_sets, var_set, test_n):
    # parallelizes the calculation of R^2 values
    
    t1 = time.time()
    for t in range(test_n):
        path = os.path.join(os.getcwd(), "data"+str(t))
        if os.path.exists(path):
            shutil.rmtree(path)  # clear previous data (if present)
        os.mkdir(path)

        R2_values = get_scores_from_training(n_elements_tr = n_trs_els, n_elements_va = n_val_els, n_valids = n_val_sets,
            variable_set = var_set)

        with open(os.path.join(path,'R2_values_TRel'+str(n_trs_els)+'_VALel'+str(n_val_els)+'_on'+str(n_val_sets)+'sets.pickle'), 'wb') as handle:
            pickle.dump(R2_values, handle, protocol=pickle.HIGHEST_PROTOCOL)
        
        print((time.time()-t1)/60,"End of test",t)