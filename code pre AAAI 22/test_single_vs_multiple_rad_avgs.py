import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import multiprocess as mp
import itertools
from itertools import chain, combinations
from calculate_bounds import create_S_matrix, calculate_bound_per_variable, calculate_bound_total, create_summand_S
from ds_creator_single_vs_multiple import calculate_and_save_R2_values

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits


class ResultDict():
    # dictionary container of the results on each trial

    def __init__(self, n_add_variables):
        self.results = dict()
        for na in n_add_variables:
            self.results[na] = dict()
            self.results[na]["single"] = []
            self.results[na]["multiple"] = []

    def add_values(self, na, res, op_type):
        self.results[na][op_type].append(res)

def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def loop_calculation(*args):
    # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable
    params = args[0]
    n_el_tr = params[0]
    n_el_val = params[1]
    n_v_sets = params[2]
    ps = params[3]
    min_val = params[4]
    n_test = params[5]
    op_type = params[6]
    var_set = params[7]

    delta = 0.05
    n = 1000
    m = n_v_sets
    sigma = np.random.rand(n,m) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 

    with open(os.path.join('.','data'+str(n_test),'R2_values_TRel'+str(n_el_tr)+'_VALel'+str(n_el_val)+'_on'+str(n_v_sets)+'sets.pickle'), 'rb') as handle:
        R2_dict = pickle.load(handle)
    
    naive_val = 0
    S, new_ps = create_S_matrix(R2_dict =  R2_dict, ps = ps, min_val = min_val, n_v_sets = n_v_sets, variable_set = var_set, naive_score = naive_val)
    corrected_delta = delta / len(new_ps)  # correction for multiple hypotesis testing
    z = max(1,abs(1-min_val))   # since the minimum value of the summand V1-V2 may be -2 in the case of V1 = 1 and V2 = -1
    c = abs(1 - abs(1-min_val)) # for the same reason of above
    
    if op_type == "single":
        sd_bounds = calculate_bound_per_variable(S, n=n, sigma = sigma, delta=corrected_delta, z = z, c = c, get_splitted_values = False)
    else:
        sd_bounds = calculate_bound_total(n=n, variable_set = var_set, S = S, S_names=new_ps, sigma = sigma, delta = delta, z=z, c=c, get_splitted_values = False)
        sd_bounds = max(sd_bounds)

    return sd_bounds, op_type, n_test


if __name__ == '__main__':

    parser = argparse.ArgumentParser('main')
    parser.add_argument('-r', '--runs', type=int, default= 1)

    parser.add_argument('-a', '--trainel', type=int, default= 500)
    parser.add_argument('-b', '--testel', type=int, default= 5)
    parser.add_argument('-c', '--testsets', type=int, default= 10)
    parser.add_argument('-v', '--addvariables', type=int, default= -1) # 22 max
    
    args = parser.parse_args()
    n_tests = args.runs

    n_trs_els = args.trainel
    n_val_els = args.testel
    n_val_sets = args.testsets
    n_add_variables = args.addvariables

    if n_add_variables == -1:
        n_add_variables = [5,10,15]
    else:
        n_add_variables = [n_add_variables]

    # n_trs_els  = [10,25,50,100,500,1000,5000,10000]
    # n_val_els  = [1, 2, 5, 10, 100]
    # n_val_sets = [1, 10, 100, 1000, 10000, 100000]

    min_val = -1  
    seed = 120395
    np.random.seed(seed=seed)
    res_container = ResultDict()
    
    print("n_tests",n_tests)
    print("n_trs_els", n_trs_els)
    print("n_val_els", n_val_els)
    print("n_val_sets", n_val_sets)
    print("n_add_variables", n_add_variables)
    print()
    print()

    for n_addition in n_add_variables:
        extra_vars = "DEFGHIJKLMNOPQRSTUVWXYZ"
        var_set = "ABCS"+extra_vars[:n_addition]

        ps = [str(t) for t in powerset(var_set)]
        ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]

        print("using",len(var_set),"variables:",var_set)
        print("ps len",len(ps))

            
        with threadpool_limits(limits=1, user_api='blas'):
            calculate_and_save_R2_values(n_trs_els = n_trs_els, n_val_els = n_val_els, n_val_sets = n_val_sets, var_set = var_set, test_n = n_tests)

            p = mp.Pool(n_tests*2)

            input = ((n_el_tr, n_el_val, n_v_sets, ps, min_val, n_test, op_type, var_set) 
                        for n_el_tr, n_el_val, n_v_sets, ps, min_val, n_test, op_type, var_set
                        in list(itertools.product([n_trs_els], [n_val_els], [n_val_sets], 
                            [ps], [min_val], [n for n in range(n_tests)], ["single","multiple"], [var_set])))
            results = p.map(loop_calculation, input)   

            p.close()
            p.join()
            
            for sd_bounds, op_type, n_test in sorted(results, key= lambda x: x[2]):
                res_container.add_values(n_addition, sd_bounds, op_type)
                    
        with open('comparison_extra_vars_on'+str(n_addition)+'.pickle', 'wb') as handle:
            pickle.dump(res_container.results, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('comparison_extra_vars_on'+str(n_add_variables)+'.pickle', 'wb') as handle:
        pickle.dump(res_container.results, handle, protocol=pickle.HIGHEST_PROTOCOL)
        