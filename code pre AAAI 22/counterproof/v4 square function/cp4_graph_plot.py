import networkx as nx
import matplotlib.pyplot as plt

dg = nx.DiGraph()

dg = nx.DiGraph()
for el in "ABCST":
    dg.add_node(el)

for el in "ABC":
    dg.add_edge(el, "S")
    dg.add_edge(el, "T")

pos = {"S" : (0,1),"A" : (-1,0),"B" : (0,0),"C" : (1,0),"T" : (0,-1)}

plt.subplot()
nx.draw(dg, pos, node_size=1500, with_labels=True, font_weight='bold', node_color="grey", edge_color="black")
# plt.show()

plt.savefig('cp3_graph.pdf')
# plt.savefig('cp2_graphonemoreel.pdf')