# creates a dataset of given size
import numpy as np
from numpy.lib import math
import pandas as pd
import argparse
import os
from tqdm import tqdm
import shutil
import math


# np.random.seed(120395)
parser = argparse.ArgumentParser('main')
parser.add_argument('-a', '--train_size', type=int, default= 10000)
parser.add_argument('-b', '--valid_size', type=int, default= 100000)
parser.add_argument('-n', '--number_rep_structure', type=int, default= 1)
parser.add_argument('-t', '--test_type', type=int, default= 0)
parser.add_argument('-v', '--variables', type=int, default= 0)
parser.add_argument('-x', '--is_test', type=bool, default= False)
parser.add_argument('-z', '--zero_init_t', type=bool, default= False)

args = parser.parse_args()

train_size = args.train_size
valid_size = args.valid_size
number_rep_structure = args.number_rep_structure
n_vars = args.variables
test_type = args.test_type
is_test = args.is_test
zero_init_t = args.zero_init_t

print("Parameters =",args)

# TEST 1 = Ma's example
if test_type == 1:
    tot_size = train_size + valid_size

    # creation of the training set
    a1 = np.random.normal(scale=2.0, size=tot_size)
    b1 = np.random.normal(scale=2.0, size=tot_size)
    c1 = np.random.normal(scale=2.0, size=tot_size)
    s1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=tot_size)
    t1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=tot_size)

    X_tot = np.array([a1,b1,c1,s1,t1]).transpose()


    df = pd.DataFrame(data = X_tot, columns = ["A","B","C","S","T"])
    path = os.path.join(os.getcwd(), "datasets")
    df.to_csv(os.path.join(path,"v1_"+str(train_size)+"_"+str(valid_size)+".csv"), index=None)
# TEST 2 = Example with structure's repetition
elif test_type == 2:
    tot_size = 100000
    # creation of the training set
    a = []
    b = []
    c = []
    if zero_init_t:
        t = np.zeros(tot_size)
    else:
        t = np.random.normal(scale=2.0, size=tot_size)

    for n in range(number_rep_structure):
        a.append(np.random.normal(scale=2.0, size=tot_size))
        b.append(a[-1] + np.random.normal(scale=2.0, size=tot_size))
        c.append(a[-1] + np.random.normal(scale=2.0, size=tot_size))

    for n in range(number_rep_structure):
        t += np.array(b[n])+np.array(c[n])

    # for i in range(tot_size):
    #     s = 0
    #     for j in range(number_rep_structure):
    #         print(b[j][i], sep = " ")
    #         s+=b[j][i]
    #     for j in range(number_rep_structure):
    #         print(c[j][i], sep = " ")
    #         s+=c[j][i]
    #     print(" = ",s)
    # print(len(a),len(b),len(c),len(t), t)
    tot_list = a
    names = ["A"+str(i+1) for i in range(len(a))]
    for n in range(number_rep_structure):
        tot_list.append(b[n])
        names.append("B"+str(n+1))
    for n in range(number_rep_structure):
        tot_list.append(c[n])
        names.append("C"+str(n+1))

    tot_list.append(t)
    # print(tot_list)
    tot_list = [item for sublist in tot_list for item in sublist]
    names.append("T")
    # print(tot_list)
    X_tot = np.reshape(tot_list,(-1,tot_size)).transpose()
    # print(X_tot.shape, len(names))
    

    df = pd.DataFrame(data = X_tot, columns = names)
    path = os.path.join(os.getcwd(), "datasets")
    if zero_init_t:
        df.to_csv(os.path.join(path,"v2_S"+str(tot_size)+"_R"+str(number_rep_structure)+"ZI.csv"), index=None)
    else:
        df.to_csv(os.path.join(path,"v2_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv"), index=None)
# TEST 3 = Example with a fully connected DAG to test the number of CI tests
elif test_type == 3:
    tot_size = valid_size
    # creation of the training set
    a = []
    b = []
    if zero_init_t:
        t = np.zeros(tot_size)
    else:
        t = np.random.normal(scale=2.0, size=tot_size)

    if is_test:
        a.append(np.ones(tot_size))
    else:
        a.append(np.random.normal(scale=2.0, size=tot_size))
    t += a[-1]

    for i in range(n_vars):
        if is_test:
            val = a[-1] + np.ones(tot_size)        
        else:
            val = a[-1] + np.random.normal(scale=2.0, size=tot_size)
        for j in range(i):
            val += b[j]
        b.append(val)

    for n in range(n_vars):
        t += np.array(b[n])

    tot_list = a
    names = ["ABCDEFGHIJKLMNOPQR"[i] for i in range(n_vars+1)]
    for n in range(n_vars):
        tot_list.append(b[n])

    tot_list.append(t)
    tot_list = [item for sublist in tot_list for item in sublist]
    names.append("T")
    X_tot = np.reshape(tot_list,(-1,tot_size)).transpose()
    
    df = pd.DataFrame(data = X_tot, columns = names)
    # print(df)
    path = os.path.join(os.getcwd(), "datasets")
    if zero_init_t:
        df.to_csv(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_R"+str(n_vars)+"ZI.csv"), index=None)
    else:
        df.to_csv(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_R"+str(n_vars)+".csv"), index=None)
# TEST 4 = Corrected repetition example + increasing sample size
elif test_type == 4:
    print("test 4")
    path = os.path.join(os.getcwd(), "datasets","multiple")
    if os.path.exists(path):
        shutil.rmtree(path)  # clear previous data (if present)
    os.mkdir(path)
    for n_test in tqdm(range(100)):
        path = os.path.join(os.getcwd(), "datasets","multiple","t"+str(n_test))
        if os.path.exists(path):
            shutil.rmtree(path)  # clear previous data (if present)
        os.mkdir(path)
        for zero_init_t in [False,True]:
            for number_rep_structure in [1,2,5]:
                for tot_size in [10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
                    # creation of the training set
                    a = []
                    b = []
                    c = []
                    if zero_init_t:
                        t = np.zeros(tot_size)
                    else:
                        t = np.random.normal(scale=2.0, size=tot_size)

                    for n in range(number_rep_structure):
                        a.append(np.random.normal(scale=2.0, size=tot_size))
                        b.append(a[-1] + np.random.normal(scale=2.0, size=tot_size))
                        c.append(a[-1] + np.random.normal(scale=2.0, size=tot_size))

                    for n in range(number_rep_structure):
                        t += np.array(b[n])+np.array(c[n])

                    tot_list = a
                    names = ["A"+str(i+1) for i in range(len(a))]
                    for n in range(number_rep_structure):
                        tot_list.append(b[n])
                        names.append("B"+str(n+1))
                    for n in range(number_rep_structure):
                        tot_list.append(c[n])
                        names.append("C"+str(n+1))

                    tot_list.append(t)
                    tot_list = [item for sublist in tot_list for item in sublist]
                    names.append("T")
                    X_tot = np.reshape(tot_list,(-1,tot_size)).transpose()
                    
                    df = pd.DataFrame(data = X_tot, columns = names)
                    if zero_init_t:
                        df.to_csv(os.path.join(path,"v4_S"+str(tot_size)+"_R"+str(number_rep_structure)+"ZI.csv"), index=None)
                    else:
                        df.to_csv(os.path.join(path,"v4_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv"), index=None)
                    #df.to_csv(os.path.join(path,"v4_"+str(tot_size)+".csv"), index=None)
# TEST 5 = Corrected fully connected example + increasing sample size
elif test_type == 5:
    print("test 5")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))
    if os.path.exists(path):
        shutil.rmtree(path)  # clear previous data (if present)
    os.mkdir(path)
    for n_test in tqdm(range(100)):
        path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type),"t"+str(n_test))
        if os.path.exists(path):
            shutil.rmtree(path)  # clear previous data (if present)
        if not os.path.exists(path):
            os.mkdir(path)
        for zero_init_t in [False,True]:
            for tot_size in [10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
                # creation of the training set
                a = []
                b = []
                c = []
                d = []
                if zero_init_t:
                    t = np.zeros(tot_size)
                else:
                    t = np.random.normal(scale=1.0, size=tot_size)

                a.append(np.random.normal(scale=1.0, size=tot_size))
                b.append(a[-1] + np.random.normal(scale=1.0, size=tot_size))
                c.append(a[-1] + b[-1] + np.random.normal(scale=1.0, size=tot_size))
                d.append(a[-1] + b[-1] + c[-1] + np.random.normal(scale=1.0, size=tot_size))

                t += np.array(a[0])+np.array(b[0])+np.array(c[0])+np.array(d[0])

                tot_list = a
                tot_list.append(b[0])
                tot_list.append(c[0])
                tot_list.append(d[0])
                tot_list.append(t)
                # print(tot_list)
                names = ["A", "B", "C", "D"]
                tot_list = [item for sublist in tot_list for item in sublist]
                names.append("T")
                X_tot = np.reshape(tot_list,(-1,tot_size)).transpose()
                
                df = pd.DataFrame(data = X_tot, columns = names)
                # print(df)
                if zero_init_t:
                    df.to_csv(os.path.join(path,"v5_S"+str(tot_size)+"ZI.csv"), index=None)
                else:
                    df.to_csv(os.path.join(path,"v5_S"+str(tot_size)+".csv"), index=None)
                #df.to_csv(os.path.join(path,"v4_"+str(tot_size)+".csv"), index=None)
# TEST 6 = discrete variables -> IT'S ON R FILE "discrete_example_creator"
# TEST 7 = test with repetitions
elif test_type == 7:
    print("test 7")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))

    n_rep = number_rep_structure
    tot_size = train_size
    test_numb = valid_size

    for n_test in tqdm(range(test_numb)):
        if not os.path.exists(os.path.join(path,"v7_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv")):
            # creation of the training set
            a = []
            b = []
            c = []

            a = np.random.normal(scale=1.0, size=(tot_size, n_rep))
            b = 3 + 1.5 * a + np.random.normal(scale=1.0, size=(tot_size, n_rep))
            c = -2 - 2 * a + np.random.normal(scale=1.0, size=(tot_size, n_rep))
            t = 4 * np.sum(b,axis=1) + 2 * np.sum(c,axis=1) + np.random.normal(scale=1.0, size=tot_size)

            df = pd.DataFrame()
            A_s = ["A"+str(i+1) for i in range(n_rep)]
            df_A = pd.DataFrame(a,columns = A_s)
            B_s = ["B"+str(i+1) for i in range(n_rep)]
            df_B = pd.DataFrame(b,columns = B_s)
            C_s = ["C"+str(i+1) for i in range(n_rep)]
            df_C = pd.DataFrame(c,columns = C_s)
            df_T = pd.DataFrame(t.T,columns = ["T"])
            
            df = pd.concat([df_A,df_B,df_C,df_T], axis = 1)
            df.to_csv(os.path.join(path,"v7_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv"), index=None)
# TEST 8 = test with repetitions
elif test_type == 8:
    print("test 8")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))

    n_rep = number_rep_structure
    tot_size = train_size
    test_numb = valid_size

    for n_test in tqdm(range(test_numb)):
        if not os.path.exists(os.path.join(path,"v8_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv")):
            # creation of the training set
            a = []
            b = []
            c = []

            a = np.random.normal(scale=1.0, size=(tot_size, n_rep))
            b = 3 + 1.5 * a + np.random.normal(scale=1.0, size=(tot_size, n_rep))
            c = -2 - 2 * a + np.random.normal(scale=1.0, size=(tot_size, n_rep))
            t = 0.5 + b - 3 * c + np.random.normal(scale=1.0, size=(tot_size, n_rep))

            df = pd.DataFrame()
            A_s = ["A"+str(i+1) for i in range(n_rep)]
            df_A = pd.DataFrame(a,columns = A_s)
            B_s = ["B"+str(i+1) for i in range(n_rep)]
            df_B = pd.DataFrame(b,columns = B_s)
            C_s = ["C"+str(i+1) for i in range(n_rep)]
            df_C = pd.DataFrame(c,columns = C_s)
            T_s = ["T"+str(i+1) for i in range(n_rep)]
            df_T = pd.DataFrame(t,columns = T_s)
            
            df = pd.concat([df_A,df_B,df_C,df_T], axis = 1)
            df.to_csv(os.path.join(path,"v8_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv"), index=None)
# TEST 9 = test with repetitions
elif test_type == 9:
    print("test 9")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))

    n_rep = number_rep_structure
    tot_size = train_size
    test_numb = valid_size

    for n_test in tqdm(range(test_numb)):
        if not os.path.exists(os.path.join(path,"v9_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv")):
            # creation of the training set
            a = []
            b = []
            c = []

            a = np.random.normal(scale=1.0, size=(tot_size, 1))
            b = 3 + 1.5 * a + np.random.normal(scale=1.0, size=(tot_size, 1))
            c = -2 - 2 * a + np.random.normal(scale=1.0, size=(tot_size, 1))
            t = 0.5 + b - 3 * c + np.random.normal(scale=1.0, size=(tot_size, 1))
            e = np.random.normal(scale=1.0, size=(tot_size, n_rep))

            df = pd.DataFrame()
            A_s = ["A"+str(i+1) for i in range(1)]
            df_A = pd.DataFrame(a,columns = A_s)
            B_s = ["B"+str(i+1) for i in range(1)]
            df_B = pd.DataFrame(b,columns = B_s)
            C_s = ["C"+str(i+1) for i in range(1)]
            df_C = pd.DataFrame(c,columns = C_s)
            T_s = ["T"+str(i+1) for i in range(1)]
            df_T = pd.DataFrame(t,columns = T_s)
            E_s = ["E"+str(i+1) for i in range(n_rep)]
            df_E = pd.DataFrame(e,columns = E_s)
            
            df = pd.concat([df_A,df_B,df_C,df_T,df_E], axis = 1)
            df.to_csv(os.path.join(path,"v9_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv"), index=None)
# TEST 10 = test with repetitions
elif test_type == 10:
    print("test 10")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))

    n_rep = number_rep_structure
    tot_size = train_size
    test_numb = valid_size

    for n_test in tqdm(range(test_numb)):
        if not os.path.exists(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv")):
            # creation of the training set
            a = []
            b = []
            c = []
            t = []

            a = np.random.normal(scale=1.0, size=(tot_size, 1))
            b.append(3 + 3 * a + np.random.normal(scale=1.0, size=(tot_size, 1)))
            c.append(-1 - 2 * a + np.random.normal(scale=1.0, size=(tot_size, 1)))
            t.append((-3 + 2 * b[-1] + 3 * c[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))/math.sqrt(14))
            for iii in range(1,n_rep):
                b.append(3 + 3 * t[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))
                c.append(-1 - 2 * t[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))
                t.append((-3 + 2 * b[-1] + 3 * c[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))/math.sqrt(14))


            df = pd.DataFrame()
            A_s = ["A"+str(i+1) for i in range(1)]
            df_A = pd.DataFrame(a,columns = A_s)
            # print(b[0])
            # print(np.reshape(np.array(b),(n_rep,-1)).T[:,0])
            B_s = ["B"+str(i+1) for i in range(n_rep)]
            df_B = pd.DataFrame(np.reshape(np.array(b),(n_rep,-1)).T,columns = B_s)
            C_s = ["C"+str(i+1) for i in range(n_rep)]
            df_C = pd.DataFrame(np.reshape(np.array(c),(n_rep,-1)).T,columns = C_s)
            T_s = ["T"+str(i+1) for i in range(n_rep)]
            df_T = pd.DataFrame(np.reshape(np.array(t),(n_rep,-1)).T,columns = T_s)
            
            df = pd.concat([df_A,df_B,df_C,df_T], axis = 1)
            df.to_csv(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv"), index=None)
# TEST 11 = test with repetitions
elif test_type == 11:
    print("test 11")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))

    n_rep = number_rep_structure
    tot_size = train_size
    test_numb = valid_size

    for n_test in tqdm(range(test_numb)):
        if not os.path.exists(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv")):
            # creation of the training set
            a = []
            b = []
            c = []
            d = []
            e = []
            f = []
            g = []
            h = []
            i = []
            o = []

            for iii in range(n_rep):
                a.append(np.random.normal(scale=1.0, size=(tot_size, 1)))
                b.append(3 + 3 * a[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))
                c.append(-1 - 0.6 * a[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))
                d.append((-3 + 2 * b[-1] + 3 * c[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                e.append((.5 + 2.5  * b[-1] + 1   * d[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                f.append((-.7 - 0.8   * c[-1] - 0.5 * d[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                g.append((-.3 + 1.2 * e[-1] - 2.5 * f[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                h.append((1 + 2     * g[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                i.append((1.3 -3    * g[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                o.append(np.random.normal(scale=1.0, size=(tot_size, 6)))


            df = pd.DataFrame()
            A_s = ["A"+str(i+1) for i in range(n_rep)]
            df_A = pd.DataFrame(np.reshape(np.array(b),(n_rep,-1)).T,columns = A_s)
            B_s = ["B"+str(i+1) for i in range(n_rep)]
            df_B = pd.DataFrame(np.reshape(np.array(b),(n_rep,-1)).T,columns = B_s)
            C_s = ["C"+str(i+1) for i in range(n_rep)]
            df_C = pd.DataFrame(np.reshape(np.array(c),(n_rep,-1)).T,columns = C_s)
            D_s = ["D"+str(i+1) for i in range(n_rep)]
            df_D = pd.DataFrame(np.reshape(np.array(d),(n_rep,-1)).T,columns = D_s)
            E_s = ["E"+str(i+1) for i in range(n_rep)]
            df_E = pd.DataFrame(np.reshape(np.array(e),(n_rep,-1)).T,columns = E_s)
            F_s = ["F"+str(i+1) for i in range(n_rep)]
            df_F = pd.DataFrame(np.reshape(np.array(f),(n_rep,-1)).T,columns = F_s)
            G_s = ["G"+str(i+1) for i in range(n_rep)]
            df_G = pd.DataFrame(np.reshape(np.array(g),(n_rep,-1)).T,columns = G_s)
            H_s = ["H"+str(i+1) for i in range(n_rep)]
            df_H = pd.DataFrame(np.reshape(np.array(h),(n_rep,-1)).T,columns = H_s)
            I_s = ["I"+str(i+1) for i in range(n_rep)]
            df_I = pd.DataFrame(np.reshape(np.array(i),(n_rep,-1)).T,columns = I_s)
            O_s = ["O"+str(i+1) for i in range(6*n_rep)]
            df_O = pd.DataFrame(np.reshape(np.array(o),(6*n_rep,-1)).T,columns = O_s)
            
            df = pd.concat([df_A,df_B,df_C,df_D,df_E,df_F,df_G,df_H,df_I,df_O], axis = 1)
            df = (df-df.mean())/df.std()
            df.to_csv(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv"), index=None)
# TEST 12 = test with repetitions
elif test_type == 12:
    print("test 12")
    path = os.path.join(os.getcwd(), "datasets","multiple"+str(test_type))

    n_rep = number_rep_structure
    tot_size = train_size
    test_numb = valid_size

    for n_test in tqdm(range(test_numb)):
        if not os.path.exists(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv")):
            # creation of the training set
            a = []
            b = []
            c = []
            d = []
            e = []
            f = []
            g = []
            h = []
            i = []

            for iii in range(n_rep):
                a.append(np.random.normal(scale=1.0, size=(tot_size, 1)))
                b.append(3 + 3 * a[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))
                c.append(-1 - 0.6 * a[-1] + np.random.normal(scale=1.0, size=(tot_size, 1)))
                d.append((-3 + 2 * b[-1] + 3 * c[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                e.append((.5 + 2.5  * b[-1] + 1   * d[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                f.append((-.7 - 0.8   * c[-1] - 0.5 * d[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                g.append((-.3 + 1.2 * e[-1] - 2.5 * f[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                h.append((1 + 2     * g[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))
                i.append((1.3 -3    * g[-1] + np.random.normal(scale=1.0, size=(tot_size, 1))))


            df = pd.DataFrame()
            A_s = ["A"+str(i+1) for i in range(n_rep)]
            df_A = pd.DataFrame(np.reshape(np.array(b),(n_rep,-1)).T,columns = A_s)
            B_s = ["B"+str(i+1) for i in range(n_rep)]
            df_B = pd.DataFrame(np.reshape(np.array(b),(n_rep,-1)).T,columns = B_s)
            C_s = ["C"+str(i+1) for i in range(n_rep)]
            df_C = pd.DataFrame(np.reshape(np.array(c),(n_rep,-1)).T,columns = C_s)
            D_s = ["D"+str(i+1) for i in range(n_rep)]
            df_D = pd.DataFrame(np.reshape(np.array(d),(n_rep,-1)).T,columns = D_s)
            E_s = ["E"+str(i+1) for i in range(n_rep)]
            df_E = pd.DataFrame(np.reshape(np.array(e),(n_rep,-1)).T,columns = E_s)
            F_s = ["F"+str(i+1) for i in range(n_rep)]
            df_F = pd.DataFrame(np.reshape(np.array(f),(n_rep,-1)).T,columns = F_s)
            G_s = ["G"+str(i+1) for i in range(n_rep)]
            df_G = pd.DataFrame(np.reshape(np.array(g),(n_rep,-1)).T,columns = G_s)
            H_s = ["H"+str(i+1) for i in range(n_rep)]
            df_H = pd.DataFrame(np.reshape(np.array(h),(n_rep,-1)).T,columns = H_s)
            I_s = ["I"+str(i+1) for i in range(n_rep)]
            df_I = pd.DataFrame(np.reshape(np.array(i),(n_rep,-1)).T,columns = I_s)
            
            df = pd.concat([df_A,df_B,df_C,df_D,df_E,df_F,df_G,df_H,df_I], axis = 1)
            df = (df-df.mean())/df.std()
            df.to_csv(os.path.join(path,"v"+str(test_type)+"_S"+str(tot_size)+"_t"+str(n_test)+"_r"+str(n_rep)+".csv"), index=None)
            
            