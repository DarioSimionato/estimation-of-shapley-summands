
import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import matplotlib.pyplot as plt
from matplotlib.transforms import offset_copy

# results

fig, ax = plt.subplots(1,1,figsize = (6,6))
ax = [ax]
res = {}
# n_trs_els  = 10000
# n_val_els  = 1
# n_val_sets = 100000
n_trs_els  = 1000
n_val_els  = 5
n_val_sets = 1000
idxs = [i for i in range(12)]
m = 0
for n_add in idxs:
    try:
        with open('data_test_single_vs_multiple/count_results_with'+str(n_add)+'add'+str(n_trs_els)+"ntels"+str(n_val_els)+"nvels"+str(n_val_sets)+'sets.pickle', 'rb') as handle:
            dd = pickle.load(handle)
            res[n_add] = dd
            m = max(m,max([max(v) for v in dd.values()]))
    except:
        pass

eps = 0.2

ax[0].plot([4+ii for ii in idxs], [np.mean(res[idx]["single"]) for idx in idxs],"-o", color ="red", label = "Single SD estimate")
ax[0].fill_between([4+ii for ii in idxs], [np.mean(res[idx]["multiple_min"]) for idx in idxs], [np.mean(res[idx]["multiple_max"]) for idx in idxs], color ="blue", alpha = 0.5, label = "One SD estimate per summand")
# ax[0].plot([1+ii for ii in range(len(idxs))], [np.mean(res[idx]["multiple_max"]) for idx in idxs],"-o")

ax[0].set_ylim([0,1.10*m])
# ax[0].set_xlim([0,len(idxs) + 1])
xs = [4+i for i in idxs]
labs = [str(4+i) for i in idxs]
ax[0].set_xticks(xs)
ax[0].set_xticklabels(labs)
ax[0].set_xlabel("Number of variables")
ax[0].set_ylabel("SD bound")
ax[0].set_title("SD bound varying variable numbers")
ax[0].legend(loc = 4)

# summs = [15, 31, 1023, 32767, 1048575]
# ax[1].plot([summs[ii] for ii in range(len(idxs))], [np.mean(res[idx]["single"]) for idx in idxs],"-o", color ="red")
# ax[1].fill_between([summs[ii] for ii in range(len(idxs))], [np.mean(res[idx]["multiple_min"]) for idx in idxs], [np.mean(res[idx]["multiple_max"]) for idx in idxs], color ="blue", alpha = 0.5)


# ax[1].set_ylim([0,1.10*m])
# # ax[1].set_xlim([0,len(idxs) + 1])
# ax[1].set_xscale( "log")
# xs = [summs[i] for i in range(len(idxs))]
# labs = [str(i) for i in summs]
# ax[1].set_xticks(xs)
# ax[1].set_xticklabels(labs)
# ax[1].set_title("n_trs_els = "+str(n_trs_els)+"\nn_val_els = "+str(n_val_els)+"\nn_val_sets = "+str(n_val_sets))

fig.show()


fig.savefig('comparison_single_vs_multiple_hyp.pdf')
# n_trs_els  = 10000
# n_val_els  = 1
# n_val_sets = 100000
# m = 0
# for n_add in idxs:
#     try:
#         with open('data_test_single_vs_multiple/count_results_with'+str(n_add)+'add'+str(n_trs_els)+"ntels"+str(n_val_els)+"nvels"+str(n_val_sets)+'sets.pickle', 'rb') as handle:
#             dd = pickle.load(handle)
#             res[n_add] = dd
#             m = max(m,max([max(v) for v in dd.values()]))
#     except:
#         pass

# eps = 0.2
# for idx in idxs:
#     ax[1].plot([1+idxs.index(idx)-eps for i in range(len(res[idx]["single"]))],res[idx]["single"],'o', color = "blue")
#     ax[1].plot([1+idxs.index(idx) for i in range(len(res[idx]["single"]))],res[idx]["multiple_min"],'o', color = "red")
#     ax[1].plot([1+idxs.index(idx)+eps for i in range(len(res[idx]["single"]))],res[idx]["multiple_max"],'o', color = "green")

# ax[1].set_ylim([0,1.10*m])
# ax[1].set_xlim([0,len(idxs) + 1])
# xs = [1+i for i in range(len(idxs))]
# labs = [str(i) for i in idxs]
# ax[1].set_xticks(xs)
# ax[1].set_xticklabels(labs)
# ax[1].set_title("n_trs_els = "+str(n_trs_els)+"\nn_val_els = "+str(n_val_els)+"\nn_val_sets = "+str(n_val_sets))

# res = {}
# idxs = [0, 1]
# for idx in idxs:
#     with open('data_test_single_vs_multiple/comparison_extra_vars_on'+str(idx)+str(idx)+'.pickle', 'rb') as handle:
#         dd = pickle.load(handle)
#         res[idx] = dd[idx]
# eps = 0.2
# for idx in idxs:
#     ax.plot([1+idxs.index(idx)-eps for i in range(10)],res[idx]["single"],'o', color = "blue")
#     ax.plot([1+idxs.index(idx) for i in range(10)],res[idx]["multiple_min"],'o', color = "red")
#     ax.plot([1+idxs.index(idx)+eps for i in range(10)],res[idx]["multiple_max"],'o', color = "green")
# ax.set_ylim([0,0.01])
# xs = [1+i for i in range(len(idxs))]
# labs = [str(i) for i in idxs]
# ax.set_xticks(xs)
# ax.set_xticklabels(labs)

# fig.show()


# fig.tight_layout()
# fig.subplots_adjust(top=0.95)
# fig.suptitle("Correct answers out of "+str(len(res[idx]["single"]))+" runs")
# fig.show()
fig.savefig('comparison_single_vs_multiple_hyp.pdf')