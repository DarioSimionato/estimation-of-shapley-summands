# contains functions to perform the evaluation of R^2 values

import numpy as np
from sklearn import linear_model
import math
from itertools import chain, combinations
import pickle as pickle
import os
import shutil
import multiprocess as mp
import itertools

def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def compose_dataset(X, col_names, selection):
    # given a selection of variables, create a dataset using only the respective variables
    X_ret = []
    for letter in selection:
        X_ret.append(X[:,col_names.index(letter)])
    return np.array(X_ret).transpose()

def get_R2_score(y_mean, y_real, y_pred):
    return 1-sum((y_real - y_pred)*(y_real - y_pred))/sum((y_real - y_mean)*(y_real - y_mean))

def get_scores_from_training(n_elements_tr, n_elements_va, n_valids, variable_set, seed = -1):
    # creates a dataset and multiple validation sets calculating R^2 considering each subset of variables.
    # Returns a dictionary with the subset as a key and an array of R^2 values of each validation set
    R2_values  = dict()
    for subset in powerset(variable_set):
        R2_values[subset] = []
        
    if seed != -1:
        np.random.seed(seed=seed)        
    train_size = n_elements_tr
    valid_size = n_elements_va
    
    # creation of the training set
    a1 = np.random.normal(scale=2.0, size=train_size)
    a1[a1>0] = 1
    a1[a1<0] = 0
    b1 = np.random.normal(scale=2.0, size=train_size)
    b1[b1>0] = 1
    b1[b1<0] = 0
    c1 = np.random.normal(scale=2.0, size=train_size)
    s1 = np.logical_xor(a1, b1)*1 + c1 + np.random.normal(scale=2.0, size=train_size)
    t1 = np.logical_xor(a1, b1)*1 + c1 + np.random.normal(scale=2.0, size=train_size)

    X_train_tot = np.array([a1,b1,c1,s1]).transpose()
    Y_train = np.array(t1)
    Y_train_mean = np.mean(Y_train)

    lin_reg = {}
    for subset in powerset(variable_set):
        # create and train one linear regression per variable subset
        X_train = compose_dataset(X = X_train_tot, col_names = variable_set, selection = subset)
        lin_reg[subset] = linear_model.LinearRegression() 
        lin_reg[subset].fit(X_train, Y_train)  
    
    for valid_number in range(n_valids):   
        
        # create a validation set
        a2 = np.random.normal(scale=2.0, size=valid_size)
        a2[a2>0] = 1
        a2[a2<0] = 0
        b2 = np.random.normal(scale=2.0, size=valid_size)
        b2[b2>0] = 1
        b2[b2<0] = 0
        c2 = np.random.normal(scale=2.0, size=valid_size)
        s2 = np.logical_xor(a2, b2)*1 + c2+ np.random.normal(scale=2.0, size=valid_size)
        t2 = np.logical_xor(a2, b2)*1 + c2 + np.random.normal(scale=2.0, size=valid_size)

        X_valid_tot = np.array([a2,b2,c2,s2]).transpose()
        Y_valid = np.array(t2)           
        
        for subset in powerset(variable_set):
            # evaluate the performance of each validation set considering every subset of variables
            X_valid = compose_dataset(X = X_valid_tot,  col_names = variable_set, selection = subset)
            # calculate R2 using training set mean
            v = get_R2_score(y_mean = Y_train_mean, y_real = Y_valid, y_pred = lin_reg[subset].predict(X_valid))
            R2_values[subset].append(v)  

    return R2_values

# def elementary_step_calculate_and_save_R2_values(path, n_el_tr, n_el_val, n_v_sets, seed = -1):
def elementary_step_calculate_and_save_R2_values(*args):
    # performs the calculation of R^2 values of a linear regression trained on a training set of n_el_tr elements
    # with respect to n_v_sets validation sets each one of n_el_val and then saves the results

    # print(args[0])
    params = args[0]
    path = params[0]
    n_el_tr = params[1]
    n_el_val = params[2]
    n_v_sets = params[3]
    seed = params[4]
    R2_values = get_scores_from_training(n_elements_tr = n_el_tr, n_elements_va = n_el_val, n_valids = n_v_sets,
        variable_set = "ABCS", seed = seed)

    with open(os.path.join(path,'R2_values_TRel'+str(n_el_tr)+'_VALel'+str(n_el_val)+'_on'+str(n_v_sets)+'sets.pickle'), 'wb') as handle:
        pickle.dump(R2_values, handle, protocol=pickle.HIGHEST_PROTOCOL)

def calculate_and_save_R2_values(n_trs_els, n_val_els, n_val_sets, process_number, seed = -1):
    # parallelizes the calculation of R^2 values

    path = os.path.join(os.getcwd(), "data")
    if os.path.exists(path):
        shutil.rmtree(path)  # clear previous data (if present)
    os.mkdir(path)

    p = mp.Pool(process_number)
    input = ((p,tr_n, val_n, val_s, s) for p,tr_n, val_n, val_s, s in list(itertools.product([path],n_trs_els, n_val_els, n_val_sets, [seed])))  # all combos of parameters
    results = p.map(elementary_step_calculate_and_save_R2_values, input)
    p.close()
    p.join()
