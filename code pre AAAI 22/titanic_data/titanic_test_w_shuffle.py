import pandas as pd
import numpy as np
from tqdm import tqdm
import time
import math
import pickle5 as pickle
from itertools import chain, combinations
from sklearn.metrics import brier_score_loss
from sklearn import linear_model, preprocessing
from sklearn.model_selection import train_test_split
import multiprocess as mp

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits

# read and clean dataset
df = pd.read_csv("data/titanicData.csv")
seed = 120395
np.random.RandomState(seed=seed)
df = df.drop(['Ticket','Cabin','Name'], axis=1)
df = df.dropna() 
dummies = []
cols = ['Pclass', 'Embarked']
for col in cols:
    dummies.append(pd.get_dummies(df[col]))
dummies[0] = dummies[0].rename({1:"Pclass == 1", 2:"Pclass == 2", 3:"Pclass == 3"},axis = 1)
dummies[1] = dummies[1].rename({"C":"Embarked == C", "Q":"Embarked == Q", "S":"Embarked == S"},axis = 1)
titanic_dummies = pd.concat(dummies, axis=1)
df = pd.concat((df,titanic_dummies), axis=1)
df = df.drop(['Pclass', 'Embarked'], axis=1)
df['Sex'] = [1 if s == "male" else 0 for s in df['Sex'].values]

X_tot = df[['PassengerId', 'Age', 'SibSp', 'Parch', 'Fare',
       'Pclass == 1', 'Pclass == 2', 'Pclass == 3', 'Sex', 'Embarked == C', 'Embarked == Q', 'Embarked == S']].values
Y_tot = np.array(df['Survived'])

translation_dict = {"A" : 'PassengerId', "B" : 'Age', "C" : 'SibSp', "D" : 'Parch', "E" : 'Fare', "F" : 'Pclass == 1',
        "G" : 'Pclass == 2', "H" : 'Pclass == 3', "I" : 'Sex', "J" : 'Embarked == C', "K" : 'Embarked == Q', "L" : 'Embarked == S'}


def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def compose_dataset(X, col_names, selection):
    X_ret = []
    for letter in selection:
#         print(letter)
        X_ret.append(X[:,col_names.index(letter)])
    return np.array(X_ret).transpose()

def loop_calculation(i):
    global X_tot
    global Y_tot
    global scaler
    permutation = np.random.permutation(m)
    X = X_tot[permutation]
    Y = Y_tot[permutation]

    m_test = 15
    m_train = m-m_test

    # test_size is the proportion of samples in the test set
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size =float(m_test)/float(m), random_state = int(time.time()))
    X_train = scaler.transform(X_train)
    X_test  = scaler.transform(X_test)
    mm =np.mean(Y_train)
    score_values_test["()"] = brier_score_loss(Y_test, [mm for i in Y_test])    #the prediction that I do having no vars is the mean on the training since it minimizes the error on the training
    for subset in list(powerset("ABCDEFGHIJKL")):
        X_tr = compose_dataset(X_train, col_names = "ABCDEFGHIJKL", selection = subset)
        X_ts  = compose_dataset(X_test,  col_names = "ABCDEFGHIJKL", selection = subset)
        
        reg = linear_model.LogisticRegressionCV(solver='liblinear',cv=10, penalty='l2', Cs = [1e9])
        reg.fit(X_tr, Y_train)  # estimate the LS coefficients
        
        score_values_test[subset] = brier_score_loss(Y_test, reg.predict_proba(X_ts)[:,1])  # predict output values on test set
    return score_values_test


subsets = list(powerset("ABCDEFGHIJKL"))
superset = "ABCDEFGHIJKL"

# raise Exception()
score_values_train = dict()
score_values_test  = dict()
score_values_train["()"] = []
score_values_test["()"] = []
for subset in powerset("ABCDEFGHIJKL"):
    score_values_train[subset] = []
    score_values_test[subset] = []

scaler = preprocessing.StandardScaler().fit(X_tot)
m = Y_tot.shape[0]
n_proc = 72

for n_tests in tqdm([1000,2500,5000,7500,10000]):
    if __name__ == '__main__':
        
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(n_proc)

            input = ((i)
                        for i
                        in list(range(n_tests)))
            results = p.map(loop_calculation, input)   

            p.close()
            p.join()

            for k in subsets:
                for r in results:
                    score_values_test[k].append(r[k])

            for r in results:
                score_values_test["()"].append(r["()"])

            with open('data/score_titanic_w_shuffle_on'+str(n_tests)+'tests.pickle', 'wb') as handle:
                pickle.dump(score_values_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

