% This is samplepaper.tex, a sample chapter demonstrating the
% LLNCS macro package for Springer Computer Science proceedings;
% Version 2.21 of 2022/01/12
%
\documentclass[runningheads]{llncs}
%
\usepackage[T1]{fontenc}
% T1 fonts will be used to generate the final print and online PDFs,
% so please use T1 fonts in your manuscript whenever possible.
% Other font encondings may result in incorrect characters.
%
\usepackage{graphicx}
% Used for displaying a sample figure. If possible, figure files should
% be included in EPS format.
%
% If you use the hyperref package, please uncomment the following two lines
% to display URLs in blue roman font according to Springer's eBook style:
%\usepackage{color}
%\renewcommand\UrlFont{\color{blue}\rmfamily}
%

%MY PACKAGES
\usepackage[ruled,noline,linesnumbered,noend]{algorithm2e}
%\usepackage{amsmath,amsthm,amssymb,paralist}
\graphicspath{{figs/}} %Setting the graphicspath
%\newtheorem{theorem}{Theorem}
%\newtheorem{proposition}{Proposition}
%\newtheorem{definition}{Definition}
%\newtheorem{corollary}{Corollary}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage[dvipsnames]{xcolor}
\newcommand{\algomb}{\texttt{RAveL-MB}}
\newcommand{\algopc}{\texttt{RAveL-PC}}
\usepackage{soul}


\newenvironment{mythm}[1]
{\renewcommand\theinnercustomthm{#1}\innercustomthm}
{\endinnercustomthm}


%MY COMMANDS
\newcommand{\fabio}{\textcolor{red}}
\newcommand{\dario}{\textcolor{ForestGreen}}
\newcommand{\q}{\textcolor{Orange}}
\newcommand\ci{\perp\!\!\!\perp}


\begin{document}
%
\title{Appendix for \\
Bounding the Family-Wise Error Rate in Local Causal Discovery using Rademacher Averages}
%
\titlerunning{Appendix for
	Local Causal Discovery using Rademacher Averages}
% If the paper title is too long for the running head, you can set
% an abbreviated paper title here
%
%\author{Dario Simionato\and
%Fabio Vandin}
%FOR DOUBLE BLIND
\author{Dario Simionato\orcidID{0000-0001-5533-425X
	} \and Fabio Vandin \orcidID{0000-0003-2244-2320}}


%FOR DOUBLE BLIND
%\author{Anonymous Authors}
%

\authorrunning{D. Simionato, F. Vandin}

% First names are abbreviated in the running head.
% If there are more than two authors, 'et al.' is used.
%
\institute{Department of Information Engineering, University of Padova, Italy\\
	\email{dario.simionato@phd.unipd.it,fabio.vandin@unipd.it}}
%
\maketitle              % typeset the header of the contribution
%



\section{PCMB, IAMB and other useful pseudocode}



\begin{algorithm}[h!]
	\KwIn{target variable $T$, set of variables $\mathbf{V}$ }
	\KwOut{$MB(T)$}
	\textit{/* Add true positives to MB */} \\
	$MB \leftarrow$ $\emptyset$\;
	\Repeat{MB does not change}{
		$Y \leftarrow \arg\max_{X\in\mathbf{V}\setminus MB \setminus \{T\}} dep(T,X, MB)$\\
		\If{$T\not\ci Y|MB$}{
			$MB \leftarrow MB \cup \{Y\}$
		}
	}
	\textit{/* Remove false positives from MB */} \\
	\ForEach{$X \in MB$}{
		\If{$T\ci X|MB\setminus\{X\}$}{
			$MB \leftarrow MB \setminus \{X\}$
		}
	}
	\Return{$MB$;}
	\caption{Pseudocode for IAMB \cite{Tsamardinos2003a}}
\end{algorithm}



\begin{algorithm}[h!]
	\KwIn{target variable $T$ }
	\KwOut{$GetPCD(T)$}
	$PCD \leftarrow$ $\emptyset$\;
	$CanPCD \leftarrow \mathbf{V}\setminus\{T\}$\;
	\Repeat{PCD does not change}{
		\textit{/* Remove false positives from CanPCD */} \\	
		\ForEach{$X \in CanPCD$}{
				$Sep[X] \leftarrow \arg\min_{\mathbf{Z}\subseteq PCD} dep(T,X|\mathbf{Z})$
		}	
		\ForEach{$X \in CanPCD$}{
			\If{$T\ci X|Sep[X]$}{
				$CanPCD \leftarrow CanPCD \setminus \{X\}$
			}
		}
		\textit{/* Add the best candidate to PCD */} \\
		$Y \leftarrow \arg\max_{X\in CanPCD} dep(T,X|Sep[X])$\\
		$PCD \leftarrow$ $PCD \cup \{Y\}$\;
		$CanPCD \leftarrow CanPCD \setminus\{Y\}$\;
		\textit{/* Remove false positives from PCD */} \\	
		\ForEach{$X \in PCD$}{
			$Sep[X] \leftarrow \arg\min_{\mathbf{Z}\subseteq PCD\setminus\{X\}} dep(T,X|\mathbf{Z})$
		}	
		\ForEach{$X \in PCD$}{
			\If{$T\ci X|Sep[X]$}{
				$PCD \leftarrow PCD \setminus \{X\}$
			}
		}
	}
	\Return{$PCD$;}
	\caption{Pseudocode for GetPCD \cite{Pena2007}}
\end{algorithm}


\begin{algorithm}[h!]
	\KwIn{target variable $T$ }
	\KwOut{$GetPC(T)$}
	$PC \leftarrow$ $\emptyset$\;
	\ForEach{$X \in GetPCD(T)$}{
		\If{$T \in GetPCD(X)$}{
			$PC \leftarrow PC \cup \{X\}$
		}	
	}	
	\Return{$PC$;}
	\caption{Pseudocode for GetPC \cite{Pena2007}}
\end{algorithm}


\begin{algorithm}[h!]
	\KwIn{target variable $T$ }
	\KwOut{$PCMB(T)$}
	\textit{/* Add true positives to MB */} \\
	$PC \leftarrow$ $GetPC(T)$\;
	$MB \leftarrow$ $PC$\;
	\textit{/* Add more true positives to MB */} \\
	\ForEach{$Y \in PC$}{
		\ForEach{$X \in GetPC(Y)$}{
			\If{$X\not \in PC$}{
				\text{Find $\mathbf{Z}$ such that $T\ci X|\mathbf{Z}$ and $T,X\not \in \mathbf{Z}$} \\
				\If{$T\not\ci X|\mathbf{Z}\cup{Y}$}{
					$MB \leftarrow MB \cup \{X\}$
				}
			}
		}	
	}	
	\Return{$MB$;}
	\caption{Pseudocode for PCMB \cite{Pena2007}}
\end{algorithm}

\section{Proofs for our algorithms}

\begin{theorem}\label{th:PCdef_corr}
	\algopc\texttt{($T$,$\mathbf{V}$,$\delta$)} outputs a set of elements in $PC(T)$ with FWER $\le \delta$.
\end{theorem}
\begin{proof}[sketch] 
	Note that the number of false positives of \algopc\texttt{($T$,$\mathbf{V}$,$\delta$)} is $>0$ if and only if there is at least one variable $X$ of $\mathbf{V}\setminus\{T\}$ that is not in $PC(T)$ and is in the set $PC$ reported by \algopc\texttt{($T$,$\mathbf{V}$,$\delta$)}.
	A variable $X$ is returned in $PC$ if and only if all independence tests between $T$ and $X$  (conditioning on the various sets $\mathbf{Z}\subseteq\mathbf{V}\setminus\{X,T\}$) reject the null hypothesis. Therefore \algopc\texttt{($T$,$\mathbf{V}$,$\delta$)} reports a false positive only if at least one independence test returns a false positive, which happens with probability at most $\delta$ by definition of \texttt{test\_indep($T$,$X$,$\mathbf{Z}$,$\delta$)}.
	%	Let us define the events $E = "\textit{PC\_def(T) outputs a false positive}"$ and $E_i = "\textit{the i-th independence test returns a false positive}"$. By applying the union bound, we have
	%	\[FWER = P(E) \le P(\bigcup_{i} E_i) \le  \delta\]
	%	since all independencies are tested calling $test\_indep$ with parameter $\delta$.
	\qed
\end{proof}


\begin{theorem}\label{th:algo_corr}
	\algomb\ outputs a set of elements in $MB(T)$ with FWER $\le \delta$.
\end{theorem}
\begin{proof}[sketch] 
	The set of \algomb's output elements is the union of the set $O_1$ of variables returned by \algopc\texttt{($T$,$\mathbf{V}$,$\delta$)}, and the set $O_2$ of candidate spouses $Y$ for which \texttt{test\_indep($T$,$Y$,$\mathbf{V}\setminus\{Y,T\}$,$\delta$)} rejects the null hypothesis. Then, a necessary condition to return a false positive is that at least one between sets $O_1$ and $O_2$ contains a false positive. The last event happens if and only if all calls to \texttt{test\_indep($T$,$X$,$\mathbf{Z}$)} returns at least a false positive, which happens with probability at most $\delta$.
	\qed
\end{proof}


\section{Additional proofs for GetPC, PCMB and IAMB}

\begin{theorem}[Study of false positives in GetPCD]\label{th:FP_study}
	An element $X\not \in PCD(T)$ gets returned from GetPCD only if not all the parents of $T$ are detected or the null hypotheses of some independence tests get wrongly rejected.
\end{theorem}
\begin{proof}
	Let us recall that an element $X$ returned by GetPCD is a False Negative if and only if $X\not \in Parents(T)\cup Descendants(T)$.
	
	We see that an element is returned by $GetPCD(T)$ only if it is not removed at lines 8 and 16, which means that the null hypothesis of tests at lines 8 and 18 get always rejected\footnote{The "if" clause does not hold since an element may be added and then subsequently removed leading to the end of the repeat cycle because PCD did not change, but there still are elements in canPCD i.e. unremoved elements.}. The independence test determines dependence of $T$ from $X$ only if conditioning on $\mathbf{Z} = sep[X]$ there is a open path between $X$ and $T$, or if the null hypothesis gets wrongly rejected.
	
	Let us now study the two cases of $X$ being disconnected to $T$ and of $T$ being connected to $T$.
	
	\textbf{Disconnected case}. Let $X$ be disconnected from $T$. Since there are no paths from $X$ to $T$ (therefore no open paths from $X$ to $T$), $X$ may be added to $GetPCD(T)$ only if independence tests at lines 8 and 18 gets wrongly rejected.
	
	\textbf{Connected case.} Let $X\not\in PCD(T)$ be connected to $T$. $X$ gets added only if in any iteration of the cycle the null hypothesis on tests at lines 8 and 18 gets wrongly rejected or there is an open path conditioning on $Sep[X]$. 
	
	By supposing not to have wrong rejections of the null hypotheses, $\mathbf{Z}=Parents(T)$ d-separates $X$ and $T$ by definition of parents since $X$ is not a descendant of $T$. This implies that if some parent of $T$ gets undetected, then it may not be possible to d-separe $X$ from $T$.
\end{proof}

\begin{theorem}\label{th:PCD_delta}
	$GetPCD(T)$ outputs a set of elements in $PCD(T)$ with FWER lower than $\delta$ if the FWER of every independence test performed by GetPCD is below $\delta$ and the infinite power assumption holds while testing independence of elements directly connected.
\end{theorem}
\begin{proof}
	By analyzing GetPCD structure as in Th. \ref{th:FP_study}, an element is returned only if both independence tests at line 8 and 18 reject the null hypothesis therefore the algorithm outputs a false positive if under infinite power assumption for elements directly connected at least one independence test returns a false positive.
	Let us define the events $E = "\textit{GetPCD(T) outputs a false positive}"$ and $E_i = "\textit{the $i$-th independence test returns a false positive}"$. We then have
	\[FWER = P(E) \le P(\bigcup_{i} E_i) \le \delta\]
	by definition of FWER.
	\qed
\end{proof}

\begin{corollary}
	Let us assume that the independence tests performed by GetPCD do not return any False Positive.
	$GetPCD(T)$ outputs a set of elements in $PCD(T)$ with FWER is lower than $\delta$ if only if the infinite power assumption while testing the independence of elements directly connected is satisfied.
\end{corollary}
\begin{proof}
	Let us prove this by proving the equivalent statement if the infinite power assumption while testing the independence of elements directly connected is NOT satisfied then $GetPCD(T)$ outputs a set of elements in $PCD(T)$
\end{proof}


\begin{theorem}\label{th:PC_delta}
	$GetPC(T)$ outputs a set of elements in $PC(T)$ whose FWER is lower than $\delta$ if the FWER of every independence test performed by GetPC is below $\delta$ and the infinite power assumption holds while testing independence of elements directly connected.
\end{theorem}
\begin{proof}
	$GetPC$ outputs a false positive only if at least one call to $GetPCD$ at lines 2-3 outputs a false positive and, under the infinite power assumption while testing independence of elements directly connected, this happens only if at least one independence test outputs a false positive. Let us define the events $E = "\textit{GetPC(T) outputs a false positive}"$ and $E_i = $``the $i$-th independence test returns a false positive''. We then have
	\[FWER = P(E) \le P(\bigcup_{i} E_i) \le \delta\]
	by definition of FWER.
	\qed
\end{proof}



\begin{theorem}\label{th:PCMB_delta}
	$PCMB(T)$ outputs a set of elements in $MB(T)$ with FWER lower than $\delta$ if the FWER of every independence test performed by PCMB is below $\delta$ and the infinite power assumption holds.
\end{theorem}
\begin{proof}
	$PCMB$ outputs a false positive only if there is a false positive in any independence test performed by $GetPC$ calls at lines 2 and 6, or if tests at lines 8 and 9 return a false negative or a false positive, respectively. Given the infinite power assumption and Corollary \ref{th:PC_delta}, $PCMB$ outputs a false postive only if at least one independence test outputs a false positive and by defining the events $E =$``PCMB(T) outputs a false positive'' and $E_i =$ ``the $i$-th independence test returns a false positive'' we have
	\[FWER = P(E) \le P(\bigcup_{i} E_i) \le \delta\]
	by definition of FWER.
	\qed
\end{proof}


\begin{theorem}\label{th:IAMB_delta}
	$IAMB(T)$ outputs a set of elements in $MB(T)$ with FWER lower than $\delta$ if the FWER of every independence test performed by IAMB is below $\delta$ and the infinite power assumption holds.
\end{theorem}
\begin{proof}
	$IAMB$ outputs a false positive only if an element $X\not\in MB(T)$ gets added to MB at lines 5-6, and it does not get removed from MB at lines 10-11. Under the infinite power assumption, all elements in $PC(T)$ gets added at lines 5-6 by definition of PC, therefore $X$ gets returned by IAMB only if independence tests at lines 10-11 output a false positive. Then, by defining the events $E =$ ``GetPC(T) outputs a false positive'' and $E_i =$ ``the $i$-th independence test returns a false positive'', we have
		\[FWER = P(E) \le P(\bigcup_{i} E_i) \le \delta\]
	by definition of FWER.
	\qed
\end{proof}



\section{Relaxation or removal of infinite power assumpion scenarios}
\begin{figure}
	\includegraphics[width=0.4\textwidth]{dag_inf_power.pdf}
	\caption{Example of Bayesian Newtork for which algorithms GetPC, PCMB, and IAMB may fail if infinite power assumption is not met.}\label{fig:graph_example}
\end{figure}

\textbf{Scenario 1: Relaxing infinite power assumption to hold only for directly connected elements} Consider as an example a set $\mathbf{Z}$ for which $T$ and $X\not \in PC(T)$ are dependent and for which adding $Y$ does not change the dependencies. If there is a false negative when testing the conditional independence of $T$ from $X$ given $\mathbf{Z}$ but the test conditioning on $\mathbf{Z}\cup \{Y\}$ outputs the correct result, then $X$ will be erroneously considered as a spouse in PCMB. Figure \ref{fig:graph_example} shows one such example by considering $T = A, X = B$, $\mathbf{Z} = \emptyset$ and $Y = C$.


\textbf{Scenario 2: Removing the infinite power assumption} Consider as an example the calculus of $GetPC(D)$ in the scenario of Figure \ref{fig:graph_example}. Let us suppose that a False Negative occurs when testing the unconditional independencies between $D$ and $B$ and between $A$ and $B$.
Let us further suppose $\mathbf{Z} = \{B,C\}$ to be the only set for which the null hypothesis of independence between $A$ and $D$ is not rejected. Then $GetPCD(D)$ will contain $A$ (because the independence conditioning on $\mathbf{Z} = \{B,C\}$ is never tested), and similarly $GetPCD(A)$ will contain $D$ leading $A$ to be returned by $GetPC(D)$.

\section{Structural model used in synthetic experiments}
Here we report the structural model used in our synthetic experiments

\begin{figure}[h!]
	\includegraphics[width=\textwidth]{graph_exp_10_r4.pdf}
	\caption{
		Bayesian Network used in our synthetic data generation.
	}\label{fig:syn_graph}
\end{figure}

\section{Variables in Boston housing dataset}
Variables description follows from dataset introductory paper \cite{harrison1978hedonic}.


\begin{tabular}{|| c | c ||}
	\hline
	\textbf{Variable name} & \textbf{Explanation}\\
	\hline
	\hline
	CRIM & per capita crime rate by town\\
	ZN & proportion of residential land zoned for lots over 25,000 sq.ft.\\
	INDUS & proportion of non-retail business acres per town.\\
	CHAS & Charles River dummy variable (1 if tract bounds river; 0 otherwise)\\
	NOX & nitric oxides concentration (parts per 10 million)\\
	RM & average number of rooms per dwelling\\
	AGE & proportion of owner-occupied units built prior to 1940\\
	DIS & weighted distances to five Boston employment centres\\
	RAD & index of accessibility to radial highways\\
	TAX & full-value property-tax rate per \$10,000\\
	PTRATIO & pupil-teacher ratio by town\\
	B & $1000(Bk - 0.63)^2$ where Bk is the proportion of blacks by town\\
	LSTAT & \% lower status of the population\\
	MEDV & Median value of owner-occupied homes in \$1000's\\
	\hline
\end{tabular}

%
% ---- Bibliography ----
%
% BibTeX users should specify bibliography style 'splncs04'.
% References will then be sorted and formatted in the correct style.
%
\bibliographystyle{splncs04}
\bibliography{bibliography}
%

\end{document}
