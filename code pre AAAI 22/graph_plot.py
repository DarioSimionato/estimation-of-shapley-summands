import networkx as nx
import matplotlib.pyplot as plt

dg = nx.DiGraph()
for el in "ABCST":
    dg.add_node(el)

for el in "ABC":
    dg.add_edge(el, "S")
    dg.add_edge(el, "T")

pos = {"S" : (0,1),"A" : (-1,0),"B" : (0,0),"C" : (1,0),"T" : (0,-1)}

plt.subplot()
nx.draw(dg, pos, node_size=1500, with_labels=True, font_weight='bold', node_color="grey", edge_color="black")
# plt.show()

plt.savefig('plots/graph.pdf')


dg = nx.DiGraph()
dg2 = nx.DiGraph()
for el in ["V1","V2","V3","T"]:
    dg.add_node(el)
for el in ["V1","V2","V3","V4","V5","V6","T"]:
    dg2.add_node(el)

dg.add_edge("V1", "V2")
dg.add_edge("V3", "V2")
dg.add_edge("V3", "T")


dg2.add_edge("V1", "V2")
dg2.add_edge("T", "V2")
dg2.add_edge("T", "V4")
dg2.add_edge("V4", "V5")
dg2.add_edge("V6", "V5")
dg2.add_edge("V2", "V3")

pos = {"V1" : (-1,0),"V2" : (0,-1),"V3" : (1,0),"T" : (2,-1)}
pos2 =  {"V1" : (-1,1),"V2" : (0,0),"T" : (1,1),"V4" : (2,0), \
    "V5":(3,-1), "V6": (4,0), "V3": (0,-1)}

fig,ax = plt.subplots(1,2, figsize = (15,5))
# n1 = nx.draw(dg, pos, node_size=1500, with_labels=True, font_weight='bold', node_color="white", edge_color="black", ax = ax[0])
# n2 = nx.draw(dg2, pos2, node_size=1500, with_labels=True, font_weight='bold', node_color="white", edge_color="black", ax = ax[1])
n1 = nx.draw_networkx_nodes(dg, pos, node_size=1500, node_color="white", ax = ax[0])
n2 = nx.draw_networkx_nodes(dg2, pos2, node_size=1500, node_color="white", ax = ax[1])

n1.set_edgecolor('black')
n2.set_edgecolor('black')
nx.draw_networkx_edges(dg, pos)
nx.draw_networkx_edges(dg2, pos2)

ax[0].text(-0.1, -0.1, "a.", transform=ax[0].transAxes, size=20, weight='bold')
ax[1].text(-0.1, -0.1, "b.", transform=ax[1].transAxes, size=20, weight='bold')

plt.show()
fig.savefig('plots/colliders_example.pdf')