# -*- coding: utf-8 -*-
from pandas.core.frame import DataFrame
from dependency_infos import dependencyInfos
from math import exp
import os
import pandas as pd
import numpy as np
from itertools import chain, combinations, repeat
import warnings
from tqdm import tqdm
from scipy.stats import chi2_contingency
from scipy.stats import chi2
import summands_lr_fitter
import summands_bounds_calculus
from sklearn.preprocessing import normalize
from sklearn.linear_model import LinearRegression
import IT_utils
import itertools
import summands_lr_fitter
import summands_bounds_calculus
import datetime
import multiprocess as mp
from threadpoolctl import threadpool_limits
import summands_bounds_calculus
import math
import csv

def calculate_residuals(target,data):
    reg = LinearRegression().fit(data, target)
    residuals = target - reg.predict(data)
    # print(residuals.shape)
    return residuals

def calculate_r_summands_parallel_interface(*args):
    params = args[0]
    conditional = params[0][0]
    xs = params[1][0]
    ys = params[1][1]
    if conditional:
        zs = params[1][2]
    else:
        zs = np.array([])
    return calculate_r_summands(xs,ys,zs)

def calculate_r_summands(xs,ys,zs = np.array([]), TT =""):
    if zs.shape[0] != 0:
        # better to have normalized data before doing the regression
        xs -= np.mean(xs)
        xs /= np.std(xs)
        ys -= np.mean(ys)
        ys /= np.std(ys)
        e_x = calculate_residuals(xs,zs) 
        e_y = calculate_residuals(ys,zs)
        xs = e_x
        ys = e_y
    n = xs.shape[0]
    xs -= np.mean(xs)
    if not np.std(xs)==0:
        xs /= np.std(xs)
    ys -= np.mean(ys)
    if not np.std(ys)==0:
        ys /= np.std(ys)
    r_summs = np.multiply(xs,ys)/ (n-1)
    return r_summs

# def calculate_signed_sum(*args):
#     # calculates the sum of sigma_i * f(z_i)
#     # signature sigma,f_matrix,sigma_col,f_col
#     params = args[0]
#     # sigma = params[0]
#     # f_matrix = params[1]
#     # sigma_col = params[2]
#     # f_col = params[3]
#     # sigma_vals = sigma[:,sigma_col]
#     # f_vals = f_matrix[:,f_col]
#     sigma_vals = params[0][0]
#     f_vals = params[1][0]
#     sigma_col = params[0][1]
#     f_col = params[1][1]
#     # sigma_vals = sigma[:,sigma_col]
#     # f_vals = f_matrix[:,f_col]
#     return sigma_col, f_col, np.dot(sigma_vals,f_vals.T)

# def calculate_max_sum(*args):
#     params = args[0]
#     # sum_matrix = params[0]
#     # row = params[1]
#     sum_row = params[0]
#     row = params[1]
#     # print(sum_row.shape)
#     # print(row)
#     # raise Exception
#     # return row, np.max(sum_matrix[row,:])
#     return row, np.max(sum_row)

# def perform_sup_calculus(sigma, r_mat, min_cores = 72):
#     print("in")
#     n_sigma = sigma.shape[0]
#     with threadpool_limits(limits=1, user_api='blas'):
#         p = mp.Pool(min(min_cores,72))
#         # sigma,f_matrix,sigma_col,f_col
#         # sigma_cols = [i for i in range(sigma.shape[1])]
#         # f_cols = [i for i in range(r_mat.shape[1])]
#         # input = ((a,b,c,d) for a,b,c,d in list(itertools.product([sigma], [r_mat], sigma_cols, f_cols)))
#         sigma_couples = [(sigma[:,i],i) for i in range(sigma.shape[1])]
#         f_couples = [(r_mat[:,i],i) for i in range(r_mat.shape[1])]
#         input = ((a,b) for a,b in list(itertools.product(sigma_couples,f_couples)))
#         results = p.map(calculate_signed_sum, input)   
#         p.close()
#         p.join()
#         print("middle")
#         p = mp.Pool(min(min_cores,72))
#         sum_mat = np.zeros((n_sigma,r_mat.shape[1]))
#         for sigma_col, f_col, sum_val in sorted(results,key= lambda x: (x[0],x[1])):
#             sum_mat[sigma_col,f_col] = sum_val    
#         print("a bit after")
#         sum_rows = [i for i in range(n_sigma)]
#         sum_couples = [(sum_mat[i,:],i) for i in range(n_sigma)]
#         # input = ((a,b) for a,b in list(itertools.product([sum_mat], sum_rows)))
#         input = ((a) for a in sum_couples)
#         results = p.map(calculate_max_sum, input)   
#         p.close()
#         p.join()        
#         print("almost")
#         sups_vector = np.zeros(n_sigma)
#         for row, sup_val in sorted(results,key= lambda x: x[0]):
#             sups_vector[row] = sup_val
#     print("out")
#     return sups_vector
            
# def calculate_r(data, col, combos, conditional, min_cores = 72):
#     with threadpool_limits(limits=1, user_api='blas'):
#         p = mp.Pool(min(min_cores,72))
        
#         if conditional:
#             # combos is a triplet (A,B,(cond_set))
#             in_triplets = [(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])) 
#                 for t in combos]
#             input = ((a,b) for a,b in list(itertools.product([conditional],in_triplets)))
#         else:
#             # combos is a couple (A,B)
#             in_couples = [(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])) 
#                 for t in combos]
#             input = ((a,b) for a,b in list(itertools.product([conditional],in_triplets)))

#         results = p.map(calculate_signed_sum, input)   
#         p.close()
#         p.join()
#     np.array([calculate_r_summands(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])) 
#             for t in triplets[i*step_size:(i+1)*step_size]])

# def calculate_r_bounds(data, delta, n_sigma):
#     col = list(data.columns.values)
#     n_vars = len(col)
#     #  calculate test on var1 and var2 conditioning on emptyset
#     couples = []
#     for i in range(data.shape[1]):
#         for j in range(data.shape[1]):
#             if i<j:
#                 couples.append((col[i],col[j]))
#     couples = np.array(couples)
#     cond_sets = np.array([z for z in IT_utils.powerset(col, max_z_size = n_vars-3, get_emptyset= False)])
#     triplets = []
#     for c in couples:
#         for s in cond_sets:
#             if not c[0] in s and not c[1] in s:
#                 triplets.append((c,s))
#     r_mat_uncond = np.array([calculate_r_summands(data[t[0]].values,data[t[1]].values) for t in couples]).T
#     r_vals = np.sum(r_mat_uncond,axis=0).tolist()
#     m = r_mat_uncond.shape[0]
#     sigma = np.random.rand(m,n_sigma) - 0.5
#     sigma[sigma>=0] = 1
#     sigma[sigma<0] = -1 
#     # sups_vector = np.zeros(n_sigma)-100
#     # with threadpool_limits(limits=1, user_api='blas'):
#     #     p = mp.Pool(min(20,72))
#     #     # sigma,f_matrix,sigma_col,f_col
#     #     sigma_cols = [i for i in range(sigma.shape[1])]
#     #     f_cols = [i for i in range(r_mat_uncond.shape[1])]
#     #     input = ((a,b,c,d) for a,b,c,d in list(itertools.product([sigma], [r_mat_uncond], sigma_cols, f_cols)))
#     #     results = p.map(calculate_signed_sum, input)   
#     #     p.close()
#     #     p.join()
#     #     p = mp.Pool(min(20,72))
#     #     sum_mat = np.zeros((n_sigma,r_mat_uncond.shape[1]))
#     #     for sigma_col, f_col, sum_val in sorted(results,key= lambda x: (x[0],x[1])):
#     #         sum_mat[sigma_col,f_col] = sum_val    
#     #     sum_rows = [i for i in range(n_sigma)]
#     #     input = ((a,b) for a,b in list(itertools.product([sum_mat], sum_rows)))
#     #     results = p.map(calculate_max_sum, input)   
#     #     p.close()
#     #     p.join()
#     #     for row, sup_val in sorted(results,key= lambda x: x[0]):
#     #         sups_vector[row] = max(sups_vector[row],sup_val
#     sups_vector = perform_sup_calculus(sigma, r_mat = r_mat_uncond, min_cores = 72)
#     step_size = 1000000
#     for i in range(int(len(triplets)/step_size)):
#         print("Conditional done",i*step_size)
#         #calculate stats for the block
#         r_mat_cond = np.array([calculate_r_summands(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])) 
#             for t in triplets[i*step_size:(i+1)*step_size]])
#         r_mat_cond = calculate_r()
#         print("pt.0")
#         sups_vector_cond = perform_sup_calculus(sigma, r_mat = r_mat_cond, min_cores = 72)
#         #update stats
#         print("pt.1")
#         r_vals.append(np.sum(r_mat_cond,axis=0).tolist())
#         print("pt.2")
#         sups_vector = np.maximum(sups_vector,sups_vector_cond)
#         print("pt.3")
#         if i == 3:
#             break
#         print("pt.4")
#         # i = 0
#         # r_mat_cond = []
#         # for t in triplets:
#         #     r_mat_cond.append(calculate_r_summands(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])))
#         #     i+=1
#         #     if i%100000 == 0:
#         #         print(datetime.datetime.now(),i)
#     # r_mat_cond = np.array([calculate_r_summands(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])) for t in triplets])
#     # i = 0
#     # r_mat_cond = []
#     # for t in triplets:
#     #     r_mat_cond.append(calculate_r_summands(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1])))
#     #     i+=1
#     #     if i%100000 == 0:
#     #         print(datetime.datetime.now(),i)
#     return r_vals, np.sum(sups_vector)/m/n_sigma, m

def select_candidates(tot_vars):
    candidates = []
    for i in range(len(tot_vars)):
        for j in range(len(tot_vars)):
            if i<j:
                candidates.append((tot_vars[i],tot_vars[j]))
    n_els_max = math.floor(len(candidates)/(len(tot_vars)-1))
    dic_els = {}
    for i in range(len(tot_vars)):
        cnt = 0
        dic_els[tot_vars[i]] = []
        for c in candidates:
            if tot_vars[i] in c and cnt != n_els_max:
                dic_els[tot_vars[i]].append(c)
                cnt += 1
        for c in dic_els[tot_vars[i]]:
            candidates.remove(c)
    # print(candidates)
    return dic_els
                

def calculate_r_bound_per_var(*args):
    # variable, filename, sigma, couples_dict
    params = args[0]
    variable = params[0]
    filename = params[1]
    sigma = params[2]
    couples_dict = params[3]
    sigma_cp = sigma.view()
    n_sigma = sigma_cp.shape[1]
    data = pd.read_csv(filename) 
    col = [c for c in list(data.columns.values) if not "Unnamed" in c]
    n_vars = len(col)
    #  calculate test on var1 and var2 conditioning on emptyset
    couples = np.array(couples_dict[variable])
    cond_sets = np.array([z for z in IT_utils.powerset(col, max_z_size = n_vars-2, get_emptyset= False)])
    triplets = []
    for c in couples:
        for s in cond_sets:
            if not c[0] in s and not c[1] in s:
                triplets.append((c,s))
    if len(triplets) > 0:
        r_mat_uncond = np.array([calculate_r_summands(data[t[0]].values,data[t[1]].values) for t in couples]).T
        r_vals = np.sum(r_mat_uncond,axis=0).tolist()
        m = r_mat_uncond.shape[0]
        sums = np.matmul(sigma_cp.T,r_mat_uncond)  # n x m * m x k = n x k matrix
        sups_vector = np.max(sums,axis = 1)
        step_size = 1000
        for i in range(math.ceil(len(triplets)/step_size)):
            # print("A")
            # if i!= 0 and i%10==0:
            #     print("B")
            #     break
            #calculate stats for the block
            r_mat_cond = np.array([calculate_r_summands(data[t[0][0]].values,data[t[0][1]].values, summands_lr_fitter.compose_dataset(data.values,col, selection=t[1]),t) 
                for t in triplets[i*step_size:(i+1)*step_size]]).T
            # print("pt.0")
            sums_cond = np.matmul(sigma_cp.T,r_mat_cond)
            sups_vector = np.maximum(sups_vector, np.max(sums_cond,axis = 1))
            #update stats
            # print("pt.1")
            r_vals.append(np.sum(r_mat_cond,axis=0).tolist())
    return variable, r_vals, sups_vector, couples, triplets
    

def calculate_r_SD_bounds(filename, tot_vars, delta, couples_dict, n_sigma = 1000):
    ##### ricontrolla m n sigma ecc ecc
    data = pd.read_csv(filename) 
    m = data.shape[0]
    # interval of size 2 with between -1 and 1
    z = 1
    c = 2
    sigma = np.random.rand(m,n_sigma) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 
    tot_couples = []
    tot_triples = []
    r_vals = []
    tot_sups = None
    with threadpool_limits(limits=2, user_api='blas'):
        p = mp.Pool(len(tot_vars))
        # variable, filename, sigma, couples_dict
        input = ((a,b,c,d) for a,b,c,d in list(itertools.product(tot_vars,[filename],[sigma],[couples_dict])))
        results = p.map(calculate_r_bound_per_var, input)   
        p.close()
        p.join()
        for v, r_vs, sups, couples, triples in sorted(results,key= lambda x: x[0]):
            tot_couples.append([couples])
            tot_triples.append([triples]) 
            r_vals.append(r_vs)
            if tot_sups is None:
                tot_sups = sups
            else:
                tot_sups = np.maximum(tot_sups,sups)
    R_hat = np.sum(tot_sups)/m/n_sigma
    # r_vals, R_hat = calculate_r_bound_per_var(variable, filename, sigma, couples_dict)
    R_tilde = R_hat + 2*z*math.sqrt(math.log(4/delta)/(2*n_sigma*m))
    return r_vals, summands_bounds_calculus.calculate_sup_dev_bound(m, n_sigma, R_tilde, delta, z = z, c = c, get_splitted_values=True), tot_couples, tot_triples
  
def write_on_csv(filename, r_vals, bound_tot_val, tot_couples, tot_triples):
    df = pd.DataFrame(columns = ["first_term", "second_term", "cond_set", "lb","stat","up"])
    for i in range(len(tot_couples)):
        firsts = []
        seconds = []
        conds = []
        lbs = []
        stats = []
        ups = []
        for j in range(len(tot_couples[i][0])):  # unconditional
            # dat = {"first_term":tot_couples[i][0][j][0],
            #     "second_term":tot_couples[i][0][j][1],
            #     "cond_set": "()",
            #     "lb":r_vals[i][j]-bound_tot_val ,
            #     "stat": r_vals[i][j],
            #     "up": r_vals[i][j]+bound_tot_val}
            # df.append(dat,ignore_index=True)
            firsts.append(tot_couples[i][0][j][0])
            seconds.append(tot_couples[i][0][j][1])
            conds.append("()")
            lbs.append(r_vals[i][j]-bound_tot_val )
            stats.append(r_vals[i][j])
            ups.append(r_vals[i][j]+bound_tot_val)
        df2 = pd.DataFrame.from_dict(
            {"first_term": firsts,
            "second_term": seconds,
            "cond_set": conds ,
            "lb": lbs ,
            "stat": stats ,
            "up": ups }
        )
        cumulative = 0
        df = pd.concat([df,df2])
        df.to_csv(filename)
        triplets_idx = len(tot_couples[i][0])
        # the first #couples elements are single values (one per couple)
        # but then there are len(triplets)/step_size arrays of maximum step_size elements
        # that are related to the couples
        for j in range(triplets_idx,len(r_vals[i])):  # unconditional
            firsts = []
            seconds = []
            conds = []
            lbs = []
            stats = []
            ups = []
            for k in range(len(r_vals[i][j])):
                # dat = {"first_term":tot_triples[i][0][j][0][0],
                #     "second_term":tot_triples[i][0][j][0][1],
                #     "cond_set": tot_triples[i][0][j][1],
                #     "lb":r_vals[i][j][k]-bound_tot_val ,
                #     "stat": r_vals[i][j][k],
                #     "up": r_vals[i][j][k]+bound_tot_val}
                # df.append(dat,ignore_index=True)
                firsts.append(tot_triples[i][0][cumulative][0][0])
                seconds.append(tot_triples[i][0][cumulative][0][1])
                conds.append(IT_utils.convert_to_string(tot_triples[i][0][cumulative][1]))
                lbs.append(r_vals[i][j][k]-bound_tot_val)
                stats.append(r_vals[i][j][k])
                ups.append(r_vals[i][j][k]+bound_tot_val)
                cumulative+=1
            df2 = pd.DataFrame.from_dict(
                {"first_term": firsts,
                "second_term": seconds,
                "cond_set": conds ,
                "lb": lbs ,
                "stat": stats ,
                "up": ups }
            )
            df = pd.concat([df,df2])
        df.to_csv(filename)

# cc = []
# dd = []
# filename = "datasets/multiple8/v8_S1000_t10_r1.csv"
# df = pd.read_csv(filename)
# col = list(df.columns.values)
# couples_dict = select_candidates(col)
# r_vals, bound, tot_couples, tot_triples = calculate_r_SD_bounds(filename, col, delta = 0.05, couples_dict= couples_dict)
# write_on_csv("test_r1.csv",r_vals, bound[0], tot_couples, tot_triples)

# # for v in col:
# #     a, b, c, d = calculate_r_SD_bounds("datasets/multiple8/v8_S1000_t10_r5.csv", v, delta = 0.05, couples_dict= couples_dict)
# #     cc.append(c)
# #     dd.append(d)

# f = open('rad_partial_results.txt', 'w')
# # create the csv writer
# # write a row to the csv file
# bound_tot_val = bound[0]
# for i in range(len(col)):
#     for j in range(len(r_vals[i])):
#         # print(tot_couples[i][0][j],'{:8.5f}'.format(r_vals[i][j]), "  INDEP" if r_vals[i][j]-bound_tot_val <= 0 <= r_vals[i][j] + bound_tot_val else "    DEP")
#         # f.write(str(tot_couples[i][0][j])+'{:8.5f}'.format(r_vals[i][j])+("  INDEP" if r_vals[i][j]-bound_tot_val <= 0 <= r_vals[i][j] + bound_tot_val else "    DEP")+'\n')
#         if tot_couples[i][0][j][0][1] == tot_couples[i][0][j][1][1] and r_vals[i][j]-bound_tot_val <= 0 <= r_vals[i][j] + bound_tot_val:
#             print("DOVREBBE ESSERE DEP")
#             print(tot_couples[i][0][j],'{:8.5f}'.format(r_vals[i][j]), "  INDEP" if r_vals[i][j]-bound_tot_val <= 0 <= r_vals[i][j] + bound_tot_val else "    DEP")

# # close the file
# f.close()