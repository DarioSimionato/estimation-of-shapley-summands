import networkx as nx
import matplotlib.pyplot as plt

dg = nx.DiGraph()
for el in "ABCST":
    dg.add_node(el)

dg.add_node("XOR(A,B)")

dg.add_edge("C", "T")
dg.add_edge("C", "S")
dg.add_edge("A", "XOR(A,B)")
dg.add_edge("B", "XOR(A,B)")
dg.add_edge("XOR(A,B)", "S")
dg.add_edge("XOR(A,B)", "T")

pos = {"S" : (0,1),"A" : (-1,0), "XOR(A,B)": (0,0), "B" : (1,0),"C" : (2,0),"T" : (0,-1)}

plt.subplot()
nx.draw(dg, pos, node_size=1500, with_labels=True, font_weight='bold', node_color="grey", edge_color="black")
# plt.show()

plt.savefig('cp1_graph.pdf')