#!/bin/env bash
cd /mnt/ssd4/dsim/estimation-of-shapley-summands/code/
source shapEnv/bin/activate
/home/dsim/estimation-of-shapley-summands/code/shapEnv/bin/python main.py -r 1 | tee output_first_test.txt
/home/dsim/estimation-of-shapley-summands/code/shapEnv/bin/python counterproof/v3\ non\ linearities/cp3_main.py -r 1 | tee output_second_test.txt
/home/dsim/estimation-of-shapley-summands/code/shapEnv/bin/python counterproof/v4\ square\ function/cp4_main.py -r 1 | tee output_third_test.txt
