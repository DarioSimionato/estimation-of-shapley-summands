# -*- coding: utf-8 -*-
import traceback
from dependency_infos import dependencyInfos
from math import exp
import os
import pandas as pd
import numpy as np
from itertools import chain, combinations, repeat
import warnings
from tqdm import tqdm
from scipy.stats import chi2_contingency
from scipy.stats import chi2
import math
from fcit import fcit
import summands_lr_fitter
import summands_bounds_calculus
try:
    import rpy2.robjects as robjects
    from rpy2.robjects import pandas2ri
    from rpy2.robjects.packages import SignatureTranslatedAnonymousPackage as STAP
    from rpy2.robjects.conversion import localconverter
    
except:
    print("ERROR WHILE IMPORTING RPY2 LIB")

# warnings.filterwarnings('ignore')


def count_elements(seq, keys = []) -> dict:
    """Utility function to count element of each histogram bar."""
    hist = {}
    for k in keys:
        hist.setdefault(k,0)
    for i in seq:
        hist[i] = hist.get(i, 0) + 1
    return hist 

def convert_to_string(z):
    if len(z) > 1:
        z = sorted(z)
    t_present = "T" in z
    if t_present:
        z = list(z)
        z.remove("T")
        z = set(z)
    digit_only = ''.join(x for x in str(z) if x.isdigit())
    if len(digit_only) > 0: #it is an experiment with A1, A2, ...
        z = ''.join(x for x in str(sorted(z, key= lambda el: (el[1],el[0]))) if x.isalpha() or x.isdigit())
    else: # it is an experiment with A, B, ...
        z = ''.join(x for x in str(sorted(z)) if x.isalpha())
    if t_present:
        z += "T" 
    if len(z) == 0:
        z = "()" 

    return z

def powerset(iterable, get_emptyset = True, max_z_size = -1):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    if max_z_size == -1:
        if len(s)>10:
            max_z_size = 4
        else:
            max_z_size = len(s)
    if get_emptyset:
        return chain.from_iterable(combinations(s, r) for r in range(max_z_size+1))
    return chain.from_iterable(combinations(s, r) for r in range(1,max_z_size+1))

def parse_IT(t,x,z, indep, ordered):
    if t<x or not ordered:
        return t+" "*indep+"!"*(not(indep))+"⊥ "+x+" | "+convert_to_string(z)
    return x+" "*indep+"!"*(not(indep))+"⊥ "+t+" | "+convert_to_string(z)

def convert_summand_to_it(summand, target):

    if len(''.join(x for x in summand if x.isdigit())) > 0:
        v = summand.split(" ")[0]
        other = summand.split(" ")[2]
        i = 0
        while len(v)*len(other) > 0 and v[i] == other[i]:
            v = v[1:]
            if len(other) == 1:
                other = ""
            else:
                other = other[1:]
        i = 1
        while len(v)*len(other) > 0 and v[-i] == other[-i]:
            v = v[:-1]
            if len(other) == 1:
                other = ""
            else:
                other = other[:-1]
    else:
        v = [var for var in summand if summand.count(var)==1][0]

    return v,target,summand.split(" ")[2]

def get_argmin_dep(dep_infos, t,x, pcd, delta, max_z_size = -1):
    ps = sorted([el for el in powerset(pcd,max_z_size = max_z_size) if t not in el and x not in el], key = lambda element: (len(element),str(element)))
    not_enough_els = set()
    deps = []
    max_p_val = 0
    for z in ps:
        if not z in not_enough_els:
            assoc = association(dep_infos,t,x,z, delta)  
            deps.append((z,assoc))    # must add z and association
            pval = -assoc  # association returns the negative p value
            max_p_val = max(max_p_val, pval)
            if dep_infos.method != "Rademacher":
                if pval == -1 and len(z)==0:  #conditioning on the emptyset creates p val = -1 => all the p vals will be -1
                    not_enough_els = ps
                    break
                elif pval == -1:
                    bad_list = [bad_z for bad_z in ps if set(z).issubset(set(bad_z))]
                    [not_enough_els.add(bad_z) for bad_z in bad_list]
                elif max_p_val == 1: # no need to get other subsets if I get a p-value of sure independence (=lowest association)
                    break
                elif max_p_val > delta and dep_infos.method: # in our example, that's a sure independence
                    break
            else:
                lb, assoc, ub = association(dep_infos,t,x,z, delta, get_rademacher_bounds=True)
                if lb<=0<=ub:   # independent since it contains 0
                    break
    dependencies = sorted(deps, key = lambda element : element[1])
    return dependencies[0][0]  # take the argmin

def is_calculated(dep_infos,t,x,z):
    df = dep_infos.df
    lookup_table = df[df["first_term"] == t]
    lookup_table = lookup_table[lookup_table["second_term"] == x]
    z_str = convert_to_string(z)
    
    if len(z_str) == 0:
        z_str = "()"
    lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
    return len(lookup_table)>0

##
# .csv file specification for conditional dependencies
#   fields
#  first_term, second_term, cond_set, p_value, statistic, lb
#  where each term and set contains only letters and the empty set is identified by '()'
#  and the p_value is the p value of the test with independence as null hypothesis.
#  In methods that do not use this independence test (e.g. summand independence methods)
#  The field lb (=lower bound) contains the lower bound of the summand under study
##

def association(dep_infos,t,x,z, delta = -1, get_rademacher_bounds = False):
    # returns conditional association of T and X given Z

    # preprocessing of z keeping only letters or numbers
    # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
    z_str = convert_to_string(z)
    df = dep_infos.df
    method = dep_infos.method
    # if x in z or t in z:
    #     print(t,x,z)
    #     print(traceback.print_stack())
    #     return None
    lookup_table = df[df["first_term"] == t]
    # print(lookup_table)
    lookup_table = lookup_table[lookup_table["second_term"] == x]
    # print(lookup_table)
    if len(z_str) == 0:
        z_str = "()"
    lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
    # print(lookup_table)
    if len(lookup_table)>0:
        if method == "p-value":
            return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
        if method == "pearson":
            return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
        if method == "dsep":
            return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
        if method == "statistic":
            return lookup_table["statistic"].values[0]
        if method == "summands":
            return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
        if method == "summands_naive":
            return lookup_table["naive_lb"].values[0]
        if method == "summands_mb":
            return lookup_table["multiple_lb"].values[0]
        if method == "summands_lb":
            return lookup_table["single_lb"].values[0]
        if method == "Rademacher":
            if get_rademacher_bounds:
                return lookup_table["lb"].values[0],lookup_table["stat"].values[0],lookup_table["up"].values[0]
            return abs(lookup_table["stat"].values[0])
        print("METHOD NOT FOUND")
    else:
        if method == "Rademacher":  # I MUST have it already calculated
            return association(dep_infos,x,t,z, delta = delta, get_rademacher_bounds=get_rademacher_bounds)
        if method == "dsep":
            calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
        elif method == "summands":
            if not dep_infos.summands_initialized:
                init_dep_infos_summands(dep_infos)
            calculate_summands_score(dep_infos,t,x,z_str,z,delta)
        else:
            if dep_infos.independence_method=="fcit":
                calculate_dep_fcit(dep_infos, t, x, z_str, z)
            elif dep_infos.independence_language == "R":
                calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
            else:
                calculate_dependency_python(dep_infos, t, x, z_str, z)
        return association(dep_infos,t,x,z)

def independence(dep_infos,t,x,z, delta, verbose = False, return_string = False, ordered = False, level = 0):
    # returns True if X and T are conditionally independent given Z, meaning I cannot reject the null hypothesis
    method = dep_infos.method
    
    if not "summands" in method:
        if "Rademacher" == method:
            lb, stat, up = association(dep_infos,t,x,z, delta, get_rademacher_bounds=True)
            res = lb <= 0 <= up # independent if bound contains 0
        else:
            res = -association(dep_infos,t,x,z, delta) > delta
    else:
        res = association(dep_infos,t,x,z, delta) <= 0   # if the lower bound is <= 0 then the vars are indep
    if verbose:
        if level == 2:
            print("        ",end = '')
        if level == 1:
            print("    ",end = '')
        if not "summands" in method:
            print(parse_IT(t,x,z, res, ordered), "p-value",-association(dep_infos,t,x,z,delta))
        else:
            print(parse_IT(t,x,z, res, ordered), method, association(dep_infos,t,x,z,delta))
    if return_string:
        return res, parse_IT(t,x,z, res, ordered)
    return res

def calculate_dependency(dep_infos, t, x, z):
    method = dep_infos.method
    independence_method = dep_infos.independence_method
    data_file = dep_infos.data_file
    df = dep_infos.df
    indep_file = dep_infos.indep_file

    if len(z) == 0:
        z = ["()"]

    if not "summand" in method:
        if data_file != "" and independence_method!="":
                
            print("Calculating summand from R call")
            # Defining the R script and loading the instance in Python
            ro = robjects.r
            robjects.r("""source(%s)""" % os.path.join(os.getcwd(), 'produce_CI.r'))
            # r['source'](os.path.join(os.getcwd(), 'produce_CI.R'))
            # Loading the function we have defined in R.
            # perform_CI <- function(c1, c2, s, dataset, test_type){
            filter_country_function_r = robjects.globalenv['perform_CI']
            # Reading and processing data
            df_data = pd.read_csv(data_file)
            #converting it into r object for passing into r function
            df_r = pandas2ri.ri2py(df_data)
            z_vec = robjects.StrVector([el for el in z])
            #Invoking the R function and getting the result
            CI_result = filter_country_function_r(t,x,z_vec, df_r, independence_method)

            # t,x,z
            p_val = CI_result[list(CI_result.do_slot('names')).index('p.value')]
            cor = CI_result[list(CI_result.do_slot('names')).index('statistic')]
            lb = 1

            print(t,x,z, p_val,cor,lb)

            # a_row = pd.Series([t,x,z, p_val,cor,lb])
            # df = pd.concat([df, a_row], ignore_index=True)
            # if indep_file != "":
            #     df.to_csv(indep_file,sep = ";")

    else:
        raise Error("CALCULUS OF SUMMAND AD HOC NOT IMPLEMENTED YET")

def calculate_dependency_hardcoded(dep_infos, t, x, z_str, z):
    method = dep_infos.method
    independence_method = dep_infos.independence_method
    data_file = dep_infos.data_file
    df = dep_infos.df
    indep_file = dep_infos.indep_file
    save_every_update = dep_infos.save_every_update
    # print("asking for ",t, x, z_str)
    if len(z) == 0:
        z = ["()"]
    if not "summand" in method:
        if data_file != "" and independence_method!="":      
            # Defining the R script and loading the instance in Python
            r_code = '''library(readr)
                library(bnlearn)
                library(Rcpp)
                library(RcppZiggurat)
                library(Rfast)
                perform_CI <- function(c1, c2, s, dataset, test_type){
                    if(c1 %in% s || c2 %in% s){
                        print("c1")
                        print(c1) 
                        print("c2")
                        print(c2) 
                        print("s")
                        print(s)
                        return <- list("p.value" = p_val, "statistic" = res$statistic)
                    }else{
                            
                        debug <- FALSE
                        if(debug){
                            print(c1) 
                            print(c2) 
                            print(s)
                        }
                        if (test_type=="x2" || test_type=="g2"){
                            dataset[] <- lapply( dataset, factor) # the "[]" keeps the dataframe structure
                        }
                        if(!test_type=="g2"){
                            if (!s[1]=="()"){
                                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
                            }else{
                                t <- ci.test(c1, c2, data = dataset, test = test_type, debug = TRUE)
                            }
                            
                        }else{
                            n <- colnames(dataset) 
                            col_c1 <- match(c1, n)
                            col_c2 <- match(c2, n)
                            cols_c3 <- c()
                            uni <- c(length(unique(dataset[c1])[[1]])[[1]],length(unique(dataset[c2])[[1]])[[1]])
                            if (!s[1]=="()"){
                                for(v in s){
                                    idx <- match(v, n)
                                    cols_c3 <- append(cols_c3,idx)
                                    uni <- append(uni,length(unique(dataset[v])[[1]])[[1]])
                                }
                            }
                            for (nn in n){
                                dataset[nn] <- unclass(as.factor(dataset[nn][[1]]))
                            }
                            ds <- as.matrix(dataset)
                            # make minimum value = 0
                            ds <- ds - 1
                            
                            if(debug){
                                # look at the number of unique values before changing, as a means of validation
                                print(sapply(1:ncol(ds), function(x) length(unique(ds[, x]))))
                                # look at the minimum, as a means of validation
                                print(sapply(1:ncol(ds), function(x) min(ds[,x])))
                            }
                            tellMe <- c(1:ncol(ds))
                            tellMe <- tellMe[-c(col_c1, col_c2, sort(cols_c3))] 
                            # rearrange using the indices
                            ds <- ds[, c(col_c1, col_c2, sort(cols_c3), tellMe)]
                            if(debug){
                                print(sapply(1:ncol(ds), function(x) min(ds[,x])))
                                print(sapply(1:ncol(ds), function(x) length(unique(ds[, x]))))
                                print("A")
                                # print(dimnames(ds))
                                print("B")
                            }
                            print(colnames(ds))
                            colnames(ds) <- NULL
                            ds <- ds[,1:2+length(cols_c3)]

                            if (length(cols_c3) == 0){
                                print("UNIVARIATE")
                                res <- g2Test_univariate(ds[,1:2], uni)
                            }else{
                                if(debug){print("MULTIVARIATE")}
                                counter_c <- c(1:length(cols_c3)) + 2
                                if(debug){
                                    print(counter_c)
                                    print(uni)
                                    print(g2Test(ds, 1, 2, counter_c, uni))
                                }
                                # always data in order: c_1 on column 1, c_2 on column 2 and the other from 3 to go
                                res <- g2Test(ds, 1, 2, counter_c, uni)
                            }
                            p_val <- pchisq(q= res$statistic, df=res$df, lower.tail=FALSE)
                            t <- list("p.value" = p_val, "statistic" = res$statistic)
                        }
                        return <- t
                    }
                }
            '''
            testy = STAP(r_code, "testy")
            # Reading and processing data
            df_data = pd.read_csv(data_file)
            ro = robjects.r
            #converting it into r object for passing into r function
            with localconverter(robjects.default_converter + pandas2ri.converter):
               df_r = robjects.conversion.py2rpy(df_data)
            # df_r = pandas2ri.ri2py(df_data)
            
            z_vec = robjects.StrVector([el for el in z])
            #Invoking the R function and getting the result
            CI_result = testy.perform_CI(t,x,z_vec, df_r, independence_method)
            p_val = CI_result[list(CI_result.do_slot('names')).index('p.value')]
            cor = CI_result[list(CI_result.do_slot('names')).index('statistic')]
            lb = 1

            #a_row = pd.DataFrame([[t,x,z, p_val[0],cor[0],lb]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            a_row = pd.DataFrame([[t,x,z_str, p_val[0],cor[0],lb]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
    else:
        raise Error("CALCULUS OF SUMMAND AD HOC NOT IMPLEMENTED YET")

def calculate_dependency_python(dep_infos, t, x, z_str, z):
    method = dep_infos.method
    independence_method = dep_infos.independence_method
    data_file = dep_infos.data_file
    df = dep_infos.df
    indep_file = dep_infos.indep_file
    save_every_update = dep_infos.save_every_update
    calculus_type = dep_infos.calculus_type

    if len(z) == 0:
        z = ["()"]
    else:
        z = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
    if not "summand" in method:
        if data_file != "" and independence_method!="": 
            selection_cols = [t,x]
            conditioning_cols = z

            df_data = pd.read_csv(data_file)
            p_val = -1
            cor = -1
            dof_tx = (len(set(df_data[t]))-1)*(len(set(df_data[x]))-1)
            dof = dof_tx
            if z != ["()"]:
                for cc in conditioning_cols:
                    dof *= len(set(df_data[cc]))
                uniques = [row[1].values for row in df_data[conditioning_cols].iterrows()]
                uniques = set([tuple(row.tolist()) for row in uniques])
                df_dict = {single_val: pd.merge(pd.DataFrame([single_val],columns=conditioning_cols), df_data, on=conditioning_cols,how='left') for single_val in uniques}
            else:
                df_dict = {"()": df_data}

            statistic = 0
            not_enough_cells = False
            for k,v in df_dict.items():
                els_c0 = list(set(v[selection_cols[0]]))
                els_c1 = list(set(v[selection_cols[1]]))
                if min(len(els_c0),len(els_c1)) > 1:
                    data = [[v[(v[selection_cols[0]]==els_c0[i]) & (v[selection_cols[1]]==els_c1[j])].shape[0]
                                    for i in range(len(els_c0))] for j in range(len(els_c1))]
                    
                    lam = "undefined"
                    if independence_method == "g2":
                        lam = "log-likelihood" 
                    if independence_method == "x2":
                        lam = "pearson"
                    stat_lib, pv_lib, dof_lib, expected = chi2_contingency(data, correction=False,lambda_ = lam)  # expected measurements
                    
                    if calculus_type == "all_>=5":
                        not_enough_cells = not_enough_cells or np.min(expected) < 5
                    elif calculus_type == "only_>=5":
                        dof = dof - dof_tx * (np.min(expected)<5)

                    if (not not_enough_cells and calculus_type != "only_>=5") or (calculus_type == "only_>=5" and np.min(expected)>=5):
                        expected = expected.ravel()
                        data = np.array(data).ravel()
                        if independence_method == "g2":
                            log_mat = np.log(np.divide(data,expected))
                            log_mat[data == 0] = 0
                            statistic += 2 * np.dot(data,log_mat)

                        if independence_method == "x2":
                            statistic += np.sum(np.divide(np.power(data-expected,2),expected))
            
            if calculus_type == "only_>=5":
                not_enough_cells = dof == 0

            if not_enough_cells:
                p_val = -1
                cor = -1
                lb = -1
            else:
                p_val = 1 - chi2.cdf(x=statistic, df = dof ) # degrees of freedom = (n_uniques T -1) * (n_uniques X -1) * product(n_uniques var_in_Z)
                cor = statistic
                lb = dof

            a_row = pd.DataFrame([[t,x,z_str, p_val,cor,lb]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
                
            dep_infos.save_counter += 1
    else:
        raise Error("CALCULUS OF SUMMAND AD HOC NOT IMPLEMENTED YET")

def calculate_dep_fcit(dep_infos, t, x, z_str, z):
    method = dep_infos.method
    independence_method = dep_infos.independence_method
    data_file = dep_infos.data_file
    df = dep_infos.df
    indep_file = dep_infos.indep_file
    save_every_update = dep_infos.save_every_update
    calculus_type = dep_infos.calculus_type

    if len(z) == 0:
        z = ["()"]
    else:
        z = [''.join(xs for xs in str(zz) if xs.isdigit() or xs.isalpha() or xs =="."  or xs =="/") for zz in z]  # selecting only letters and numbers
    if not "summand" in method:
        if data_file != "" and independence_method!="": 
            selection_cols = [t,x]
            conditioning_cols = z

            df_data = pd.read_csv(data_file)
            t_col = np.reshape(df_data[t].values,(-1,1))
            x_col = np.reshape(df_data[x].values,(-1,1))
            try:
                if z != ["()"]:
                    z_cols = np.reshape(df_data[z].values,(-1,len(z)))
                    p_val = fcit.test(t_col, x_col, z_cols)
                else:
                    p_val = fcit.test(t_col, x_col
                    )
            except:
                print("eccezione ",t,x,z)
                raise Exception
            cor = -1
            lb = -1

            a_row = pd.DataFrame([[t,x,z_str, p_val,cor,lb]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
                
            dep_infos.save_counter += 1
    else:
        raise Error("CALCULUS OF SUMMAND AD HOC NOT IMPLEMENTED YET")

def calculate_summands_score(dep_infos, t, x, z_str, z, delta):
    df = dep_infos.df
    indep_file = dep_infos.indep_file
    save_every_update = dep_infos.save_every_update
    sigma_rad = dep_infos.sigma_rad
    n_rad = dep_infos.n_rad
    z_rad = dep_infos.z_rad
    c_rad = dep_infos.c_rad

    if len(z) == 0:
        z = ["()"]
    else:
        z = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
    
    ##### to check  
    scores = summands_lr_fitter.get_IT_score(dep_infos,t,x,z, variable_list = dep_infos.data_df_columns) 
    bound = summands_bounds_calculus.calculate_single_bound(scores,n_rad,sigma_rad,delta,z_rad,c_rad)

    value = np.mean(scores)
    stat = -1
    lb = np.mean(scores)-bound
    ##### to check 
       
    a_row = pd.DataFrame([[t,x,z_str, value,stat,lb]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    dep_infos.df = pd.concat([df, a_row])
    if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
        save_IT_dataframe(dep_infos)
        dep_infos.save_counter = 0
        
    dep_infos.save_counter += 1



def save_IT_dataframe(dep_infos):
    dep_infos.df.to_csv(dep_infos.indep_file,sep = ";")

def calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z):
    df = dep_infos.df
    indep_file = dep_infos.indep_file
    save_every_update = dep_infos.save_every_update

    if len(z) == 0:
        z = ["()"]
    else:
        z = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
    
    res = get_dseparation(x,t,z, "alarm")

    p_val = 1 * res
    cor = -1
    lb = -1
  
    a_row = pd.DataFrame([[t,x,z_str, p_val,cor,lb]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    dep_infos.df = pd.concat([df, a_row])
    if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
        save_IT_dataframe(dep_infos)
        dep_infos.save_counter = 0
    dep_infos.save_counter += 1

def get_mb(v, dag_struc):
    r_code = '''
        library(bnlearn)
        calculate_mb <- function(v, dag_struc){
            dag = model2network(dag_struc)
            return <- mb(dag,v)
        }
    '''
    testy = STAP(r_code, "testy")
    #Invoking the R function and getting the result

    if "[" not in dag_struc:
        
        dag_struc =dependencyInfos.initialize_dag_struc(dag_struc)
    mb = testy.calculate_mb(v,dag_struc)
    return [el for el in mb]

def get_el_dist_le_2(v, dag_struc):
    r_code = '''
        library(bnlearn)
        calculate_el_dist_le_2 <- function(v,dag_struc){
            dag = model2network(dag_struc)

            pc <- nbr(dag, v)
            dist_2 <-  pc
            for (el in pc){
                dist_2 <- append(dist_2,nbr(dag, el))
            }
            return <- dist_2
        }
    '''
    testy = STAP(r_code, "testy")
    if "[" not in dag_struc:
        
        dag_struc =dependencyInfos.initialize_dag_struc(dag_struc)
    #Invoking the R function and getting the result
    dist_le_2 = testy.calculate_el_dist_le_2(v,dag_struc)
    return sorted(set([el for el in dist_le_2 if el!=v]))

def get_pc(v, dag_struc):
    r_code = '''
        library(bnlearn)
        calculate_pc <- function(v, dag_struc){
            dag = model2network(dag_struc)

            pc <- nbr(dag, v)
            return <- pc
        }
    '''
    testy = STAP(r_code, "testy")
    if "[" not in dag_struc:
        
        dag_struc =dependencyInfos.initialize_dag_struc(dag_struc)
    #Invoking the R function and getting the result
    dist_le_2 = testy.calculate_pc(v,dag_struc)
    return sorted(set([el for el in dist_le_2]))

def get_parents(v, dag_struc):
    r_code = '''
        library(bnlearn)
        calculate_parents <- function(v, dag_struc){
            dag = model2network(dag_struc)

            pa <- parents(dag, v)
            return <- pa
        }
    '''
    testy = STAP(r_code, "testy")
    if "[" not in dag_struc:
        
        dag_struc =dependencyInfos.initialize_dag_struc(dag_struc)
    #Invoking the R function and getting the result
    parents = testy.calculate_parents(v,dag_struc)
    return sorted(set([el for el in parents]))

def is_descendant(x,y, dag_struc):
    r_code = '''
        library(bnlearn)
        calculate_is_descendant <- function(x,y, dag_struc){
            dag = model2network(dag_struc)
            
            return <- x %in% descendants(dag, y)
        }
    '''
    testy = STAP(r_code, "testy")
    if "[" not in dag_struc:
        
        dag_struc =dependencyInfos.initialize_dag_struc(dag_struc)
    #Invoking the R function and getting the result
    is_desc = bool(testy.calculate_is_descendant(x,y,dag_struc)[0])
    return is_desc

def is_in_pcd(x,y,dag_struc):
    return x in get_pc(y,dag_struc) or is_descendant(x,y, dag_struc)

def get_dseparation(v,t,z, dag_struc):
    r_code = '''
        library(bnlearn)
        get_dsep <- function(v,t,z, dag_struc){
            dag = model2network(dag_struc)
            if (z==""){
                return <- dsep(dag, v, t)
            }else{
                return <- dsep(dag, v, t, z)
            }
        }
    '''
    testy = STAP(r_code, "testy")
    #Invoking the R function and getting the result
    if z == ["()"]:
        z = ""
    else:
        z = robjects.StrVector([el for el in z])
    if "[" not in dag_struc:
        
        dag_struc =dependencyInfos.initialize_dag_struc(dag_struc)
    dsep = testy.get_dsep(v,t,z, dag_struc)
    return dsep[0]

def init_dep_infos_summands(dep_infos):
    dep_infos.lookout_scores_per_model = {"()" : 0} 

    df_data = pd.read_csv(dep_infos.data_file)
    tr_els = int(df_data.shape[0]*0.5)
    dep_infos.training_dataset = df_data.values[:tr_els]
    dep_infos.test_dataset = df_data.values[tr_els:]
    dep_infos.data_df_columns = df_data.columns.tolist()

    dep_infos.n_rad = 1000
    dep_infos.m_rad = dep_infos.test_dataset.shape[0]

    sigma = np.random.rand(dep_infos.n_rad,dep_infos.m_rad) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 

    dep_infos.sigma_rad = sigma
    dep_infos.min_val = 0
    dep_infos.z_rad = max(1,abs(1-dep_infos.min_val))
    dep_infos.c_rad = abs(1 - abs(1-dep_infos.min_val))

    dep_infos.summands_initialized = True