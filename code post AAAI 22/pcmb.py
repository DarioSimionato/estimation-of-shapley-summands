from operator import index

from pandas.io.parsers import read_csv
from dependency_infos import dependencyInfos
import os
import pandas as pd
import numpy as np
import multiprocess as mp
import itertools
from itertools import chain, combinations, repeat
import argparse
from datetime import datetime
import warnings
from tqdm import tqdm
import math
#warnings.filterwarnings('ignore')
from threadpoolctl import threadpool_limits
import subprocess
import IT_utils
import pickle
import rad_utils

def get_pcd(dep_infos,t,v, delta, track_ITs = False, ordered_ITs = False, verbose = False, max_z_size = -1):
    if t in dep_infos.lookout_PCD:
        return dep_infos.lookout_PCD[t]
    # V set of variables, T target
    pcd = set()
    can_pcd = v.copy()
    can_pcd.remove(t)

    if track_ITs:
        ITs = set()
        
    if verbose:
        print("+++ GetPCD(",t,")","V=",v)
        print()

    pcd_changed = True

    while pcd_changed:
        old_pcd = pcd.copy()
        if verbose:
            print("        Begin of iteration", "canPCD:",can_pcd,"PCD:", pcd)

        # remove false positives from can_pcd
        sep = dict()
        for x in can_pcd:
            # using a function to avoid computation of p values for superset of conditioning sets with not enoguh elements in it
            sep[x] = IT_utils.get_argmin_dep(dep_infos, t,x, pcd, delta, max_z_size = max_z_size)

        to_remove = []
        for x in can_pcd:
            if IT_utils.independence(dep_infos,t,x,sep[x], delta = delta, verbose = bool(int(verbose/10)), level = 2):
                if verbose:
                    print("        Removing",x)
                to_remove.append(x)
                dep_infos.lookout_independence[(t,x)] = sep[x]
            if track_ITs:
                ITs.add(IT_utils.independence(dep_infos,t,x,sep[x], delta = delta, return_string = True, ordered = ordered_ITs)[1])

        for x in to_remove: #cannot do it all in once since there are runtime errors
            can_pcd.remove(x)

        if verbose:
            print("        Adding an element to PCD", can_pcd, pcd)

        if len(can_pcd) > 0:  # I might remove all elements from can_pcd
            # add the best candidate to pcd
            dependencies = [(x, IT_utils.association(dep_infos,t,x,sep[x], delta)) for x in can_pcd]
            dependencies = sorted(dependencies, key = lambda element : element[1], reverse = True)
            y = dependencies[0][0]  # take the argmax
            # print("- added to PCD",y)
            pcd.add(y)
            can_pcd.remove(y)

        if verbose:
            print("        Removing false positives from PCD", can_pcd, pcd)    
        
        # remove false positives from pcd
        for x in pcd:
            sep[x] = IT_utils.get_argmin_dep(dep_infos, t,x, pcd, delta, max_z_size = max_z_size)
            
        remove_el = []
        for x in pcd:
            if IT_utils.independence(dep_infos,t,x,sep[x], delta = delta, verbose = bool(int(verbose/10)), level = 2):
                if verbose:
                    print("        Removing",x)
                remove_el.append(x) # to solve runtime errors
                dep_infos.lookout_independence[(t,x)] = sep[x]
            if track_ITs:
                ITs.add(IT_utils.independence(dep_infos,t,x,sep[x], delta = delta, return_string = True, ordered = ordered_ITs)[1])

        for x in remove_el:
            pcd.remove(x)
        
        if verbose:
            print("        End of iteration", "canPCD:",can_pcd,"PCD:", pcd)
            print()
        pcd_changed = not old_pcd == pcd

    if verbose:
        print("--- GetPCD(",t,")","Result=",pcd)
    dep_infos.lookout_PCD[t] = pcd
    if track_ITs:
        return pcd, ITs
    return pcd

def get_pc(dep_infos,t, v, delta, track_ITs = False, ordered_ITs = False, verbose = False, max_z_size = -1):
    if t in dep_infos.lookout_PC:
        return dep_infos.lookout_PC[t]
    pc = set()
    ITs = set()
    if track_ITs:
        ITs = ITs.union(get_pcd(dep_infos,t,v, delta, track_ITs, ordered_ITs, max_z_size = max_z_size)[1])
    if verbose:
        print("+++ GetPC(",t,")","V=",v)
        print()

    for x in get_pcd(dep_infos,t,v, delta, verbose = int(verbose/10) * True, max_z_size = max_z_size):
        if t in get_pcd(dep_infos,x,v, delta, verbose = int(verbose/10) * True, max_z_size = max_z_size):
            pc.add(x)
            if verbose:
                print("   ",x,"added to PC(",t,")")

        if track_ITs:
            ITs = ITs.union(get_pcd(dep_infos,x,v, delta, track_ITs, ordered_ITs, max_z_size = max_z_size)[1])

    if verbose:
        print()
        print("--- GetPC(",t,")","Result=",pc)

    dep_infos.lookout_PC[t] = pc
    if track_ITs:
        return pc, ITs
    return pc

def pcmb(dep_infos,t, v, delta, track_ITs = False, ordered_ITs = False, test_with_all_els = False, verbose = False, max_z_size = -1):

    if t in dep_infos.lookout_MB[test_with_all_els]:
        return dep_infos.lookout_MB[test_with_all_els][t]

    if verbose:
        print("+++ PCMB(",t,")","V=",v)
        print()

    # add true positives to MB
    pc = get_pc(dep_infos,t, v,  delta, verbose = int(verbose/10) * True, max_z_size = max_z_size)
    mb = pc.copy()

    ITs = set()
    if track_ITs:
        ITs = ITs.union(get_pc(dep_infos,t,v, delta, track_ITs, ordered_ITs)[1])

    # add more true positives to MB
    for y in pc:
        if track_ITs:
            ITs = ITs.union(get_pc(dep_infos,y,v, delta, track_ITs, ordered_ITs)[1])

        for x in get_pc(dep_infos,y,v, delta, verbose = int(verbose/10) * True, max_z_size = max_z_size):
            if not x in pc and x!=t:
                if not test_with_all_els:
                    # find z such that indep(t,x|z) and t,x not in z
                    # zz = None
                    # for z in IT_utils.powerset(v):
                    #     # makes sense to find the independence set from the one I already calculated as suggested in the paper page 10 before th 7
                    #     if x not in z and t not in z and IT_utils.is_calculated(dep_infos,t,x,z) and zz is None:    
                    #         if IT_utils.independence(dep_infos,t,x,z, delta = delta, verbose = verbose):
                    #             if verbose*True:
                    #                 print(z,"d separates",t,"and",x," (I am considering element",y," of PC)")
                    #             zz = z
                    #             break
                    #         if track_ITs:
                    #             ITs.add(IT_utils.independence(dep_infos,t,x,z, delta = delta, return_string = True, ordered = ordered_ITs)[1])
                    # if zz is None:  # might be in case for which X in PCD(T) but not vice versa therefore I didnt calculated Z that d-separes T by X but the one that did X by T
                    #     for z in IT_utils.powerset(v):
                    #         # makes sense to find the independence set from the one I already calculated as suggested in the paper page 10 before th 7
                    #         if x not in z and t not in z and IT_utils.is_calculated(dep_infos,x,t,z) and zz is None:    
                    #             if IT_utils.independence(dep_infos,x,t,z, delta = delta, verbose = verbose):
                    #                 if verbose*True:
                    #                     print(z,"d separates",t,"and",x," (I am considering element",y," of PC)")
                    #                 zz = z
                    #                 break
                    #             if track_ITs:
                    #                 ITs.add(IT_utils.independence(dep_infos,x,t,z, delta = delta, return_string = True, ordered = ordered_ITs)[1])
                    # if zz is None:   
                    #     print("t",t,"x",x,"y",y)
                    #     print(t,":","PCD = ",dep_infos.lookout_PCD[t])
                    #     print(" "," ","PC = ",dep_infos.lookout_PC[t])
                    #     print(y,":","PCD = ",dep_infos.lookout_PCD[y])
                    #     print(" "," ","PC = ",dep_infos.lookout_PC[y])
                    #     print(x,":","PCD = ",dep_infos.lookout_PCD[x])
                    #     print(" "," ","PC = ",dep_infos.lookout_PC.get(x,"None"))
                    #     for z in IT_utils.powerset(v):
                    #         if (IT_utils.is_calculated(dep_infos,x,t,z) or IT_utils.is_calculated(dep_infos,t,x,z)) and IT_utils.independence(dep_infos,x,t,z, delta = delta) != IT_utils.independence(dep_infos,t,x,z, delta = delta):
                    #             print("Different results with z",z)
                    #             IT_utils.independence(dep_infos,t,x,z, delta = delta,verbose=True)
                    #             IT_utils.independence(dep_infos,x,t,z, delta = delta,verbose=True)
                    #     print()

                    if (t,x) in dep_infos.lookout_independence:
                        z = dep_infos.lookout_independence[(t,x)]
                    else:
                        z = dep_infos.lookout_independence[(x,t)]

                    z_and_y = set(list(z))
                    z_and_y.add(y)
                    if not IT_utils.independence(dep_infos,t,x,z_and_y,   delta = delta):
                        mb.add(x)
                        if verbose*True:
                            print(x,"added to MB (I am considering element",y," of PC)")
                    else:
                        if verbose*True:
                            print(x,"NOT added to MB (I am considering element",y," of PC)")
                    if track_ITs:
                        ITs.add(IT_utils.independence(dep_infos,t,x,z_and_y,   delta = delta, return_string = True, ordered = ordered_ITs)[1])
                else:
                    z_and_y = set([va for va in v if va!=x and va!=t])
                    # print(v)
                    # print(z_and_y)
                    if not IT_utils.independence(dep_infos,t,x,z_and_y, delta = delta):
                        mb.add(x)
    
    if verbose*True:
        print()
        print("--- PCMB(",t,")","Result=",mb)
    
    dep_infos.lookout_MB[test_with_all_els][t] = mb
    if track_ITs:
        return mb, ITs
    return mb

def mod_pcd(dep_infos,t, v, delta, N, track_ITs = False, ordered_ITs = False, verbose = False, max_z_size = -1):
    if t in dep_infos.lookout_Mod_PCD:
        return dep_infos.lookout_Mod_PCD[t]
    pcd = set()
    can_pcd = v.copy()
    can_pcd.remove(t)
    no_changes = False
    while not no_changes:   # incremental phase using delta
        pcd_copy = pcd.copy()

        sep = dict()
        for x in can_pcd:
            # using a function to avoid computation of p values for superset of conditioning sets with not enoguh elements in it
            sep[x] = IT_utils.get_argmin_dep(dep_infos, t,x, pcd, delta,max_z_size)

        to_remove = []
        for x in can_pcd:
            if IT_utils.independence(dep_infos,t,x,sep[x], delta = delta, verbose = bool(int(verbose/10)), level = 2):
                if verbose:
                    print("        Removing",x)
                to_remove.append(x)
                dep_infos.lookout_independence[(t,x)] = sep[x]

        for x in to_remove: #cannot do it all in once since there are runtime errors
            can_pcd.remove(x)
        
        if len(can_pcd) > 0:  # I might remove all elements from can_pcd
            # add the best candidate to pcd
            dependencies = [(x, IT_utils.association(dep_infos,t,x,sep[x], delta)) for x in can_pcd]
            dependencies = sorted(dependencies, key = lambda element : element[1], reverse = True)
            y = dependencies[0][0]  # take the argmax
            # print("- added to PCD",y)
            pcd.add(y)
            can_pcd.remove(y)

        no_changes = pcd_copy == pcd
    if verbose:
        print("        After additions",pcd)
    # remove false positives from pcd using delta/N
    for x in pcd:
        sep[x] = IT_utils.get_argmin_dep(dep_infos, t,x, pcd, delta/N,max_z_size)
        
    remove_el = []
    for x in pcd:      #  removal phase using delta/N
        if IT_utils.independence(dep_infos,t,x,sep[x], delta = delta/N, verbose = bool(int(verbose/10)), level = 2):
            if verbose:
                print("        Removing",x,sep[x])
            remove_el.append(x) # to solve runtime errors
            dep_infos.lookout_independence[(t,x)] = sep[x]
    
    for x in remove_el:
        pcd.remove(x)
        
    dep_infos.lookout_Mod_PCD[t] = pcd
    
    if verbose:
        print("        After cleanup",pcd)
    return pcd

def mod_pc(dep_infos,t, v, delta, N, track_ITs = False, ordered_ITs = False, verbose = False,max_z_size = -1):
    if t in dep_infos.lookout_Mod_PC:
        return dep_infos.lookout_Mod_PC[t]
    pc = set()
    ITs = set()
    if track_ITs:
        ITs = ITs.union(mod_pcd(dep_infos,t,v, delta, N, track_ITs, ordered_ITs, max_z_size = max_z_size)[1])
    if verbose:
        print("+++ Mod_PC(",t,")","V=",v)
        print()

    for x in mod_pcd(dep_infos,t,v, delta, N, verbose = int(verbose/10) * True, max_z_size = max_z_size):
        if t in mod_pcd(dep_infos,x,v, delta, N, verbose = int(verbose/10) * True, max_z_size = max_z_size):
            pc.add(x)
            if verbose:
                print("   ",x,"added to PC(",t,")")

        if track_ITs:
            ITs = ITs.union(mod_pcd(dep_infos,x,v, delta, N, track_ITs, ordered_ITs, max_z_size= max_z_size)[1])

    if verbose:
        print()
        print("--- Mod_PC(",t,")","Result=",pc)

    dep_infos.lookout_Mod_PC[t] = pc
    if track_ITs:
        return pc, ITs
    return pc

def pcmb_mod(dep_infos,t, v, delta, N, track_ITs = False, ordered_ITs = False, test_with_all_els = False, verbose = False, max_z_size = -1):

    if t in dep_infos.lookout_MB_MOD[test_with_all_els]:
        return dep_infos.lookout_MB_MOD[test_with_all_els][t]

    if verbose:
        print("+++ PCMB(",t,")","V=",v)
        print()

    # add true positives to MB
    pc = mod_pc(dep_infos,t, v,  delta, N, verbose = int(verbose/10) * True, max_z_size = max_z_size)
    mb = pc.copy()

    ITs = set()
    if track_ITs:
        ITs = ITs.union(mod_pc(dep_infos,t,v, delta, N, track_ITs, ordered_ITs, max_z_size = max_z_size)[1])

    # add more true positives to MB
    for y in pc:
        if track_ITs:
            ITs = ITs.union(mod_pc(dep_infos,y,v, delta, N, track_ITs, ordered_ITs, max_z_size = max_z_size)[1])

        for x in mod_pc(dep_infos,y,v, delta, N, verbose = int(verbose/10) * True, max_z_size = max_z_size):
            if not x in pc and x!=t:
                if not test_with_all_els:
                    # zz = None
                    # # find z such that indep(t,x|z) and t,x not in z
                    # for z in IT_utils.powerset(v):
                    #     # makes sense to find the independence set from the one I already calculated as suggested in the paper page 10 before th 7
                    #     if x not in z and t not in z and IT_utils.is_calculated(dep_infos,t,x,z) and zz is None:    
                    #         if IT_utils.independence(dep_infos,t,x,z, delta = delta/N, verbose = verbose):
                    #             if verbose*True:
                    #                 print(z,"d separates",t,"and",x," (I am considering element",y," of PC)")
                    #             zz = z
                    #             break
                    #         if track_ITs:
                    #             ITs.add(IT_utils.independence(dep_infos,t,x,z, delta = delta/N, return_string = True, ordered = ordered_ITs)[1])
                    # if zz is None:  # might be in case for which X in PCD(T) but not vice versa therefore I didnt calculated Z that d-separes T by X but the one that did X by T
                    #     for z in IT_utils.powerset(v):
                    #         # makes sense to find the independence set from the one I already calculated as suggested in the paper page 10 before th 7
                    #         if x not in z and t not in z and IT_utils.is_calculated(dep_infos,x,t,z) and zz is None:    
                    #             if IT_utils.independence(dep_infos,x,t,z, delta = delta/N, verbose = verbose):
                    #                 if verbose*True:
                    #                     print(z,"d separates",t,"and",x," (I am considering element",y," of PC)")
                    #                 zz = z
                    #                 break
                    #             if track_ITs:
                    #                 ITs.add(IT_utils.independence(dep_infos,x,t,z, delta = delta/N, return_string = True, ordered = ordered_ITs)[1])
                    # if zz is None:  
                    #     print("t",t,"x",x,"y",y)
                    #     print(t,":","Mod_PCD = ",dep_infos.lookout_Mod_PCD[t])
                    #     print(" "," ","Mod_PC = ",dep_infos.lookout_Mod_PC[t])
                    #     print(y,":","Mod_PCD = ",dep_infos.lookout_Mod_PCD[y])
                    #     print(" "," ","Mod_PC = ",dep_infos.lookout_Mod_PC[y])
                    #     print(x,":","Mod_PCD = ",dep_infos.lookout_Mod_PCD[x])
                    #     print(" "," ","Mod_PC = ",dep_infos.lookout_Mod_PC.get(x,"None"))
                    #     for z in IT_utils.powerset(v):
                    #         if (IT_utils.is_calculated(dep_infos,x,t,z) or IT_utils.is_calculated(dep_infos,t,x,z)) and IT_utils.independence(dep_infos,x,t,z, delta = delta/N) != IT_utils.independence(dep_infos,t,x,z, delta = delta/N):
                    #             print("Different results with z",z)
                    #             IT_utils.independence(dep_infos,t,x,z, delta = delta/N,verbose=True)
                    #             IT_utils.independence(dep_infos,x,t,z, delta = delta/N,verbose=True)
                    #     print()

                    if (t,x) in dep_infos.lookout_independence:
                        z = dep_infos.lookout_independence[(t,x)]
                    else:
                        z = dep_infos.lookout_independence[(x,t)]

                    z_and_y = set(list(z))
                    z_and_y.add(y)

                    if not IT_utils.independence(dep_infos,t,x,z_and_y, delta = delta/N):
                        mb.add(x)
                        if verbose*True:
                            print(x,"added to MB (I am considering element",y," of PC)")
                    else:
                        if verbose*True:
                            print(x,"NOT added to MB (I am considering element",y," of PC)")
                    if track_ITs:
                        ITs.add(IT_utils.independence(dep_infos,t,x,z_and_y,delta = delta/N, return_string = True, ordered = ordered_ITs)[1])
                else:
                    z_and_y = set([va for va in v if va!=x and va!=t])
                    # print(v)
                    # print(z_and_y)
                    if not IT_utils.independence(dep_infos,t,x,z_and_y, delta = delta/N):
                        mb.add(x)
    
    if verbose*True:
        print()
        print("--- PCMB(",t,")","Result=",mb)
    
    dep_infos.lookout_MB_MOD[test_with_all_els][t] = mb
    if track_ITs:
        return mb, ITs
    return mb

def iamb(dep_infos,t, v, delta, track_ITs = False, ordered_ITs = False, test_with_all_els = False, verbose = False, max_z_size = -1):
    mb = []
    no_changes = False
    while not no_changes:
        mb_old = mb.copy()
        y = sorted([(IT_utils.association(dep_infos, t, vv, set(mb)),vv) for vv in v if t != vv and vv not in mb], key = lambda x: x[0], reverse= True)[0][1]
        if not IT_utils.independence(dep_infos,t,y,set(mb),delta):
            mb.append(y)
        no_changes = mb_old == mb
    updated_mb = mb.copy() # cannot modify mb since it is on cycle term
    for x in mb:
        mb_setminus_x = set([vv for vv in updated_mb if vv!=x])
        if IT_utils.independence(dep_infos, t,x,mb_setminus_x, delta):
            updated_mb = [vv for vv in updated_mb if vv!=x]
    return updated_mb

def pc_def(dep_infos,t, v, delta, N):
    if t in dep_infos.lookout_PC_def:
        return dep_infos.lookout_PC_def[t]
    pc = []
    for x in v:
        if x != t:
            els_cond_set = [z for z in v if z not in [t,x]]
            z = IT_utils.get_argmin_dep(dep_infos, t,x, els_cond_set, delta/N, max_z_size = len(v)-2)
            if not IT_utils.independence(dep_infos,t,x,z, delta/N):
                pc.append(x)
    dep_infos.lookout_PC_def[t] = pc
    return pc

def mb_algo(dep_infos,t, v, delta, N):
    if t in dep_infos.lookout_MB_algo:
        return dep_infos.lookout_MB_algo[t]

    pc = pc_def(dep_infos,t, v,delta,N)
    mb = pc.copy()

    for y in pc:
        for x in pc_def(dep_infos,y,v, delta, N):
            if not x in mb and x!=t:
                z_and_y = set([va for va in v if va!=x and va!=t])
                if not IT_utils.independence(dep_infos,t,x,z_and_y, delta = delta/N):
                    mb.append(x)
    dep_infos.lookout_MB_algo[t] = mb
    return mb


# np.random.seed(120395)
parser = argparse.ArgumentParser('main') 
parser.add_argument('-b', '--valid_size', type=int, default= 100000)
parser.add_argument('-c', '--n_cores', type=int, default= 72)
parser.add_argument('-d', '--delta_input', type=float, default= 0.05)
parser.add_argument('-t', '--test_number', type=int, default= 0)
parser.add_argument('-i', '--input_test_number', type=int, default= 0)
parser.add_argument('-n', '--n_variables', type=int, default= 0)
parser.add_argument('-f', '--file', type=str, default= "")
parser.add_argument('-v', '--verbose', type=int, default= 0)
# parser.add_argument('-v', '--verbose', type=bool, default= False)
parser.add_argument('-z', '--zero_init_t', type=bool, default= False)

args = parser.parse_args()
 
valid_size = args.valid_size
input_test_number = args.input_test_number
test_number = args.test_number
n_vars = args.n_variables
verbose = args.verbose
file = args.file
zero_init_t = args.zero_init_t
n_cores = args.n_cores
delta_input = args.delta_input

print("Parameters =",args)

# TEST 1 = initial test of PCMB correctness
if test_number == 1:
    v_set = set(["A","B","C","S","T"])

    in_path = "corrected_tests.csv"
    df = pd.read_csv("corrected_tests.csv")
    dep_infos = dependencyInfos(df,data_file = in_path)

    print("A dep  T | S  ", IT_utils.association(dep_infos,"T","A","S"," "),IT_utils.independence(dep_infos,"T","A","S"," ",0.05))
    print("S dep  T | A  ", IT_utils.association(dep_infos,"T","S","A"," "),IT_utils.independence(dep_infos,"T","S","A"," ",0.05))
    print("S perp T | ABC", IT_utils.association(dep_infos,"T","S","ABC"," "),IT_utils.independence(dep_infos,"T","S","ABC"," ",0.05))
    print()
    print("PCD  call:",get_pcd(dep_infos,t = "T",v = v_set, delta = 0.05))
    print("PC   call:",get_pc(dep_infos,t = "T",v = v_set, delta = 0.05))
    print("PCMB call:",pcmb(dep_infos,t = "T",v = v_set, delta = 0.05))


    print()
    print()

    in_path = "results_test.csv"
    df = pd.read_csv("results_test.csv")
    dep_infos = dependencyInfos(df,data_file = in_path)

    print("A dep  T | S  ", IT_utils.association(dep_infos,"T","A","S"," "),IT_utils.independence(dep_infos,"T","A","S"," ",0.05))
    print("S dep  T | A  ", IT_utils.association(dep_infos,"T","S","A"," "),IT_utils.independence(dep_infos,"T","S","A"," ",0.05))
    print("S perp T | ABC", IT_utils.association(dep_infos,"T","S","ABC"," "),IT_utils.independence(dep_infos,"T","S","ABC"," ",0.05))
    print()
    print("PCD  call:",get_pcd(dep_infos,t = "T",v = v_set, delta = 0.05))
    print("PC   call:",get_pc(dep_infos,t = "T",v = v_set, delta = 0.05))
    print("PCMB call:",pcmb(dep_infos,t = "T",v = v_set, delta = 0.05))
# TEST 2 = first tests with Gaussian data of bnlearn library
if test_number == 2:

    v_set = set(["A","B","C","D","E","F","G"])

    N = len(v_set)*(len(v_set)-1)*2^(len(v_set)-3)

    df = pd.read_csv("IT_results/gauss_test_cor.csv")
    in_path = "IT_results/gauss_test_cor.csv"
    dep_infos = dependencyInfos(df,data_file = in_path)

    for v in "ABCDEFG":
        print(v)
        print("PCD  call:",get_pcd(dep_infos,t = v,v = v_set, delta = 0.05))
        print("PC   call:",get_pc(dep_infos,t = v,v = v_set, delta = 0.05))
        print("PCMB call:",pcmb(dep_infos,t = v,v = v_set, delta = 0.05))
        print()

    print()
    print()

    for v in "ABCDEFG":
        print(v)
        print("PCMB call:",pcmb(dep_infos,t = v,v = v_set, delta = 0.05))
        print("PCMB call:",pcmb(dep_infos,t = v,v = v_set, delta = 0.05/N))
        print()
# TEST 3 = test with repetition of structure by changing the number of repetitions
if test_number == 3:
    r = [1,2]
    s = [10,25,50,100,150,200,250,300,400,500,
        750,1000,2500,5000,10000,25000,50000,75000,100000]
    test_type = 'mi-g'  # cor, mi-g

    for zi in (True, False):
        print("ZI = ", zi)
        for r_sel in r:
            tot_list = []

            v_set = list(["A1","B1","C1"])
            for i in range(1,r_sel):
                v_set.append("A"+str(i+1))
                v_set.append("B"+str(i+1))
                v_set.append("C"+str(i+1))
            v_set.append("T")
            v_set = sorted(set(v_set))
            # print(v_set)
            # print([l for l in IT_utils.powerset(v_set)])

            N = (len(v_set)-1)^2 + (len(v_set)-1)*(len(v_set))^3

            print("R:", r_sel)
            
            if zi:
                paths = ["IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+"ZI.csv" for s_sel in s]
            else:
                paths = ["IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+".csv" for s_sel in s]
            if zi:
                dfs = [pd.read_csv("IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+"ZI.csv") for s_sel in s]
            else:
                dfs = [pd.read_csv("IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+".csv") for s_sel in s]
            
            zs = list(IT_utils.powerset(v_set))
            # print(v_set, len(zs))

            for v in v_set:
                for vv in v_set:
                    for z in sorted(zs):
                        if v!=vv and not v in z and not vv in z:
                            tot_list.append(v)
                            tot_list.append(vv)
                            tot_list.append(IT_utils.convert_to_string(z))
                            for d in dfs:
                                df = d
                                in_path = paths[dfs.index(d)]
                                dep_infos = dependencyInfos(df,data_file = in_path, independence_method=test_type)
                                tot_list.append(str(IT_utils.independence(dep_infos,v,vv,z, delta= 0.05)).upper())
                                tot_list.append(str(IT_utils.independence(dep_infos,v,vv,z, delta= 0.05/N)).upper())
            
            # print(tot_list)
            tot_list = np.reshape(tot_list,(-1,3+2*len(s)))
            # print(tot_list)
            cols = ["first","second","cond_set"]
            for s_sel in s:
                cols.append(str(s_sel)+" 0.05")
                cols.append(str(s_sel)+" corr")
            tot_df = pd.DataFrame(tot_list, columns=cols)
            if zi:
                pd.DataFrame.to_csv(tot_df,test_type+"_single_independencies_R"+str(r_sel)+"_ZI.csv", sep=";")
            else:
                pd.DataFrame.to_csv(tot_df,test_type+"_single_independencies_R"+str(r_sel)+".csv", sep=";")
# TEST 4 = test with repetition of structure by correcting for MHT or not
if test_number == 4:
    r = [1,2]
    s = [10,25,50,100,150,200,250,300,400,500,
        750,1000,2500,5000,10000,25000,50000,75000,100000]
    test_type = 'mi-g'  # cor, mi-g

    for zi in (True, False):
        print("ZI = ", zi)
        for r_sel in r:

            v_set = list(["A1","B1","C1"])
            for i in range(1,r_sel):
                v_set.append("A"+str(i+1))
                v_set.append("B"+str(i+1))
                v_set.append("C"+str(i+1))
            v_set.append("T")
            v_set = sorted(set(v_set))
            # print(v_set)
            # print([l for l in IT_utils.powerset(v_set)])

            N = (len(v_set)-1)^2 + (len(v_set)-1)*(len(v_set))^3

            print("R:", r_sel)
            for s_sel in s:
                if zi:
                    df = pd.read_csv("IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+"ZI.csv")
                    path = "IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+"ZI.csv"
                else:
                    df = pd.read_csv("IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+".csv")
                    path = "IT_results/"+test_type+"_S"+str(s_sel)+"_R"+str(r_sel)+".csv"
                
                in_path = path
                dep_infos = dependencyInfos(df,data_file = in_path, independence_method=test_type)
                print("---",s_sel)
                print("   d=0.05   ", pcmb(dep_infos,"T",v_set, delta= 0.05))
                print("   corrected", pcmb(dep_infos,"T",v_set, delta= 0.05/N))
            print()
            print()
        print()
        print()
# TEST 5 = test counting the number of CI tested - fully connected BN
if test_number == 5:
    v_set = set("ABCDEFGHIJKLMNOPQR"[:n_vars+1])
    v_set.add("T")
    print("Considering ",v_set," of ", len(v_set)-1, " variables (without T)")

    # N = len(v_set)*(len(v_set)-1)*2^(len(v_set)-3)
    if zero_init_t:
        df = pd.read_csv("IT_results/v"+str(input_test_number)+"_S"+str(valid_size)+"_R"+str(n_vars)+"ZI.csv")
        path = "IT_results/v"+str(input_test_number)+"_S"+str(valid_size)+"_R"+str(n_vars)+"ZI.csv"
    else:
        df = pd.read_csv("IT_results/v"+str(input_test_number)+"_S"+str(valid_size)+"_R"+str(n_vars)+".csv")
        path = "IT_results/v"+str(input_test_number)+"_S"+str(valid_size)+"_R"+str(n_vars)+".csv"

    dep_infos = dependencyInfos(df,data_file = path, method = "p-value")
    # pcd, IT = get_pcd(dep_infos,"T",v_set, method = "p-value", delta = 0.05, track_ITs = True, ordered_ITs = False, verbose= verbose)
    # mb, IT = pcmb(dep_infos,"T",v_set, method = "p-value", delta = 0.05, track_ITs = True, ordered_ITs = False, verbose= verbose)

    pcd, IT = get_pcd(dep_infos,"T",v_set,delta = 0.05, track_ITs = True, ordered_ITs = False)
    print("PCD(T)", sorted(pcd), " with ",len(IT),"IT performed")
    print(sorted(IT, key= lambda x: x.replace("!"," ")),sep = ",")
    print()
    pc, IT = get_pc(dep_infos,"T",v_set,delta = 0.05, track_ITs = True, ordered_ITs = False)
    print("PC(T)", sorted(pc), " with ",len(IT),"IT performed")
    print(sorted(IT, key= lambda x: x.replace("!"," ")),sep = ",")
    print()
    mb, IT = pcmb(dep_infos,"T",v_set,delta = 0.05, track_ITs = True, ordered_ITs = False)
    print("MB(T)", sorted(mb), " with ",len(IT),"IT performed")
    print(sorted(IT, key= lambda x: x.replace("!"," ")),sep = ",")

    print()
    print()
    print()
    dep_infos = dependencyInfos(df,data_file = path, method = "statistic")
    pcd, IT = get_pcd(dep_infos,"T",v_set, delta = 0.05, track_ITs = True, ordered_ITs = False)
    print("PCD(T)", sorted(pcd), " with ",len(IT),"IT performed")
    print(sorted(IT, key= lambda x: x.replace("!"," ")),sep = ",")
    print()
    pc, IT = get_pc(dep_infos,"T",v_set, delta = 0.05, track_ITs = True, ordered_ITs = False)
    print("PC(T)", sorted(pc), " with ",len(IT),"IT performed")
    print(sorted(IT, key= lambda x: x.replace("!"," ")),sep = ",")
    print()
    mb, IT = pcmb(dep_infos,"T",v_set, delta = 0.05, track_ITs = True, ordered_ITs = False)
    print("MB(T)", sorted(mb), " with ",len(IT),"IT performed")
    print(sorted(IT, key= lambda x: x.replace("!"," ")),sep = ",")
# TEST 6 = verbose test
if test_number == 6:
    r = 2
    for s in [500]:
        print("DATASET SIZE = ",s)
        test_type = 'cor'  # cor, mi-g
        zi = zero_init_t
    
        tot_list = []

        v_set = list(["A1","B1","C1"])
        for i in range(1,r):
            v_set.append("A"+str(i+1))
            v_set.append("B"+str(i+1))
            v_set.append("C"+str(i+1))
        v_set.append("T")
        v_set = sorted(set(v_set))
        # print(v_set)
        # print([l for l in IT_utils.powerset(v_set)])
        N = (len(v_set)-1)^2 + (len(v_set)-1)*(len(v_set))^3
        
        if zi:
            df = pd.read_csv("IT_results/"+test_type+"_S"+str(s)+"_R"+str(r)+"ZI.csv")
            path = "IT_results/"+test_type+"_S"+str(s)+"_R"+str(r)+"ZI.csv"
        else:
            df = pd.read_csv("IT_results/"+test_type+"_S"+str(s)+"_R"+str(r)+".csv")
            path = "IT_results/"+test_type+"_S"+str(s)+"_R"+str(r)+".csv"
        
        dep_infos = dependencyInfos(df,data_file = path)

        print("WITHOUT CORRECTION")
        pcmb(dep_infos,"T",v_set, delta = 0.05  , track_ITs = True, ordered_ITs = False, verbose= verbose)
        print()
        print("WITH CORRECTION")
        pcmb(dep_infos,"T",v_set, delta = 0.05/N, track_ITs = True, ordered_ITs = False, verbose= verbose)
        print()
        print()
    print(0.05/N)
# TEST 7 = test with repetition on 100 trials extracting PCMB result
if test_number == 7:
    base_path = os.path.join("IT_results","multiple")

    for number_rep_structure in [1,2]:
        for zero_init_t in [True, False]:    
            results = []
            r_sel = number_rep_structure
            v_set = list(["A1","B1","C1"])
            for i in range(1,r_sel):
                v_set.append("A"+str(i+1))
                v_set.append("B"+str(i+1))
                v_set.append("C"+str(i+1))
            v_set.append("T")
            v_set = sorted(set(v_set))

            for test_n in tqdm(range(100)):
                for tot_size in [10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
                    if zero_init_t:
                        df = pd.read_csv(os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+"ZI.csv"))
                        path = os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+"ZI.csv")
                    else:
                        df = pd.read_csv(os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv"))
                        path = os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv")
                    
                    dep_infos = dependencyInfos(df,data_file = path)
                    results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))
            results = np.reshape(results,(100,-1))
            cols = [str(n) for n in [10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]]
            tot_df = pd.DataFrame(results, columns=cols)
            if zero_init_t:
                pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"results_multiple_runs_r"+str(number_rep_structure))+"_ZI.csv", sep=";")
            else:
                pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"results_multiple_runs_r"+str(number_rep_structure))+".csv", sep=";")
# TEST 8 = .csv file with IT_utils.independence tests per trial
if test_number == 8:
    number_rep_structure = n_vars
    tot_size = valid_size
    base_path = os.path.join("IT_results","multiple")

    r_sel = number_rep_structure
    v_set = list(["A1","B1","C1"])
    for i in range(1,r_sel):
        v_set.append("A"+str(i+1))
        v_set.append("B"+str(i+1))
        v_set.append("C"+str(i+1))
    v_set.append("T")
    v_set = sorted(set(v_set))
    zs = list(IT_utils.powerset(v_set))

    tot_list = []
    for v in v_set:
        for vv in v_set:
            for z in sorted(zs):
                if v!=vv and not v in z and not vv in z:
                    tot_list.append(v)
                    tot_list.append(vv)
                    tot_list.append(IT_utils.convert_to_string(z))
    n_row = len(tot_list)
    for test_n in tqdm(range(100)):
        if zero_init_t:
            df = pd.read_csv(os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+"ZI.csv"))
            path = os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+"ZI.csv")
        else:
            df = pd.read_csv(os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv"))
            path = os.path.join(base_path,"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv")

        dep_infos = dependencyInfos(df,data_file = path)
        for v in v_set:
            for vv in v_set:
                for z in sorted(zs):
                    if v!=vv and not v in z and not vv in z:
                        tot_list.append(str(IT_utils.independence(dep_infos,v,vv,z, delta= 0.05)).upper())

    cols = ["first","second","cond_set"]
    for n in range(1,101):
        cols.append("t"+str(n))
    tot_list = np.reshape(tot_list,(-1,n_row)).transpose()

    tot_df = pd.DataFrame(tot_list, columns=cols)
    if zi:
        pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"ITs_multiple_runs_S"+str(tot_size)+"_R"+str(r_sel)+"_ZI.csv"), sep=";")
    else:
        pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"ITs_multiple_runs_S"+str(tot_size)+"_R"+str(r_sel)+".csv"), sep=";")
# TEST 9 = test calculating IT with ad-hoc fashion
if test_number == 9:
    print("Test made manually.")
# TEST 10 = create resume out of multiple test with discrete vars 
if test_number == 10:
    base_path = os.path.join("IT_results","multiple","discrete")

    for test_type in [1,2,3]:  
        results = []
        v_set = list(["A","B"])
        if test_type == 3:
            v_set.append("C")
            v_set.append("D")
        v_set.append("T")
        v_set = sorted(set(v_set))

        for test_n in tqdm(range(100)):
            for tot_size in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
                df = pd.read_csv(os.path.join(base_path,"x2_test_"+str(test_type)+"_s"+str(tot_size)+"_t"+str(test_n)+".csv"), sep=";")
                path = os.path.join(base_path,"x2_test_"+str(test_type)+"_s"+str(tot_size)+"_t"+str(test_n)+".csv")
                
                dep_infos = dependencyInfos(df,data_file = path)
                results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

        results = np.reshape(results,(100,-1))
        cols = [str(n) for n in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]]
        tot_df = pd.DataFrame(results, columns=cols)
        
        pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"discrete_multiple_runs_t"+str(test_type))+".csv", sep=";")
# TEST 11 = verbose of a single file
if test_number == 11:

    df = pd.read_csv(file, sep = ";")
    path = file
    
    v_set = sorted(set(df["first_term"]))
    dep_infos = dependencyInfos(df,data_file = path)

    pcmb(dep_infos,"T",v_set, delta = 0.05, track_ITs = True, ordered_ITs = False, verbose= verbose)
    print()
# TEST 12 = create resume out of multiple test with continuous vars
if test_number == 12:
    base_path = os.path.join("IT_results","multiple")

    results = []
    v_set = list(["A","B","C","D"])
    v_set.append("T")
    v_set = sorted(set(v_set))

    for test_n in tqdm(range(100)):
        for tot_size in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
            df = pd.read_csv(os.path.join(base_path,"fully_t"+str(test_n),"cor_fully_S"+str(tot_size)+".csv"))
            path = os.path.join(base_path,"fully_t"+str(test_n),"cor_fully_S"+str(tot_size)+".csv")
            
            dep_infos = dependencyInfos(df,data_file = path)
            results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

    results = np.reshape(results,(100,-1))
    cols = [str(n) for n in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]]
    tot_df = pd.DataFrame(results, columns=cols)
    
    pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"fully_multiple_runs.csv"), sep=";")
# TEST 13 = create resume out of multiple test number 4 with discrete vars 
if test_number == 13:
    base_path = os.path.join("IT_results","multiple","discrete")

    results = []
    v_set = list(["A","B","C"])
    v_set.append("T")
    v_set = sorted(set(v_set))

    for test_n in tqdm(range(100)):
        for tot_size in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
            df = pd.read_csv(os.path.join(base_path,"x2_test_4"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv"), sep=";")
            path = os.path.join(base_path,"x2_test_4"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv")
            
            dep_infos = dependencyInfos(df,data_file = path)
            results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

    results = np.reshape(results,(100,-1))
    cols = [str(n) for n in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]]
    tot_df = pd.DataFrame(results, columns=cols)
    
    pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"discrete_multiple_runs_t4")+".csv", sep=";")
# TEST 14 = create resume out of multiple test 1-4 with discrete vars adding the analysis on -b elements
if test_number == 14:
    base_path = os.path.join("IT_results","multiple","discrete")
    tot_size = valid_size
    size_list = [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]
    size_list.append(tot_size)

    for test_type in [1,2,3]:  
        results = []
        v_set = list(["A","B"])
        if test_type == 3:
            v_set.append("C")
            v_set.append("D")
        v_set.append("T")
        v_set = sorted(set(v_set))

        for test_n in tqdm(range(100)):
            for tot_size in size_list:
                df = pd.read_csv(os.path.join(base_path,"x2_test_"+str(test_type)+"_s"+str(tot_size)+"_t"+str(test_n)+".csv"), sep=";")
                path = os.path.join(base_path,"x2_test_"+str(test_type)+"_s"+str(tot_size)+"_t"+str(test_n)+".csv")
                
                dep_infos = dependencyInfos(df,data_file = path)
                results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

        results = np.reshape(results,(100,-1))
        cols = [str(n) for n in size_list]
        tot_df = pd.DataFrame(results, columns=cols)
        
        pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"discrete_multiple_runs_t"+str(test_type))+".csv", sep=";")

    results = []
    v_set = list(["A","B","C"])
    v_set.append("T")
    v_set = sorted(set(v_set))

    for test_n in tqdm(range(100)):
        for tot_size in size_list:
            df = pd.read_csv(os.path.join(base_path,"x2_test_4"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv"), sep=";")
            path = os.path.join(base_path,"x2_test_4"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv")
            
            dep_infos = dependencyInfos(df,data_file = path)
            results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

    results = np.reshape(results,(100,-1))
    cols = [str(n) for n in size_list]
    tot_df = pd.DataFrame(results, columns=cols)
    
    pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"discrete_multiple_runs_t4")+".csv", sep=";")
# TEST 15 = create resume out of multiple test number 4 with discrete vars 
if test_number == 15:
    base_path = os.path.join("IT_results","multiple","discrete")

    results = []
    v_set = list(["A1","B1","C1","A2","B2","C2"])
    v_set.append("T")
    v_set = sorted(set(v_set))

    for test_n in tqdm(range(100)):
        for tot_size in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
            df = pd.read_csv(os.path.join(base_path,"x2_test_5"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv"), sep=";")
            path = os.path.join(base_path,"x2_test_5"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv")
            
            dep_infos = dependencyInfos(df,data_file = path)
            results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

    results = np.reshape(results,(100,-1))
    cols = [str(n) for n in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]]
    tot_df = pd.DataFrame(results, columns=cols)
    
    pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"discrete_multiple_runs_t5")+".csv", sep=";")

    results = []
    for test_n in tqdm(range(100)):
        for tot_size in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]:
            df = pd.read_csv(os.path.join(base_path,"x2_test_6"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv"), sep=";")
            path = os.path.join(base_path,"x2_test_6"+"_s"+str(tot_size)+"_t"+str(test_n)+".csv")
            
            dep_infos = dependencyInfos(df,data_file = path)
            results.append(sorted(pcmb(dep_infos,"T",v_set,0.05)))

    results = np.reshape(results,(100,-1))
    cols = [str(n) for n in [50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]]
    tot_df = pd.DataFrame(results, columns=cols)
    
    pd.DataFrame.to_csv(tot_df,os.path.join(base_path,"discrete_multiple_runs_t6")+".csv", sep=";")
# TEST 16 = reproduce ALARM experiment using x2
if test_number == 16:
    # open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    precisions = dict()
    recalls = dict()
    distances = dict()
    for s in ds_sizes:
        precisions[s] = []
        recalls[s] = []
        distances[s] = []

    for size in ds_sizes:
        for test_n in tqdm(range(10)):
            # create alarm subdataset and additional elements to perform independence test
            repeat_shuffle = True
            while repeat_shuffle:
                sub_df = alarm.sample(n=size, ignore_index=True)
                repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
            data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
            sub_df.to_csv(data_file)
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_file = os.path.join("IT_results","alarm_test","IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
            indep_df.to_csv(indep_file)
            dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="x2", data_file=data_file, indep_file=indep_file)

            # perform experiment
            prec = []
            rec = []
            dist = []
            for v in vars:
                mb_res = set(pcmb(dep_infos,v,vars,0.01))
                mb_real = set(IT_utils.get_mb(v,"alarm"))
                prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
                rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
                dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

            precisions[size].append(np.mean(prec))
            recalls[size].append(np.mean(rec))
            distances[size].append(np.mean(dist))
        
        print("size",size,":")
        print("   precision ","min",min(precisions[size]), "max",max(precisions[size]), "mean",np.mean(precisions[size]))
        print("   recall    ","min",min(recalls[size]),    "max",max(recalls[size]),    "mean",np.mean(recalls[size]))
        print("   distance  ","min",min(distances[size]),  "max",max(distances[size]),  "mean",np.mean(distances[size]))
        print("")

    print("RECAP")
    for size in ds_sizes:
            print("size",size,":")
            print("   precision ","min",min(precisions[size]), "max",max(precisions[size]), "mean",np.mean(precisions[size]))
            print("   recall    ","min",min(recalls[size]),    "max",max(recalls[size]),    "mean",np.mean(recalls[size]))
            print("   distance  ","min",min(distances[size]),  "max",max(distances[size]),  "mean",np.mean(distances[size]))
            print("")
# TEST 17 = reproduce ALARM experiment using g2
if test_number == 17:
    # open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    precisions = dict()
    recalls = dict()
    distances = dict()
    for s in ds_sizes:
        precisions[s] = []
        recalls[s] = []
        distances[s] = []

    for size in ds_sizes:
        for test_n in tqdm(range(10)):
            # create alarm subdataset and additional elements to perform independence test
            repeat_shuffle = True
            while repeat_shuffle:
                sub_df = alarm.sample(n=size, ignore_index=True)
                repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
            data_file = os.path.join("datasets","alarm_test","g_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
            sub_df.to_csv(data_file)
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_file = os.path.join("IT_results","alarm_test","IT_g_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
            indep_df.to_csv(indep_file)
            dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="g2", data_file=data_file, indep_file=indep_file)

            # perform experiment
            prec = []
            rec = []
            dist = []
            for v in vars:
                mb_res = set(pcmb(dep_infos,v,vars,0.01))
                mb_real = set(IT_utils.get_mb(v,"alarm"))
                prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
                rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
                dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

            precisions[size].append(np.mean(prec))
            recalls[size].append(np.mean(rec))
            distances[size].append(np.mean(dist))
        
        print("size",size,":")
        print("   precision ","min",min(precisions[size]), "max",max(precisions[size]), "mean",np.mean(precisions[size]))
        print("   recall    ","min",min(recalls[size]),    "max",max(recalls[size]),    "mean",np.mean(recalls[size]))
        print("   distance  ","min",min(distances[size]),  "max",max(distances[size]),  "mean",np.mean(distances[size]))
        print("")

    print("RECAP")
    for size in ds_sizes:
            print("size",size,":")
            print("   precision ","min",min(precisions[size]), "max",max(precisions[size]), "mean",np.mean(precisions[size]))
            print("   recall    ","min",min(recalls[size]),    "max",max(recalls[size]),    "mean",np.mean(recalls[size]))
            print("   distance  ","min",min(distances[size]),  "max",max(distances[size]),  "mean",np.mean(distances[size]))
            print("")
# TEST 18 = reproduce single ALARM experiment using x2
if test_number == 18:
    # open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    # print(vars)
    ds_sizes = [1000]
    # ds_sizes = [100,200]
    precisions = dict()
    recalls = dict()
    distances = dict()
    for s in ds_sizes:
        precisions[s] = []
        recalls[s] = []
        distances[s] = []

    for size in ds_sizes:
        for test_n in [0]:
            # # create alarm subdataset and additional elements to perform independence test
            # repeat_shuffle = True
            # while repeat_shuffle:
            #     sub_df = alarm.sample(n=size, ignore_index=True)
            #     repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
            data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
            # sub_df.to_csv(data_file)
            # indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_file = os.path.join("IT_results","alarm_test","IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
            # indep_df.to_csv(indep_file)
            indep_df = pd.read_csv(indep_file, sep = ";")
            indep_df = indep_df.drop(indep_df.columns[0], axis=1)
            # print(indep_df)
            dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="x2", data_file=data_file, indep_file=indep_file)

            # perform experiment
            prec = []
            rec = []
            dist = []
            for v in vars:
                mb_res = set(pcmb(dep_infos,v,vars,0.01))
                mb_real = set(IT_utils.get_mb(v,"alarm"))

                if mb_res != mb_real:
                    print(v,"  PCMB:",mb_res,"  REAL:",mb_real)
                    print()
                prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
                rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
                dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

            precisions[size].append(np.mean(prec))
            recalls[size].append(np.mean(rec))
            distances[size].append(np.mean(dist))
        
        print("size",size,":")
        print("   precision ","min",min(precisions[size]), "max",max(precisions[size]), "mean",np.mean(precisions[size]))
        print("   recall    ","min",min(recalls[size]),    "max",max(recalls[size]),    "mean",np.mean(recalls[size]))
        print("   distance  ","min",min(distances[size]),  "max",max(distances[size]),  "mean",np.mean(distances[size]))
        print("")
# TEST 19 = ALARM on x2 and g2 with parallelized trials ++++ mod : using python
if test_number == 19:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]

        data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

        # indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(0.01)+".csv")

        # indep_df.to_csv(indep_file)

        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file)

        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="py")

        # perform experiment
        prec = []
        rec = []
        dist = []
        for v in vars:
            mb_res = set(pcmb(dep_infos,v,vars,0.01))
            mb_real = set(IT_utils.get_mb(v,"alarm"))
            prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
            rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
            dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

        IT_utils.save_IT_dataframe(dep_infos)
        return test_type, size, np.mean(prec), np.mean(rec), np.mean(dist)
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    precisions = dict()
    recalls = dict()
    distances = dict()
    for test_type in ["x2","g2"]:
        for s in ds_sizes:
            precisions[(test_type,s)] = []
            recalls[(test_type,s)] = []
            distances[(test_type,s)] = []

    # for size in ds_sizes:
    #     for test_n in tqdm(range(10)):
    #         # create alarm subdataset and additional elements to perform independence test
    #         repeat_shuffle = True
    #         while repeat_shuffle:
    #             sub_df = alarm.sample(n=size, ignore_index=True)
    #             repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
    #         data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    #         sub_df.to_csv(data_file)

    print("Dataset creation ended")

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,20))
            ts = [i for i in range(10)]
            test_t = ["x2","g2"]
            input = ((a,b,c,d) for a,b,c,d in list(itertools.product(test_t, [size], ts, [vars])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for ty, si, pr, re, di in sorted(results,key= lambda x: (x[0],x[1])):
                # print("considering", ty, si)
                precisions[(ty,si)].append(pr)
                recalls[(ty,si)].append(re)
                distances[(ty,si)].append(di)
            
            for ty in ["x2","g2"]:
                print("type",ty,"   size",size,":")
                print("   precision ","min",'{:5.2f}'.format(min(precisions[(ty,size)])), "max",'{:5.2f}'.format(max(precisions[(ty,size)])), "mean",'{:5.2f}'.format(np.mean(precisions[(ty,size)])))
                print("   recall    ","min",'{:5.2f}'.format(min(recalls[(ty,size)])),    "max",'{:5.2f}'.format(max(recalls[(ty,size)])),    "mean",'{:5.2f}'.format(np.mean(recalls[(ty,size)])))
                print("   distance  ","min",'{:5.2f}'.format(min(distances[(ty,size)])),  "max",'{:5.2f}'.format(max(distances[(ty,size)])),  "mean",'{:5.2f}'.format(np.mean(distances[(ty,size)])))
                print("")

    print("RECAP")
    for ty in ["x2","g2"]:
        for size in ds_sizes:
            print("type",ty,"   size",size,":")
            print("   precision ","min",'{:5.2f}'.format(min(precisions[(ty,size)])), "max",'{:5.2f}'.format(max(precisions[(ty,size)])), "mean",'{:5.2f}'.format(np.mean(precisions[(ty,size)])))
            print("   recall    ","min",'{:5.2f}'.format(min(recalls[(ty,size)])),    "max",'{:5.2f}'.format(max(recalls[(ty,size)])),    "mean",'{:5.2f}'.format(np.mean(recalls[(ty,size)])))
            print("   distance  ","min",'{:5.2f}'.format(min(distances[(ty,size)])),  "max",'{:5.2f}'.format(max(distances[(ty,size)])),  "mean",'{:5.2f}'.format(np.mean(distances[(ty,size)])))
            print("")            
# TEST 20 = debug for g2
if test_number == 20:
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200]
    test_type = "g2"
    size = 100
    test_n = 8
    print("inizio 8")

    data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test",test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=True)

    # perform experiment
    prec = []
    rec = []
    dist = []
    for v in vars:
        mb_res = set(pcmb(dep_infos,v,vars,0.01))
        mb_real = set(IT_utils.get_mb(v,"alarm"))
        prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
        rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
        dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

    IT_utils.save_IT_dataframe(dep_infos)
    print("fine 8")
    

    test_type = "g2"
    size = 100
    test_n = 9
    print("inizio 9")

    data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test",test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=True)

    # perform experiment
    prec = []
    rec = []
    dist = []
    for v in vars:
        mb_res = set(pcmb(dep_infos,v,vars,0.01, verbose = 10000))
        mb_real = set(IT_utils.get_mb(v,"alarm"))
        prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
        rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
        dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

    IT_utils.save_IT_dataframe(dep_infos)
    print("fine 9")
# TEST 21 = ad hoc debug for g2
if test_number == 21:

    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200]

    test_type = "g2"
    size = 100
    test_n = 9
    print("inizio 9")

    data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test",test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=True)

    print(pcmb(dep_infos, "ECO2", vars, 0.01, verbose = 10000))

    # print(IT_utils.association(dep_infos, "ACO2", "VTUB", ["()"]))
    # print(IT_utils.association(dep_infos, "ACO2", "VMCH", ["()"]))
    # print(IT_utils.independence(dep_infos, "ACO2", "VTUB", ["()"], 0.01))
    # print(IT_utils.independence(dep_infos, "ACO2", "VMCH", ["()"], 0.01))
    # print(IT_utils.association(dep_infos, "VTUB", "ACO2",  ["()"]))
    # print(IT_utils.association(dep_infos, "VMCH", "ACO2",  ["()"]))
    # print(IT_utils.independence(dep_infos, "VTUB", "ACO2",  ["()"], 0.01))
    # print(IT_utils.independence(dep_infos, "VMCH", "ACO2",  ["()"], 0.01))
   
    # IT_utils.save_IT_dataframe(dep_infos)
    print("fine 9")
# TEST 22 = fully connected BN test on number of hypotheses
if test_number == 22:
    v_set = set("ABCDEFGHIJKLMNOPQR"[:n_vars+1])
    v_set.add("T")
    print("Considering ",v_set," of ", len(v_set)-1, " variables (without T)")
    counts = []
    for run in range(100):
        data_file = pd.read_csv("datasets/multiple5/t"+str(run)+"/v5_S10000.csv")
        data_file = "datasets/multiple5/t"+str(run)+"/v5_S10000.csv"
        indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        indep_file = os.path.join("IT_results","appoggio_counter.csv")
        test_type = "cor"
        indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False)

        pcmb(dep_infos,"T",v_set,0.05)
        IT_utils.save_IT_dataframe(dep_infos)
        counts.append(dep_infos.df.shape[0])

    print(counts)
# TEST 23 = IT on python
if test_number == 23:
    v_set = set("ABCDEFGHIJKLMNOPQR"[:n_vars+1])
    v_set.add("T")
    print("Considering ",v_set," of ", len(v_set)-1, " variables (without T)")
    counts = []
    data_file = "datasets/discrete/discrete_test_4_s1000_t0.csv"
    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","appoggio_py_g2.csv")
    test_type = "g2"
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="py")
    pcmb(dep_infos,"T",v_set,0.05)
    IT_utils.save_IT_dataframe(dep_infos)

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","appoggio_R_x2.csv")
    test_type = "x2"
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="R")
    pcmb(dep_infos,"T",v_set,0.05)
    IT_utils.save_IT_dataframe(dep_infos)

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","appoggio_py_x2.csv")
    test_type = "x2"
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="py")
    pcmb(dep_infos,"T",v_set,0.05)
    IT_utils.save_IT_dataframe(dep_infos)

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","appoggio_R_g2.csv")
    test_type = "g2"
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=True, independence_language="R")
    pcmb(dep_infos,"T",v_set,0.05)
    IT_utils.save_IT_dataframe(dep_infos)
# TEST 24 = ALARM on x2  with parallelized trials USING R
if test_number == 24:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]

        data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

        indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        indep_file = os.path.join("IT_results","alarm_test",test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
        indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="R")

        # perform experiment
        prec = []
        rec = []
        dist = []
        for v in vars:
            mb_res = set(pcmb(dep_infos,v,vars,0.01))
            mb_real = set(IT_utils.get_mb(v,"alarm"))
            prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
            rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
            dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

        IT_utils.save_IT_dataframe(dep_infos)
        return test_type, size, np.mean(prec), np.mean(rec), np.mean(dist)
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    precisions = dict()
    recalls = dict()
    distances = dict()
    for test_type in ["x2"]:
        for s in ds_sizes:
            precisions[(test_type,s)] = []
            recalls[(test_type,s)] = []
            distances[(test_type,s)] = []

    # for size in ds_sizes:
    #     for test_n in tqdm(range(10)):
    #         # create alarm subdataset and additional elements to perform independence test
    #         repeat_shuffle = True
    #         while repeat_shuffle:
    #             sub_df = alarm.sample(n=size, ignore_index=True)
    #             repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
    #         data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    #         sub_df.to_csv(data_file)

    # print("Dataset creation ended")

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,10))
            ts = [i for i in range(10)]
            test_t = ["x2"]
            input = ((a,b,c) for a,b,c in list(itertools.product(test_t, [size], ts)))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for ty, si, pr, re, di in sorted(results,key= lambda x: (x[0],x[1])):
                # print("considering", ty, si)
                precisions[(ty,si)].append(pr)
                recalls[(ty,si)].append(re)
                distances[(ty,si)].append(di)
            
            for ty in ["x2"]:
                print("type",ty,"   size",size,":")
                print("   precision ","min",min(precisions[(ty,size)]), "max",max(precisions[(ty,size)]), "mean",np.mean(precisions[(ty,size)]))
                print("   recall    ","min",min(recalls[(ty,size)]),    "max",max(recalls[(ty,size)]),    "mean",np.mean(recalls[(ty,size)]))
                print("   distance  ","min",min(distances[(ty,size)]),  "max",max(distances[(ty,size)]),  "mean",np.mean(distances[(ty,size)]))
                print("")

    print("RECAP")
    for ty in ["x2"]:
        for size in ds_sizes:
            print("type",ty,"   size",size,":")
            print("   precision ","min",min(precisions[(ty,size)]), "max",max(precisions[(ty,size)]), "mean",np.mean(precisions[(ty,size)]))
            print("   recall    ","min",min(recalls[(ty,size)]),    "max",max(recalls[(ty,size)]),    "mean",np.mean(recalls[(ty,size)]))
            print("   distance  ","min",min(distances[(ty,size)]),  "max",max(distances[(ty,size)]),  "mean",np.mean(distances[(ty,size)]))
            print("")      
# TEST 25 = debug IT removing els
if test_number == 25:
    v_set = set("ABCDEFGHIJKLMNOPQR"[:n_vars+1])
    v_set.add("T")
    print("Considering ",v_set," of ", len(v_set)-1, " variables (without T)")
    counts = []
    data_file = "datasets/discrete/discrete_test_4_s1000_t0.csv"
    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","appoggio_py_x2_debug.csv")
    test_type = "x2"
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="py")
    pcmb(dep_infos,"T",v_set,0.05, verbose = True)
    IT_utils.save_IT_dataframe(dep_infos)
# TEST 26 = ad hoc debug for removing IT tests
if test_number == 26:

    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]

    test_type = "x2"
    size = 2000
    test_n = 0

    data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test","dsep_"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="dsep", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language = "py", calculus_type = "all_>=5")
    print("dsep")
    print("GetPCD = ",get_pcd(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    print("GetPC  = ",get_pc(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    pp = pcmb(dep_infos, "ECO2", vars, 0.01, verbose = 0)
    print("PCMB   = ",pp)
    mb_res = set(pp)
    mb_real = set(IT_utils.get_mb("ECO2","alarm"))
    prec = len(mb_res.intersection(mb_real))/max(1,len(mb_res))
    rec = len(mb_res.intersection(mb_real))/max(1,len(mb_real))
    dist = math.sqrt((1-prec)*(1-prec)+(1-rec)*(1-rec))
    print("found",mb_res,"real",mb_real)
    print("prec  ", prec)
    print("rec   ", rec)
    print("dist  ", dist)
    
    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test","all_>=5_"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language = "py", calculus_type = "all_>=5")
    print("all_>=5")
    print("GetPCD = ",get_pcd(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    print("GetPC  = ",get_pc(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    pp = pcmb(dep_infos, "ECO2", vars, 0.01, verbose = 0)
    print("PCMB   = ",pp)
    mb_res = set(pp)
    mb_real = set(IT_utils.get_mb("ECO2","alarm"))
    prec = len(mb_res.intersection(mb_real))/max(1,len(mb_res))
    rec = len(mb_res.intersection(mb_real))/max(1,len(mb_real))
    dist = math.sqrt((1-prec)*(1-prec)+(1-rec)*(1-rec))
    print("found",mb_res,"real",mb_real)
    print("prec  ", prec)
    print("rec   ", rec)
    print("dist  ", dist)


    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test","only_>=5_"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language = "py", calculus_type = "only_>=5")

    print("only_>=5")
    print("GetPCD = ",get_pcd(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    print("GetPC  = ",get_pc(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    pp = pcmb(dep_infos, "ECO2", vars, 0.01, verbose = 0)
    print("PCMB   = ",pp)
    mb_res = set(pp)
    mb_real = set(IT_utils.get_mb("ECO2","alarm"))
    prec = len(mb_res.intersection(mb_real))/max(1,len(mb_res))
    rec = len(mb_res.intersection(mb_real))/max(1,len(mb_real))
    dist = math.sqrt((1-prec)*(1-prec)+(1-rec)*(1-rec))
    print("found",mb_res,"real",mb_real)
    print("prec  ", prec)
    print("rec   ", rec)
    print("dist  ", dist)


    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
    indep_file = os.path.join("IT_results","alarm_test","all_"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    indep_df.to_csv(indep_file)
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language = "py", calculus_type = "all")

    print("all")
    print("GetPCD = ",get_pcd(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    print("GetPC  = ",get_pc(dep_infos, "ECO2", vars, 0.01, verbose = 0))
    pp = pcmb(dep_infos, "ECO2", vars, 0.01, verbose = 0)
    print("PCMB   = ",pp)
    mb_res = set(pp)
    mb_real = set(IT_utils.get_mb("ECO2","alarm"))
    prec = len(mb_res.intersection(mb_real))/max(1,len(mb_res))
    rec = len(mb_res.intersection(mb_real))/max(1,len(mb_real))
    dist = math.sqrt((1-prec)*(1-prec)+(1-rec)*(1-rec))
    print("found",mb_res,"real",mb_real)
    print("prec  ", prec)
    print("rec   ", rec)
    print("dist  ", dist)
# TEST 27 = first test with summands
if test_number == 27:

    for number_rep_structure in [1,2]:
        for zero_init_t in [False]:    
            results = []
            r_sel = number_rep_structure
            v_set = list(["A1","B1","C1"])
            for i in range(1,r_sel):
                v_set.append("A"+str(i+1))
                v_set.append("B"+str(i+1))
                v_set.append("C"+str(i+1))
            v_set.append("T")
            v_set = sorted(set(v_set))

            for test_n in [0]:
                for tot_size in [100000]:
                    
                    method = "summands"
                    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                    indep_file = os.path.join("IT_results","multiple","summands_"+"t"+str(test_n)+"cor_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv")
                    indep_df.to_csv(indep_file)
                    data_file = os.path.join("datasets","multiple","t"+str(test_n),"v4_S"+str(tot_size)+"_R"+str(number_rep_structure)+".csv")
        
                    dep_infos = dependencyInfos(df = indep_df,data_file = data_file, indep_file = indep_file, method = method)
                    for v in v_set:
                        print("PCMB(",v,"):   ",pcmb(dep_infos,v,v_set,0.05))
            
            # da far girare su tutte le var
# TEST 28 = ALARM with summands   NOT FINISHED: CAN'T USE LINEAR REG
if test_number == 28:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]

        data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")

        indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        indep_file = os.path.join("IT_results","alarm_test","summands_py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+".csv")
        indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="summands", independence_method=test_type, data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="py")


        # perform experiment
        prec = []
        rec = []
        dist = []
        for v in vars:
            mb_res = set(pcmb(dep_infos,v,vars,0.01))
            mb_real = set(IT_utils.get_mb(v,"alarm"))
            prec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_res)))
            rec.append(len(mb_res.intersection(mb_real))/max(1,len(mb_real)))
            dist.append(math.sqrt((1-prec[-1])*(1-prec[-1])+(1-rec[-1])*(1-rec[-1])))

        IT_utils.save_IT_dataframe(dep_infos)
        return test_type, size, np.mean(prec), np.mean(rec), np.mean(dist)
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    precisions = dict()
    recalls = dict()
    distances = dict()
    for test_type in ["summands"]:
        for s in ds_sizes:
            precisions[(test_type,s)] = []
            recalls[(test_type,s)] = []
            distances[(test_type,s)] = []

    # for size in ds_sizes:
    #     for test_n in tqdm(range(10)):
    #         # create alarm subdataset and additional elements to perform independence test
    #         repeat_shuffle = True
    #         while repeat_shuffle:
    #             sub_df = alarm.sample(n=size, ignore_index=True)
    #             repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
    #         data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    #         sub_df.to_csv(data_file)

    print("Dataset creation ended")

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,20))
            ts = [i for i in range(10)]
            test_t = ["summands"]
            input = ((a,b,c,d) for a,b,c,d in list(itertools.product(test_t, [size], ts, [vars])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for ty, si, pr, re, di in sorted(results,key= lambda x: (x[0],x[1])):
                # print("considering", ty, si)
                precisions[(ty,si)].append(pr)
                recalls[(ty,si)].append(re)
                distances[(ty,si)].append(di)
            
            for ty in ["summands"]:
                print("type",ty,"   size",size,":")
                print("   precision ","min",min(precisions[(ty,size)]), "max",max(precisions[(ty,size)]), "mean",np.mean(precisions[(ty,size)]))
                print("   recall    ","min",min(recalls[(ty,size)]),    "max",max(recalls[(ty,size)]),    "mean",np.mean(recalls[(ty,size)]))
                print("   distance  ","min",min(distances[(ty,size)]),  "max",max(distances[(ty,size)]),  "mean",np.mean(distances[(ty,size)]))
                print("")

    print("RECAP")
    for ty in ["summands"]:
        for size in ds_sizes:
            print("type",ty,"   size",size,":")
            print("   precision ","min",min(precisions[(ty,size)]), "max",max(precisions[(ty,size)]), "mean",np.mean(precisions[(ty,size)]))
            print("   recall    ","min",min(recalls[(ty,size)]),    "max",max(recalls[(ty,size)]),    "mean",np.mean(recalls[(ty,size)]))
            print("   distance  ","min",min(distances[(ty,size)]),  "max",max(distances[(ty,size)]),  "mean",np.mean(distances[(ty,size)]))
            print("")            
# TEST 29 = ALARM on x2 and g2 with parallelized trials using python on standard and mod deltas
if test_number == 29:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]

        data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta < 0.0001:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta)[:1]+str(delta)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta)+".csv")
        
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="py")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        for v in vars:
            pcds[v] = get_pcd(dep_infos,v,vars,delta)
            pcs[v]  = get_pc(dep_infos,v,vars,delta)
            mbs[v]  = pcmb(dep_infos,v,vars,delta)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta, test_type, pcds, pcs, mbs
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    delta = delta_input
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    delta_corrected = delta / N
    ts = [i for i in range(10)]
    test_t = ["x2","g2"]
    tot_els = len(ts)*len(test_t)*2
    
    # for size in ds_sizes:
    #     for test_n in tqdm(range(10)):
    #         # create alarm subdataset and additional elements to perform independence test
    #         repeat_shuffle = True
    #         while repeat_shuffle:
    #             sub_df = alarm.sample(n=size, ignore_index=True)
    #             repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
    #         data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
    #         sub_df.to_csv(data_file)

    print("Dataset creation skipped")

    pcds = dict([(ty,{}) for ty in test_t])
    pcs = dict([(ty,{}) for ty in test_t])
    mbs = dict([(ty,{}) for ty in test_t])
    pcds_c = dict([(ty,{}) for ty in test_t])  # _c is for corrected deltas
    pcs_c = dict([(ty,{}) for ty in test_t])
    mbs_c = dict([(ty,{}) for ty in test_t])
    for size in ds_sizes:
        for ty in test_t:
            pcds[ty][size]   = {}
            pcs[ty][size]    = {}
            mbs[ty][size]    = {}
            pcds_c[ty][size] = {} 
            pcs_c[ty][size]  = {}
            mbs_c[ty][size]  = {}
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e) for a,b,c,d,e in list(itertools.product(test_t, [size], ts, [vars],[delta, delta_corrected])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb in sorted(results,key= lambda x: x[0]):
                if d == delta:
                    pcds[test_type][size][test_n] = pcd
                    pcs[test_type][size][test_n] = pc
                    mbs[test_type][size][test_n] = mb
                else:
                    pcds_c[test_type][size][test_n] = pcd
                    pcs_c[test_type][size][test_n] = pc
                    mbs_c[test_type][size][test_n] = mb

            if (True): #data saving part
                for test_type in test_t:
                    #result resume creation
                    d = delta
                    if d < 0.0001:
                        name = os.path.join("IT_results","alarm_test","res_"+test_type+"_alarm_delta"+str(d)[:1]+str(d)[-4:]+".csv")
                    else:
                        name = os.path.join("IT_results","alarm_test","res_"+test_type+"_alarm_delta"+str(d)+".csv")
                    cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                    cc = ["type"] + [item for sublist in cc for item in sublist]
                    data = []
                    for s in ds_sizes:
                        row = []
                        row.append("PCD_"+str(s))
                        dd = pcds.get(test_type)
                        dd = dd.get(s,{})
                        for v in vars:
                            for t in ts:
                                dt = dd.get(t,{})
                                row.append(dt.get(v,"-"))
                        data.append(row)
                        row = []
                        row.append("PC_"+str(s))
                        dd = pcs.get(test_type)
                        dd = dd.get(s,{})
                        for v in vars:
                            for t in ts:
                                dt = dd.get(t,{})
                                row.append(dt.get(v,"-"))
                        data.append(row)
                        row = []
                        row.append("PCMB_"+str(s))
                        dd = mbs.get(test_type)
                        dd = dd.get(s,{})
                        for v in vars:
                            for t in ts:
                                dt = dd.get(t,{})
                                row.append(dt.get(v,"-"))
                        data.append(row)
                    res_df = pd.DataFrame(data,columns=cc)
                    res_df.to_csv(name)
                        
                    d = delta_corrected
                    if d < 0.0001:
                        name = os.path.join("IT_results","alarm_test","res_"+test_type+"_alarm_delta"+str(d)[:1]+str(d)[-4:]+".csv")
                    else:
                        name = os.path.join("IT_results","alarm_test","res_"+test_type+"_alarm_delta"+str(d)+".csv")
                    data = []
                    for s in ds_sizes:
                        row = []
                        row.append("PCD_"+str(s))
                        dd = pcds_c.get(test_type)
                        dd = dd.get(s,{})
                        for v in vars:
                            for t in ts:
                                dt = dd.get(t,{})
                                row.append(dt.get(v,"-"))
                        data.append(row)
                        row = []
                        row.append("PC_"+str(s))
                        dd = pcs_c.get(test_type)
                        dd = dd.get(s,{})
                        for v in vars:
                            for t in ts:
                                dt = dd.get(t,{})
                                row.append(dt.get(v,"-"))
                        data.append(row)
                        row = []
                        row.append("PCMB_"+str(s))
                        dd = mbs_c.get(test_type)
                        dd = dd.get(s,{})
                        for v in vars:
                            for t in ts:
                                dt = dd.get(t,{})
                                row.append(dt.get(v,"-"))
                        data.append(row)
                    res_df = pd.DataFrame(data,columns=cc)
                    res_df.to_csv(name)
# TEST 30 = result analysis of previous tests
if test_number == 30:
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    deltas = ["0.05", "0.025", "2e-15", "1e-15"]
    ds_sizes = [100,200,500,1000,2000,5000,10000]

    for test_type in ["x2","g2"]:
        
        counter_pcd  = {"0.05":{}, "0.025":{}, "2e-15":{}, "1e-15":{}}
        counter_pc   = {"0.05":{}, "0.025":{}, "2e-15":{}, "1e-15":{}}
        counter_pcmb = {"0.05":{}, "0.025":{}, "2e-15":{}, "1e-15":{}}
        for version in ["","2"]:
            for d in deltas:
                in_file = pd.read_csv(os.path.join("IT_results","alarm_test","res"+version+"_"+test_type+"_alarm_delta"+d+".csv"),sep = ",")

                for size in tqdm(ds_sizes):
                    df_pcd = in_file[in_file["type"]=="PCD_"+str(size)].values[0,2:]
                    df_pcd = np.reshape(df_pcd,(-1,10))
                    df_pc = in_file[in_file["type"]=="PC_"+str(size)].values[0,2:]
                    df_pc = np.reshape(df_pc,(-1,10))
                    df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)].values[0,2:]
                    df_pcmb = np.reshape(df_pcmb,(-1,10))

                    counter_pcd[d][size] = []
                    counter_pc[d][size] = []
                    counter_pcmb[d][size] = []

                    for i in range(len(vars)):
                        v = vars[i]
                        c_pcd = 0
                        c_pc = 0
                        pc = IT_utils.get_pc(v,"alarm")
                        c_pcmb = 0
                        el2 = IT_utils.get_el_dist_le_2(v,"alarm")
                        # print("  ",v)
                        for j in range(10):
                            if df_pcd[i,j] != "-":
                                val = eval(df_pcd[i,j])
                                c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,"alarm") for element in val]))
                                # print()
                                # print(val)
                                # print("c_pcd",c_pcd)
                                val = eval(df_pc[i,j])
                                c_pc += 1*(any([not element in pc for element in val]))
                                # print()
                                # print(val)
                                # print("PC:",pc)
                                # print("c_pc",1*(any([not element in pc for element in val])))
                                val = eval(df_pcmb[i,j])
                                c_pcmb += 1*(any([not element in el2 for element in val]))
                                # print()
                                # print(val)
                                # print("EL2:",el2)
                                # print("c_pcmb",1*(any([not element in el2 for element in val])))
                                # input()
                            else:
                                c_pcd = -1
                                c_pc = -1
                                c_pcmb = -1
                        counter_pcd[d][size].append(c_pcd)
                        counter_pc[d][size].append(c_pc)
                        counter_pcmb[d][size].append(c_pcmb)

            if version == "":
                print("Version 1")
            else:
                print("Version 2")
            print("Recap for ", test_type)
            print()
            print("PCD")
            for size in ds_sizes:
                print("- size:", size)
                for d in deltas:
                    print("  delta:",d," sum", np.sum(counter_pcd[d][size])," mean", np.mean(counter_pcd[d][size])/10," mean on 10:", np.mean(counter_pcd[d][size]), " max", np.max(counter_pcd[d][size]))
            print("PC")
            for size in ds_sizes:
                print("- size:", size)
                for d in deltas:
                    print("  delta:",d," sum", np.sum(counter_pc[d][size])," mean", np.mean(counter_pc[d][size])/10," mean on 10:", np.mean(counter_pc[d][size]), " max", np.max(counter_pc[d][size]))
            print("PCMB")
            for size in ds_sizes:
                print("- size:", size)
                for d in deltas:
                    print("  delta:",d," sum", np.sum(counter_pcmb[d][size])," mean", np.mean(counter_pcmb[d][size])/10," mean on 10:", np.mean(counter_pcmb[d][size]), " max", np.max(counter_pcmb[d][size]))
# TEST 31 = debug for pcd
if test_number == 31:
    size = 10000
    test_n = 1
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    # ds_sizes = [100,200]
    sv = len(vars)
    test_type = "x2"
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    # for test_n in range(10):
    print("TESTS ",test_n)
    for delta in [0.05,0.05/N]:
        if delta < 0.0001:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta)[:1]+str(delta)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta)+".csv")
        
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, 
                        indep_file=indep_file, save_every_update=False, independence_language="py")
        print("   DELTA", delta)
        print("   ",get_pcd(dep_infos, "ECO2", vars, delta, verbose = 10))
# TEST 32: Repeat test 29-30         
if test_number == 32:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]

        data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta < 0.0001:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta)[:1]+str(delta)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta)+".csv")
        
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="py")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        for v in vars:
            pcds[v] = get_pcd(dep_infos,v,vars,delta)
            pcs[v]  = get_pc(dep_infos,v,vars,delta)
            mbs[v]  = pcmb(dep_infos,v,vars,delta)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta, test_type, pcds, pcs, mbs
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05, 0.025, 0.05/N, 0.025/N]
    ts = [i for i in range(10)]
    test_t = ["x2","g2"]
    tot_els = len(ts)*len(test_t)*2
    
    print("Dataset creation skipped")

    pcds = dict([(d,{}) for d in deltas])
    pcs = dict([(d,{}) for d in deltas])
    mbs = dict([(d,{}) for d in deltas])
    for d in deltas:
        pcds[d] =  dict([(ty,{}) for ty in test_t])
        pcs[d] =  dict([(ty,{}) for ty in test_t])
        mbs[d] =  dict([(ty,{}) for ty in test_t])
    for d in deltas:
        for size in ds_sizes:
            for ty in test_t:
                pcds[d][ty][size]   = {}
                pcs[d][ty][size]    = {}
                mbs[d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e) for a,b,c,d,e in list(itertools.product(test_t, [size], ts, [vars],deltas)))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb in sorted(results,key= lambda x: x[0]):
                pcds[d][test_type][size][test_n] = pcd
                pcs[d][test_type][size][test_n] = pc
                mbs[d][test_type][size][test_n] = mb

            if (True): #data saving part
                for test_type in test_t:
                    #result resume creation
                    for d in deltas:
                        if d < 0.0001:
                            name = os.path.join("IT_results","alarm_test","res1_"+test_type+"_alarm_delta"+str(d)[:1]+str(d)[-4:]+".csv")
                        else:
                            name = os.path.join("IT_results","alarm_test","res1_"+test_type+"_alarm_delta"+str(d)+".csv")
                        cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                        cc = ["type"] + [item for sublist in cc for item in sublist]
                        data = []
                        for s in ds_sizes:
                            row = []
                            row.append("PCD_"+str(s))
                            dd = pcds.get(d)
                            dd = dd.get(test_type)
                            dd = dd.get(s,{})
                            for v in vars:
                                for t in ts:
                                    dt = dd.get(t,{})
                                    row.append(dt.get(v,"-"))
                            data.append(row)
                            row = []
                            row.append("PC_"+str(s))
                            dd = pcs.get(d)
                            dd = dd.get(test_type)
                            dd = dd.get(s,{})
                            for v in vars:
                                for t in ts:
                                    dt = dd.get(t,{})
                                    row.append(dt.get(v,"-"))
                            data.append(row)
                            row = []
                            row.append("PCMB_"+str(s))
                            dd = mbs.get(d)
                            dd = dd.get(test_type)
                            dd = dd.get(s,{})
                            for v in vars:
                                for t in ts:
                                    dt = dd.get(t,{})
                                    row.append(dt.get(v,"-"))
                            data.append(row)
                        res_df = pd.DataFrame(data,columns=cc)
                        res_df.to_csv(name)
                        
            if (True): #data merging part
                for tt in test_t:
                        
                    in_files = []
                    for d in deltas:
                        if d < 0.0001:
                            in_files.append(pd.read_csv(os.path.join("IT_results","alarm_test","res1_"+tt+"_alarm_delta"+str(d)[:1]+str(d)[-4:]+".csv"),sep = ",") )
                        else:
                            in_files.append(pd.read_csv(os.path.join("IT_results","alarm_test","res1_"+tt+"_alarm_delta"+str(d)+".csv"),sep = ",") )
                    for i in range(len(in_files)):
                        in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                        in_files[i]["delta"] = [deltas[i] for j in range(in_files[i].shape[0])]

                    out_df = pd.DataFrame(columns=in_files[0].columns)
                    for f in in_files:
                        out_df=out_df.append(f)

                    cols = list(out_df.columns)
                    cols = cols[-2:] + cols[:-2]
                    out_df = out_df[cols]
                    out_df = out_df.sort_values("key")
                    out_df.to_csv(os.path.join("IT_results","total_res","tot_alarm_v1_"+tt+".csv"))
# TEST 33: Repeat test 29-30 using the modified mod_pc function      
if test_number == 33:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]

        data_file = os.path.join("datasets","alarm_test","alarm_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:1]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","alarm_test","py"+test_type+"_IT_alarm_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file)
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="py")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        for v in vars:
            pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
            pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
            mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs
    
    #open alarm.csv, get variable names
    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    # ds_sizes = [100,200]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05, 0.025]
    deltas_tot = [0.05, 0.025, 0.05/N, 0.025/N]
    ts = [i for i in range(10)]
    test_t = ["x2","g2"]
    tot_els = len(ts)*len(test_t)*2
    
    print("Dataset creation skipped")

    pcds = dict([(d,{}) for d in deltas_tot])
    pcs = dict([(d,{}) for d in deltas_tot])
    mbs = dict([(d,{}) for d in deltas_tot])
    for d in deltas_tot:
        pcds[d] =  dict([(ty,{}) for ty in test_t])
        pcs[d] =  dict([(ty,{}) for ty in test_t])
        mbs[d] =  dict([(ty,{}) for ty in test_t])
    for d in deltas_tot:
        for size in ds_sizes:
            for ty in test_t:
                pcds[d][ty][size]   = {}
                pcs[d][ty][size]    = {}
                mbs[d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f) for a,b,c,d,e,f in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb in sorted(results,key= lambda x: x[0]):
                pcds[d][test_type][size][test_n] = pcd
                pcs[d][test_type][size][test_n] = pc
                mbs[d][test_type][size][test_n] = mb

            if (True): #data saving part
                for test_type in test_t:
                    #result resume creation
                    for d in deltas_tot:
                        if d < 0.0001:
                            name = os.path.join("IT_results","alarm_test","res2_"+test_type+"_alarm_delta"+str(d)[:1]+str(d)[-4:]+".csv")
                        else:
                            name = os.path.join("IT_results","alarm_test","res2_"+test_type+"_alarm_delta"+str(d)+".csv")
                        cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                        cc = ["type"] + [item for sublist in cc for item in sublist]
                        data = []
                        for s in ds_sizes:
                            row = []
                            row.append("PCD_"+str(s))
                            dd = pcds.get(d)
                            dd = dd.get(test_type)
                            dd = dd.get(s,{})
                            for v in vars:
                                for t in ts:
                                    dt = dd.get(t,{})
                                    row.append(dt.get(v,"-"))
                            data.append(row)
                            row = []
                            row.append("PC_"+str(s))
                            dd = pcs.get(d)
                            dd = dd.get(test_type)
                            dd = dd.get(s,{})
                            for v in vars:
                                for t in ts:
                                    dt = dd.get(t,{})
                                    row.append(dt.get(v,"-"))
                            data.append(row)
                            row = []
                            row.append("PCMB_"+str(s))
                            dd = mbs.get(d)
                            dd = dd.get(test_type)
                            dd = dd.get(s,{})
                            for v in vars:
                                for t in ts:
                                    dt = dd.get(t,{})
                                    row.append(dt.get(v,"-"))
                            data.append(row)
                        res_df = pd.DataFrame(data,columns=cc)
                        res_df.to_csv(name)
                        
            if (True): #data merging part
                for tt in test_t:
                        
                    in_files = []
                    for d in deltas_tot:
                        if d < 0.0001:
                            in_files.append(pd.read_csv(os.path.join("IT_results","alarm_test","res2_"+tt+"_alarm_delta"+str(d)[:1]+str(d)[-4:]+".csv"),sep = ",") )
                        else:
                            in_files.append(pd.read_csv(os.path.join("IT_results","alarm_test","res2_"+tt+"_alarm_delta"+str(d)+".csv"),sep = ",") )
                    for i in range(len(in_files)):
                        in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                        in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                    out_df = pd.DataFrame(columns=in_files[0].columns)
                    for f in in_files:
                        out_df=out_df.append(f)

                    cols = list(out_df.columns)
                    cols = cols[-2:] + cols[:-2]
                    out_df = out_df[cols]
                    out_df = out_df.sort_values("key")
                    out_df.to_csv(os.path.join("IT_results","total_res","tot_alarm_v2_"+tt+".csv"))
# TEST 34: test on insurance dataset, full data   
if test_number == 34:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]

        data_file = os.path.join("datasets","insurance_test","insurance_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","insurance_test","py"+test_type+"_IT_insurance_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","insurance_test","py"+test_type+"_IT_insurance_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file, sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="py")

        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        try:
            if version == 1:
                for v in vars:  
                    pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                    pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                    mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
            else:
                for v in vars:  
                    pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                    pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                    mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)
        except:
            print(indep_file)
            print(indep_df)    
            raise Exception
        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    #open insurance.csv, get variable names
    insurance = pd.read_csv(os.path.join("datasets","insurance.csv"))
    vars = [col_name for col_name in insurance.columns]
    ds_sizes = [500,1000,2500,5000,10000,20000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05, 0.03]
    deltas_tot = [0.05, 0.03, 0.05/N, 0.03/N]
    versions = [1,2]
    ts = [i for i in range(10)]
    test_t = ["x2"]
    tot_els = len(ts)*len(test_t)*2
    
    for size in ds_sizes:
        for test_n in tqdm(range(10)):
            # create insurance subdataset and additional elements to perform independence test
            if not os.path.exists(os.path.join("datasets","insurance_test","insurance_s"+str(size)+"_t"+str(test_n)+".csv")):
                repeat_shuffle = True
                while repeat_shuffle:
                    sub_df = insurance.sample(n=size, ignore_index=True)
                    repeat_shuffle = any([len(set(sub_df[c]))==1 for c in vars])
                data_file = os.path.join("datasets","insurance_test","insurance_s"+str(size)+"_t"+str(test_n)+".csv")
                sub_df.to_csv(data_file)
    print("Dataset creation done")

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        for vvv in versions:
            with threadpool_limits(limits=1, user_api='blas'):
                for ddd in deltas:
                    p = mp.Pool(min(n_cores,72,tot_els))
                    input = ((a,b,c,d,e,f,g) for a,b,c,d,e,f,g in list(itertools.product(test_t, [size], ts, [vars],[ddd],[1,N],[vvv])))
                    results = p.map(perform_test, input)   
                    p.close()
                    p.join()

                    for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                        pcds[version][d][test_type][size][test_n] = pcd
                        pcs[version][d][test_type][size][test_n] = pc
                        mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","insurance_test","res"+str(ver)+"_"+test_type+"_insurance_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","insurance_test","res"+str(ver)+"_"+test_type+"_insurance_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","insurance_test","res"+str(ver)+"_"+tt+"_insurance_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","insurance_test","res"+str(ver)+"_"+tt+"_insurance_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_insurance_v"+str(ver)+"_"+tt+".csv"))
# TEST 35: test with continuous and repetitions   
if test_number == 35:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple7","v7_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple7",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple7",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    ds_sizes = [100,250,500,1000,5000,10000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    else:
        print("Error on n_rep", 1/0)
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    
    # for size in ds_sizes:
    #     # create insurance subdataset and additional elements to perform independence test
    #     if not os.path.exists(os.path.join("datasets","multiple7","v7_S"+str(size)+"_t99_r"+str(n_rep)+".csv")):
    #         p = subprocess.call(("python", "ids_creator.py -t 7 -a "+str(size)+" -b 10 -n "+str(n_rep)))
    #         # rint("begin creation for size", size)
    #         # os.system("python ids_creator.py -a "+str(size)+" -b 10 -n "+str(n_rep))
    #         print("ended creation for size", size)
    # print("Dataset creation done")

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple7","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","multiple7","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple7","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple7","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+".csv"))
# TEST 36: debug on 35
if test_number == 36:
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    ds_sizes = [500,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    else:
        print("Error on n_rep", 1/0)
        
    # versions = [1,2]
    versions = [1]
    ts = [i for i in range(10)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    
    # for size in ds_sizes:
    #     # create insurance subdataset and additional elements to perform independence test
    #     if not os.path.exists(os.path.join("datasets","multiple7","v7_S"+str(size)+"_t99_r"+str(n_rep)+".csv")):
    #         p = subprocess.call(("python", "ids_creator.py -t 7 -a "+str(size)+" -b 10 -n "+str(n_rep)))
    #         # rint("begin creation for size", size)
    #         # os.system("python ids_creator.py -a "+str(size)+" -b 10 -n "+str(n_rep))
    #         print("ended creation for size", size)
    # print("Dataset creation done")

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
    
        for a in test_t:
            for b in ts:
                for c in deltas:
                    for nn in [1,N]:
                        for vvv in versions:
                            test_type = a
                            test_n = b
                            delta = c
                            N_val = nn
                            version = vvv

                            data_file = os.path.join("datasets","multiple7","v7_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
                            if delta/N_val < 0.0001:
                                indep_file = os.path.join("IT_results","multiple7",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N_val)[:3]+str(delta/N_val)[-4:]+".csv")
                            else:
                                indep_file = os.path.join("IT_results","multiple7",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N_val)+".csv")
                            
                            if os.path.exists(indep_file):
                                try:
                                    indep_df = pd.read_csv(indep_file,sep = ";")
                                except:
                                    print("!!!",indep_file)
                                    indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                                    indep_df.to_csv(indep_file, sep = ";")
                            else:
                                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                                indep_df.to_csv(indep_file, sep = ";")
                            dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                                            indep_file=indep_file, save_every_update=False, independence_language="R")


                            # perform experiment
                            pcds = {}
                            pcs = {}
                            mbs = {}
                            if version == 1:
                                for v in vars:  
                                    get_pcd(dep_infos,v,vars,delta/N_val, max_z_size=2)
                                    get_pc(dep_infos,v,vars,delta/N_val, max_z_size=2)
                                    pcmb(dep_infos,v,vars,delta/N_val, max_z_size=2)
                            else:
                                for v in vars:  
                                    mod_pcd(dep_infos,v,vars,delta, N_val, max_z_size=2)
                                    mod_pc(dep_infos,v,vars,delta, N_val, max_z_size=2)
                                    pcmb_mod(dep_infos,v,vars,delta, N_val, max_z_size=2)

                            IT_utils.save_IT_dataframe(dep_infos)
# TEST 37: theorem result analysis for test 32,33,34,35,38
if test_number == 37:
    n_exps = 10

    alarm = pd.read_csv(os.path.join("datasets","alarm.csv"))
    vars = [col_name for col_name in alarm.columns]
    ds_sizes = [100,200,500,1000,2000,5000,10000,20000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05, 0.025, 0.05/N, 0.025/N]
    test_t = ["x2","g2"]
    alarm_1 = (vars, ds_sizes, [1,2],deltas,"x2_alarm","alarm")
    alarm_2 = (vars, ds_sizes, [1,2],deltas,"g2_alarm","alarm")

    insurance = pd.read_csv(os.path.join("datasets","insurance.csv"))
    vars = [col_name for col_name in insurance.columns]
    ds_sizes = [500,1000,2500,5000,10000,20000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas_tot = [0.05, 0.03, 0.05/N, 0.03/N]
    insurance = (vars, ds_sizes, [1,2],deltas_tot,"x2_insurance","insurance")

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"]" for i in range(n_rep)])
    dag_struc += "[T|"+"".join(["B"+str(i+1)+":C"+str(i+1)+":" for i in range(n_rep)])[:-1]+"]"
    ds_sizes = [100,250,500,1000,5000,10000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    multiple5 = (vars, ds_sizes, [1,2],deltas_tot,"multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"]" for i in range(n_rep)])
    dag_struc += "[T|"+"".join(["B"+str(i+1)+":C"+str(i+1)+":" for i in range(n_rep)])[:-1]+"]"
    ds_sizes = [100,250,500,1000,5000,10000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    multiple10 = (vars, ds_sizes, [1,2],deltas_tot,"multiple_rep"+str(n_rep),dag_struc)

    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250,500,1000,5000,10000,50000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    deltas_tot = [0.05, 0.02, 0.05/N, 0.02/N]
    dag_struc = "[b1191][cspG][eutG][cspA|cspG][fixC|b1191][sucA|eutG][yecO|cspG][yedE|cspG][atpG|sucA][cchB|fixC][dnaJ|sucA][flgD|sucA][gltA|sucA][lpdA|yedE][pspB|cspG:yedE][sucD|sucA][tnaA|b1191:fixC:sucA][yceP|eutG:fixC][yfiA|cspA][ygbD|fixC][ygcE|b1191:sucA][yhdM|sucA][yjbO|fixC][asnA|ygcE][atpD|sucA:ygcE][hupB|cspA:yfiA][ibpB|eutG:yceP][pspA|cspG:pspB:yedE][yfaD|eutG:sucA:yceP][icdA|asnA:ygcE][lacA|asnA:cspG][nmpC|pspA][yheI|atpD:yedE][aceB|icdA][b1963|yheI][dnaK|yheI][folK|yheI][lacY|asnA:cspG:eutG:lacA][ycgX|fixC:yheI][dnaG|ycgX:yheI][lacZ|asnA:lacA:lacY][nuoM|lacY][b1583|lacA:lacZ:yceP][mopB|dnaK:lacZ][yaeM|cspG:lacA:lacZ][ftsJ|mopB]"
    ecoli = (vars, ds_sizes, [1,2],deltas_tot,"ecoli",dag_struc)

    parameter_list = [alarm_1, alarm_2, insurance, multiple5, multiple10, ecoli]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "x2_alarm" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_alarm_v"+str(ver)+"_x2.csv")
                dag_struc = dependencyInfos.initialize_dag_struc("alarm")
            if "g2_alarm" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_alarm_v"+str(ver)+"_g2.csv")
                dag_struc = dependencyInfos.initialize_dag_struc("alarm")
            if "x2_insurance" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_insurance_v"+str(ver)+"_x2.csv")
                dag_struc = dependencyInfos.initialize_dag_struc("insurance")
            if "multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_multiple_cont_r5_v"+str(ver)+"_cor.csv")
            if "multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_multiple_cont_r10_v"+str(ver)+"_cor.csv")
            if "ecoli" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_ecoli_v"+str(ver)+"_cor.csv")
                
            in_df = pd.read_csv(f_name_selected)
            counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
            counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
            cc = [[str(v)+"_t"+str(t) for t in range(10)] for v in vars_selected]
            cc = [item for sublist in cc for item in sublist]
            
            for d in deltas_selected:
                if d-2.18497225651154e-15<1e-18 and "alarm" in method_name:  # fix for alarm ds
                    in_file = in_df[in_df['delta'] == 2.18497225651154e-15]
                elif d-2.746058882164639e-18 < 1e-20 and "ecoli" == method_name:
                    in_file = in_df[in_df['delta'] == 2.746058882164639e-18]
                else:
                    in_file = in_df[in_df['delta'] == d]

                for size in sizes_selected:
                    df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                    df_pcd = df_pcd[cc].values
                    df_pcd = np.reshape(df_pcd,(-1,n_exps))
                    df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                    df_pc = df_pc[cc].values
                    df_pc = np.reshape(df_pc,(-1,n_exps))
                    df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                    df_pcmb = df_pcmb[cc].values
                    df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                    # counter_pcd[d][size] = []
                    # counter_pc[d][size] = []
                    # counter_pcmb[d][size] = []
                    look_pc = {}
                    look_el2 = {}
                    
                    for i in range(len(vars_selected)):
                        v = vars_selected[i]
                        c_pcd = 0
                        c_pc = 0
                        pc = IT_utils.get_pc(v,dag_struc)
                        look_pc[v] = pc
                        c_pcmb = 0
                        el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                        look_el2[v] = el2

                    #     if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                    #         for j in range(n_exps):
                    #             if df_pcd[i,j] != "-":
                    #                 val = eval(df_pcd[i,j])
                    #                 c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
                    #                 val = eval(df_pc[i,j])
                    #                 c_pc += 1*(any([not element in pc for element in val]))
                    #                 val = eval(df_pcmb[i,j])
                    #                 c_pcmb += 1*(any([not element in el2 for element in val]))
                    #             else:
                    #                 c_pcd = -1
                    #                 c_pc = -1
                    #                 c_pcmb = -1
                    #     else:
                    #         c_pcd = -1
                    #         c_pc = -1
                    #         c_pcmb = -1
                    #     counter_pcd[d][size].append(c_pcd)
                    #     counter_pc[d][size].append(c_pc)
                    #     counter_pcmb[d][size].append(c_pcmb)

                    
                    counter_pcd_per_test[d][size] = []
                    counter_pc_per_test[d][size] = []
                    counter_pcmb_per_test[d][size] = []
                    
                    for j in range(n_exps):
                        c_pcd = -1
                        c_pc = -1
                        c_pcmb = -1
                        if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            if df_pcd[i,j] != "-":
                                c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
                        counter_pcd_per_test[d][size].append(c_pcd)
                        counter_pc_per_test[d][size].append(c_pc)
                        counter_pcmb_per_test[d][size].append(c_pcmb)

                    # for j in range(n_exps):
                    #     res_1 = 0
                    #     res_2 = 0
                    #     res_3 = 0
                    #     for v in vars_selected:
                    #         i = vars_selected.index(v)
                    #         pcd_v = eval(df_pcd[i,j])
                    #         res_1 = res_1 + np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v])
                    #         pc_v = eval(df_pc[i,j])
                    #         res_2 = res_2 + np.sum([not el in look_pc[v] for el in pc_v])
                    #         pcmb_v = eval(df_pcmb[i,j])
                    #         res_3 = res_3 + np.sum([not el in look_el2[v] for el in pcmb_v])
                        
                    #     if res_1!=counter_pcd_per_test[d][size][j] or res_2!=counter_pc_per_test[d][size][j] or res_3!=counter_pcmb_per_test[d][size][j]:
                    #         print(res_1,counter_pcd_per_test[d][size][j])
                    #         print(res_2,counter_pc_per_test[d][size][j])
                    #         print(res_3,counter_pcmb_per_test[d][size][j])
                    #     else:
                    #         print("yo")
                    
            # print("counter_pcd_per_test",counter_pcd_per_test)
            # print("counter_pc_per_test",counter_pc_per_test)
            # print("counter_pcmb_per_test",counter_pcmb_per_test)
            # input()

            if ver == 1:
                print("Version 1")
            else:
                print("Version 2")
            print()
            if ver == 1:
                print("PCD - FPs")
            else:
                print("Mod_PCD - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[d][size]))), " max", np.max(counter_pcd_per_test[d][size]))
            if ver == 1:
                print("PC - FPs")
            else:
                print("Mod_PC - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[d][size]))), " max", np.max(counter_pc_per_test[d][size]))
            if ver == 1:
                print("PCMB - el a dist > 2")
            else:
                print("PCMB mod - el a dist > 2")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[d][size]))), " max", np.max(counter_pcmb_per_test[d][size]))
        print()
        print()
        print()
# TEST 38: ecoli test 
if test_number == 38:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]

        data_file = os.path.join("datasets","ecoli_test","ecoli_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=6)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=6)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=6)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=6)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=6)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=6)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250,500,1000,5000,10000,50000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05,0.02]
    deltas_tot = [0.05, 0.02, 0.05/N, 0.02/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    
    # for size in ds_sizes:
    #     # create insurance subdataset and additional elements to perform independence test
    #     if not os.path.exists(os.path.join("datasets","multiple7","v7_S"+str(size)+"_t99_r"+str(n_rep)+".csv")):
    #         p = subprocess.call(("python", "ids_creator.py -t 7 -a "+str(size)+" -b 10 -n "+str(n_rep)))
    #         # rint("begin creation for size", size)
    #         # os.system("python ids_creator.py -a "+str(size)+" -b 10 -n "+str(n_rep))
    #         print("ended creation for size", size)
    # print("Dataset creation done")

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g) for a,b,c,d,e,f,g in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions)))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+test_type+"_ecoli_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+test_type+"_ecoli_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+tt+"_ecoli_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+tt+"_ecoli_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_"+tt+".csv"))
# TEST 39: theorem result analysis for extended tests 35,38
if test_number == 39:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"]" for i in range(n_rep)])
    dag_struc += "[T|"+"".join(["B"+str(i+1)+":C"+str(i+1)+":" for i in range(n_rep)])[:-1]+"]"
    ds_sizes = [100,250,500,1000,5000,10000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    multiple5 = (vars, ds_sizes, [1,2],deltas_tot,"multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"]" for i in range(n_rep)])
    dag_struc += "[T|"+"".join(["B"+str(i+1)+":C"+str(i+1)+":" for i in range(n_rep)])[:-1]+"]"
    ds_sizes = [100,250,500,1000,5000,10000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    multiple10 = (vars, ds_sizes, [1,2],deltas_tot,"multiple_rep"+str(n_rep),dag_struc)

    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250,500,1000,5000,10000,50000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    deltas_tot = [0.05, 0.02, 0.05/N, 0.02/N]
    dag_struc = "[b1191][cspG][eutG][cspA|cspG][fixC|b1191][sucA|eutG][yecO|cspG][yedE|cspG][atpG|sucA][cchB|fixC][dnaJ|sucA][flgD|sucA][gltA|sucA][lpdA|yedE][pspB|cspG:yedE][sucD|sucA][tnaA|b1191:fixC:sucA][yceP|eutG:fixC][yfiA|cspA][ygbD|fixC][ygcE|b1191:sucA][yhdM|sucA][yjbO|fixC][asnA|ygcE][atpD|sucA:ygcE][hupB|cspA:yfiA][ibpB|eutG:yceP][pspA|cspG:pspB:yedE][yfaD|eutG:sucA:yceP][icdA|asnA:ygcE][lacA|asnA:cspG][nmpC|pspA][yheI|atpD:yedE][aceB|icdA][b1963|yheI][dnaK|yheI][folK|yheI][lacY|asnA:cspG:eutG:lacA][ycgX|fixC:yheI][dnaG|ycgX:yheI][lacZ|asnA:lacA:lacY][nuoM|lacY][b1583|lacA:lacZ:yceP][mopB|dnaK:lacZ][yaeM|cspG:lacA:lacZ][ftsJ|mopB]"
    ecoli = (vars, ds_sizes, [1,2],deltas_tot,"ecoli",dag_struc)

    parameter_list = [multiple5, multiple10, ecoli]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont_r5_v"+str(ver)+"_cor.csv")
            if "multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont_r10_v"+str(ver)+"_cor.csv")
            if "ecoli" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_cor.csv")
                
            in_df = pd.read_csv(f_name_selected)
            counter_pcd  = dict([(d,{}) for d in deltas_selected])
            counter_pc   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb = dict([(d,{}) for d in deltas_selected])
            counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
            counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
            cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
            cc = [item for sublist in cc for item in sublist]
            
            for d in deltas_selected:
                if d-2.18497225651154e-15<1e-18 and "alarm" in method_name:  # fix for alarm ds
                    in_file = in_df[in_df['delta'] == 2.18497225651154e-15]
                elif d-2.746058882164639e-18 < 1e-20 and "ecoli" == method_name:
                    in_file = in_df[in_df['delta'] == 2.746058882164639e-18]
                else:
                    in_file = in_df[in_df['delta'] == d]

                for size in sizes_selected:
                    df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                    df_pcd = df_pcd[cc].values
                    df_pcd = np.reshape(df_pcd,(-1,n_exps))
                    df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                    df_pc = df_pc[cc].values
                    df_pc = np.reshape(df_pc,(-1,n_exps))
                    df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                    df_pcmb = df_pcmb[cc].values
                    df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                    # counter_pcd[d][size] = []
                    # counter_pc[d][size] = []
                    # counter_pcmb[d][size] = []
                    look_pc = {}
                    look_el2 = {}
                    
                    for i in range(len(vars_selected)):
                        v = vars_selected[i]
                        c_pcd = 0
                        c_pc = 0
                        pc = IT_utils.get_pc(v,dag_struc)
                        look_pc[v] = pc
                        c_pcmb = 0
                        el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                        look_el2[v] = el2

                    #     if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                    #         for j in range(n_exps):
                    #             if df_pcd[i,j] != "-":
                    #                 val = eval(df_pcd[i,j])
                    #                 c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
                    #                 val = eval(df_pc[i,j])
                    #                 c_pc += 1*(any([not element in pc for element in val]))
                    #                 val = eval(df_pcmb[i,j])
                    #                 c_pcmb += 1*(any([not element in el2 for element in val]))
                    #             else:
                    #                 c_pcd = -1
                    #                 c_pc = -1
                    #                 c_pcmb = -1
                    #     else:
                    #         c_pcd = -1
                    #         c_pc = -1
                    #         c_pcmb = -1
                    #     counter_pcd[d][size].append(c_pcd)
                    #     counter_pc[d][size].append(c_pc)
                    #     counter_pcmb[d][size].append(c_pcmb)

                    
                    counter_pcd_per_test[d][size] = []
                    counter_pc_per_test[d][size] = []
                    counter_pcmb_per_test[d][size] = []
                    
                    for j in range(n_exps):
                        c_pcd = -1
                        c_pc = -1
                        c_pcmb = -1
                        if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            if df_pcd[i,j] != "-":
                                c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
                        counter_pcd_per_test[d][size].append(c_pcd)
                        counter_pc_per_test[d][size].append(c_pc)
                        counter_pcmb_per_test[d][size].append(c_pcmb)

                    # for j in range(n_exps):
                    #     res_1 = 0
                    #     res_2 = 0
                    #     res_3 = 0
                    #     for v in vars_selected:
                    #         i = vars_selected.index(v)
                    #         pcd_v = eval(df_pcd[i,j])
                    #         res_1 = res_1 + np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v])
                    #         pc_v = eval(df_pc[i,j])
                    #         res_2 = res_2 + np.sum([not el in look_pc[v] for el in pc_v])
                    #         pcmb_v = eval(df_pcmb[i,j])
                    #         res_3 = res_3 + np.sum([not el in look_el2[v] for el in pcmb_v])
                        
                    #     if res_1!=counter_pcd_per_test[d][size][j] or res_2!=counter_pc_per_test[d][size][j] or res_3!=counter_pcmb_per_test[d][size][j]:
                    #         print(res_1,counter_pcd_per_test[d][size][j])
                    #         print(res_2,counter_pc_per_test[d][size][j])
                    #         print(res_3,counter_pcmb_per_test[d][size][j])
                    #     else:
                    #         print("yo")
                    
            # print("counter_pcd_per_test",counter_pcd_per_test)
            # print("counter_pc_per_test",counter_pc_per_test)
            # print("counter_pcmb_per_test",counter_pcmb_per_test)
            # input()

            if ver == 1:
                print("Version 1")
            else:
                print("Version 2")
            print()
            if ver == 1:
                print("PCD - FPs")
            else:
                print("Mod_PCD - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[d][size]))), " max", np.max(counter_pcd_per_test[d][size]))
            if ver == 1:
                print("PC - FPs")
            else:
                print("Mod_PC - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[d][size]))), " max", np.max(counter_pc_per_test[d][size]))
            if ver == 1:
                print("PCMB - el a dist > 2")
            else:
                print("PCMB mod - el a dist > 2")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[d][size]))), " max", np.max(counter_pcmb_per_test[d][size]))
        print()
        print()
        print()
# TEST 40: debug 39
if test_number == 40:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"]" for i in range(n_rep)])
    dag_struc += "[T|"+"".join(["B"+str(i+1)+":C"+str(i+1)+":" for i in range(n_rep)])[:-1]+"]"
    ds_sizes = [1000,5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    multiple5 = (vars, ds_sizes, [1,2],deltas_tot,"multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"]" for i in range(n_rep)])
    dag_struc += "[T|"+"".join(["B"+str(i+1)+":C"+str(i+1)+":" for i in range(n_rep)])[:-1]+"]"
    ds_sizes = [5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 10:
        deltas = [0.03]
        deltas_tot = [0.03, 0.03/N]
    multiple10 = (vars, ds_sizes, [1,2],deltas_tot,"multiple_rep"+str(n_rep),dag_struc)

    # ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    # vars = [col_name for col_name in ecoli.columns]
    # ds_sizes = [50,100,250,500,1000,5000,10000,50000,100000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    # deltas_tot = [0.05, 0.02, 0.05/N, 0.02/N]
    # dag_struc = "[b1191][cspG][eutG][cspA|cspG][fixC|b1191][sucA|eutG][yecO|cspG][yedE|cspG][atpG|sucA][cchB|fixC][dnaJ|sucA][flgD|sucA][gltA|sucA][lpdA|yedE][pspB|cspG:yedE][sucD|sucA][tnaA|b1191:fixC:sucA][yceP|eutG:fixC][yfiA|cspA][ygbD|fixC][ygcE|b1191:sucA][yhdM|sucA][yjbO|fixC][asnA|ygcE][atpD|sucA:ygcE][hupB|cspA:yfiA][ibpB|eutG:yceP][pspA|cspG:pspB:yedE][yfaD|eutG:sucA:yceP][icdA|asnA:ygcE][lacA|asnA:cspG][nmpC|pspA][yheI|atpD:yedE][aceB|icdA][b1963|yheI][dnaK|yheI][folK|yheI][lacY|asnA:cspG:eutG:lacA][ycgX|fixC:yheI][dnaG|ycgX:yheI][lacZ|asnA:lacA:lacY][nuoM|lacY][b1583|lacA:lacZ:yceP][mopB|dnaK:lacZ][yaeM|cspG:lacA:lacZ][ftsJ|mopB]"
    # ecoli = (vars, ds_sizes, [1,2],deltas_tot,"ecoli",dag_struc)

    parameter_list = [multiple5, multiple10]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            print("version", ver)
            if "multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont_r5_v"+str(ver)+"_cor.csv")
            if "multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont_r10_v"+str(ver)+"_cor.csv")
            if "ecoli" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_cor.csv")
                
            in_df = pd.read_csv(f_name_selected)
            counter_pcd  = dict([(d,{}) for d in deltas_selected])
            counter_pc   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb = dict([(d,{}) for d in deltas_selected])
            counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
            counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
            cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
            cc = [item for sublist in cc for item in sublist]
            
            for d in deltas_selected:
                if d-2.18497225651154e-15<1e-18 and "alarm" in method_name:  # fix for alarm ds
                    in_file = in_df[in_df['delta'] == 2.18497225651154e-15]
                elif d-2.746058882164639e-18 < 1e-20 and "ecoli" == method_name:
                    in_file = in_df[in_df['delta'] == 2.746058882164639e-18]
                else:
                    in_file = in_df[in_df['delta'] == d]

                for size in sizes_selected:
                    print("size",size)
                    df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                    df_pcd = df_pcd[cc].values
                    df_pcd = np.reshape(df_pcd,(-1,n_exps))
                    df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                    df_pc = df_pc[cc].values
                    df_pc = np.reshape(df_pc,(-1,n_exps))
                    df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                    df_pcmb = df_pcmb[cc].values
                    df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                    counter_pcd[d][size] = []
                    counter_pc[d][size] = []
                    counter_pcmb[d][size] = []
                    look_pc = {}
                    look_el2 = {}
                    
                    for i in range(len(vars_selected)):
                        v = vars_selected[i]
                        c_pcd = 0
                        c_pc = 0
                        pc = IT_utils.get_pc(v,dag_struc)
                        look_pc[v] = pc
                        c_pcmb = 0
                        el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                        look_el2[v] = el2

                        if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            for j in range(n_exps):
                                if df_pcd[i,j] != "-":
                                    val = eval(df_pcd[i,j])
                                    c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
                                    val = eval(df_pc[i,j])
                                    c_pc += 1*(any([not element in pc for element in val]))
                                    val = eval(df_pcmb[i,j])
                                    c_pcmb += 1*(any([not element in el2 for element in val]))
                                else:
                                    c_pcd = -1
                                    c_pc = -1
                                    c_pcmb = -1
                        else:
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb = -1
                        counter_pcd[d][size].append(c_pcd)
                        counter_pc[d][size].append(c_pc)
                        counter_pcmb[d][size].append(c_pcmb)

                    
                    counter_pcd_per_test[d][size] = []
                    counter_pc_per_test[d][size] = []
                    counter_pcmb_per_test[d][size] = []
                    
                    for j in range(n_exps):
                        c_pcd = -1
                        c_pc = -1
                        c_pcmb = -1
                        if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            if df_pcd[i,j] != "-":
                                c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
                        counter_pcd_per_test[d][size].append(c_pcd)
                        counter_pc_per_test[d][size].append(c_pc)
                        counter_pcmb_per_test[d][size].append(c_pcmb)

                    for j in range(n_exps):
                        res_1 = 0
                        res_2 = 0
                        res_3 = 0
                        for v in vars_selected:
                            if res_1 == 0:
                                i = vars_selected.index(v)
                                pcd_v = eval(df_pcd[i,j])
                                res_1 = res_1 + np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v])
                                proof = ""
                                for el in pcd_v:
                                    if not IT_utils.is_in_pcd(el,v,dag_struc):
                                        proof = el
                                if res_1 > 0 and ver == 1:
                                    print("t.",j,"sbagliato perché", proof ,"not in PCD ",v)
                                    break
                                if res_1 > 0 and ver == 2:
                                    print("t.",j,"sbagliato perché", proof ,"not in Mod_PCD",v)
                                    break
                print()
                print()
# TEST 41: test with continuous and repetitions 8  
if test_number == 41:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple8","v8_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    # if n_rep == 5:
    #     deltas = [0.045]
    #     deltas_tot = [0.045, 0.045/N]
    # elif n_rep == 10:
    #     deltas = [0.02]
    #     deltas_tot = [0.02, 0.02/N]
    # else:
    #     print("Error on n_rep", 1/0)
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"v2.csv")
                            else:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"v2.csv"))
# TEST 42: test with continuous and repetitions 9  
if test_number == 42:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple9","v9_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    # ds_sizes = [100,250,500,1000,5000,10000]
    ds_sizes = [50000,100000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 10:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 25:
        deltas = [0.035]
        deltas_tot = [0.035, 0.035/N]
    elif n_rep == 50:
        deltas = [0.015]
        deltas_tot = [0.015, 0.015/N]
    elif n_rep == 100:
        deltas = [0.009]
        deltas_tot = [0.009, 0.009/N]
    else:
        print("Error on n_rep", 1/0)
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"v2.csv")
                            else:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"v2.csv"))
# TEST 43: theorem result analysis for tests 41, 42
if test_number == 43:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.045]
        deltas_tot = [0.045, 0.045/N]
    elif n_rep == 10:
        deltas = [0.02]
        deltas_tot = [0.02, 0.02/N]
    multiple8_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 5:
        deltas = [0.045]
        deltas_tot = [0.045, 0.045/N]
    elif n_rep == 10:
        deltas = [0.02]
        deltas_tot = [0.02, 0.02/N]
    multiple8_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 10:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 25:
        deltas = [0.035]
        deltas_tot = [0.035, 0.035/N]
    elif n_rep == 50:
        deltas = [0.015]
        deltas_tot = [0.015, 0.015/N]
    elif n_rep == 100:
        deltas = [0.009]
        deltas_tot = [0.009, 0.009/N]
    multiple9_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 25
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 10:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 25:
        deltas = [0.035]
        deltas_tot = [0.035, 0.035/N]
    elif n_rep == 50:
        deltas = [0.015]
        deltas_tot = [0.015, 0.015/N]
    elif n_rep == 100:
        deltas = [0.009]
        deltas_tot = [0.009, 0.009/N]
    multiple9_r25 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 50
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 10:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 25:
        deltas = [0.035]
        deltas_tot = [0.035, 0.035/N]
    elif n_rep == 50:
        deltas = [0.015]
        deltas_tot = [0.015, 0.015/N]
    elif n_rep == 100:
        deltas = [0.009]
        deltas_tot = [0.009, 0.009/N]
    multiple9_r50 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 100
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    if n_rep == 10:
        deltas = [0.05]
        deltas_tot = [0.05, 0.05/N]
    elif n_rep == 25:
        deltas = [0.035]
        deltas_tot = [0.035, 0.035/N]
    elif n_rep == 50:
        deltas = [0.015]
        deltas_tot = [0.015, 0.015/N]
    elif n_rep == 100:
        deltas = [0.009]
        deltas_tot = [0.009, 0.009/N]
    multiple9_r100 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)


    parameter_list = [multiple8_r5, multiple8_r10, multiple9_r10, multiple9_r25, multiple9_r50, multiple9_r100]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v8_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r5_v"+str(ver)+"_cor.csv")
            if "v8_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r10_v"+str(ver)+"_cor.csv")
            if "v9_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r10_v"+str(ver)+"_cor.csv")
            if "v9_multiple_rep25" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r25_v"+str(ver)+"_cor.csv")
            if "v9_multiple_rep50" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r50_v"+str(ver)+"_cor.csv")
            if "v9_multiple_rep100" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r100_v"+str(ver)+"_cor.csv")
                
            in_df = pd.read_csv(f_name_selected)
            counter_pcd  = dict([(d,{}) for d in deltas_selected])
            counter_pc   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb = dict([(d,{}) for d in deltas_selected])
            counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
            counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
            cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
            cc = [item for sublist in cc for item in sublist]
            
            for d in deltas_selected:
                
                in_file = in_df[in_df['delta'] == d]
                if in_file.shape[0] == 0:
                    for dd in set(in_df['delta']):
                        if np.abs(dd-d) < d*1e-7:
                            in_file = in_df[in_df['delta'] == dd]

                for size in sizes_selected:
                    df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                    df_pcd = df_pcd[cc].values
                    df_pcd = np.reshape(df_pcd,(-1,n_exps))
                    df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                    df_pc = df_pc[cc].values
                    df_pc = np.reshape(df_pc,(-1,n_exps))
                    df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                    df_pcmb = df_pcmb[cc].values
                    df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                    # counter_pcd[d][size] = []
                    # counter_pc[d][size] = []
                    # counter_pcmb[d][size] = []
                    look_pc = {}
                    look_mb = {}
                    look_el2 = {}
                    
                    for i in range(len(vars_selected)):
                        v = vars_selected[i]
                        c_pcd = 0
                        c_pc = 0
                        pc = IT_utils.get_pc(v,dag_struc)
                        look_pc[v] = pc
                        c_pcmb_mb = 0
                        mb = IT_utils.get_mb(v,dag_struc)
                        look_mb[v] = mb
                        c_pcmb = 0
                        el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                        look_el2[v] = el2

                        # if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                        #     for j in range(n_exps):
                        #         if df_pcd[i,j] != "-":
                        #             val = eval(df_pcd[i,j])
                        #             c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
                        #             val = eval(df_pc[i,j])
                        #             c_pc += 1*(any([not element in pc for element in val]))
                        #             val = eval(df_pcmb[i,j])
                        #             c_pcmb += 1*(any([not element in el2 for element in val]))
                        #         else:
                        #             c_pcd = -1
                        #             c_pc = -1
                        #             c_pcmb = -1
                        # else:
                        #     c_pcd = -1
                        #     c_pc = -1
                        #     c_pcmb = -1
                        # counter_pcd[d][size].append(c_pcd)
                        # counter_pc[d][size].append(c_pc)
                        # counter_pcmb[d][size].append(c_pcmb)

                    
                    counter_pcd_per_test[d][size] = []
                    counter_pc_per_test[d][size] = []
                    counter_pcmb_mb_per_test[d][size] = []
                    counter_pcmb_per_test[d][size] = []
                    
                    for j in range(n_exps):
                        c_pcd = -1
                        c_pc = -1
                        c_pcmb_mb = -1
                        c_pcmb = -1
                        if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            if df_pcd[i,j] != "-":
                                c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
                        counter_pcd_per_test[d][size].append(c_pcd)
                        counter_pc_per_test[d][size].append(c_pc)
                        counter_pcmb_mb_per_test[d][size].append(c_pcmb_mb)
                        counter_pcmb_per_test[d][size].append(c_pcmb)

                    # for j in range(n_exps):
                    #     res_1 = 0
                    #     res_2 = 0
                    #     res_3 = 0
                    #     for v in vars_selected:
                    #         i = vars_selected.index(v)
                    #         pcd_v = eval(df_pcd[i,j])
                    #         res_1 = res_1 + np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v])
                    #         pc_v = eval(df_pc[i,j])
                    #         res_2 = res_2 + np.sum([not el in look_pc[v] for el in pc_v])
                    #         pcmb_v = eval(df_pcmb[i,j])
                    #         res_3 = res_3 + np.sum([not el in look_el2[v] for el in pcmb_v])
                        
                    #     if res_1!=counter_pcd_per_test[d][size][j] or res_2!=counter_pc_per_test[d][size][j] or res_3!=counter_pcmb_per_test[d][size][j]:
                    #         print(res_1,counter_pcd_per_test[d][size][j])
                    #         print(res_2,counter_pc_per_test[d][size][j])
                    #         print(res_3,counter_pcmb_per_test[d][size][j])
                    #     else:
                    #         print("yo")
                    
            # print("counter_pcd_per_test",counter_pcd_per_test)
            # print("counter_pc_per_test",counter_pc_per_test)
            # print("counter_pcmb_per_test",counter_pcmb_per_test)
            # input()

            if ver == 1:
                print("Version 1")
            else:
                print("Version 2")
            print()
            if ver == 1:
                print("PCD - FPs")
            else:
                print("Mod_PCD - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[d][size]))), " max", np.max(counter_pcd_per_test[d][size]))
            if ver == 1:
                print("PC - FPs")
            else:
                print("Mod_PC - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[d][size]))), " max", np.max(counter_pc_per_test[d][size]))
            if ver == 1:
                print("PCMB - el not in MB")
            else:
                print("PCMB mod - el not in MB")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[d][size]))), " max", np.max(counter_pcmb_mb_per_test[d][size]))
            if ver == 1:
                print("PCMB - el a dist > 2")
            else:
                print("PCMB mod - el a dist > 2")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[d][size]))), " max", np.max(counter_pcmb_per_test[d][size]))
        print()
        print()
        print()
# TEST 44: ecoli error analysis + debug
if test_number == 44:
    # n_exps = 100

    # ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    # vars = [col_name for col_name in ecoli.columns]
    # ds_sizes = [250,500,1000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    # deltas_tot = [0.05/N]
    # dag_struc = "[b1191][cspG][eutG][cspA|cspG][fixC|b1191][sucA|eutG][yecO|cspG][yedE|cspG][atpG|sucA][cchB|fixC][dnaJ|sucA][flgD|sucA][gltA|sucA][lpdA|yedE][pspB|cspG:yedE][sucD|sucA][tnaA|b1191:fixC:sucA][yceP|eutG:fixC][yfiA|cspA][ygbD|fixC][ygcE|b1191:sucA][yhdM|sucA][yjbO|fixC][asnA|ygcE][atpD|sucA:ygcE][hupB|cspA:yfiA][ibpB|eutG:yceP][pspA|cspG:pspB:yedE][yfaD|eutG:sucA:yceP][icdA|asnA:ygcE][lacA|asnA:cspG][nmpC|pspA][yheI|atpD:yedE][aceB|icdA][b1963|yheI][dnaK|yheI][folK|yheI][lacY|asnA:cspG:eutG:lacA][ycgX|fixC:yheI][dnaG|ycgX:yheI][lacZ|asnA:lacA:lacY][nuoM|lacY][b1583|lacA:lacZ:yceP][mopB|dnaK:lacZ][yaeM|cspG:lacA:lacZ][ftsJ|mopB]"
    # ecoli = (vars, ds_sizes, [1,2],deltas_tot,"ecoli",dag_struc)

    # parameter_list = [ecoli]
    # for pl in parameter_list:
    #     vars_selected = pl[0]
    #     sizes_selected = pl[1]
    #     versions_selected = pl[2]
    #     deltas_selected = pl[3]
    #     method_name = pl[4]
    #     dag_struc = pl[5]
        
    #     print("RECAP for", method_name)
    #     for ver in versions_selected:
    #         if "ecoli" == method_name:
    #             f_name_selected = os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_cor.csv")
                
    #         in_df = pd.read_csv(f_name_selected)
    #         counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
    #         counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
    #         counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
    #         counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
    #         cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
    #         cc = [item for sublist in cc for item in sublist]
            
    #         for d in deltas_selected:
    #             if d-2.18497225651154e-15<1e-18 and "alarm" in method_name:  # fix for alarm ds
    #                 in_file = in_df[in_df['delta'] == 2.18497225651154e-15]
    #             elif d-2.746058882164639e-18 < 1e-20 and "ecoli" == method_name:
    #                 in_file = in_df[in_df['delta'] == 2.746058882164639e-18]
    #             else:
    #                 in_file = in_df[in_df['delta'] == d]

    #             for size in sizes_selected:
    #                 print("size",size)
    #                 df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
    #                 df_pcd = df_pcd[cc].values
    #                 df_pcd = np.reshape(df_pcd,(-1,n_exps))
    #                 df_pc = in_file[in_file["type"]=="PC_"+str(size)]
    #                 df_pc = df_pc[cc].values
    #                 df_pc = np.reshape(df_pc,(-1,n_exps))
    #                 df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
    #                 df_pcmb = df_pcmb[cc].values
    #                 df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

    #                 # counter_pcd[d][size] = []
    #                 # counter_pc[d][size] = []
    #                 # counter_pcmb[d][size] = []
    #                 look_pc = {}
    #                 look_el2 = {}
                    
    #                 for i in range(len(vars_selected)):
    #                     v = vars_selected[i]
    #                     c_pcd = 0
    #                     c_pc = 0
    #                     pc = IT_utils.get_pc(v,dag_struc)
    #                     look_pc[v] = pc
    #                     c_pcmb = 0
    #                     el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
    #                     look_el2[v] = el2

    #                 #     if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
    #                 #         for j in range(n_exps):
    #                 #             if df_pcd[i,j] != "-":
    #                 #                 val = eval(df_pcd[i,j])
    #                 #                 c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
    #                 #                 val = eval(df_pc[i,j])
    #                 #                 c_pc += 1*(any([not element in pc for element in val]))
    #                 #                 val = eval(df_pcmb[i,j])
    #                 #                 c_pcmb += 1*(any([not element in el2 for element in val]))
    #                 #             else:
    #                 #                 c_pcd = -1
    #                 #                 c_pc = -1
    #                 #                 c_pcmb = -1
    #                 #     else:
    #                 #         c_pcd = -1
    #                 #         c_pc = -1
    #                 #         c_pcmb = -1
    #                 #     counter_pcd[d][size].append(c_pcd)
    #                 #     counter_pc[d][size].append(c_pc)
    #                 #     counter_pcmb[d][size].append(c_pcmb)

                    
    #                 # counter_pcd_per_test[d][size] = []
    #                 # counter_pc_per_test[d][size] = []
    #                 # counter_pcmb_per_test[d][size] = []
                    
    #                 # for j in range(n_exps):
    #                 #     c_pcd = -1
    #                 #     c_pc = -1
    #                 #     c_pcmb = -1
    #                 #     if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
    #                 #         if df_pcd[i,j] != "-":
    #                 #             c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
    #                 #             c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
    #                 #             c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
    #                 #     counter_pcd_per_test[d][size].append(c_pcd)
    #                 #     counter_pc_per_test[d][size].append(c_pc)
    #                 #     counter_pcmb_per_test[d][size].append(c_pcmb)

    #                 for j in range(n_exps):
    #                     res_1 = 0
    #                     res_2 = 0
    #                     res_3 = 0
    #                     for v in vars_selected:
    #                         i = vars_selected.index(v)
    #                         pcd_v = eval(df_pcd[i,j])
    #                         if np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v]) > 0:
    #                             print(j,v,"   ",pcd_v)
                    
    #                 print()
    #                 print()

    # yceP test 2


    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [250,500,1000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas_tot = [0.05]
        
    versions = [2]
    ts = [2]
    test_t = ["cor"]
    
    test_type = test_t[0]
    test_n = ts[0]
    delta = deltas_tot[0]
    version = versions[0]

    for size in ds_sizes:

        data_file = os.path.join("datasets","ecoli_test","ecoli_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        if version == 1:
            get_pcd(dep_infos,"yceP",vars,delta/N, max_z_size=6, verbose = True)
        else:
            mod_pcd(dep_infos,"yceP",vars,delta,N, max_z_size=6, verbose = True)
        # IT_utils.independence(dep_infos, "ygcE", "yceP",set(),delta/N,verbose = True)
        # IT_utils.independence(dep_infos, "ygcE", "yceP",set(["eutG"]),delta/N,verbose = True)
        input()

    # res
    # 250  get_pcd(yceP) = {'eutG'}
    # 500  get_pcd(yceP) = {'eutG', 'ygcE'}
    # 1000 get_pcd(yceP) = {'eutG', 'yfaD', 'ygcE'}
# TEST 45: ecoli error analysis + debug
if test_number == 45:
    n_exps = 100

    # ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    # vars = [col_name for col_name in ecoli.columns]
    # ds_sizes = [5000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    # deltas_tot = [0.05/N]
    # dag_struc = "[b1191][cspG][eutG][cspA|cspG][fixC|b1191][sucA|eutG][yecO|cspG][yedE|cspG][atpG|sucA][cchB|fixC][dnaJ|sucA][flgD|sucA][gltA|sucA][lpdA|yedE][pspB|cspG:yedE][sucD|sucA][tnaA|b1191:fixC:sucA][yceP|eutG:fixC][yfiA|cspA][ygbD|fixC][ygcE|b1191:sucA][yhdM|sucA][yjbO|fixC][asnA|ygcE][atpD|sucA:ygcE][hupB|cspA:yfiA][ibpB|eutG:yceP][pspA|cspG:pspB:yedE][yfaD|eutG:sucA:yceP][icdA|asnA:ygcE][lacA|asnA:cspG][nmpC|pspA][yheI|atpD:yedE][aceB|icdA][b1963|yheI][dnaK|yheI][folK|yheI][lacY|asnA:cspG:eutG:lacA][ycgX|fixC:yheI][dnaG|ycgX:yheI][lacZ|asnA:lacA:lacY][nuoM|lacY][b1583|lacA:lacZ:yceP][mopB|dnaK:lacZ][yaeM|cspG:lacA:lacZ][ftsJ|mopB]"
    # ecoli = (vars, ds_sizes, [1,2],deltas_tot,"ecoli",dag_struc)

    # parameter_list = [ecoli]
    # for pl in parameter_list:
    #     vars_selected = pl[0]
    #     sizes_selected = pl[1]
    #     versions_selected = pl[2]
    #     deltas_selected = pl[3]
    #     method_name = pl[4]
    #     dag_struc = pl[5]
        
    #     print("RECAP for", method_name)
    #     for ver in versions_selected:
    #         if "ecoli" == method_name:
    #             f_name_selected = os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_cor.csv")
                
    #         in_df = pd.read_csv(f_name_selected)
    #         counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
    #         counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
    #         counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
    #         counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
    #         cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
    #         cc = [item for sublist in cc for item in sublist]
            
    #         for d in deltas_selected:
    #             if d-2.18497225651154e-15<1e-18 and "alarm" in method_name:  # fix for alarm ds
    #                 in_file = in_df[in_df['delta'] == 2.18497225651154e-15]
    #             elif d-2.746058882164639e-18 < 1e-20 and "ecoli" == method_name:
    #                 in_file = in_df[in_df['delta'] == 2.746058882164639e-18]
    #             else:
    #                 in_file = in_df[in_df['delta'] == d]

    #             for size in sizes_selected:
    #                 print("size",size)
    #                 df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
    #                 df_pcd = df_pcd[cc].values
    #                 df_pcd = np.reshape(df_pcd,(-1,n_exps))
    #                 df_pc = in_file[in_file["type"]=="PC_"+str(size)]
    #                 df_pc = df_pc[cc].values
    #                 df_pc = np.reshape(df_pc,(-1,n_exps))
    #                 df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
    #                 df_pcmb = df_pcmb[cc].values
    #                 df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

    #                 # counter_pcd[d][size] = []
    #                 # counter_pc[d][size] = []
    #                 # counter_pcmb[d][size] = []
    #                 look_pc = {}
    #                 look_el2 = {}
                    
    #                 for i in range(len(vars_selected)):
    #                     v = vars_selected[i]
    #                     c_pcd = 0
    #                     c_pc = 0
    #                     pc = IT_utils.get_pc(v,dag_struc)
    #                     look_pc[v] = pc
    #                     c_pcmb = 0
    #                     el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
    #                     look_el2[v] = el2

    #                 #     if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
    #                 #         for j in range(n_exps):
    #                 #             if df_pcd[i,j] != "-":
    #                 #                 val = eval(df_pcd[i,j])
    #                 #                 c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
    #                 #                 val = eval(df_pc[i,j])
    #                 #                 c_pc += 1*(any([not element in pc for element in val]))
    #                 #                 val = eval(df_pcmb[i,j])
    #                 #                 c_pcmb += 1*(any([not element in el2 for element in val]))
    #                 #             else:
    #                 #                 c_pcd = -1
    #                 #                 c_pc = -1
    #                 #                 c_pcmb = -1
    #                 #     else:
    #                 #         c_pcd = -1
    #                 #         c_pc = -1
    #                 #         c_pcmb = -1
    #                 #     counter_pcd[d][size].append(c_pcd)
    #                 #     counter_pc[d][size].append(c_pc)
    #                 #     counter_pcmb[d][size].append(c_pcmb)

                    
    #                 # counter_pcd_per_test[d][size] = []
    #                 # counter_pc_per_test[d][size] = []
    #                 # counter_pcmb_per_test[d][size] = []
                    
    #                 # for j in range(n_exps):
    #                 #     c_pcd = -1
    #                 #     c_pc = -1
    #                 #     c_pcmb = -1
    #                 #     if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
    #                 #         if df_pcd[i,j] != "-":
    #                 #             c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
    #                 #             c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
    #                 #             c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
    #                 #     counter_pcd_per_test[d][size].append(c_pcd)
    #                 #     counter_pc_per_test[d][size].append(c_pc)
    #                 #     counter_pcmb_per_test[d][size].append(c_pcmb)

    #                 for j in range(n_exps):
    #                     res_1 = 0
    #                     res_2 = 0
    #                     res_3 = 0
    #                     for v in vars_selected:
    #                         i = vars_selected.index(v)
    #                         pcd_v = eval(df_pcd[i,j])
    #                         if np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v]) > 0:
    #                             print(j,v,"   ",pcd_v)
                    
    #                 print()
    #                 print()

    # # lacA test 0


    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [1000,5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas_tot = [0.05]
        
    versions = [2]
    ts = [0]
    test_t = ["cor"]
    
    test_type = test_t[0]
    test_n = ts[0]
    delta = deltas_tot[0]
    version = versions[0]

    for size in ds_sizes:

        data_file = os.path.join("datasets","ecoli_test","ecoli_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        if version == 1:
            get_pcd(dep_infos,"lacA",vars,delta/N, max_z_size=6, verbose = True)
        else:
            mod_pcd(dep_infos,"lacA",vars,delta,N, max_z_size=6, verbose = True)
        # IT_utils.independence(dep_infos, "ygcE", "lacA",set(),delta/N,verbose = True)
        # IT_utils.independence(dep_infos, "ygcE", "lacA",set(["eutG"]),delta/N,verbose = True)
        input()

    # res
    # 250  get_pcd(yceP) = {'eutG'}
    # 500  get_pcd(yceP) = {'eutG', 'ygcE'}
    # 1000 get_pcd(yceP) = {'eutG', 'yfaD', 'ygcE'}
# TEST 46: test with continuous and repetitions 8 with imposed infinite power
if test_number == 46:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple8","v8_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power.csv")
        else:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+"_inf_power.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        
        dep_infos.look_dsep[key] = dep_infos.look_dsep.get(key,IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(dep_infos.n_rep)])))
        if not dep_infos.look_dsep[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test
    n_rep=input_test_number
    if n_rep not in [5,10]:
        print(1/0)
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power.csv")
                            else:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_inf_power.csv"))
# TEST 47: theorem result analysis for tests 46, 48
if test_number == 47:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 100
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple9_r100 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)


    parameter_list = [multiple8_r5,multiple8_r10,multiple9_r100]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v8_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r5_v"+str(ver)+"_cor_inf_power.csv")
            if "v8_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r10_v"+str(ver)+"_cor_inf_power.csv")
            if "v9_multiple_rep100" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r100_v"+str(ver)+"_cor_inf_power.csv")
            
            
            in_df = pd.read_csv(f_name_selected)
            counter_pcd  = dict([(d,{}) for d in deltas_selected])
            counter_pc   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb = dict([(d,{}) for d in deltas_selected])
            counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
            counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
            counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
            cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
            cc = [item for sublist in cc for item in sublist]
            
            for d in deltas_selected:
                
                in_file = in_df[in_df['delta'] == d]
                if in_file.shape[0] == 0:
                    for dd in set(in_df['delta']):
                        if np.abs(dd-d) < d*1e-7:
                            in_file = in_df[in_df['delta'] == dd]

                for size in sizes_selected:
                    df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                    df_pcd = df_pcd[cc].values
                    df_pcd = np.reshape(df_pcd,(-1,n_exps))
                    df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                    df_pc = df_pc[cc].values
                    df_pc = np.reshape(df_pc,(-1,n_exps))
                    df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                    df_pcmb = df_pcmb[cc].values
                    df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                    # counter_pcd[d][size] = []
                    # counter_pc[d][size] = []
                    # counter_pcmb[d][size] = []
                    look_pc = {}
                    look_mb = {}
                    look_el2 = {}
                    
                    for i in range(len(vars_selected)):
                        v = vars_selected[i]
                        c_pcd = 0
                        c_pc = 0
                        pc = IT_utils.get_pc(v,dag_struc)
                        look_pc[v] = pc
                        c_pcmb_mb = 0
                        mb = IT_utils.get_mb(v,dag_struc)
                        look_mb[v] = mb
                        c_pcmb = 0
                        el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                        look_el2[v] = el2

                        # if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                        #     for j in range(n_exps):
                        #         if df_pcd[i,j] != "-":
                        #             val = eval(df_pcd[i,j])
                        #             c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
                        #             val = eval(df_pc[i,j])
                        #             c_pc += 1*(any([not element in pc for element in val]))
                        #             val = eval(df_pcmb[i,j])
                        #             c_pcmb += 1*(any([not element in el2 for element in val]))
                        #         else:
                        #             c_pcd = -1
                        #             c_pc = -1
                        #             c_pcmb = -1
                        # else:
                        #     c_pcd = -1
                        #     c_pc = -1
                        #     c_pcmb = -1
                        # counter_pcd[d][size].append(c_pcd)
                        # counter_pc[d][size].append(c_pc)
                        # counter_pcmb[d][size].append(c_pcmb)

                    
                    counter_pcd_per_test[d][size] = []
                    counter_pc_per_test[d][size] = []
                    counter_pcmb_mb_per_test[d][size] = []
                    counter_pcmb_per_test[d][size] = []
                    
                    for j in range(n_exps):
                        c_pcd = -1
                        c_pc = -1
                        c_pcmb_mb = -1
                        c_pcmb = -1
                        if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            if df_pcd[i,j] != "-":
                                c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                
                        counter_pcd_per_test[d][size].append(c_pcd)
                        counter_pc_per_test[d][size].append(c_pc)
                        counter_pcmb_mb_per_test[d][size].append(c_pcmb_mb)
                        counter_pcmb_per_test[d][size].append(c_pcmb)

                    # for j in range(n_exps):
                    #     res_1 = 0
                    #     res_2 = 0
                    #     res_3 = 0
                    #     for v in vars_selected:
                    #         i = vars_selected.index(v)
                    #         pcd_v = eval(df_pcd[i,j])
                    #         res_1 = res_1 + np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v])
                    #         pc_v = eval(df_pc[i,j])
                    #         res_2 = res_2 + np.sum([not el in look_pc[v] for el in pc_v])
                    #         pcmb_v = eval(df_pcmb[i,j])
                    #         res_3 = res_3 + np.sum([not el in look_el2[v] for el in pcmb_v])
                        
                    #     if res_1!=counter_pcd_per_test[d][size][j] or res_2!=counter_pc_per_test[d][size][j] or res_3!=counter_pcmb_per_test[d][size][j]:
                    #         print(res_1,counter_pcd_per_test[d][size][j])
                    #         print(res_2,counter_pc_per_test[d][size][j])
                    #         print(res_3,counter_pcmb_per_test[d][size][j])
                    #     else:
                    #         print("yo")
                    
            # print("counter_pcd_per_test",counter_pcd_per_test)
            # print("counter_pc_per_test",counter_pc_per_test)
            # print("counter_pcmb_per_test",counter_pcmb_per_test)
            # input()

            if ver == 1:
                print("Version 1")
            else:
                print("Version 2")
            print()
            if ver == 1:
                print("PCD - FPs")
            else:
                print("Mod_PCD - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[d][size]))), " max", np.max(counter_pcd_per_test[d][size]))
            if ver == 1:
                print("PC - FPs")
            else:
                print("Mod_PC - FPs")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[d][size]))), " max", np.max(counter_pc_per_test[d][size]))
            if ver == 1:
                print("PCMB - el not in MB")
            else:
                print("PCMB mod - el not in MB")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[d][size]))), " max", np.max(counter_pcmb_mb_per_test[d][size]))
            if ver == 1:
                print("PCMB - el a dist > 2")
            else:
                print("PCMB mod - el a dist > 2")
            for size in sizes_selected:
                print("- size:", size)
                for d in deltas_selected:
                    print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[d][size]))), " max", np.max(counter_pcmb_per_test[d][size]))
        print()
        print()
        print()
# TEST 48: test with continuous and repetitions 9 with imposed infinite power
if test_number == 48:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple9","v9_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power.csv")
        else:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+"_inf_power.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        # print(zzz)
        dep_infos.look_dsep[key] = dep_infos.look_dsep.get(key,IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(dep_infos.n_rep)])))
        if not dep_infos.look_dsep[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test
    n_rep=input_test_number
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power.csv")
                            else:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_inf_power.csv"))
# TEST 49: test with continuous and repetitions 8 with imposed infinite power ONLY ON PARENTS
if test_number == 49:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple8","v8_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power_v2.csv")
        else:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+"_inf_power_v2.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dconn_and_par = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        d_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(dep_infos.n_rep)])
        dep_infos.look_dconn_and_par[key] = dep_infos.look_dconn_and_par.get(key,not IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = d_struc) and x in IT_utils.get_parents(t,d_struc))
        if dep_infos.look_dconn_and_par[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test
    n_rep=input_test_number
    if n_rep not in [5,10]:
        print(1/0)
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv")
                            else:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_inf_power_v2.csv"))
# TEST 50: theorem result analysis for tests 49, 51, 52, 53
if test_number == 50:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 100
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple9_r100 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)

    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    deltas_tot = [0.05, 0.05/N]
    ecoli1 = (vars, ds_sizes, [1,2],deltas_tot,"ecoli1","ecoli")

    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))   # diffrently from the notes, T is in vars
    deltas_tot = [0.05, 0.05/N]
    ecoli2 = (vars, ds_sizes, [1,2],deltas_tot,"ecoli2","ecoli")

    parameter_list = [multiple8_r5,multiple8_r10,multiple9_r100, ecoli1, ecoli2]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v8_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r5_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v8_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r10_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v9_multiple_rep100" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r100_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "ecoli1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "ecoli2" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_cor_inf_power_v2.csv")
            
            try:
                in_df = pd.read_csv(f_name_selected)
                counter_pcd  = dict([(d,{}) for d in deltas_selected])
                counter_pc   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                        # counter_pcd[d][size] = []
                        # counter_pc[d][size] = []
                        # counter_pcmb[d][size] = []
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                            # if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                            #     for j in range(n_exps):
                            #         if df_pcd[i,j] != "-":
                            #             val = eval(df_pcd[i,j])
                            #             c_pcd += 1*(any([not IT_utils.is_in_pcd(element,v,dag_struc) for element in val]))
                            #             val = eval(df_pc[i,j])
                            #             c_pc += 1*(any([not element in pc for element in val]))
                            #             val = eval(df_pcmb[i,j])
                            #             c_pcmb += 1*(any([not element in el2 for element in val]))
                            #         else:
                            #             c_pcd = -1
                            #             c_pc = -1
                            #             c_pcmb = -1
                            # else:
                            #     c_pcd = -1
                            #     c_pc = -1
                            #     c_pcmb = -1
                            # counter_pcd[d][size].append(c_pcd)
                            # counter_pc[d][size].append(c_pc)
                            # counter_pcmb[d][size].append(c_pcmb)

                        
                        counter_pcd_per_test[d][size] = []
                        counter_pc_per_test[d][size] = []
                        counter_pcmb_mb_per_test[d][size] = []
                        counter_pcmb_per_test[d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[d][size].append(c_pcd)
                            counter_pc_per_test[d][size].append(c_pc)
                            counter_pcmb_mb_per_test[d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[d][size].append(c_pcmb)

                        # for j in range(n_exps):
                        #     res_1 = 0
                        #     res_2 = 0
                        #     res_3 = 0
                        #     for v in vars_selected:
                        #         i = vars_selected.index(v)
                        #         pcd_v = eval(df_pcd[i,j])
                        #         res_1 = res_1 + np.sum([not IT_utils.is_in_pcd(el,v,dag_struc) for el in pcd_v])
                        #         pc_v = eval(df_pc[i,j])
                        #         res_2 = res_2 + np.sum([not el in look_pc[v] for el in pc_v])
                        #         pcmb_v = eval(df_pcmb[i,j])
                        #         res_3 = res_3 + np.sum([not el in look_el2[v] for el in pcmb_v])
                            
                        #     if res_1!=counter_pcd_per_test[d][size][j] or res_2!=counter_pc_per_test[d][size][j] or res_3!=counter_pcmb_per_test[d][size][j]:
                        #         print(res_1,counter_pcd_per_test[d][size][j])
                        #         print(res_2,counter_pc_per_test[d][size][j])
                        #         print(res_3,counter_pcmb_per_test[d][size][j])
                        #     else:
                        #         print("yo")
                        
                # print("counter_pcd_per_test",counter_pcd_per_test)
                # print("counter_pc_per_test",counter_pc_per_test)
                # print("counter_pcmb_per_test",counter_pcmb_per_test)
                # input()

                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD - FPs")
                else:
                    print("Mod_PCD - FPs")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[d][size]))), " max", np.max(counter_pcd_per_test[d][size]))
                if ver == 1:
                    print("PC - FPs")
                else:
                    print("Mod_PC - FPs")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[d][size]))), " max", np.max(counter_pc_per_test[d][size]))
                if ver == 1:
                    print("PCMB - el not in MB")
                else:
                    print("PCMB mod - el not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[d][size]))), " max", np.max(counter_pcmb_mb_per_test[d][size]))
                if ver == 1:
                    print("PCMB - el a dist > 2")
                else:
                    print("PCMB mod - el a dist > 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[d][size]))), " max", np.max(counter_pcmb_per_test[d][size]))
            except:
                print("--  Error --")
        print()
        print()
        print()
# TEST 51: test with continuous and repetitions 9 with imposed infinite power ONLY ON PARENTS
if test_number == 51:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple9","v9_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power_v2.csv")
        else:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+"_inf_power_v2.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dconn_and_par = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        # print(zzz)
        d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(dep_infos.n_rep)])
        dep_infos.look_dconn_and_par[key] = dep_infos.look_dconn_and_par.get(key,not IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = d_struc) and x in IT_utils.get_parents(t,d_struc))
        if dep_infos.look_dconn_and_par[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test
    n_rep=input_test_number
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv")
                            else:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_inf_power_v2.csv"))
# TEST 52: ecoli test with infinite power 
if test_number == 52:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]

        data_file = os.path.join("datasets","ecoli_test","ecoli_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power_v1.csv")
        else:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+"_inf_power_v1.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}

        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=4)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=4)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=4)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=4)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=4)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=4)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        # print(zzz)
        dep_infos.look_dsep[key] = dep_infos.look_dsep.get(key,IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = "ecoli"))
        if not dep_infos.look_dsep[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test

    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05,0.02]
    deltas_tot = [0.05, 0.02, 0.05/N, 0.02/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    
    # for size in ds_sizes:
    #     # create insurance subdataset and additional elements to perform independence test
    #     if not os.path.exists(os.path.join("datasets","multiple7","v7_S"+str(size)+"_t99_r"+str(n_rep)+".csv")):
    #         p = subprocess.call(("python", "ids_creator.py -t 7 -a "+str(size)+" -b 10 -n "+str(n_rep)))
    #         # rint("begin creation for size", size)
    #         # os.system("python ids_creator.py -a "+str(size)+" -b 10 -n "+str(n_rep))
    #         print("ended creation for size", size)
    # print("Dataset creation done")

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g) for a,b,c,d,e,f,g in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions)))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+test_type+"_ecoli_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v1.csv")
                            else:
                                name = os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+test_type+"_ecoli_delta"+str(d)+"_inf_power_v1.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+tt+"_ecoli_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v1.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+tt+"_ecoli_delta"+str(d)+"_inf_power_v1.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_"+tt+"_inf_power_v1.csv"))
# TEST 53: ecoli test with infinite power ONLY PARENTS
if test_number == 53:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]

        data_file = os.path.join("datasets","ecoli_test","ecoli_s"+str(size)+"_t"+str(test_n)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power_v2.csv")
        else:
            indep_file = os.path.join("IT_results","ecoli_test",test_type+"_ecoli_s"+str(size)+"_t"+str(test_n)+"_delta"+str(delta/N)+"_inf_power_v2.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dconn_and_par = {}

        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=4)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=4)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=4)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=4)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=4)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=4)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    

    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        # print(zzz)
        dep_infos.look_dconn_and_par[key] = dep_infos.look_dconn_and_par.get(key,not IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = "ecoli") and x in IT_utils.get_parents(t,"ecoli"))
        if dep_infos.look_dconn_and_par[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test

    ecoli = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s1000_t0.csv"))
    vars = [col_name for col_name in ecoli.columns]
    ds_sizes = [50,100,250]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05,0.02]
    deltas_tot = [0.05, 0.02, 0.05/N, 0.02/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    
    # for size in ds_sizes:
    #     # create insurance subdataset and additional elements to perform independence test
    #     if not os.path.exists(os.path.join("datasets","multiple7","v7_S"+str(size)+"_t99_r"+str(n_rep)+".csv")):
    #         p = subprocess.call(("python", "ids_creator.py -t 7 -a "+str(size)+" -b 10 -n "+str(n_rep)))
    #         # rint("begin creation for size", size)
    #         # os.system("python ids_creator.py -a "+str(size)+" -b 10 -n "+str(n_rep))
    #         print("ended creation for size", size)
    # print("Dataset creation done")

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g) for a,b,c,d,e,f,g in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions)))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+test_type+"_ecoli_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv")
                            else:
                                name = os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+test_type+"_ecoli_delta"+str(d)+"_inf_power_v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+tt+"_ecoli_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","ecoli_test","res_on100_"+str(ver)+"_"+tt+"_ecoli_delta"+str(d)+"_inf_power_v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_ecoli_v"+str(ver)+"_"+tt+"_inf_power_v2.csv"))
# TEST 54: test with continuous and repetitions 10 with imposed infinite power ONLY ON PARENTS
if test_number == 54:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power_v2.csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+"_inf_power_v2.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dconn_and_par = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        # print(zzz)
        d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,dep_infos.n_rep)])
        dep_infos.look_dconn_and_par[key] = dep_infos.look_dconn_and_par.get(key,not IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = d_struc) and x in IT_utils.get_parents(t,d_struc))
        if dep_infos.look_dconn_and_par[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_inf_power_v2.csv"))
# TEST 55: theorem result analysis for tests 54
if test_number == 55:
    n_exps = 100

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_infinite_power",d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_infinite_power",d_struc)

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    parameter_list = [multiple10_r10, multiple10_r20]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v10_multiple_rep10_infinite_power" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "v10_multiple_rep20_infinite_power" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "v10_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v10_multiple_rep20" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor_inf_power_v2.csv")
            
            try:
                in_df = pd.read_csv(f_name_selected)
                counter_pcd  = dict([(d,{}) for d in deltas_selected])
                counter_pc   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test = dict([(d,{}) for d in deltas_selected])
                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))

                        # counter_pcd[d][size] = []
                        # counter_pc[d][size] = []
                        # counter_pcmb[d][size] = []
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[d][size] = []
                        counter_pc_per_test[d][size] = []
                        counter_pcmb_mb_per_test[d][size] = []
                        counter_pcmb_per_test[d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[d][size].append(c_pcd)
                            counter_pc_per_test[d][size].append(c_pc)
                            counter_pcmb_mb_per_test[d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[d][size].append(c_pcmb)


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD - FPs")
                else:
                    print("Mod_PCD - FPs")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[d][size]))), " max", np.max(counter_pcd_per_test[d][size]))
                if ver == 1:
                    print("PC - FPs")
                else:
                    print("Mod_PC - FPs")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[d][size]))), " max", np.max(counter_pc_per_test[d][size]))
                if ver == 1:
                    print("PCMB - el not in MB")
                else:
                    print("PCMB mod - el not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[d][size]))), " max", np.max(counter_pcmb_mb_per_test[d][size]))
                if ver == 1:
                    print("PCMB - el a dist > 2")
                else:
                    print("PCMB mod - el a dist > 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d)," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[d][size]))), " max", np.max(counter_pcmb_per_test[d][size]))
            except:
                print("--  Error --")
        print()
        print()
        print()
# TEST 56: test with continuous and repetitions 10 with imposed infinite power 
if test_number == 56:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+"_inf_power_v1.csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+"_inf_power_v1.csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    def max_power_IT_test(dep_infos,t,x,z, delta = -1):
        key = "("+str(t)+","+str(x)+","+IT_utils.convert_to_string(z)+")"

        zzz = ["()"]
        if len(z) != 0:
            zzz = [''.join(x for x in str(zz) if x.isdigit() or x.isalpha()) for zz in z]  # selecting only letters and numbers
        # print(zzz)
        d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,dep_infos.n_rep)])
        # dep_infos.look_dconn_and_par[key] = dep_infos.look_dconn_and_par.get(key,not IT_utils.get_dseparation(t,x,zzz, 
        #     dag_struc = d_struc) and x in IT_utils.get_parents(t,d_struc))
        dep_infos.look_dsep[key] = dep_infos.look_dsep.get(key,IT_utils.get_dseparation(t,x,zzz, 
            dag_struc = d_struc))
        if not dep_infos.look_dsep[key]:
            method = dep_infos.method
            independence_method = dep_infos.independence_method
            data_file = dep_infos.data_file
            df = dep_infos.df
            indep_file = dep_infos.indep_file
            save_every_update = dep_infos.save_every_update

            a_row = pd.DataFrame([[t,x,IT_utils.convert_to_string(z), 0,0,0]], columns = ["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            dep_infos.df = pd.concat([df, a_row])
            if indep_file != "" and (save_every_update or dep_infos.save_counter==1000):
                IT_utils.save_IT_dataframe(dep_infos)
                dep_infos.save_counter = 0
            # print("done",t, x, z_str)
            dep_infos.save_counter += 1
            return 0
        else:
            # returns conditional association of T and X given Z

            # preprocessing of z keeping only letters or numbers
            # z = ''.join(x for x in str(sorted(z)) if x.isalpha() or x.isnumeric())
            z_str = IT_utils.convert_to_string(z)
            df = dep_infos.df
            method = dep_infos.method
            # if x in z or t in z:
            #     print(t,x,z)
            #     print(traceback.print_stack())
            #     return None
            lookup_table = df[df["first_term"] == t]
            # print(lookup_table)
            lookup_table = lookup_table[lookup_table["second_term"] == x]
            # print(lookup_table)
            if len(z_str) == 0:
                z_str = "()"
            lookup_table = lookup_table[lookup_table["cond_set"] == z_str]
            # print(lookup_table)
            if len(lookup_table)>0:
                if method == "p-value":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "pearson":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "dsep":
                    return -lookup_table["p_value"].values[0]  # the stronger the association, the lower the p-value for independence
                if method == "statistic":
                    return lookup_table["statistic"].values[0]
                if method == "summands":
                    return lookup_table["lb"].values[0]   #no need of "-"" sign since the higher the lb, the higher the stronger the dependence (opposite of p values)
                if method == "summands_naive":
                    return lookup_table["naive_lb"].values[0]
                if method == "summands_mb":
                    return lookup_table["multiple_lb"].values[0]
                if method == "summands_lb":
                    return lookup_table["single_lb"].values[0]
                print("METHOD NOT FOUND")
            else:
                if method == "dsep":
                    IT_utils.calculate_dsep_dependency_alarm(dep_infos, t, x, z_str, z)
                elif method == "summands":
                    if not dep_infos.summands_initialized:
                        init_dep_infos_summands(dep_infos)
                    IT_utils.calculate_summands_score(dep_infos,t,x,z_str,z,delta)
                else:
                    if dep_infos.independence_language == "R":
                        IT_utils.calculate_dependency_hardcoded(dep_infos, t, x, z_str, z)
                    else:
                        IT_utils.calculate_dependency_python(dep_infos, t, x, z_str, z)
                return IT_utils.association(dep_infos,t,x,z)

    IT_utils.association = max_power_IT_test
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v1.csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v1.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_inf_power_v1.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_inf_power_v1.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_inf_power_v1.csv"))
# TEST 57: theorem result analysis with FPs and FNs
if test_number == 57:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 50
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple9_r50 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10_ip = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_infinite_power_v1",d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20_ip = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_infinite_power_v1",d_struc)

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple8_r5, multiple8_r10, multiple9_r50, multiple10_r10_ip, multiple10_r20_ip, multiple10_r10, multiple10_r20]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v10_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor.csv")
            if "v10_multiple_rep20" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor.csv")
            if "v10_multiple_rep10_infinite_power_1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "v10_multiple_rep20_infinite_power_1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "v10_multiple_rep10_infinite_power_2" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v10_multiple_rep20_infinite_power_2" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v9_multiple_rep50" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r50_v"+str(ver)+"_cor_v2.csv")
            if "v8_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r5_v"+str(ver)+"_corv2.csv")
            if "v8_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r10_v"+str(ver)+"_corv2.csv")

            try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["TP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["TP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["TP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["TP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            except:
                print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 58: test with continuous and repetitions 9  
if test_number == 58:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple9","v9_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_v2.csv")
                            else:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_v2.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_v2.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_v2.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_v2.csv"))
# TEST 59: check FPs and FNs correctness
if test_number == 59:
    n_exps = 100

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 50
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple9_r50 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10_ip = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_infinite_power_v1",d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20_ip = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_infinite_power_v1",d_struc)

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    parameter_list = [multiple8_r5, multiple8_r10, multiple9_r50, multiple10_r10_ip, multiple10_r20_ip, multiple10_r10, multiple10_r20]
    
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v10_multiple_rep10_infinite_power_v1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "v10_multiple_rep20_infinite_power_v1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor_inf_power_v1.csv")
            if "v10_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v10_multiple_rep20" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor_inf_power_v2.csv")
            if "v9_multiple_rep50" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r50_v"+str(ver)+"_cor_v2.csv")
            if "v8_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r5_v"+str(ver)+"_corv2.csv")
            if "v8_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r10_v"+str(ver)+"_corv2.csv")

            try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        if not(d==0.05 or size not in [5000,10000]):
                                
                            print("size",size,"ver",ver,"d",d)
                            df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                            df_pcd = df_pcd[cc].values
                            df_pcd = np.reshape(df_pcd,(-1,n_exps))
                            df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                            df_pc = df_pc[cc].values
                            df_pc = np.reshape(df_pc,(-1,n_exps))
                            df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                            df_pcmb = df_pcmb[cc].values
                            df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                            look_pc = {}
                            look_mb = {}
                            look_el2 = {}
                            
                            for i in range(len(vars_selected)):
                                v = vars_selected[i]
                                c_pcd = 0
                                c_pc = 0
                                pc = IT_utils.get_pc(v,dag_struc)
                                look_pc[v] = pc
                                c_pcmb_mb = 0
                                mb = IT_utils.get_mb(v,dag_struc)
                                look_mb[v] = mb
                                c_pcmb = 0
                                el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                                look_el2[v] = el2

                            counter_pcd_per_test[dic_key]["FP"][d][size] = []
                            counter_pc_per_test[dic_key]["FP"][d][size] = []
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                            counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                            counter_pcd_per_test[dic_key]["FN"][d][size] = []
                            counter_pc_per_test[dic_key]["FN"][d][size] = []
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                            counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                            
                            for j in range(n_exps):
                                c_pcd = -1
                                c_pc = -1
                                c_pcmb_mb = -1
                                c_pcmb = -1
                                if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                    if df_pcd[i,j] != "-":
                                        c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                        c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                        c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                        c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                        
                                counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                                counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                                counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                                counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)
                                # if c_pcd + c_pc + c_pcmb_mb + c_pcmb >0:
                                #     for i in range(len(vars_selected)):
                                #         if any([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for element in eval(df_pcd[i,j])]):
                                #             print("FP for variable", vars_selected[i], "exp",j)
                                #             print(c_pcd,"found:",eval(df_pcd[i,j])," real:",look_pc[vars_selected[i]])
                                #             print(c_pc,"found:",eval(df_pc[i,j])," real:",look_pc[vars_selected[i]])
                                #             print(c_pcmb_mb,"found:",eval(df_pcmb[i,j])," real:",look_mb[vars_selected[i]])
                                #             print(c_pcmb,"found:",eval(df_pcmb[i,j])," real:",look_el2[vars_selected[i]])
                                #             print()
                                #             print()
                                #             input()

                                c_pcd = -1
                                c_pc = -1
                                c_pcmb_mb = -1
                                c_pcmb = -1
                                if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                    if df_pcd[i,j] != "-":
                                        c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                        c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                        c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                        c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                        
                                counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                                counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                                counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                                counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)

                                # if c_pcd + c_pc + c_pcmb_mb + c_pcmb >0:                               
                                #     for i in range(len(vars_selected)):
                                #         if any([not element in eval(df_pcmb[i,j]) for element in look_mb[vars_selected[i]]]):
                                #             print("FN for variable", vars_selected[i], "exp",j)
                                #             print(c_pcd,"found:",eval(df_pcd[i,j])," real:",look_pc[vars_selected[i]])
                                #             print(c_pc,"found:",eval(df_pc[i,j])," real:",look_pc[vars_selected[i]])
                                #             print(c_pcmb_mb,"found:",eval(df_pcmb[i,j])," real:",look_mb[vars_selected[i]])
                                #             print(c_pcmb,"found:",eval(df_pcmb[i,j])," real:",look_el2[vars_selected[i]])
                                #             print()
                                #             print()
                                #             input()
                            print()
            except:
                print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

        print()
        print()
        print()
# TEST 60: test with continuous and repetitions 10 
if test_number == 60:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+".csv"))
# TEST 61: test with continuous and repetitions 11 
if test_number == 61:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple11","v11_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple11",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple11",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)] + ["E"+str(i+1) for i in range(n_rep)] + ["F"+str(i+1) for i in range(n_rep)] + ["G"+str(i+1) for i in range(n_rep)] + ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)] + ["O"+str(i+1) for i in range(6*n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple11","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","multiple11","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple11","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple11","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont11_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+".csv"))
# TEST 62: theorem result analysis with FPs and FNs for tests 11
if test_number == 62:
    n_exps = 100

    n_rep = 1
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)]+ ["E"+str(i+1) for i in range(n_rep)]+ ["F"+str(i+1) for i in range(n_rep)]+ ["G"+str(i+1) for i in range(n_rep)]+ ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)] + ["O"+str(i+1) for i in range(6*n_rep)] 
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][D"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"][E"+str(i+1)+"|B"+str(i+1)+":D"+str(i+1)+"][F"+str(i+1)+"|C"+str(i+1)+":D"+str(i+1)+"][G"+str(i+1)+"|E"+str(i+1)+":F"+str(i+1)+"][H"+str(i+1)+"|G"+str(i+1)+"][I"+str(i+1)+"|G"+str(i+1)+"]" for i in range(n_rep)]) + "".join(["[O"+str(i+1)+"]" for i in range(6*n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple11_r1 = (vars, ds_sizes, [1,2],deltas_tot,"v11_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 2
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)]+ ["E"+str(i+1) for i in range(n_rep)]+ ["F"+str(i+1) for i in range(n_rep)]+ ["G"+str(i+1) for i in range(n_rep)]+ ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)] + ["O"+str(i+1) for i in range(6*n_rep)] 
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][D"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"][E"+str(i+1)+"|B"+str(i+1)+":D"+str(i+1)+"][F"+str(i+1)+"|C"+str(i+1)+":D"+str(i+1)+"][G"+str(i+1)+"|E"+str(i+1)+":F"+str(i+1)+"][H"+str(i+1)+"|G"+str(i+1)+"][I"+str(i+1)+"|G"+str(i+1)+"]" for i in range(n_rep)]) + "".join(["[O"+str(i+1)+"]" for i in range(6*n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple11_r2 = (vars, ds_sizes, [1,2],deltas_tot,"v11_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 3
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)]+ ["E"+str(i+1) for i in range(n_rep)]+ ["F"+str(i+1) for i in range(n_rep)]+ ["G"+str(i+1) for i in range(n_rep)]+ ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)] + ["O"+str(i+1) for i in range(6*n_rep)] 
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][D"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"][E"+str(i+1)+"|B"+str(i+1)+":D"+str(i+1)+"][F"+str(i+1)+"|C"+str(i+1)+":D"+str(i+1)+"][G"+str(i+1)+"|E"+str(i+1)+":F"+str(i+1)+"][H"+str(i+1)+"|G"+str(i+1)+"][I"+str(i+1)+"|G"+str(i+1)+"]" for i in range(n_rep)]) + "".join(["[O"+str(i+1)+"]" for i in range(6*n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple11_r3 = (vars, ds_sizes, [1,2],deltas_tot,"v11_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)]+ ["E"+str(i+1) for i in range(n_rep)]+ ["F"+str(i+1) for i in range(n_rep)]+ ["G"+str(i+1) for i in range(n_rep)]+ ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)] + ["O"+str(i+1) for i in range(6*n_rep)] 
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][D"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"][E"+str(i+1)+"|B"+str(i+1)+":D"+str(i+1)+"][F"+str(i+1)+"|C"+str(i+1)+":D"+str(i+1)+"][G"+str(i+1)+"|E"+str(i+1)+":F"+str(i+1)+"][H"+str(i+1)+"|G"+str(i+1)+"][I"+str(i+1)+"|G"+str(i+1)+"]" for i in range(n_rep)]) + "".join(["[O"+str(i+1)+"]" for i in range(6*n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple11_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v11_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 1
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)]+ ["E"+str(i+1) for i in range(n_rep)]+ ["F"+str(i+1) for i in range(n_rep)]+ ["G"+str(i+1) for i in range(n_rep)]+ ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][D"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"][E"+str(i+1)+"|B"+str(i+1)+":D"+str(i+1)+"][F"+str(i+1)+"|C"+str(i+1)+":D"+str(i+1)+"][G"+str(i+1)+"|E"+str(i+1)+":F"+str(i+1)+"][H"+str(i+1)+"|G"+str(i+1)+"][I"+str(i+1)+"|G"+str(i+1)+"]" for i in range(n_rep)]) 
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple12_r1 = (vars, ds_sizes, [1,2],deltas_tot,"v12_multiple_rep"+str(n_rep),dag_struc)


    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_11.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_11.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_11.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_11.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple11_r1,multiple11_r2,multiple11_r3,multiple11_r5,multiple12_r1]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v11" in method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont11_r"+method_name[-1]+"_v"+str(ver)+"_cor.csv")
            if "v12" in method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont12_r"+method_name[-1]+"_v"+str(ver)+"_cor.csv")
            
            try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            except:
                print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_11.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_11.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_11.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_11.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 63: test with continuous and repetitions 11 
if test_number == 63:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple12","v12_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple12",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple12",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N, max_z_size=2)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N, max_z_size=2)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, max_z_size=2)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N, max_z_size=2)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N, max_z_size=2)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, max_z_size=2)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)] + ["E"+str(i+1) for i in range(n_rep)] + ["F"+str(i+1) for i in range(n_rep)] + ["G"+str(i+1) for i in range(n_rep)] + ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)] 
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple12","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","multiple12","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple12","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple12","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont12_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+".csv"))
# TEST 64: theorem result analysis with FPs and FNs for tests 11
if test_number == 64:
    n_exps = 100

    n_rep = 1
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["D"+str(i+1) for i in range(n_rep)]+ ["E"+str(i+1) for i in range(n_rep)]+ ["F"+str(i+1) for i in range(n_rep)]+ ["G"+str(i+1) for i in range(n_rep)]+ ["H"+str(i+1) for i in range(n_rep)] + ["I"+str(i+1) for i in range(n_rep)]
    dag_struc = "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][D"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"][E"+str(i+1)+"|B"+str(i+1)+":D"+str(i+1)+"][F"+str(i+1)+"|C"+str(i+1)+":D"+str(i+1)+"][G"+str(i+1)+"|E"+str(i+1)+":F"+str(i+1)+"][H"+str(i+1)+"|G"+str(i+1)+"][I"+str(i+1)+"|G"+str(i+1)+"]" for i in range(n_rep)]) 
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple12_r1 = (vars, ds_sizes, [1,2],deltas_tot,"v12_multiple_rep"+str(n_rep),dag_struc)


    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_12.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_12.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_12.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_12.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple12_r1]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v11" in method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont11_r"+method_name[-1]+"_v"+str(ver)+"_cor.csv")
            if "v12" in method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont12_r"+method_name[-1]+"_v"+str(ver)+"_cor.csv")
            
            try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            except:
                print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_12.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_12.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_12.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_12.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 65: test 8 rep 5 with no max size
if test_number == 65:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple8","v8_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple8",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars

    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"v2_no_maxsize.csv")
                            else:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"v2_no_maxsize.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"v2_no_maxsize.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"v2_no_maxsize.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"v2_no_maxsize.csv"))
# TEST 66: test 9 with no max size
if test_number == 66:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple9","v9_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple9",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_v2_no_max_size.csv")
                            else:
                                name = os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_v2_no_max_size.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_v2_no_max_size.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple9","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_v2_no_max_size.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_v2_no_max_size.csv"))
# TEST 67: test with continuous and repetitions 10  no max size
if test_number == 67:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size.csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_no_max_size.csv"))
# TEST 68: RADEMACHER test on multiple 8 rep 1 2 3 - 4 only
if test_number == 68:
    for n_rep in range(4,5):
        vvs = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
        couples_dict = rad_utils.select_candidates(vvs)
        for s in [100,250,500,1000,5000,10000]:
            for t in range(100):
                # os.path.join("datasets","multiple8","v8_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv")
                filename = "v8_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv"
                r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","multiple8",filename), vvs, delta = 0.05, couples_dict= couples_dict)
                rad_utils.write_on_csv(os.path.join("IT_results","multiple8","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 69: RADEMACHER test on multiple 8 rep 5
if test_number == 69:
    for n_rep in [5]:
        vvs = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
        couples_dict = rad_utils.select_candidates(vvs)
        for s in [100,250,500,1000,5000,10000]:
            for t in range(100):
                # os.path.join("datasets","multiple8","v8_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv")
                filename = "v8_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv"
                r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","multiple8",filename), vvs, delta = 0.05, couples_dict= couples_dict)
                rad_utils.write_on_csv(os.path.join("IT_results","multiple8","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 70: PCMB with RADEMACHERS on test 8
if test_number == 70:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        filename = "v8_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv"
                
        data_file = os.path.join("datasets","multiple8","v8_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple8","RAD_"+filename)
        else:
            indep_file = os.path.join("IT_results","multiple8","RAD_"+filename)
        
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        #     except:
        #         print("!!!",indep_file)
        #         indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        #         indep_df.to_csv(indep_file, sep = ";")
        # else:
        #     indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        #     indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="-")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars: # in these exps, N = 1 since rademacher already performs MHT correction 
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)

        # IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["Rademacher"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1], # no need to add N for correction of deltas since rademacher is already corrected
                versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv")
                            else:
                                name = os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple8","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+".csv"))
# TEST 71: theorem result analysis with FPs and FNs
if test_number == 71:
    n_exps = 100

    n_rep = 1
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r1 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 2
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r2 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 3
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r3 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 1
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    multiple8_r1_RAD = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep)+"_RAD",dag_struc)

    n_rep = 2
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    multiple8_r2_RAD = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep)+"_RAD",dag_struc)

    # RAD 3 not ready yet
    n_rep = 3
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    multiple8_r3_RAD = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep)+"_RAD",dag_struc)

    n_rep = 5
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 10
    vars = ["A"+str(i+1) for i in range(n_rep)] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[A"+str(i+1)+"][B"+str(i+1)+"|A"+str(i+1)+"][C"+str(i+1)+"|A"+str(i+1)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple8_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v8_multiple_rep"+str(n_rep),dag_struc)

    n_rep = 50
    vars = ["A1"] + ["B1"] + ["C1"] + ["T1"] + ["E"+str(i+1) for i in range(n_rep)]
    dag_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]"+ "".join(["[E"+str(i+1)+"]" for i in range(n_rep)])
    ds_sizes = [100,250,500,1000,5000,10000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple9_r50 = (vars, ds_sizes, [1,2],deltas_tot,"v9_multiple_rep"+str(n_rep),dag_struc)


    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 10
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r10_mb_no_fp = (vars, ds_sizes, [1,2],deltas_tot,"MB_NO_FP_v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 20
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r20 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)


    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_71.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_71.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_71.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_71.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple8_r1, multiple8_r2, multiple8_r3, multiple8_r1_RAD, multiple8_r2_RAD, 
        multiple8_r3_RAD, 
        multiple8_r5, multiple8_r10, multiple9_r50, multiple10_r10, multiple10_r10_mb_no_fp, multiple10_r20]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v10_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cor.csv")
            if "v10_multiple_rep20" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r20_v"+str(ver)+"_cor.csv")
            if "v9_multiple_rep50" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont9_r50_v"+str(ver)+"_cor_v2.csv")
            if "v8_multiple_rep1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r1_v"+str(ver)+"_corv2.csv")
            if "v8_multiple_rep2" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r2_v"+str(ver)+"_corv2.csv")
            if "v8_multiple_rep3" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r3_v"+str(ver)+"_corv2.csv")
            if "v8_multiple_rep1_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r1_v"+str(ver)+"_Rademacher.csv")
            if "v8_multiple_rep2_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r2_v"+str(ver)+"_Rademacher.csv")
            if "v8_multiple_rep3_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r3_v"+str(ver)+"_Rademacher.csv")
            if "v8_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r5_v"+str(ver)+"_corv2_no_maxsize.csv")
            if "v8_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont8_r10_v"+str(ver)+"_corv2_no_maxsize.csv")
            if "MB_NO_FP_v10_multiple_rep10" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r10_v"+str(ver)+"_cortest_all_els.csv")

            try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["TP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["TP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["TP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["TP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            except:
                print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_71.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_71.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_71.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_71.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 72: test with continuous and repetitions 10 ON MODIFIED PCMB TESTING ALL DEPS
if test_number == 72:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N, test_with_all_els = True)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, test_with_all_els = True)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"test_all_els.csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"test_all_els.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"test_all_els.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"test_all_els.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"test_all_els.csv"))
# TEST 73: RADEMACHER test on multiple 10 rep 5
if test_number == 73:
    for n_rep in [5]:
        vvs = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
        couples_dict = rad_utils.select_candidates(vvs)
        for s in [10000,25000,50000]:
            for t in range(100):
                # os.path.join("datasets","multiple8","v8_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv")
                filename = "v10_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv"
                r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","multiple10",filename), vvs, delta = 0.05, couples_dict= couples_dict)
                rad_utils.write_on_csv(os.path.join("IT_results","multiple10","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 74: test with continuous and repetitions 10  no max size ---  ds sizes up to 50k
if test_number == 74:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")

        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        mods_mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
                mods_mbs[v]  = pcmb(dep_infos,v,vars,delta/N,test_with_all_els=True)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)
                mods_mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, test_with_all_els=True)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version, mods_mbs
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000,25000,50000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = 2*len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version,mod_PCMB in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = {}
                pcs[version][d][test_type][size][test_n] = {}
                mbs[version][d][test_type][size][test_n] = {}

            for test_n, d, test_type, pcd,pc,mb,version,mod_PCMB in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n][False] = pcd
                pcs[version][d][test_type][size][test_n][False] = pc
                mbs[version][d][test_type][size][test_n][False] = mb
                pcds[version][d][test_type][size][test_n][True] = pcd
                pcs[version][d][test_type][size][test_n][True] = pc
                mbs[version][d][test_type][size][test_n][True] = mod_PCMB

            if (True): #data saving part
                for mod_PCMB in [True,False]:
                    for ver in versions:
                        for test_type in test_t:
                            #result resume creation
                            for d in deltas_tot:
                                if d < 0.0001:
                                    name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv")
                                else:
                                    name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv")
                                cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                                cc = ["type"] + [item for sublist in cc for item in sublist]
                                data = []
                                for s in ds_sizes:
                                    row = []
                                    row.append("PCD_"+str(s))
                                    dd = pcds.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                    row = []
                                    row.append("PC_"+str(s))
                                    dd = pcs.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                    row = []
                                    row.append("PCMB_"+str(s))
                                    dd = mbs.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                res_df = pd.DataFrame(data,columns=cc)
                                res_df.to_csv(name)
                        
            if (True): #data merging part
                for mod_PCMB in [True,False]:
                    for ver in versions:
                        for tt in test_t:
                                
                            in_files = []
                            for d in deltas_tot:
                                if d < 0.0001:
                                    in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv"),sep = ",") )
                                else:
                                    in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv"),sep = ",") )
                            for i in range(len(in_files)):
                                in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                                in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                            out_df = pd.DataFrame(columns=in_files[0].columns)
                            for f in in_files:
                                out_df=out_df.append(f)

                            cols = list(out_df.columns)
                            cols = cols[-2:] + cols[:-2]
                            out_df = out_df[cols]
                            out_df = out_df.sort_values("key")
                            out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv"))
# TEST 75: test with continuous and repetitions 10  no max size ---  ds sizes up to 50k  RADEMACHERS 
if test_number == 75:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]
        
        filename = "v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv"
                
        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10","RAD_"+filename)
        else:
            indep_file = os.path.join("IT_results","multiple10","RAD_"+filename)
        
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        #     except:
        #         print("!!!",indep_file)
        #         indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        #         indep_df.to_csv(indep_file, sep = ";")
        # else:
        #     indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        #     indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="-")


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        mods_mbs = {}
        if version == 1:
            for v in vars:  
                pcds[v] = get_pcd(dep_infos,v,vars,delta/N)
                pcs[v]  = get_pc(dep_infos,v,vars,delta/N)
                mbs[v]  = pcmb(dep_infos,v,vars,delta/N)
                mods_mbs[v]  = pcmb(dep_infos,v,vars,delta/N,test_with_all_els=True)
        else:
            for v in vars:  
                pcds[v] = mod_pcd(dep_infos,v,vars,delta, N)
                pcs[v]  = mod_pc(dep_infos,v,vars,delta, N)
                mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N)
                mods_mbs[v]  = pcmb_mod(dep_infos,v,vars,delta, N, test_with_all_els=True)

        # IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version, mods_mbs
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000,25000,50000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
        
    versions = [1,2]
    ts = [i for i in range(100)]
    test_t = ["Rademacher"]
    tot_els = 2*len(list(itertools.product(test_t, ts, [vars],deltas,[1],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version,mod_PCMB in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = {}
                pcs[version][d][test_type][size][test_n] = {}
                mbs[version][d][test_type][size][test_n] = {}

            for test_n, d, test_type, pcd,pc,mb,version,mod_PCMB in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n][False] = pcd
                pcs[version][d][test_type][size][test_n][False] = pc
                mbs[version][d][test_type][size][test_n][False] = mb
                pcds[version][d][test_type][size][test_n][True] = pcd
                pcs[version][d][test_type][size][test_n][True] = pc
                mbs[version][d][test_type][size][test_n][True] = mod_PCMB

            if (True): #data saving part
                for mod_PCMB in [True,False]:
                    for ver in versions:
                        for test_type in test_t:
                            #result resume creation
                            for d in deltas_tot:
                                if d < 0.0001:
                                    name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv")
                                else:
                                    name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv")
                                cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                                cc = ["type"] + [item for sublist in cc for item in sublist]
                                data = []
                                for s in ds_sizes:
                                    row = []
                                    row.append("PCD_"+str(s))
                                    dd = pcds.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                    row = []
                                    row.append("PC_"+str(s))
                                    dd = pcs.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                    row = []
                                    row.append("PCMB_"+str(s))
                                    dd = mbs.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                res_df = pd.DataFrame(data,columns=cc)
                                res_df.to_csv(name)
                        
            if (True): #data merging part
                for mod_PCMB in [True,False]:
                    for ver in versions:
                        for tt in test_t:
                                
                            in_files = []
                            for d in deltas_tot:
                                if d < 0.0001:
                                    in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv"),sep = ",") )
                                else:
                                    in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv"),sep = ",") )
                            for i in range(len(in_files)):
                                in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                                in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                            out_df = pd.DataFrame(columns=in_files[0].columns)
                            for f in in_files:
                                out_df=out_df.append(f)

                            cols = list(out_df.columns)
                            cols = cols[-2:] + cols[:-2]
                            out_df = out_df[cols]
                            out_df = out_df.sort_values("key")
                            out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_no_max_size"+mod_PCMB*"mod_PCMB"+".csv"))
# TEST 76: RADEMACHER test on multiple 10 rep 4
if test_number == 76:
    for n_rep in [input_test_number]:
        vvs = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
        couples_dict = rad_utils.select_candidates(vvs)
        for s in [100,250,500,1000,5000,10000,25000,50000]:
            for t in range(100):
                # os.path.join("datasets","multiple8","v8_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv")
                filename = "v10_S"+str(s)+"_t"+str(t)+"_r"+str(n_rep)+".csv"
                r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","multiple10",filename), vvs, delta = 0.05, couples_dict= couples_dict)
                rad_utils.write_on_csv(os.path.join("IT_results","multiple10","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 77: theorem result analysis on test 10 rep 4-5 with FPs and FNs with and without rademachers
if test_number == 77:
    n_exps = 100

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r4 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r4_mb_no_fp = (vars, ds_sizes, [1,2],deltas_tot,"MB_NO_FP_v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    multiple10_r4_rad = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_RAD",d_struc)

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    multiple10_r4_mb_no_fp_rad = (vars, ds_sizes, [1,2],deltas_tot,"MB_NO_FP_v10_multiple_rep"+str(n_rep)+"_RAD",d_struc)

    n_rep = 5
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r5 = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 5
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r5_mb_no_fp = (vars, ds_sizes, [1,2],deltas_tot,"MB_NO_FP_v10_multiple_rep"+str(n_rep),d_struc)

    # n_rep = 5
    # vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    # d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    # ds_sizes = [100,250,500, 1000, 5000, 10000,25000,50000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    # deltas = [0.05]
    # deltas_tot = [0.05]
    # multiple10_r5_rad = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_RAD",d_struc)

    # n_rep = 5
    # vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    # d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    # ds_sizes = [100,250,500, 1000, 5000, 10000,25000,50000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    # deltas = [0.05]
    # deltas_tot = [0.05]
    # multiple10_r5_mb_no_fp_rad = (vars, ds_sizes, [1,2],deltas_tot,"MB_NO_FP_v10_multiple_rep"+str(n_rep)+"_RAD",d_struc)

    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_77.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_77.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_77.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_77.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple10_r4, multiple10_r4_mb_no_fp, multiple10_r4_rad, multiple10_r4_mb_no_fp_rad,
        multiple10_r5, multiple10_r5_mb_no_fp
        # , multiple10_r5_rad, multiple10_r5_mb_no_fp_rad
    ]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            
            if "v10_multiple_rep4" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_cor_no_max_size.csv")
            if "MB_NO_FP_v10_multiple_rep4" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_cor_no_max_sizemod_PCMB.csv")
            if "v10_multiple_rep4_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_Rademacher_no_max_size.csv")
            if "MB_NO_FP_v10_multiple_rep4_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_Rademacher_no_max_sizemod_PCMB.csv")
            if "v10_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r5_v"+str(ver)+"_cor_no_max_size.csv")
            if "MB_NO_FP_v10_multiple_rep5" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r5_v"+str(ver)+"_cor_no_max_sizemod_PCMB.csv")
            if "v10_multiple_rep5_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r5_v"+str(ver)+"_Rademacher_no_max_size.csv")
            if "MB_NO_FP_v10_multiple_rep5_RAD" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r5_v"+str(ver)+"_Rademacher_no_max_sizemod_PCMB.csv")

            try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            except:
                print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_77.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_77.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_77.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_77.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 78: test IAMB with continuous and repetitions 10  no max size ---  ds sizes up to 50k
if test_number == 78:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")

        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        mods_mbs = {}
        for v in vars:  
            mbs[v]  = iamb(dep_infos,v,vars,delta/N)

        IT_utils.save_IT_dataframe(dep_infos)
        # return test_n, delta/N, test_type, pcds, pcs, mbs, version, mods_mbs
        return test_n, delta/N, test_type, mbs,  version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000,25000,50000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
        
    versions = [1]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    tot_els = 2*len(list(itertools.product(test_t, ts, [vars],deltas,[1],versions)))
    

    # pcds = dict([(v,{}) for v in versions])
    # pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        # pcds[v] =  dict([(d,{}) for d in deltas_tot])
        # pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            # pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            # pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    # pcds[v][d][ty][size]   = {}
                    # pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type,mb,version in sorted(results,key= lambda x: x[0]):
                # pcds[version][d][test_type][size][test_n] = {}
                # pcs[version][d][test_type][size][test_n] = {}
                mbs[version][d][test_type][size][test_n] = {}

            for test_n, d, test_type, mb,version in sorted(results,key= lambda x: x[0]):
                # pcds[version][d][test_type][size][test_n][False] = pcd
                # pcs[version][d][test_type][size][test_n][False] = pc
                mbs[version][d][test_type][size][test_n][False] = mb
                # pcds[version][d][test_type][size][test_n][True] = pcd
                # pcs[version][d][test_type][size][test_n][True] = pc
                # mbs[version][d][test_type][size][test_n][True] = mod_PCMB

            if (True): #data saving part
                for mod_PCMB in [False]:
                    for ver in versions:
                        for test_type in test_t:
                            #result resume creation
                            for d in deltas_tot:
                                if d < 0.0001:
                                    name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size_iamb.csv")
                                else:
                                    name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size_iamb.csv")
                                cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                                cc = ["type"] + [item for sublist in cc for item in sublist]
                                data = []
                                for s in ds_sizes:
                                    # row = []
                                    # row.append("PCD_"+str(s))
                                    # dd = pcds.get(ver)
                                    # dd = dd.get(d)
                                    # dd = dd.get(test_type)
                                    # dd = dd.get(s,{})
                                    # for v in vars:
                                    #     for t in ts:
                                    #         dt = dd.get(t,{})
                                    #         dt = dt.get(mod_PCMB,{})
                                    #         row.append(dt.get(v,"-"))
                                    # data.append(row)
                                    # row = []
                                    # row.append("PC_"+str(s))
                                    # dd = pcs.get(ver)
                                    # dd = dd.get(d)
                                    # dd = dd.get(test_type)
                                    # dd = dd.get(s,{})
                                    # for v in vars:
                                    #     for t in ts:
                                    #         dt = dd.get(t,{})
                                    #         dt = dt.get(mod_PCMB,{})
                                    #         row.append(dt.get(v,"-"))
                                    # data.append(row)
                                    row = []
                                    row.append("PCMB_"+str(s))
                                    dd = mbs.get(ver)
                                    dd = dd.get(d)
                                    dd = dd.get(test_type)
                                    dd = dd.get(s,{})
                                    for v in vars:
                                        for t in ts:
                                            dt = dd.get(t,{})
                                            dt = dt.get(mod_PCMB,{})
                                            row.append(dt.get(v,"-"))
                                    data.append(row)
                                res_df = pd.DataFrame(data,columns=cc)
                                res_df.to_csv(name)
                        
            if (True): #data merging part
                for mod_PCMB in [False]:
                    for ver in versions:
                        for tt in test_t:
                                
                            in_files = []
                            for d in deltas_tot:
                                if d < 0.0001:
                                    in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"_no_max_size_iamb.csv"),sep = ",") )
                                else:
                                    in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"_no_max_size_iamb.csv"),sep = ",") )
                            for i in range(len(in_files)):
                                in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                                in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                            out_df = pd.DataFrame(columns=in_files[0].columns)
                            for f in in_files:
                                out_df=out_df.append(f)

                            cols = list(out_df.columns)
                            cols = cols[-2:] + cols[:-2]
                            out_df = out_df[cols]
                            out_df = out_df.sort_values("key")
                            out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"_no_max_size_iamb.csv"))
# TEST 79: theorem result analysis on test 10 rep 4-5 with FPs and FNs with and without rademachers
if test_number == 79:
    n_exps = 100

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r4 = (vars, ds_sizes, [1],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    # n_rep = 5
    # vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    # d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    # ds_sizes = [100,250,500, 1000, 5000, 10000,25000,50000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    # deltas = [0.05]
    # deltas_tot = [0.05]
    # multiple10_r5_rad = (vars, ds_sizes, [1,2],deltas_tot,"v10_multiple_rep"+str(n_rep)+"_RAD",d_struc)

    # n_rep = 5
    # vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    # d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    # ds_sizes = [100,250,500, 1000, 5000, 10000,25000,50000]
    # sv = len(vars)
    # N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    # deltas = [0.05]
    # deltas_tot = [0.05]
    # multiple10_r5_mb_no_fp_rad = (vars, ds_sizes, [1,2],deltas_tot,"MB_NO_FP_v10_multiple_rep"+str(n_rep)+"_RAD",d_struc)

    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_78.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_78.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_78.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_78.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple10_r4 ]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            
            if "v10_multiple_rep4" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_cor_no_max_size_iamb.csv")
            if True:
            # try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)+"IAMB"
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            # pc = IT_utils.get_pc(v,dag_struc)
                            # look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                            if df_pcmb.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcmb[i,j] != "-":    
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                            if df_pcmb.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcmb[i,j] != "-":    
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            # except:
            #     print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_78.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_78.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_78.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_78.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 80: test on real data
if test_number == 80:
    run_type = input_test_number
    # df = pd.read_csv(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31.csv")).dropna()
    # df["cbwd"] = df["cbwd"].astype('category')
    # df["cbwd"] = df["cbwd"].cat.codes
    # df.to_csv(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31.csv"))
    vvs = ['year', 'month', 'day', 'hour', 'pm2.5','DEWP', 'TEMP', 'PRES', 'cbwd', 'Iws', 'Is', 'Ir']
    t = 'pm2.5'
    delta = 0.1
    if input_test_number%2 == 0 :
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31.csv")
    if delta/N < 0.0001:
        indep_file = os.path.join("IT_results","real_data_test","PRSA_data"+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
    else:
        indep_file = os.path.join("IT_results","real_data_test","PRSA_data"+"_delta"+str(delta/N)+".csv")

    if os.path.exists(indep_file):
        try:
            indep_df = pd.read_csv(indep_file,sep = ";")
        except:
            print("!!!",indep_file)
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
    else:
        indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        indep_df.to_csv(indep_file, sep = ";")
    dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="fcit", data_file=data_file, 
                    indep_file=indep_file, save_every_update=False, independence_language="py")
    
    if int(run_type/2) == 0:
        res = pcmb(dep_infos, t, vvs,delta/N)
    if int(run_type/2) == 1:
        res = pcmb_mod(dep_infos, t, vvs, delta, N)
    if int(run_type/2) == 2:
        res = pcmb(dep_infos, t, vvs,delta/N, test_with_all_els= True)
    if int(run_type/2) == 3:
        res = pcmb_mod(dep_infos, t, vvs, delta, N, test_with_all_els = True)
    if int(run_type/2) == 4:
        res = iamb(dep_infos, t, vvs, delta/N)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","results.txt"),"a")
    f.write(str(run_type)+" "+str(res))
    f.close()
# TEST 81: test on real med data
if test_number == 81:
    run_type = input_test_number
    vvs = ["praf","pmek","plcg","PIP2","PIP3","p44/42","pakts473","PKA","PKC","P38","pjnk"]
    delta = 0.05
    if input_test_number%2 == 0 :
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","med_ds_real.csv")

    if run_type < 10:
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","real_data_test","med_ds"+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","real_data_test","med_ds"+"_delta"+str(delta/N)+".csv")

        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="cor", data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
    else:
        filename = "med_ds"+"_delta"+str(0.05)+".csv"
        indep_file = os.path.join("IT_results","real_data_test","RAD_"+filename)
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)
        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="-")

    for t in vvs:
        if int(run_type/2) == 0:
            res = pcmb(dep_infos, t, vvs,delta/N)
        if int(run_type/2) == 1:
            res = pcmb_mod(dep_infos, t, vvs, delta, N)
        if int(run_type/2) == 2:
            res = pcmb(dep_infos, t, vvs,delta/N, test_with_all_els= True)
        if int(run_type/2) == 3:
            res = pcmb_mod(dep_infos, t, vvs, delta, N, test_with_all_els = True)
        if int(run_type/2) == 4:
            res = iamb(dep_infos, t, vvs, delta/N)
        if run_type == 10:
            res = pcmb(dep_infos, t, vvs,delta/N)
        if run_type == 11:
            res = pcmb_mod(dep_infos, t, vvs, delta, 1)
        if run_type == 12:
            res = iamb(dep_infos, t, vvs, delta/1)
        if run_type == 13:
            res = pcmb(dep_infos, t, vvs,delta/1, test_with_all_els = True)
        if run_type == 14:
            res = pcmb_mod(dep_infos, t, vvs, delta, 1, test_with_all_els = True)

        IT_utils.save_IT_dataframe(dep_infos)
        f = open(os.path.join("IT_results","real_data_test","med_results_run"+str(run_type)+".txt"),"a")
        f.write(t+" : "+str(res)+"\n")
        f.close()
# TEST 82: RADEMACHER test on med data
if test_number == 82:
    vvs = ["praf","pmek","plcg","PIP2","PIP3","p44/42","pakts473","PKA","PKC","P38","pjnk"]
    couples_dict = rad_utils.select_candidates(vvs)
    
    filename = "med_ds"+"_delta"+str(delta_input)+".csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","med_ds_real.csv"), vvs, delta = 0.05, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 83: test on real data pollution without considering wind direction
if test_number == 83:
    run_type = input_test_number
    # df = pd.read_csv(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31.csv")).dropna()
    # df["cbwd"] = df["cbwd"].astype('category')
    # df["cbwd"] = df["cbwd"].cat.codes
    # df.to_csv(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31.csv"))
    vvs = ['year', 'month', 'day', 'hour', 'pm2.5','DEWP', 'TEMP', 'PRES', 'Iws', 'Is', 'Ir']
    t = 'pm2.5'
    delta = delta_input
    if input_test_number%2 == 0 and not input_test_number > 10:
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31v2.csv")

    if run_type < 10:
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","real_data_test","PRSA_data"+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","real_data_test","PRSA_data"+"_delta"+str(delta/N)+".csv")
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="cor", data_file=data_file, 
                    indep_file=indep_file, save_every_update=False, independence_language="R")
    else:
        filename = "pollution_ds"+"_delta"+str(delta_input)+".csv"
        indep_file = os.path.join("IT_results","real_data_test","RAD_"+filename)
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
    
    if int(run_type/2) == 0:
        res = pcmb(dep_infos, t, vvs,delta/N)
    if int(run_type/2) == 1:
        res = pcmb_mod(dep_infos, t, vvs, delta, N)
    if int(run_type/2) == 2:
        res = pcmb(dep_infos, t, vvs,delta/N, test_with_all_els= True)
    if int(run_type/2) == 3:
        res = pcmb_mod(dep_infos, t, vvs, delta, N, test_with_all_els = True)
    if int(run_type/2) == 4:
        res = iamb(dep_infos, t, vvs, delta/N)
    if run_type == 10:
        res = pcmb(dep_infos, t, vvs,delta, test_with_all_els = True)
    if run_type == 11:
        res = pcmb_mod(dep_infos, t, vvs, delta, 1, test_with_all_els = True)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","pollution_v2.txt"),"a")
    f.write(str(delta)+" " +str(run_type)+" "+str(res)+"\n")
    f.close()
# TEST 84: RADEMACHER calculus on pollution data
if test_number == 84:
    vvs = ['year', 'month', 'day', 'hour', 'pm2.5','DEWP', 'TEMP', 'PRES', 'Iws', 'Is', 'Ir']
    couples_dict = rad_utils.select_candidates(vvs)
    df = pd.read_csv(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31.csv"))
    df = df.drop(columns = ["cbwd"]).dropna().astype('float32')
    df.to_csv(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31v2.csv"))
    filename = "pollution_ds"+"_delta"+str(delta_input)+".csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","real_data_test","PRSA_data_2010.1.1-2014.12.31v2.csv"), vvs, delta = delta_input, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 85: RADEMACHER calculus on Antonio's data
if test_number == 85:
    vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    couples_dict = rad_utils.select_candidates(vvs)
    filename = "anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled.csv"), vvs, delta = delta_input, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 86: test on antonio's data
if test_number == 86:
    run_type = input_test_number
    vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    t = 'class'
    delta = delta_input
    if input_test_number%2 == 0 and not input_test_number > 10:
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled.csv")

    if run_type < 10:
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","real_data_test","anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv")
        else:
            indep_file = os.path.join("IT_results","real_data_test","anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv")
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="cor", data_file=data_file, 
                    indep_file=indep_file, save_every_update=False, independence_language="R")
    else:
        filename = "pollution_ds"+"_delta"+str(delta_input)+".csv"
        indep_file = os.path.join("IT_results","real_data_test","RAD_anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv")
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
    
    
    if run_type == 10:
        res = mb_algo(dep_infos, t, vvs,delta,1)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","anto_med_res_tr_shuffled.txt"),"a")
    f.write(str(delta)+"MB " +str(run_type)+" "+str(res)+"\n")
    f.write(str(delta)+"PC " +str(run_type)+" "+str(pc_def(dep_infos, t, vvs, delta, 1))+"\n")
    f.close()
# TEST 87: RADEMACHER calculus on ecoli's data
if test_number == 87:
    # vvs = ["asnA","cspG","cspA","yecO","yedE","lacA","lacY","lacZ","nuoM","b1553","yaeM","mopB","pspA","eutG","yceP","lbpB","yfaD","sucA"]  #dist 2 from lacY 18 vars
    vvs = ["asnA","cspG","lacA","lacY","lacZ","nuoM","b1583","yaeM","mopB","eutG","yceP","icdA","ygcE","dnaK", "ftsJ"] # dist 2 from lacZ 15 vars
    couples_dict = rad_utils.select_candidates(vvs)
    for size in tqdm([50,100,250,500,1000,5000,10000,50000,100000]):
        df = pd.read_csv(os.path.join("datasets","ecoli_test","ecoli_s"+str(size)+"_t0.csv"))[vvs]
        df.to_csv(os.path.join("datasets","ecoli_test","ecoli_reduced_s"+str(size)+".csv"), index=False)
    for size in tqdm([50,100,250,500,1000,5000,10000,50000,100000]):
        filename = "ecoli_reduced_s"+str(size)+"_delta_"+str(delta_input)+".csv"
        r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","ecoli_test","ecoli_reduced_s"+str(size)+".csv"), vvs, delta = delta_input, couples_dict= couples_dict)
        rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)
# TEST 88: test on antonio's data
if test_number == 88:
    run_type = input_test_number
    vvs = ["asnA","cspG","lacA","lacY","lacZ","nuoM","b1583","yaeM","mopB","eutG","yceP","icdA","ygcE","dnaK", "ftsJ"] # dist 2 from lacZ 15 vars
    sv = len(vvs)
    t = 'lacZ'
    delta = delta_input
    
    # N = sv*(sv-1)*(2**(sv-3))
    for size in tqdm([50,100,250,500,1000,5000,10000,50000,100000]):
        data_file = os.path.join("datasets","ecoli_test","ecoli_reduced_s"+str(size)+".csv")

        indep_file = os.path.join("IT_results","real_data_test","ecoli_reduced_s"+str(size)+"_delta"+str(delta_input)+".csv")
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="cor", data_file=data_file, 
                    indep_file=indep_file, save_every_update=False, independence_language="R")
        res = pcmb(dep_infos, t, vvs,delta)

        IT_utils.save_IT_dataframe(dep_infos)
        f = open(os.path.join("IT_results","real_data_test","ecoli_reduced_res.txt"),"a")
        f.write("SIZE: "+str(size)+"\n")
        f.write("PCMB no corr \n")
        f.write(str(delta)+"MB " +str(run_type)+" "+str( pcmb(dep_infos, t, vvs,delta))+"\n")
        f.write(str(delta)+"PC " +str(run_type)+" "+str(get_pc(dep_infos, t, vvs,delta))+"\n")
        f.close()
            
        run_type = 10
        filename = "ecoli_reduced_s"+str(size)+"_delta_"+str(delta_input)+".csv"
        indep_file = os.path.join("IT_results","real_data_test","RAD_"+filename)
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
        
        res = pcmb(dep_infos, t, vvs,delta, test_with_all_els = True)

        f = open(os.path.join("IT_results","real_data_test","ecoli_reduced_res.txt"),"a")
        f.write("Our algo \n")
        f.write(str(delta)+"MB " +str(run_type)+" "+str(pcmb(dep_infos, t, vvs,delta, test_with_all_els = True))+"\n")
        f.write(str(delta)+"PC " +str(run_type)+" "+str(get_pc(dep_infos, t, vvs,delta))+"\n")
        f.close()
# TEST 89: test with continuous and repetitions 10  on mb_algo
if test_number == 89:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]

        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)[:3]+str(delta/N)[-4:]+".csv")
        else:
            indep_file = os.path.join("IT_results","multiple10",test_type+"_multiple_s"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+"_delta"+str(delta/N)+".csv")
        
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="R")
        dep_infos.look_dsep = {}
        dep_infos.n_rep = n_rep


        # perform experiment
        pcds = {}
        pcs = {}
        mbs = {}
        for v in vars:  
            pcs[v]  = pc_def(dep_infos,v,vars,delta,N)
            mbs[v]  = mb_algo(dep_infos,v,vars,delta,N)

        IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    if verbose > 0: #escamotage for running only part of exps if needed
        ds_sizes = [10000,25000]
    else:
        ds_sizes = [100,250,500, 1000, 5000, 10000,25000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    # deltas_tot = [0.05, 0.05/N]
    deltas_tot = [ 0.05/N]
        
    versions = [1]
    ts = [i for i in range(100)]
    test_t = ["cor"]
    # tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[1,N],versions)))
    tot_els = len(list(itertools.product(test_t, ts, [vars],deltas,[N],versions)))
    

    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            # input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1,N],versions, [n_rep])))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[N],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"test_algos_def"+"c2"*(verbose > 0)+".csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"test_algos_def"+"c2"*(verbose > 0)+".csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"test_algos_def"+"c2"*(verbose > 0)+".csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"test_algos_def"+"c2"*(verbose > 0)+".csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"test_algos_def"+"c2"*(verbose > 0)+".csv"))
# TEST 90: RADEMACHER test on multiple 10 rep 5
if test_number == 90:
    def perform_test(*args):
        # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
        # stats for each variable
        params = args[0]
        test_type = params[0]
        size = params[1]
        test_n = params[2]
        vars = params[3]
        delta = params[4]
        N = params[5]
        version = params[6]
        n_rep = params[7]
        
        filename = "v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv"
                
        data_file = os.path.join("datasets","multiple10","v10_S"+str(size)+"_t"+str(test_n)+"_r"+str(n_rep)+".csv")
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","multiple10","RAD_"+filename)
        else:
            indep_file = os.path.join("IT_results","multiple10","RAD_"+filename)
        
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        #     except:
        #         print("!!!",indep_file)
        #         indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        #         indep_df.to_csv(indep_file, sep = ";")
        # else:
        #     indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
        #     indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method=test_type, data_file=data_file, 
                        indep_file=indep_file, save_every_update=False, independence_language="-")        
        pcds = {}
        pcs = {}
        mbs = {}
        for v in vars:  
            pcs[v]  = pc_def(dep_infos,v,vars,delta,N)
            mbs[v]  = mb_algo(dep_infos,v,vars,delta,N)
        # IT_utils.save_IT_dataframe(dep_infos)
        return test_n, delta/N, test_type, pcds, pcs, mbs, version
    
    n_rep=input_test_number
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    ds_sizes = [100,250,500,1000,5000,10000,25000,50000]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
        
    versions = [1]
    ts = [i for i in range(100)]
    test_t = ["Rademacher"]
    tot_els = 2*len(list(itertools.product(test_t, ts, [vars],deltas,[1],versions)))
    
    pcds = dict([(v,{}) for v in versions])
    pcs = dict([(v,{}) for v in versions])
    mbs = dict([(v,{}) for v in versions])
    for v in versions:
        pcds[v] =  dict([(d,{}) for d in deltas_tot])
        pcs[v] =  dict([(d,{}) for d in deltas_tot])
        mbs[v] =  dict([(d,{}) for d in deltas_tot])
    for v in versions:
        for d in deltas_tot:
            pcds[v][d] =  dict([(ty,{}) for ty in test_t])
            pcs[v][d] =  dict([(ty,{}) for ty in test_t])
            mbs[v][d] =  dict([(ty,{}) for ty in test_t])
    for v in versions:
        for d in deltas_tot:
            for size in ds_sizes:
                for ty in test_t:
                    pcds[v][d][ty][size]   = {}
                    pcs[v][d][ty][size]    = {}
                    mbs[v][d][ty][size]    = {}
        

    for size in ds_sizes:
        print(datetime.now(), "Starting cycle with",size,"elements")
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(min(n_cores,72,tot_els))
            input = ((a,b,c,d,e,f,g,h) for a,b,c,d,e,f,g,h in list(itertools.product(test_t, [size], ts, [vars],deltas,[1],versions, [n_rep])))
            results = p.map(perform_test, input)   
            p.close()
            p.join()

            for test_n, d, test_type, pcd,pc,mb,version in sorted(results,key= lambda x: x[0]):
                pcds[version][d][test_type][size][test_n] = pcd
                pcs[version][d][test_type][size][test_n] = pc
                mbs[version][d][test_type][size][test_n] = mb

            if (True): #data saving part
                for ver in versions:
                    for test_type in test_t:
                        #result resume creation
                        for d in deltas_tot:
                            if d < 0.0001:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"test_algos_def.csv")
                            else:
                                name = os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+test_type+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"test_algos_def.csv")
                            cc = [[str(v)+"_t"+str(t) for t in ts] for v in vars]
                            cc = ["type"] + [item for sublist in cc for item in sublist]
                            data = []
                            for s in ds_sizes:
                                row = []
                                row.append("PCD_"+str(s))
                                dd = pcds.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PC_"+str(s))
                                dd = pcs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                                row = []
                                row.append("PCMB_"+str(s))
                                dd = mbs.get(ver)
                                dd = dd.get(d)
                                dd = dd.get(test_type)
                                dd = dd.get(s,{})
                                for v in vars:
                                    for t in ts:
                                        dt = dd.get(t,{})
                                        row.append(dt.get(v,"-"))
                                data.append(row)
                            res_df = pd.DataFrame(data,columns=cc)
                            res_df.to_csv(name)
                        
            if (True): #data merging part
                for ver in versions:
                    for tt in test_t:
                            
                        in_files = []
                        for d in deltas_tot:
                            if d < 0.0001:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)[:3]+str(d)[-4:]+"test_algos_def.csv"),sep = ",") )
                            else:
                                in_files.append(pd.read_csv(os.path.join("IT_results","multiple10","res_on100_"+str(ver)+"_"+tt+"_multiple_r"+str(n_rep)+"_delta"+str(d)+"test_algos_def.csv"),sep = ",") )
                        for i in range(len(in_files)):
                            in_files[i]["key"] = [j+0.1*i for j in range(in_files[i].shape[0])]
                            in_files[i]["delta"] = [deltas_tot[i] for j in range(in_files[i].shape[0])]

                        out_df = pd.DataFrame(columns=in_files[0].columns)
                        for f in in_files:
                            out_df=out_df.append(f)

                        cols = list(out_df.columns)
                        cols = cols[-2:] + cols[:-2]
                        out_df = out_df[cols]
                        out_df = out_df.sort_values("key")
                        out_df.to_csv(os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r"+str(n_rep)+"_v"+str(ver)+"_"+tt+"test_algos_def.csv"))
# TEST 91: theorem result analysis on test 10 rep 1 - ... - 4 with FPs and FNs with and without rademachers
if test_number == 91:
    n_exps = 100

    n_rep = 1
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    rad_multiple10_r1 = (vars, ds_sizes, [1],deltas_tot,"RAD_v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 2
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    rad_multiple10_r2 = (vars, ds_sizes, [1],deltas_tot,"RAD_v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 3
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    rad_multiple10_r3 = (vars, ds_sizes, [1],deltas_tot,"RAD_v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05]
    rad_multiple10_r4 = (vars, ds_sizes, [1],deltas_tot,"RAD_v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 1
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r1 = (vars, ds_sizes, [1],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 2
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r2 = (vars, ds_sizes, [1],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 3
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r3 = (vars, ds_sizes, [1],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [100,250,500, 1000, 5000, 10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r4 = (vars, ds_sizes, [1],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_91.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_91.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_91.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_91.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [rad_multiple10_r1,rad_multiple10_r2,rad_multiple10_r3,rad_multiple10_r4,multiple10_r1,multiple10_r2,multiple10_r3,multiple10_r4 ]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "RAD_v10_multiple_rep1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r1_v"+str(ver)+"_Rademachertest_algos_def.csv")
            if "RAD_v10_multiple_rep2" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r2_v"+str(ver)+"_Rademachertest_algos_def.csv")
            if "RAD_v10_multiple_rep3" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r3_v"+str(ver)+"_Rademachertest_algos_def.csv")
            if "RAD_v10_multiple_rep4" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_Rademachertest_algos_def.csv")
            if "v10_multiple_rep1" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r1_v"+str(ver)+"_cortest_algos_def.csv")
            if "v10_multiple_rep2" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r2_v"+str(ver)+"_cortest_algos_def.csv")
            if "v10_multiple_rep3" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r3_v"+str(ver)+"_cortest_algos_def.csv")
            if "v10_multiple_rep4" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_cortest_algos_def.csv")
            if True:
            # try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                            if df_pc.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pc[i,j] != "-":    
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                            if df_pcmb.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcmb[i,j] != "-":    
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                            if df_pc.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pc[i,j] != "-":    
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                            if df_pcmb.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcmb[i,j] != "-":    
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            # except:
            #     print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_91.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_91.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_91.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_91.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 92: RADEMACHER total on Antonio's data
if test_number == 92:
    size = input_test_number
    df = pd.read_csv(os.path.join("datasets","real_data_test","Cleaned_ds_anto.csv"))[['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size', 'nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']].sample(frac=1, random_state = 120395)
    df.head(int(df.shape[0]*input_test_number/100)).to_csv(os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled.csv"))
    df.tail(int(df.shape[0]*(100-input_test_number)/100)).to_csv(os.path.join("datasets","real_data_test","Cleaned_ds_anto_ts_shuffled.csv"))

    vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    couples_dict = rad_utils.select_candidates(vvs)
    filename = "anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled.csv"), vvs, delta = delta_input, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)


    run_type = 10
    vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    t = 'class'
    delta = delta_input
    if run_type%2 == 0 and not run_type > 10:
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled.csv")

    if run_type < 10:
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","real_data_test","anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv")
        else:
            indep_file = os.path.join("IT_results","real_data_test","anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv")
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="cor", data_file=data_file, 
                    indep_file=indep_file, save_every_update=False, independence_language="R")
    else:
        filename = "pollution_ds"+"_delta"+str(delta_input)+".csv"
        indep_file = os.path.join("IT_results","real_data_test","RAD_anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled.csv")
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
    
    
    if run_type == 10:
        res = mb_algo(dep_infos, t, vvs,delta,1)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","anto_med_res_tr_shuffled.txt"),"a")
    f.write("Split %"+str(input_test_number)+"\n")
    f.write(str(delta)+"MB " +str(run_type)+" "+str(res)+"\n")
    f.write(str(delta)+"PC " +str(run_type)+" "+str(pc_def(dep_infos, t, vvs, delta, 1))+"\n")
    f.close()
# TEST 93: RADEMACHER total on Antonio's data
if test_number == 93:
    input_test_number = 75
    df = pd.read_csv(os.path.join("datasets","real_data_test","Cleaned_ds_anto.csv"))[['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size', 'nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']].sample(frac=1, random_state = 120395)
    ds_x5 = pd.concat([df.head(int(df.shape[0]*input_test_number/100))]*1, ignore_index=True)
    ds_x5.astype("float64").to_csv(os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled_x5.csv"))
    df.tail(int(df.shape[0]*(100-input_test_number)/100)).to_csv(os.path.join("datasets","real_data_test","Cleaned_ds_anto_ts_shuffled.csv"))

    vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    couples_dict = rad_utils.select_candidates(vvs)
    filename = "anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled_x5.csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled_x5.csv"), vvs, delta = delta_input, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)


    run_type = 10
    vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    t = 'class'
    delta = delta_input
    if run_type%2 == 0 and not run_type > 10:
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","Cleaned_ds_anto_tr_shuffled_x5.csv")

    if run_type < 10:
        if delta/N < 0.0001:
            indep_file = os.path.join("IT_results","real_data_test","anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled_x5.csv")
        else:
            indep_file = os.path.join("IT_results","real_data_test","anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled_x5.csv")
        if os.path.exists(indep_file):
            try:
                indep_df = pd.read_csv(indep_file,sep = ";")
            except:
                print("!!!",indep_file)
                indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
                indep_df.to_csv(indep_file, sep = ";")
        else:
            indep_df = pd.DataFrame(columns=["first_term", "second_term", "cond_set", "p_value", "statistic", "lb"])
            indep_df.to_csv(indep_file, sep = ";")
        dep_infos = dependencyInfos(indep_df, method="p-value", independence_method="cor", data_file=data_file, 
                    indep_file=indep_file, save_every_update=False, independence_language="R")
    else:
        filename = "pollution_ds"+"_delta"+str(delta_input)+".csv"
        indep_file = os.path.join("IT_results","real_data_test","RAD_anto_med_ds"+"_delta"+str(delta_input)+"_tr_shuffled_x5.csv")
        if os.path.exists(indep_file):
            indep_df = pd.read_csv(indep_file,sep = ";")
            if len(indep_df.columns.values) < 2:
                indep_df = pd.read_csv(indep_file)

        dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
    
    
    if run_type == 10:
        res = mb_algo(dep_infos, t, vvs,delta,1)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","anto_med_res_tr_shuffled_x5.txt"),"a")
    f.write("Split %"+str(input_test_number)+"\n")
    f.write(str(delta)+"MB " +str(run_type)+" "+str(res)+"\n")
    f.write(str(delta)+"PC " +str(run_type)+" "+str(pc_def(dep_infos, t, vvs, delta, 1))+"\n")
    f.close()
# TEST 94: RADEMACHER total on Antonio's data and Boston housing both x5 datasets
if test_number == 94:
    input_test_number = 75

    ############ boston ds 

    # CRIM per capita crime rate by town 
    # ZN proportion of residential land zoned for lots over 25,000 sq.ft. 
    # INDUS proportion of non-retail business acres per town 
    # CHAS Charles River dummy variable (= 1 if tract bounds river; 0 otherwise) 
    # NOX nitric oxides concentration (parts per 10 million) 
    # RM average number of rooms per dwelling 
    # AGE proportion of owner-occupied units built prior to 1940 
    # DIS weighted distances to five Boston employment centres 
    # RAD index of accessibility to radial highways 
    # TAX full-value property-tax rate per $10,000 
    # PTRATIO pupil-teacher ratio by town 
    # B 1000(Bk 0.63)^2 where Bk is the proportion of blacks by town 
    # LSTAT % lower status of the population 
    # MEDV Median value of owner-occupied homes in $1000's

    vvs = ["CRIM" ,"ZN","INDUS","CHAS","NOX","RM","AGE","DIS","RAD","TAX","PTRATIO","B","LSTAT","MEDV"]

    df = pd.read_csv(os.path.join("datasets","real_data_test","housing.csv"), header=None, delimiter=r"\s+", names = vvs)
    # df.columns = vvs
    df = df.sample(frac=1, random_state = 120395)
    ds_x5 = pd.concat([df.head(int(df.shape[0]*input_test_number/100))]*1, ignore_index=True)
    ds_x5.astype("float64").to_csv(os.path.join("datasets","real_data_test","housing_tr_shuffled_x1.csv"))
    df.tail(int(df.shape[0]*(100-input_test_number)/100)).to_csv(os.path.join("datasets","real_data_test","housing_ts_shuffled.csv"))
    # input("bk")

    # vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    couples_dict = rad_utils.select_candidates(vvs)
    filename = "housing"+"_delta"+str(delta_input)+"_tr_shuffled_x1.csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","real_data_test","housing_tr_shuffled_x1.csv"), vvs, delta = delta_input, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)


    run_type = 10
    vvs = ["CRIM" ,"ZN","INDUS","CHAS","NOX","RM","AGE","DIS","RAD","TAX","PTRATIO","B","LSTAT"]
    t = "MEDV"
    delta = delta_input
    if run_type%2 == 0 and not run_type > 10:
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","housing_tr_shuffled_x1.csv")

    filename = data_file
    indep_file = os.path.join("IT_results","real_data_test","RAD_housing"+"_delta"+str(delta_input)+"_tr_shuffled_x1.csv")
    if os.path.exists(indep_file):
        indep_df = pd.read_csv(indep_file,sep = ";")
        if len(indep_df.columns.values) < 2:
            indep_df = pd.read_csv(indep_file)

    dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
    
    
    if run_type == 10:
        res = mb_algo(dep_infos, t, vvs,delta,1)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","housing_tr_shuffled_x1.txt"),"a")
    f.write("Split %"+str(input_test_number)+"\n")
    f.write(str(delta)+"MB " +str(run_type)+" "+str(res)+"\n")
    f.write(str(delta)+"PC " +str(run_type)+" "+str(pc_def(dep_infos, t, vvs, delta, 1))+"\n")
    f.close()
# TEST 95: theorem result analysis on test 10 rep 1 - ... - 4 with FPs and FNs with and without rademachers on second run
if test_number == 95:
    n_exps = 100

    n_rep = 4
    vars = ["A1"] + ["B"+str(i+1) for i in range(n_rep)] + ["C"+str(i+1) for i in range(n_rep)] + ["T"+str(i+1) for i in range(n_rep)]
    d_struc = "[A1][B1|A1][C1|A1][T1|B1:C1]" + "".join(["[B"+str(i+1)+"|T"+str(i)+"][C"+str(i+1)+"|T"+str(i)+"][T"+str(i+1)+"|B"+str(i+1)+":C"+str(i+1)+"]" for i in range(1,n_rep)])
    ds_sizes = [10000,25000
    # ,50000
    ]
    sv = len(vars)
    N = sv*(sv-1)*(2**(sv-3))  # diffrently from the notes, T is in vars
    deltas = [0.05]
    deltas_tot = [0.05, 0.05/N]
    multiple10_r4 = (vars, ds_sizes, [1],deltas_tot,"v10_multiple_rep"+str(n_rep),d_struc)

    counter_pcd_per_test = dict()
    with open(os.path.join('IT_results','counter_pcd_per_test_95.pickle'), 'wb') as handle:
        pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pc_per_test = dict()
    with open(os.path.join('IT_results','counter_pc_per_test_95.pickle'), 'wb') as handle:
        pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_mb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_mb_per_test_95.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcmb_per_test = dict()
    with open(os.path.join('IT_results','counter_pcmb_per_test_95.pickle'), 'wb') as handle:
        pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)

    counter_pcd_per_test = dict()
    counter_pc_per_test = dict()
    counter_pcmb_mb_per_test = dict()
    counter_pcmb_per_test = dict()

    parameter_list = [multiple10_r4 ]
    for pl in parameter_list:
        vars_selected = pl[0]
        sizes_selected = pl[1]
        versions_selected = pl[2]
        deltas_selected = pl[3]
        method_name = pl[4]
        dag_struc = pl[5]
        
        print("RECAP for", method_name)
        for ver in versions_selected:
            if "v10_multiple_rep4" == method_name:
                f_name_selected = os.path.join("IT_results","total_res","tot_on100_multiple_cont10_r4_v"+str(ver)+"_cortest_algos_defc2.csv")
            if True:
            # try:
                in_df = pd.read_csv(f_name_selected)
                dic_key = method_name + "_version_" + str(ver)
                counter_pcd_per_test[dic_key] = dict()
                counter_pc_per_test[dic_key] = dict()
                counter_pcmb_mb_per_test[dic_key] = dict()
                counter_pcmb_per_test[dic_key] = dict()
                counter_pcd_per_test[dic_key]["FP"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FP"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FP"] = dict([(d,{}) for d in deltas_selected])
                counter_pcd_per_test[dic_key]["FN"]  = dict([(d,{}) for d in deltas_selected])
                counter_pc_per_test[dic_key]["FN"]   = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_mb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])
                counter_pcmb_per_test[dic_key]["FN"] = dict([(d,{}) for d in deltas_selected])

                cc = [[str(v)+"_t"+str(t) for t in range(n_exps)] for v in vars_selected]
                cc = [item for sublist in cc for item in sublist]
                
                for d in deltas_selected:
                    
                    in_file = in_df[in_df['delta'] == d]
                    if in_file.shape[0] == 0:
                        for dd in set(in_df['delta']):
                            if np.abs(dd-d) < d*1e-7:
                                in_file = in_df[in_df['delta'] == dd]

                    for size in sizes_selected:
                        df_pcd = in_file[in_file["type"]=="PCD_"+str(size)]
                        df_pcd = df_pcd[cc].values
                        df_pcd = np.reshape(df_pcd,(-1,n_exps))
                        df_pc = in_file[in_file["type"]=="PC_"+str(size)]
                        df_pc = df_pc[cc].values
                        df_pc = np.reshape(df_pc,(-1,n_exps))
                        df_pcmb = in_file[in_file["type"]=="PCMB_"+str(size)]
                        df_pcmb = df_pcmb[cc].values
                        df_pcmb = np.reshape(df_pcmb,(-1,n_exps))
                        look_pc = {}
                        look_mb = {}
                        look_el2 = {}
                        
                        for i in range(len(vars_selected)):
                            v = vars_selected[i]
                            c_pcd = 0
                            c_pc = 0
                            pc = IT_utils.get_pc(v,dag_struc)
                            look_pc[v] = pc
                            c_pcmb_mb = 0
                            mb = IT_utils.get_mb(v,dag_struc)
                            look_mb[v] = mb
                            c_pcmb = 0
                            el2 = IT_utils.get_el_dist_le_2(v,dag_struc)
                            look_el2[v] = el2

                        counter_pcd_per_test[dic_key]["FP"][d][size] = []
                        counter_pc_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FP"][d][size] = []
                        counter_pcd_per_test[dic_key]["FN"][d][size] = []
                        counter_pc_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_mb_per_test[dic_key]["FN"][d][size] = []
                        counter_pcmb_per_test[dic_key]["FN"][d][size] = []
                        
                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not IT_utils.is_in_pcd(element,vars_selected[i],dag_struc) for i in range(len(vars_selected)) for element in eval(df_pcd[i,j])])
                            if df_pc.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pc[i,j] != "-":    
                                    c_pc = np.sum([not element in look_pc[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pc[i,j])])
                            if df_pcmb.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcmb[i,j] != "-":    
                                    c_pcmb_mb = np.sum([not element in look_mb[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    c_pcmb = np.sum([not element in look_el2[vars_selected[i]] for i in range(len(vars_selected)) for element in eval(df_pcmb[i,j])])
                                    
                            counter_pcd_per_test[dic_key]["FP"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FP"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FP"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FP"][d][size].append(c_pcmb)

                        for j in range(n_exps):
                            c_pcd = -1
                            c_pc = -1
                            c_pcmb_mb = -1
                            c_pcmb = -1
                            if df_pcd.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcd[i,j] != "-":
                                    c_pcd = np.sum([not element in eval(df_pcd[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                            if df_pc.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pc[i,j] != "-":    
                                    c_pc = np.sum([not element in eval(df_pc[i,j]) for i in range(len(vars_selected)) for element in look_pc[vars_selected[i]]])
                            if df_pcmb.shape[0] == len(vars_selected):  # may be that I don't have any measure for these vals
                                if df_pcmb[i,j] != "-":    
                                    c_pcmb_mb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_mb[vars_selected[i]]])
                                    c_pcmb = np.sum([not element in eval(df_pcmb[i,j]) for i in range(len(vars_selected)) for element in look_el2[vars_selected[i]]])
                                    
                            counter_pcd_per_test[dic_key]["FN"][d][size].append(c_pcd)
                            counter_pc_per_test[dic_key]["FN"][d][size].append(c_pc)
                            counter_pcmb_mb_per_test[dic_key]["FN"][d][size].append(c_pcmb_mb)
                            counter_pcmb_per_test[dic_key]["FN"][d][size].append(c_pcmb)
                            


                if ver == 1:
                    print("Version 1")
                else:
                    print("Version 2")
                print()
                if ver == 1:
                    print("PCD")
                else:
                    print("Mod_PCD")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcd_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcd_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcd_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PC")
                else:
                    print("Mod_PC")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pc_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pc_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pc_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el in/not in MB")
                else:
                    print("PCMB mod - el in/not in MB")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_mb_per_test[dic_key]["FN"][d][size]))
                if ver == 1:
                    print("PCMB - el a dist >/<= 2")
                else:
                    print("PCMB mod - el a dist >/<= 2")
                for size in sizes_selected:
                    print("- size:", size)
                    for d in deltas_selected:
                        print("  delta:",'{:.2e}'.format(d))
                        print("FP"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FP"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FP"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FP"][d][size]))
                        print("FN"," emp. prob.", '{:6.3f}'.format(np.mean(np.array(counter_pcmb_per_test[dic_key]["FN"][d][size]) > 0))," sum", '{:3d}'.format(int(np.sum(counter_pcmb_per_test[dic_key]["FN"][d][size]))), " max", np.max(counter_pcmb_per_test[dic_key]["FN"][d][size]))
            # except:
            #     print("--  Error --")
            
            with open(os.path.join('IT_results','counter_pcd_per_test_95.pickle'), 'wb') as handle:
                pickle.dump(counter_pcd_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pc_per_test_95.pickle'), 'wb') as handle:
                pickle.dump(counter_pc_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_mb_per_test_95.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_mb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
            with open(os.path.join('IT_results','counter_pcmb_per_test_95.pickle'), 'wb') as handle:
                pickle.dump(counter_pcmb_per_test, handle, protocol=pickle.HIGHEST_PROTOCOL)
        print()
        print()
        print()
# TEST 96: RADEMACHER total on Antonio's data and Boston housing both x5 datasets
if test_number == 96:
    input_test_number = 75

    ############ boston ds 

    # CRIM per capita crime rate by town 
    # ZN proportion of residential land zoned for lots over 25,000 sq.ft. 
    # INDUS proportion of non-retail business acres per town 
    # CHAS Charles River dummy variable (= 1 if tract bounds river; 0 otherwise) 
    # NOX nitric oxides concentration (parts per 10 million) 
    # RM average number of rooms per dwelling 
    # AGE proportion of owner-occupied units built prior to 1940 
    # DIS weighted distances to five Boston employment centres 
    # RAD index of accessibility to radial highways 
    # TAX full-value property-tax rate per $10,000 
    # PTRATIO pupil-teacher ratio by town 
    # B 1000(Bk 0.63)^2 where Bk is the proportion of blacks by town 
    # LSTAT % lower status of the population 
    # MEDV Median value of owner-occupied homes in $1000's

    vvs = ["CRIM" ,"ZN","INDUS","CHAS","NOX","RM","AGE","DIS","RAD","TAX","PTRATIO","B","LSTAT","MEDV"]

    df = pd.read_csv(os.path.join("datasets","real_data_test","housing.csv"), header=None, delimiter=r"\s+", names = vvs)
    # df.columns = vvs
    df = df.sample(frac=1, random_state = 120395)
    ds_x5 = pd.concat([df.head(int(df.shape[0]*input_test_number/100))]*10, ignore_index=True)
    ds_x5.astype("float64").to_csv(os.path.join("datasets","real_data_test","housing_tr_shuffled_x10.csv"))
    df.tail(int(df.shape[0]*(100-input_test_number)/100)).to_csv(os.path.join("datasets","real_data_test","housing_ts_shuffled.csv"))
    # input("bk")

    # vvs = ['id', 'thickness', 'unif.size', 'unif.shape', 'adhesion', 'size','nuclei', 'chromatin', 'nucleoli', 'mitoses', 'class']
    couples_dict = rad_utils.select_candidates(vvs)
    filename = "housing"+"_delta"+str(delta_input)+"_tr_shuffled_x10.csv"
    r_vals, bound, tot_couples, tot_triples = rad_utils.calculate_r_SD_bounds(os.path.join("datasets","real_data_test","housing_tr_shuffled_x10.csv"), vvs, delta = delta_input, couples_dict= couples_dict)
    rad_utils.write_on_csv(os.path.join("IT_results","real_data_test","RAD_"+filename),r_vals, bound[0], tot_couples, tot_triples)


    run_type = 10
    vvs = ["CRIM" ,"ZN","INDUS","CHAS","NOX","RM","AGE","DIS","RAD","TAX","PTRATIO","B","LSTAT"]
    t = "MEDV"
    delta = delta_input
    if run_type%2 == 0 and not run_type > 10:
        N = 1
    else:
        sv = len(vvs)
        N = sv*(sv-1)*(2**(sv-3))
    data_file = os.path.join("datasets","real_data_test","housing_tr_shuffled_x10.csv")

    filename = data_file
    indep_file = os.path.join("IT_results","real_data_test","RAD_housing"+"_delta"+str(delta_input)+"_tr_shuffled_x10.csv")
    if os.path.exists(indep_file):
        indep_df = pd.read_csv(indep_file,sep = ";")
        if len(indep_df.columns.values) < 2:
            indep_df = pd.read_csv(indep_file)

    dep_infos = dependencyInfos(indep_df, method="Rademacher", independence_method="Rademacher", data_file=data_file, indep_file=indep_file, save_every_update=False, independence_language="-")
    
    
    if run_type == 10:
        res = mb_algo(dep_infos, t, vvs,delta,1)

    IT_utils.save_IT_dataframe(dep_infos)
    f = open(os.path.join("IT_results","real_data_test","housing_tr_shuffled_x10.txt"),"a")
    f.write("Split %"+str(input_test_number)+"\n")
    f.write(str(delta)+"MB " +str(run_type)+" "+str(res)+"\n")
    f.write(str(delta)+"PC " +str(run_type)+" "+str(pc_def(dep_infos, t, vvs, delta, 1))+"\n")
    f.close()