import numpy as np
import os
import sys
from tqdm import tqdm
import multiprocess as mp
import itertools
from itertools import chain, combinations

from numpy.core.fromnumeric import var
from summands_bounds_calculus import create_S_matrix, calculate_bound_per_variable, calculate_bound_total, create_summand_S
from summands_lr_fitter import get_R2_scores_dict
import pandas as pd

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits
import IT_utils

# seed = 120395

def loop_calculation1(*args):
    # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable
    params = args[0]
    s = params[0]
    r = params[1]
    test_n = params[2]
    zi = params[3]
    v_set = list(["A1","B1","C1"])
    for i in range(1,r):
        v_set.append("A"+str(i+1))
        v_set.append("B"+str(i+1))
        v_set.append("C"+str(i+1))
    v_set.append("T")
    v_set = sorted(set(v_set))

    if zi:
        df = pd.read_csv(os.path.join("datasets","multiple","t"+str(test_n),"v4_S"+str(s)+"_R"+str(r)+"ZI.csv"))
    else:
        df = pd.read_csv(os.path.join("datasets","multiple","t"+str(test_n),"v4_S"+str(s)+"_R"+str(r)+".csv"))

    lbs = dict()
    for w in sorted(v_set):
        vars = set([v for v in v_set if v!=w])
        ps = [IT_utils.convert_to_string(el) for el in sorted(IT_utils.powerset(vars), key = lambda x: IT_utils.convert_to_string(x))]
        summand_names, naive, multiple, single = calculate_summands_lb(df, sorted(vars), w, ps, delta = 0.05, n = 1000, min_val = 0)
        lbs[w] = (summand_names,naive,multiple,single)

    data = []
    for w in sorted(v_set):
        summands,naive,multiple,single = lbs[w] 
        for sum in summands:
            first, second, z = IT_utils.convert_summand_to_it(sum, w)
            data.append([first,second,z])
            data.append([naive[summands.index(sum)], multiple[summands.index(sum)], single[summands.index(sum)]])

    data = np.reshape(data,(-1,6))
    out_df = pd.DataFrame(data, columns=["first_term", "second_term", "cond_set", "naive_lb", "multiple_lb", "single_lb"])

    if zi:
        out_df.to_csv(os.path.join("IT_results","multiple","t"+str(test_n)+"summ_S"+str(s)+"_R"+str(r)+"ZI.csv"))
    else:
        out_df.to_csv(os.path.join("IT_results","multiple","t"+str(test_n)+"summ_S"+str(s)+"_R"+str(r)+".csv"))
    
    return True

def loop_calculation2(*args):
    # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable
    params = args[0]
    s = params[0]
    test_n = params[1]
    zi = params[2]
    r = 1
    v_set = list(["A","B","C","D","T"])

    if zi:
        df = pd.read_csv(os.path.join("datasets","multiple5","t"+str(test_n),"v4_S"+str(s)+"ZI.csv"))
    else:
        df = pd.read_csv(os.path.join("datasets","multiple5","t"+str(test_n),"v4_S"+str(s)+".csv"))

    lbs = dict()
    for w in sorted(v_set):
        vars = set([v for v in v_set if v!=w])
        ps = [IT_utils.convert_to_string(el) for el in sorted(IT_utils.powerset(vars), key = lambda x: IT_utils.convert_to_string(x))]
        summand_names, naive, multiple, single = calculate_summands_lb(df, sorted(vars), w, ps, delta = 0.05, n = 1000, min_val = 0)
        lbs[w] = (summand_names,naive,multiple,single)

    data = []
    for w in sorted(v_set):
        summands,naive,multiple,single = lbs[w] 
        for sum in summands:
            first, second, z = IT_utils.convert_summand_to_it(sum, w)
            data.append([first,second,z])
            data.append([naive[summands.index(sum)], multiple[summands.index(sum)], single[summands.index(sum)]])

    data = np.reshape(data,(-1,6))
    out_df = pd.DataFrame(data, columns=["first_term", "second_term", "cond_set", "naive_lb", "multiple_lb", "single_lb"])

    path = os.path.join(os.getcwd(), "IT_results","multiple5")
    if not os.path.exists(path):        
        os.mkdir(path)

    if zi:
        out_df.to_csv(os.path.join(path,"t"+str(test_n)+"summ_S"+str(s)+"_R"+str(r)+"ZI.csv"))
    else:
        out_df.to_csv(os.path.join(path,"t"+str(test_n)+"summ_S"+str(s)+"_R"+str(r)+".csv"))
    
    return True

def calculate_summands_lb(df, variable_list, t, ps, delta = 0.05, n = 1000, min_val = 0):
    n_train = min(10000,max(10,int(len(df.values)/10)))
    data_v = np.transpose([df[v].values for v in variable_list])
    target = df[t].values
    scores = get_R2_scores_dict(data_v, target, n_train, variable_list)

    m = len(target) - n_train
    sigma = np.random.rand(n,m) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 
    
    naive_val = 0
    summ_matrix, summand_names = create_S_matrix(R2_dict =  scores, ps = ps, min_val = min_val, n_v_sets = m, variable_list = variable_list, naive_score = naive_val)
    corrected_delta = delta / len(summand_names)  # correction for multiple hypotesis testing
    z = max(1,abs(1-min_val))   # since the minimum value of the summand V1-V2 may be -2 in the case of V1 = 1 and V2 = -1
    c = abs(1 - abs(1-min_val)) # for the same reason of above
    lb_naive  = np.mean(summ_matrix, axis = 0)

    sd_bounds_mb = calculate_bound_per_variable(summ_matrix, n=n, sigma = sigma, delta=corrected_delta, z = z, c = c, get_splitted_values = False)
    sd_bounds_sb = calculate_bound_total(n=n, variable_list = variable_list, S = summ_matrix, S_names=summand_names, sigma = sigma, delta = delta, z=z, c=c, get_splitted_values = False)
    
    return summand_names, lb_naive, lb_naive-sd_bounds_mb, lb_naive-sd_bounds_sb
    
# df = pd.read_csv("datasets/v2_S100000_R1.csv")
# variable_set = set(["A1","B1","C1", "T"])
# lbs = dict()
# for w in sorted(variable_set):
#     vars = set([v for v in variable_set if v!=w])
#     ps = [IT_utils.convert_to_string(el) for el in sorted(IT_utils.powerset(vars), key = lambda x: IT_utils.convert_to_string(x))]
#     summand_names, naive, multiple, single = calculate_summands_lb(df, sorted(vars), w, ps, delta = 0.05, n = 1000, min_val = 0)
#     lbs[w] = (summand_names,naive,multiple,single)

# data = []
# for w in sorted(variable_set):
#     summands,naive,multiple,single = lbs[w] 
#     for sum in summands:
#         first, second, z = IT_utils.convert_summand_to_it(sum, w)
#         data.append([first,second,z])
#         data.append([naive[summands.index(sum)], multiple[summands.index(sum)], single[summands.index(sum)]])
# data = np.reshape(data,(-1,6))
# out_df = pd.DataFrame(data, columns=["first_term", "second_term", "cond_set", "naive_lb", "multiple_lb", "single_lb"])
# print(out_df)

if __name__ == '__main__':
    parser = argparse.ArgumentParser('main')
    parser.add_argument('-n', '--test_n', type=int, default= -1)
    parser.add_argument('-p', '--processors', type=int, default= -1)
    parser.add_argument('-r', '--r', type=int, default= -1)
    parser.add_argument('-s', '--samples', type=int, default= -1)
    parser.add_argument('-t', '--time', type=bool, default= True)
    parser.add_argument('-v', '--version', type=int, default= -1)
    parser.add_argument('-z', '--zi', type=bool, default= False)
    
    args = parser.parse_args()
    
    process_number = args.processors
    test_n = args.test_n
    r = args.r
    s = args.samples
    output_time = args.time
    version = args.version
    zi = args.zi
    print("Parameters =",args)
    # version 1 = test with recurrent structures
    if version == 1:
        if r!= -1 and s!=-1 and test_n !=-1:
            rs = [r]
            ss = [s]
            ts = [test_n]
        else:
            print("Performing all calculations")
            rs = [1,2]
            ss = [10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]
            ts = [i for i in range(100)]
            zs = [True, False]

        # np.random.seed(seed=seed)

        if process_number < 1:
            process_number = len(rs)*len(ss)*len(ts)*len(zs)
        if process_number > 72:
            print("Limiting process number to 72")
            process_number = 72
            
        if output_time:
            t1 = time.time()
        
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(process_number)
            input = ((a,b,c,d) for a,b,c,d in list(itertools.product(ss, rs, ts, zs)))
            results = p.map(loop_calculation1, input)   
            p.close()
            p.join()
            
        if output_time:
            print((time.time()-t1)/60,"minutes elapsed for",len(rs)*len(ss)*len(ts)*len(zs),"runs")
    # version 2 = test with fully connected DAG
    if version == 2:
        if s!=-1 and test_n !=-1:
            ss = [s]
            ts = [test_n]
            zs = [zi]
        else:
            print("Performing all calculations")
            ss = [10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000]
            ts = [i for i in range(100)]
            zs = [True, False]

        # np.random.seed(seed=seed)

        if process_number < 1:
            process_number = len(ss)*len(ts)*len(zs)
        if process_number > 72:
            print("Limiting process number to 72")
            process_number = 72
            
        if output_time:
            t1 = time.time()
        
        with threadpool_limits(limits=1, user_api='blas'):
            p = mp.Pool(process_number)
            input = ((a,b,c) for a,b,c in list(itertools.product(ss, ts, zs)))
            results = p.map(loop_calculation2, input)   
            p.close()
            p.join()
            
        if output_time:
            print((time.time()-t1)/60,"minutes elapsed for",len(ss)*len(ts)*len(zs),"runs")

    