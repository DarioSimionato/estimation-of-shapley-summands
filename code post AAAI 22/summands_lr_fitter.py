# contains functions to perform the evaluation of R^2 values

from os import error
import numpy as np
from sklearn import linear_model
import math
import IT_utils

def compose_dataset(X, col_names, selection):
    # given a selection of variables, create a dataset using only the respective variables
    X_ret = []
    for var in selection:
        X_ret.append(X[:,col_names.index(var)])
    return np.array(X_ret).transpose()

def get_R2_score(y_mean, y_real, y_pred):
    return 1-sum((y_real - y_pred)*(y_real - y_pred))/sum((y_real - y_mean)*(y_real - y_mean))

def get_R2_scores_dict(data, target, n_elements_tr, variable_list):
    # creates a dataset and multiple validation sets calculating R^2 considering each subset of variables.
    # Returns a dictionary with the subset as a key and an array of R^2 values of each validation set
    data_tr = data[:n_elements_tr]
    data_ts = data[n_elements_tr:]
    target_tr = target[:n_elements_tr]
    target_ts = target[n_elements_tr:]
    variable_set = set(variable_list)
    n_elements_va = 1  # unused
    Y_train_mean = np.mean(target_tr)
    
    R2_values  = dict()
    for subset in IT_utils.powerset(variable_set, get_emptyset= False):
        R2_values[IT_utils.convert_to_string(subset)] = []

    lin_reg = {}
    for subset in IT_utils.powerset(variable_set, get_emptyset= False):
        # create and train one linear regression per variable subset
        X_train = compose_dataset(X = data_tr, col_names = variable_list, selection = subset)
        lin_reg[IT_utils.convert_to_string(subset)] = get_ad_hoc_classifier(X_train, target_tr)

    for valid_number in range(len(data_ts)):    
        for subset in IT_utils.powerset(variable_set, get_emptyset= False):
            # evaluate the performance of each validation set considering every subset of variables
            X_valid = compose_dataset(X = data_ts,  col_names = variable_list, selection = subset)
            # calculate R2 using training set mean
            v = get_R2_score(y_mean = Y_train_mean, y_real = [target_ts[valid_number]], 
                y_pred = lin_reg[IT_utils.convert_to_string(subset)].predict([X_valid[valid_number]]))
            R2_values[IT_utils.convert_to_string(subset)].append(v)  

    return R2_values

def get_IT_score(dep_infos, t, x, selection, variable_list):
    if hasattr(dep_infos, "training_dataset") and hasattr(dep_infos, "test_dataset"):
        score_dict = dep_infos.lookout_scores_per_model
        df_tr = dep_infos.training_dataset
        df_ts = dep_infos.test_dataset

        part2 = IT_utils.convert_to_string(selection)
        if part2 not in score_dict:
            score_dict[part2] = train_and_get_score(df_tr, df_ts, t, selection, variable_list, dep_infos.min_val)
        if part2 == "()":
            selection = [x]
        else:
            selection.append(x)
        part1 = IT_utils.convert_to_string(selection)
        if part1 not in score_dict:
            score_dict[part1] = train_and_get_score(df_tr, df_ts, t, selection, variable_list, dep_infos.min_val)
        dep_infos.lookout_scores_per_model = score_dict
        return score_dict[part1] - score_dict[part2]
    else:
        print("Training or test set not found")
        raise error

def train_and_get_score(df_tr, df_ts, t, selection, variable_list, min_val):
    X_train = compose_dataset(X = df_tr, col_names = variable_list, selection = selection)
    target_tr = df_tr[:,variable_list.index(t)]
    Y_train_mean = np.mean(target_tr)
    X_test  = compose_dataset(X = df_ts, col_names = variable_list, selection = selection)
    target_ts = df_ts[:,variable_list.index(t)]
    clf = get_ad_hoc_classifier(X_train, target_tr)
    v = [get_R2_score(y_mean = Y_train_mean, y_real = [target_ts[row]], 
                y_pred = clf.predict([X_test[row]])) for row in range(df_ts.shape[0])]
    v = np.array([max(el, min_val) for el in v])  

    return v

def get_ad_hoc_classifier(X_tr, Y_tr):
    clf = linear_model.LinearRegression() 
    clf.fit(X_tr, Y_tr)  
    return clf



# def get_scores_from_training(n_elements_tr, n_elements_va, n_valids, variable_set):
#     # creates a dataset and multiple validation sets calculating R^2 considering each subset of variables.
#     # Returns a dictionary with the subset as a key and an array of R^2 values of each validation set
#     R2_values  = dict()
#     for subset in IT_utils.powerset(variable_set):
#         R2_values[subset] = []
               
#     train_size = n_elements_tr
#     valid_size = n_elements_va
    
#     # creation of the training set
#     a1 = np.random.normal(scale=2.0, size=train_size)
#     b1 = np.random.normal(scale=2.0, size=train_size)
#     c1 = np.random.normal(scale=2.0, size=train_size)
#     s1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=train_size)
#     t1 = a1 + b1 + c1 + np.random.normal(scale=2.0, size=train_size)

#     X_train_tot = np.array([a1,b1,c1,s1]).transpose()
#     Y_train = np.array(t1)
#     Y_train_mean = np.mean(Y_train)

#     lin_reg = {}
#     for subset in IT_utils.powerset(variable_set):
#         # create and train one linear regression per variable subset
#         X_train = compose_dataset(X = X_train_tot, col_names = variable_set, selection = subset)
#         lin_reg[subset] = linear_model.LinearRegression() 
#         lin_reg[subset].fit(X_train, Y_train)  
    
#     for valid_number in range(n_valids):   
        
#         # create a validation set
#         a2 = np.random.normal(scale=2.0, size=valid_size)
#         b2 = np.random.normal(scale=2.0, size=valid_size)
#         c2 = np.random.normal(scale=2.0, size=valid_size)
#         s2 = a2 + b2 + c2 + np.random.normal(scale=2.0, size=valid_size)
#         t2 = a2 + b2 + c2 + np.random.normal(scale=2.0, size=valid_size)

#         X_valid_tot = np.array([a2,b2,c2,s2]).transpose()
#         Y_valid = np.array(t2)           
        
#         for subset in IT_utils.powerset(variable_set):
#             # evaluate the performance of each validation set considering every subset of variables
#             X_valid = compose_dataset(X = X_valid_tot,  col_names = variable_set, selection = subset)
#             # calculate R2 using training set mean
#             v = get_R2_score(y_mean = Y_train_mean, y_real = Y_valid, y_pred = lin_reg[subset].predict(X_valid))
#             R2_values[subset].append(v)  

#     return R2_values

# # def elementary_step_calculate_and_save_R2_values(path, n_el_tr, n_el_val, n_v_sets):
# def elementary_step_calculate_and_save_R2_values(*args):
#     # performs the calculation of R^2 values of a linear regression trained on a training set of n_el_tr elements
#     # with respect to n_v_sets validation sets each one of n_el_val and then saves the results

#     params = args[0]
#     path = params[0]
#     n_el_tr = params[1]
#     n_el_val = params[2]
#     n_v_sets = params[3]
#     R2_values = get_scores_from_training(n_elements_tr = n_el_tr, n_elements_va = n_el_val, n_valids = n_v_sets,
#         variable_set = "ABCS")

#     with open(os.path.join(path,'R2_values_TRel'+str(n_el_tr)+'_VALel'+str(n_el_val)+'_on'+str(n_v_sets)+'sets.pickle'), 'wb') as handle:
#         pickle.dump(R2_values, handle, protocol=pickle.HIGHEST_PROTOCOL)

# def calculate_and_save_R2_values(n_trs_els, n_val_els, n_val_sets, process_number):
#     # parallelizes the calculation of R^2 values

#     path = os.path.join(os.getcwd(), "data")
#     if os.path.exists(path):
#         shutil.rmtree(path)  # clear previous data (if present)
#     os.mkdir(path)

#     p = mp.Pool(process_number)
#     input = ((p,tr_n, val_n, val_s) for p,tr_n, val_n, val_s in list(itertools.product([path],n_trs_els, n_val_els, n_val_sets)))  # all combos of parameters
#     results = p.map(elementary_step_calculate_and_save_R2_values, input)
#     p.close()
#     p.join()
    