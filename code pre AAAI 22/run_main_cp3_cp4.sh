#!/bin/env bash
cd /mnt/ssd4/dsim/estimation-of-shapley-summands/code/
source shapEnv/bin/activate
/home/dsim/estimation-of-shapley-summands/code/shapEnv/bin/python main.py | tee output_first_run.txt
/home/dsim/estimation-of-shapley-summands/code/shapEnv/bin/python counterproof/v3\ non\ linearities/cp3_main.py | tee output_second_run.txt
/home/dsim/estimation-of-shapley-summands/code/shapEnv/bin/python counterproof/v4\ square\ function/cp4_main.py | tee output_third_run.txt
