
import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import matplotlib.pyplot as plt
from matplotlib.transforms import offset_copy

# results

n_trs_els  = [10,25,50,100,500,1000,5000,10000]
n_val_els  = [1, 2, 5, 10, 100]
n_val_sets = [1, 10, 100, 1000, 10000, 100000]
n_tests    = 100

fig, ax = plt.subplots(3,2, figsize = (12,15))
pad = 5 # in points

# ax[0][0].annotate("Correct answers out of 100 runs\nby changing training set size", xy=(0, 0.5), xytext=(-ax[0][0].yaxis.labelpad - pad, 0),
#                 xycoords=ax[0][0].yaxis.label, textcoords='offset points',
#                 size='large', ha='right', va='center', rotation = 90)
ax[0][0].set_ylabel('By changing training set size', multialignment='center')
ax[1][0].set_ylabel('By changing test set size', multialignment='center')
ax[2][0].set_ylabel('By changing number of test sets', multialignment='center')

with open('count_results_on100.pickle', 'rb') as handle:
    results = pickle.load(handle)

    to_show_naive        = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"]      for el_tr in n_trs_els]
    to_show_prior_kn     = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["prior_kn"]["correct"]   for el_tr in n_trs_els]
    to_show_multiple_bounds   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
    to_show_single_bound   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
    
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_multiple_bounds, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='o', linestyle = 'solid')
    
    ax[0][0].set_title("First test")
    ax[0][0].set_ylim(-5,105)
    ax[0][0].set_xlabel("Training set size")
    ax[0][0].set_ylabel("Percentage of correct tests")
    ax[0][0].legend()

    to_show_naive        = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"]      for val_el in n_val_els]
    to_show_prior_kn     = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["prior_kn"]["correct"]   for val_el in n_val_els]
    to_show_multiple_bounds   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
    to_show_single_bound   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
    
    ax[1][0].plot([str(nn) for nn in n_val_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[1][0].plot([str(nn) for nn in n_val_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    ax[1][0].plot([str(nn) for nn in n_val_els], to_show_multiple_bounds, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[1][0].plot([str(nn) for nn in n_val_els], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='o', linestyle = 'solid')
    
    ax[1][0].set_ylim(-5,105)
    ax[1][0].set_xlabel("Test set size")
    ax[1][0].set_ylabel("Percentage of correct tests")
    ax[1][0].legend()

    to_show_naive        = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"]      for val_set in n_val_sets]
    to_show_prior_kn     = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["prior_kn"]["correct"]   for val_set in n_val_sets]
    to_show_multiple_bounds   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
    to_show_single_bound   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
    
    ax[2][0].plot([str(nn) for nn in n_val_sets], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[2][0].plot([str(nn) for nn in n_val_sets], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
    ax[2][0].plot([str(nn) for nn in n_val_sets], to_show_multiple_bounds, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
    ax[2][0].plot([str(nn) for nn in n_val_sets], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='o', linestyle = 'solid')
    
    ax[2][0].set_ylim(-5,105)
    ax[2][0].set_xlabel("Number of test sets")
    ax[2][0].set_ylabel("Percentage of correct tests")
    ax[2][0].legend()


with open(os.path.join(os.getcwd(),'counterproof','v4 square function','cp4_count_LRresults_on100.pickle'), 'rb') as handle:
    resultsLR = pickle.load(handle)
    with open(os.path.join(os.getcwd(),'counterproof','v4 square function','cp4_count_NNresults_on100.pickle'), 'rb') as handle:
        resultsNN = pickle.load(handle)

        to_show_naiveLR        = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"] for el_tr in n_trs_els]
        to_show_multiple_boundsLR   = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
        to_show_single_boundLR   = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
        to_show_naiveNN        = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"] for el_tr in n_trs_els]
        to_show_multiple_boundsNN   = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
        to_show_single_boundNN   = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
        
        ax[0][1].plot([str(nn) for nn in n_trs_els], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[0][1].plot([str(nn) for nn in n_trs_els], to_show_multiple_boundsLR, color = "green", label = "multiple_bounds_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[0][1].plot([str(nn) for nn in n_trs_els], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[0][1].plot([str(nn) for nn in n_trs_els], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[0][1].plot([str(nn) for nn in n_trs_els], to_show_multiple_boundsNN, color = "lime", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[0][1].plot([str(nn) for nn in n_trs_els], to_show_single_boundNN, color = "yellow", label = "single_bound_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        
        ax[0][1].set_title("Second test")
        ax[0][1].set_xlabel("Training set size")
        ax[0][1].set_ylabel("Percentage of correct tests")
        ax[0][1].set_ylim(-5,105)
        ax[0][1].legend()

        to_show_naiveLR        = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"] for val_el in n_val_els]
        to_show_multiple_boundsLR   = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
        to_show_single_boundLR   = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
        to_show_naiveNN        = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"] for val_el in n_val_els]
        to_show_multiple_boundsNN   = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
        to_show_single_boundNN   = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
        
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_multiple_boundsLR, color = "green", label = "multiple_bounds_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_multiple_boundsNN, color = "lime", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[1][1].plot([str(nn) for nn in n_val_els], to_show_single_boundNN, color = "yellow", label = "single_bound_NN", alpha = 0.5, marker='o', linestyle = 'solid')

        ax[1][1].set_ylim(-5,105)
        ax[1][1].set_xlabel("Test set size")
        ax[1][1].set_ylabel("Percentage of correct tests")
        ax[1][1].legend()

        to_show_naiveLR        = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"] for val_set in n_val_sets]
        to_show_multiple_boundsLR   = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
        to_show_single_boundLR   = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
        to_show_naiveNN        = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"] for val_set in n_val_sets]
        to_show_multiple_boundsNN   = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
        to_show_single_boundNN   = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
        
        ax[2][1].plot([str(nn) for nn in n_val_sets], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[2][1].plot([str(nn) for nn in n_val_sets], to_show_multiple_boundsLR, color = "green", label = "multiple_bounds_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[2][1].plot([str(nn) for nn in n_val_sets], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[2][1].plot([str(nn) for nn in n_val_sets], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[2][1].plot([str(nn) for nn in n_val_sets], to_show_multiple_boundsNN, color = "lime", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        ax[2][1].plot([str(nn) for nn in n_val_sets], to_show_single_boundNN, color = "yellow", label = "single_bound_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        
        ax[2][1].set_ylim(-5,105)
        ax[2][1].set_xlabel("Number of test sets")
        ax[2][1].set_ylabel("Percentage of correct tests")
        ax[2][1].legend()


# with open(os.path.join(os.getcwd(),'counterproof','v3 non linearities','cp3_count_LRresults_on100.pickle'), 'rb') as handle:
#     resultsLR = pickle.load(handle)
#     with open(os.path.join(os.getcwd(),'counterproof','v3 non linearities','cp3_count_NNresults_on100.pickle'), 'rb') as handle:
#         resultsNN = pickle.load(handle)

#         to_show_naiveLR        = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"] for el_tr in n_trs_els]
#         to_show_multiple_boundsLR   = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
#         to_show_single_boundLR   = [resultsLR[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
#         to_show_naiveNN        = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"] for el_tr in n_trs_els]
#         to_show_multiple_boundsNN   = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
#         to_show_single_boundNN   = [resultsNN[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
        
#         ax[0][2].plot([str(nn) for nn in n_trs_els], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[0][2].plot([str(nn) for nn in n_trs_els], to_show_multiple_boundsLR, color = "green", label = "multiple_bounds_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[0][2].plot([str(nn) for nn in n_trs_els], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[0][2].plot([str(nn) for nn in n_trs_els], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[0][2].plot([str(nn) for nn in n_trs_els], to_show_multiple_boundsNN, color = "lime", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[0][2].plot([str(nn) for nn in n_trs_els], to_show_single_boundNN, color = "yellow", label = "single_bound_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        
#         ax[0][2].set_title("Third test")
#         ax[0][2].set_ylim(-5,105)
#         ax[0][2].legend()

#         to_show_naiveLR        = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"] for val_el in n_val_els]
#         to_show_multiple_boundsLR   = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
#         to_show_single_boundLR   = [resultsLR[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
#         to_show_naiveNN        = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"] for val_el in n_val_els]
#         to_show_multiple_boundsNN   = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
#         to_show_single_boundNN   = [resultsNN[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
        
#         ax[1][2].plot([str(nn) for nn in n_val_els], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[1][2].plot([str(nn) for nn in n_val_els], to_show_multiple_boundsLR, color = "green", label = "multiple_bounds_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[1][2].plot([str(nn) for nn in n_val_els], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[1][2].plot([str(nn) for nn in n_val_els], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[1][2].plot([str(nn) for nn in n_val_els], to_show_multiple_boundsNN, color = "lime", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[1][2].plot([str(nn) for nn in n_val_els], to_show_single_boundNN, color = "yellow", label = "single_bound_NN", alpha = 0.5, marker='o', linestyle = 'solid')

#         ax[1][2].set_ylim(-5,105)
#         ax[1][2].legend()

#         to_show_naiveLR        = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"] for val_set in n_val_sets]
#         to_show_multiple_boundsLR   = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
#         to_show_single_boundLR   = [resultsLR[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
#         to_show_naiveNN        = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"] for val_set in n_val_sets]
#         to_show_multiple_boundsNN   = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
#         to_show_single_boundNN   = [resultsNN[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
        
#         ax[2][2].plot([str(nn) for nn in n_val_sets], to_show_naiveLR, color = "blue", label = "naive_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[2][2].plot([str(nn) for nn in n_val_sets], to_show_multiple_boundsLR, color = "green", label = "multiple_bounds_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[2][2].plot([str(nn) for nn in n_val_sets], to_show_single_boundLR, color = "purple", label = "single_bound_LR", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[2][2].plot([str(nn) for nn in n_val_sets], to_show_naiveNN, color = "red", label = "naive_NN", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[2][2].plot([str(nn) for nn in n_val_sets], to_show_multiple_boundsNN, color = "lime", label = "multiple_bounds_NN", alpha = 0.5, marker='o', linestyle = 'solid')
#         ax[2][2].plot([str(nn) for nn in n_val_sets], to_show_single_boundNN, color = "yellow", label = "single_bound_NN", alpha = 0.5, marker='o', linestyle = 'solid')
        
#         ax[2][2].set_ylim(-5,105)
#         ax[2][2].legend()

fig.tight_layout()
fig.subplots_adjust(left=0.05, top=0.95)
fig.suptitle("Correct answers out of 100 runs")
# fig.show()
fig.savefig('plots_only_correct_100.pdf')

# horizontal alligment one
# import numpy as np
# import os
# import sys
# from tqdm import tqdm
# import pickle5 as pickle
# import matplotlib.pyplot as plt
# from matplotlib.transforms import offset_copy

# # results

# n_trs_els  = [10,25,50,100,500,1000,5000,10000]
# n_val_els  = [1, 2, 5, 10, 100]
# n_val_sets = [1, 10, 100, 1000, 10000, 100000]
# n_tests    = 100

# fig, ax = plt.subplots(3,3, figsize = (15,15), sharey = "row")
# pad = 5 # in points

# # ax[0][0].annotate("Correct answers out of 100 runs\nby changing training set size", xy=(0, 0.5), xytext=(-ax[0][0].yaxis.labelpad - pad, 0),
# #                 xycoords=ax[0][0].yaxis.label, textcoords='offset points',
# #                 size='large', ha='right', va='center', rotation = 90)
# ax[0][0].set_ylabel('First test', multialignment='center')
# ax[1][0].set_ylabel('Second test', multialignment='center')
# ax[2][0].set_ylabel('Third test', multialignment='center')

# with open('count_results_on100.pickle', 'rb') as handle:
#     results = pickle.load(handle)
    
#     to_show_naive        = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["naive"]["correct"]      for el_tr in n_trs_els]
#     to_show_prior_kn     = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["prior_kn"]["correct"]   for el_tr in n_trs_els]
#     to_show_multiple_bounds   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["multiple_bounds"]["correct"] for el_tr in n_trs_els]
#     to_show_single_bound   = [results[el_tr][n_val_els[-1]][n_val_sets[-1]]["single_bound"]["correct"] for el_tr in n_trs_els]
    
#     ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
#     ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
#     ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_multiple_bounds, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
#     ax[0][0].plot([str(nn) for nn in n_trs_els], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='o', linestyle = 'solid')
    
#     ax[0][0].set_title("By changing training set size")
#     ax[0][0].set_ylim(-5,105)
#     ax[0][0].legend()

#     to_show_naive        = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["naive"]["correct"]      for val_el in n_val_els]
#     to_show_prior_kn     = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["prior_kn"]["correct"]   for val_el in n_val_els]
#     to_show_multiple_bounds   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["multiple_bounds"]["correct"] for val_el in n_val_els]
#     to_show_single_bound   = [results[n_trs_els[-1]][val_el][n_val_sets[-1]]["single_bound"]["correct"] for val_el in n_val_els]
    
#     ax[0][1].plot([str(nn) for nn in n_val_els], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
#     ax[0][1].plot([str(nn) for nn in n_val_els], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
#     ax[0][1].plot([str(nn) for nn in n_val_els], to_show_multiple_bounds, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
#     ax[0][1].plot([str(nn) for nn in n_val_els], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='o', linestyle = 'solid')
    
#     ax[0][1].set_ylim(-5,105)
#     ax[0][1].set_title("By changing number of elements per test set")
#     ax[0][1].legend()

#     to_show_naive        = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["naive"]["correct"]      for val_set in n_val_sets]
#     to_show_prior_kn     = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["prior_kn"]["correct"]   for val_set in n_val_sets]
#     to_show_multiple_bounds   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["multiple_bounds"]["correct"] for val_set in n_val_sets]
#     to_show_single_bound   = [results[n_trs_els[-1]][n_val_els[-1]][val_set]["single_bound"]["correct"] for val_set in n_val_sets]
    
#     ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_naive, color = "blue", label = "naive", alpha = 0.5, marker='o', linestyle = 'solid')
#     ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_prior_kn, color = "red", label = "prior_kn", alpha = 0.5, marker='o', linestyle = 'dashed')
#     ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_multiple_bounds, color = "green", label = "multiple_bounds", alpha = 0.5, marker='o', linestyle = 'solid')
#     ax[0][2].plot([str(nn) for nn in n_val_sets], to_show_single_bound, color = "purple", label = "single_bound", alpha = 0.5, marker='o', linestyle = 'solid')
    
#     ax[0][2].set_ylim(-5,105)
#     ax[0][2].set_title("By changing number of test sets")
#     ax[0][2].legend()

# fig.tight_layout()
# fig.subplots_adjust(left=0.05, top=0.95)
# fig.suptitle("Correct answers out of 100 runs")
# # fig.show()
# fig.savefig('plots_only_correct_100.pdf')



