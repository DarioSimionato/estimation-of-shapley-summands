# calculates the rademacher average estimate of the supreme deviation of each summand

import numpy as np
import math
from itertools import chain, combinations
import math
import pickle5 as pickle
import csv
import os
from tqdm import tqdm

def convert_to_matrix(m, R2_dict, ps):
    # takes a R^2 values dictionary and converts it in matrix form to be consistent with single_bound notation.
    # Therefore in each column there are all the R^2 values of a given varible set.
    # The order of the column is given by the order of the elements in the powerset ps
    R2_mat = np.zeros((m ,len(ps)))
    for k in R2_dict.keys():
        k_conv = ''.join(sorted(x for x in k if x.isalpha()))
        R2_mat[:, ps.index(k_conv)] = R2_dict[k]
    return R2_mat

def clip_values(S, min_val):
    S[S<min_val] = min_val
    return S

def create_summand_S(S,ps,variable_set, naive_score = 0):
    # Given a matrix of R^2 measures of variables sets it returns a matrix of R^2 values
    # with respect to each summand. It also returns the new labelling of each column. 
    new_ps = []
    new_S = []
    for var in variable_set:
        for s1 in ps:
            if var in s1:
                s2 = s1.replace(var, "")                
                if s2 == "":
                    s2 = "()" 
                new_ps.append(s1+" - "+s2)
                if s2 in ps:
                    new_S.append(S[:,ps.index(s1)] - S[:,ps.index(s2)])
                else:
                    new_S.append(S[:,ps.index(s1)] - naive_score) 
    new_S = np.reshape(new_S,(-1,S.shape[0])).transpose()
    return new_S, new_ps

def create_S_matrix(R2_dict,ps, min_val, n_v_sets, variable_set, naive_score = 0, keep_metrics_divided = False):
    # Takes a dictionary of R^2 measurements and it converts it into a matrix of R^2
    # measures for which in every column there are all the R^2 values of a given summand.
    # If keep_metrics_divided, then each column contains the value of every part of the summand
    # (e.g. m(ABC)) instead of the summans (e.g. m(ABC)-m(AB)) 
    R2_mat  = convert_to_matrix(n_v_sets, R2_dict, ps)   
    R2_mat  = clip_values(R2_mat, min_val)  
    if keep_metrics_divided:
        return R2_mat, ps
    else:
        S, new_ps = create_summand_S(R2_mat,ps, variable_set, naive_score = naive_score) 
        return S, new_ps

def calculate_r_hat_multiple_bounds(S_vec,sigma):
    # calulates the Rademacher averages using Monte-Carlo method
    n = sigma.shape[0]
    m = len(S_vec)
    
    return np.abs(np.sum(np.multiply(sigma,S_vec))/m/n)
    
def calculate_r_hat_single_bound(S_matrix,sigma):
    # calulates the Rademacher averages using Monte-Carlo method
    n = sigma.shape[0]
    m = len(S_matrix)
    # print("n",n,"   m",m,"   sigma",sigma.shape, "   S_matrix", S_matrix.shape)
    # print("A",np.matmul(sigma,S_matrix).shape)
    # print("B",np.max(np.matmul(sigma,S_matrix)/m, axis = 1).shape)
    # print("I should have",math.factorial(4),"estimates from which to take the max")
    # print("MB   sigma",sigma.shape, "   S_matrix", S_matrix.shape, "   R_hat",np.abs(np.sum(np.max(np.matmul(sigma,S_matrix)/m, axis = 1))/n) )
    # print("Petrificus totalus")
    # raise Exception
    return np.abs(np.sum(np.max(np.matmul(sigma,S_matrix)/m, axis = 1))/n)

def calculate_sup_dev_bound(m, n, R_tilde, delta, z, c, get_splitted_values):
    # calculating the supreme deviation bound and splitting each contribution into summands
    s1 = 2*R_tilde
    s2 = math.sqrt(c*(4*m*R_tilde+c*math.log(4/delta))*math.log(4/delta))/m
    s3 = c*math.log(4/delta)/m
    s4 = c*math.sqrt(math.log(4/delta)/(2*m))
    if get_splitted_values:
        return s1 + s2 + s3 + s4, (s1,s2,s3,s4)
    return s1 + s2 + s3 + s4

def calculate_bound_total(n, variable_set, S, S_names, sigma, delta, z, c, get_splitted_values = False):
    m = S.shape[0]   # number of functs to estimate
    R_hat = calculate_r_hat_single_bound(S,sigma)
    R_tilde = R_hat + 2*z*math.sqrt(math.log(4/delta)/2/n/m)
    return calculate_sup_dev_bound(m, n, R_tilde, delta, z, c, get_splitted_values)

def calculate_bound_per_variable(S, n, sigma, delta, z, c, get_splitted_values = False):
    # given a matrix of summands, it returns the bound of the supreme deviation for each summand
    # using Monte-Carlo method for estimating Rademacher averages
    m = S.shape[0]
    summand_number = S.shape[1]
       
    R_hat_per_var = np.array([calculate_r_hat_multiple_bounds(S[:,i],sigma) for i in range(summand_number)])
    R_tilde = R_hat_per_var + 2*z*math.sqrt(math.log(4/delta)/(2*n*m))
    return np.array([calculate_sup_dev_bound(m, n, R_tilde[i], delta, z = z, c = c, get_splitted_values = get_splitted_values) for i in range(summand_number)])