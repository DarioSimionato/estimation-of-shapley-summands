# R script to load values from the dataset, run all independence tests and save them result on a .csv file

##
# .csv file specification for conditional dependencies
#   fields
#  first_term, second_term, cond_set, p_value, statistic, lb
#  where each term and set contains only letters and the empty set is identified by '()'
#  and the p_value is the p value of the test with independence as null hypothesis.
#  In methods that do not use this independence test (e.g. summand independence methods)
#  The field lb (=lower bound) contains the lower bound of the summand under study
##

library(readr)
library(bnlearn)
library(progress)


enum.choose <- function(x, k) {
  if(k > length(x)) stop('k > length(x)')
  if(choose(length(x), k)==1){
    list(as.vector(combn(x, k)))
  } else {
    cbn <- combn(x, k)
    lapply(seq(ncol(cbn)), function(i) cbn[,i])
  }
}

subsets <- function(x){
  res <-  list("()")
  for (k in 1:length(x)){
    res <- append(res, enum.choose(col_names,k))
  }
  return <- res
}

perform_CI <- function(c1, c2, s, dataset, test_type){
  if (!s[1]=="()"){
    t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
  }else{
    t <- ci.test(c1, c2, data = dataset, test = test_type, debug = TRUE)
  }
  print(t)
  return <- t
}

#dataset <- read_csv("Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/v1_10000_100000.csv")
#dataset <- read_csv("datasets/v1_10000_100000.csv")
#col_names <- c("A","B","C","S","T")
args <- commandArgs(trailingOnly = TRUE)
if (length(args)==0) {
  stop("At least one argument must be supplied", call.=FALSE)
}
print(as.character(args))
test_number <- as.integer(args[1])

# TEST 1 = test using data from bnlearn library
if(test_number==1){
  # d_names <-c("insurance")
  # test_type <- "mi"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  
  d_names <- c("gauss_test")
  test_type <- "cor"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  
  for (data_name in d_names){
    dataset <- read_csv(paste("datasets/",data_name,".csv", sep = ""))
    if (data_name == "gauss_test"){
      # col_names <- c("A","B","C","D","E","F","G")
      col_names <- c("A","B","C","D")
    }
    if (data_name == "alarm"){
      col_names <- c("CVP","PCWP","HIST","TPR","BP","CO","HRBP","HREK","HRSA","PAP","SAO2","FIO2","PRSS","ECO2","MINV","MVS","HYP","LVF","APL","ANES","PMB","INT","KINK","DISC","LVV","STKV","CCHL","ERLO","HR","ERCA","SHNT","PVS","ACO2","VALV","VLNG","VTUB","VMCH")
    }
    if (data_name == "insurance"){
      col_names <- c("GoodStudent","Age","SocioEcon","RiskAversion","VehicleYear","ThisCarDam","RuggedAuto","Accident","MakeModel","DrivQuality","Mileage","Antilock","DrivingSkill","SeniorTrain","ThisCarCost","Theft","CarValue","HomeBase","AntiTheft","PropCost","OtherCarCost","OtherCar","MedCost","Cushioning","Airbag","ILiCost","DrivHist")
    }
    col_subsets <- subsets(col_names)
    first_item <- list()
    second_item <- list()
    cond_set <- list()
    p_value <- list()
    cor <- list()
    lb <- list()
    for (c1 in col_names){
      for (c2 in col_names){
        for (s in col_subsets){
          if (!c1 %in% s & !c2 %in% s & c1 != c2){
            # perform the test and add it to the dataframe
            # if (!s[1]=="()"){
            #  t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
            # }else{
            #  t <- ci.test(c1, c2, data = dataset, test = test_type, debug = TRUE)
            # }
            t <- perform_CI(c1, c2, s, dataset, test_type)
            first_item <- append(first_item,c1)
            second_item <- append(second_item,c2)
            cond_set <- append(cond_set, paste(s, collapse = ''))
            p_value <- append(p_value,t$p.value)
            cor <- append(cor,t$statistic[[1]])
            lb <- append(lb,-1)
          }
        }
      }
    }
    df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
    write.table(df,paste("IT_results/",data_name,"_",test_type,".csv", sep = ""), 
                row.names = FALSE,
                col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                sep = ",")
  }
}
# TEST 2 = test using data handmade (starting with v2, so with multiple structure repetitions)
if(test_number==2){
  r <- as.integer(args[2])
  zi <- as.logical(args[3])
  test_type <- "mi-g"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  n_tot_els <- 100000
  args <- commandArgs(trailingOnly = TRUE)
  if (zi){
    d_name <- paste("datasets/v2_S",format(n_tot_els, scientific = FALSE),"_R",r,"ZI.csv", sep = "")
  }else{
    d_name <- paste("datasets/v2_S",format(n_tot_els, scientific = FALSE),"_R",r,".csv", sep = "")
  }
  dataset <- read_csv(d_name)
  col_names <- c("A1","B1","C1")
  # print(col_names)
  
  if (r>1){
    for (i in 2:r){
      col_names <- append(col_names,c(paste("A",i,sep = ""),paste("B",i,sep = ""),paste("C",i,sep = "")))
      # print(col_names)
    }
    # print(col_names)
  }
  col_names <- append(col_names,"T")  
  col_subsets <- subsets(col_names)
  print("Total number of subsets")
  print(length(col_subsets))
  # print(col_names)
  
  
  list_vals <- c(10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,7500,10000,25000,50000,75000,100000)
  # list_vals <- c(10,25)
  
  for (max_els in list_vals){
    limited_data <- dataset[1:max_els,]
    
    # print(limited_data)
    # print(col_names)
    
    first_item <- list()
    second_item <- list()
    cond_set <- list()
    p_value <- list()
    cor <- list()
    lb <- list()
    pb <- progress_bar$new(total = length(col_names)^2)
    for (c1 in col_names){
      for (c2 in col_names){
        for (s in col_subsets){
          if (!c1 %in% s & !c2 %in% s & c1 != c2){
            # perform the test and add it to the dataframe
            # print(c1)
            # print(c2)
            # print(c(s))
            # if (!s[1]=="()"){
            #   t <- ci.test(c1, c2, c(s), data = limited_data, test = test_type)
            # }else{
            #   t <- ci.test(c1, c2, data = limited_data, test = test_type)
            # }
            t <- perform_CI(c1, c2, s, dataset, test_type)
            first_item <- append(first_item,c1)
            second_item <- append(second_item,c2)
            cond_set <- append(cond_set, paste(s, collapse = ''))
            p_value <- append(p_value,t$p.value)
            cor <- append(cor,t$statistic[[1]])
            lb <- append(lb,-1)
          }
        }
        pb$tick()
      }
    }
    df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
    
    if (zi){
      res_name <- paste(test_type,"_S",format(max_els, scientific = FALSE),"_R",r,"ZI.csv", sep = "")
    }else{
      res_name <- paste(test_type,"_S",format(max_els, scientific = FALSE),"_R",r,".csv", sep = "")
    }
    
    write.table(df,paste("IT_results/",res_name, sep = ""), 
                row.names = FALSE,
                col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                sep = ",")
  }
}
# TEST 3 = test using a fully connected DAG for testing the number of conditional independencies
if(test_number==3){
  v <- as.integer(args[2])
  zi <- as.logical(args[3])
  test_type <- "cor"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  n_tot_els <- as.integer(args[4])
  if (zi){
    d_name <- paste("datasets/v3_S",format(n_tot_els, scientific = FALSE),"_R",v,"ZI.csv", sep = "")
  }else{
    d_name <- paste("datasets/v3_S",format(n_tot_els, scientific = FALSE),"_R",v,".csv", sep = "")
  }
  dataset <- read_csv(d_name)
  col_names <- c(LETTERS)[1:(v+1)]
  col_names <- append(col_names,"T")
  print(c("Col names", col_names))
  col_subsets <- subsets(col_names)
  print(c("Total number of subsets",length(col_subsets)))
    
  first_item <- list()
  second_item <- list()
  cond_set <- list()
  p_value <- list()
  cor <- list()
  lb <- list()
  pb <- progress_bar$new(total = length(col_names)^2)
  for (c1 in col_names){
    for (c2 in col_names){
      for (s in col_subsets){
        if (!c1 %in% s & !c2 %in% s & c1 != c2){
          # perform the test and add it to the dataframe
          if (!s[1]=="()"){
            t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
          }else{
            t <- ci.test(c1, c2, data = dataset, test = test_type)
          }
          first_item <- append(first_item,c1)
          second_item <- append(second_item,c2)
          cond_set <- append(cond_set, paste(s, collapse = ''))
          p_value <- append(p_value,t$p.value)
          cor <- append(cor,t$statistic[[1]])
          lb <- append(lb,-1)
        }
      }
      pb$tick()
    }
  }
  df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
  
  if (zi){
    res_name <- paste("v",test_number,"_S",format(n_tot_els, scientific = FALSE),"_R",v,"ZI.csv", sep = "")
  }else{
    res_name <- paste("v",test_number,"_S",format(n_tot_els, scientific = FALSE),"_R",v,".csv", sep = "")
  }
  
  write.table(df,paste("IT_results/",res_name, sep = ""), 
              row.names = FALSE,
              col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
              sep = ",")
  print(c("Saved ITs in",paste("IT_results/",res_name, sep = "")))
}
# TEST 4 = test with repetition on 100 tentatives
if(test_number==4){
  r <- as.integer(args[2])
  zi <- as.logical(args[3])
  test_type <- "cor"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  args <- commandArgs(trailingOnly = TRUE)
  col_names <- c("A1","B1","C1")
  if (r>1){
    for (i in 2:r){
      col_names <- append(col_names,c(paste("A",i,sep = ""),paste("B",i,sep = ""),paste("C",i,sep = "")))
    }
  }
  col_names <- append(col_names,"T")  
  col_subsets <- subsets(col_names)
  list_vals <- c(10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000)
  
  subDir <- "IT_results/multiple"
  
  if (!file.exists(subDir)){
    dir.create(file.path(subDir))
  }
  
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    tnumb <- paste("t",format(test_n, scientific = FALSE), sep = '')
    folder <- paste(subDir,tnumb, sep = "/")
    if (!file.exists(folder)){
      dir.create(file.path(folder))
    }
    for (n_tot_els in list_vals){
      if (zi){
        d_name <- paste("datasets/multiple/",tnumb,"/v4_S",format(n_tot_els, scientific = FALSE),"_R",r,"ZI.csv", sep = "")
      }else{
        d_name <- paste("datasets/multiple/",tnumb,"/v4_S",format(n_tot_els, scientific = FALSE),"_R",r,".csv", sep = "")
      }
      dataset <- read_csv(d_name)
      
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
      }
      df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
      
      if (zi){
        res_name <- paste(test_type,"_S",format(n_tot_els, scientific = FALSE),"_R",r,"ZI.csv", sep = "")
      }else{
        res_name <- paste(test_type,"_S",format(n_tot_els, scientific = FALSE),"_R",r,".csv", sep = "")
      }
      
      write.table(df,paste(folder,res_name, sep = ""), 
                  row.names = FALSE,
                  col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                  sep = ",")
    }
    pb$tick()
  }
}
# TEST 5 = test with a fully connected DAG on 100 tentatives
if(test_number==5){
  
  test_type <- "cor"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  args <- commandArgs(trailingOnly = TRUE)
  col_names <- c("A","B","C","D")
  col_names <- append(col_names,"T")
  print(c("Col names", col_names))
  col_subsets <- subsets(col_names)
  print(c("Total number of subsets",length(col_subsets)))
  list_vals <- c(10,25,50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000)
  
  subDir <- "IT_results/multiple"
  
  if (!file.exists(subDir)){
    dir.create(file.path(subDir))
  }
  
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    tnumb <- paste("fully_t",format(test_n, scientific = FALSE), sep = '')
    folder <- paste(subDir,tnumb, sep = "/")
    if (!file.exists(folder)){
      dir.create(file.path(folder))
    }
    for (zi in c(TRUE,FALSE)){
      for (n_tot_els in list_vals){
        if (zi){
          d_name <- paste("datasets/multiple5/t",test_n,"/v5_S",format(n_tot_els, scientific = FALSE),"ZI.csv", sep = "")
        }else{
          d_name <- paste("datasets/multiple5/t",test_n,"/v5_S",format(n_tot_els, scientific = FALSE),".csv", sep = "")
        }
        dataset <- read_csv(d_name)
        
        first_item <- list()
        second_item <- list()
        cond_set <- list()
        p_value <- list()
        cor <- list()
        lb <- list()
        for (c1 in col_names){
          for (c2 in col_names){
            for (s in col_subsets){
              if (!c1 %in% s & !c2 %in% s & c1 != c2){
                # perform the test and add it to the dataframe
                if (!s[1]=="()"){
                  t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
                }else{
                  t <- ci.test(c1, c2, data = dataset, test = test_type)
                }
                first_item <- append(first_item,c1)
                second_item <- append(second_item,c2)
                cond_set <- append(cond_set, paste(s, collapse = ''))
                p_value <- append(p_value,t$p.value)
                cor <- append(cor,t$statistic[[1]])
                lb <- append(lb,-1)
              }
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        if (zi){
          res_name <- paste(test_type,"_fully_S",format(n_tot_els, scientific = FALSE),"ZI.csv", sep = "")
        }else{
          res_name <- paste(test_type,"_fully_S",format(n_tot_els, scientific = FALSE),".csv", sep = "")
        }
        
        write.table(df,paste(folder,res_name, sep = "/"), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ",")
      }
    }
    pb$tick()
  }
}
# TEST 6 = test with discrete variables version 1-3
if(test_number==6){
  test_type <- "x2"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  list_vals <- c(50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000)
  
  subDir <- "IT_results/multiple/discrete/"
  col_names <- c("A","B","T")  
  col_subsets <- subsets(col_names)
  
  if (!file.exists(subDir)){
    dir.create(file.path(subDir))
  }
  # v1 = A,B,T two values each
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    for(size in list_vals){
      d_name <- paste("datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      #d_name <- paste("/Users/dariosimionato/Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B), T = factor(ds$T))
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    }
    pb$tick()
  }
  # v2 = A,B,T with more than two values each
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    for(size in list_vals){
      d_name <- paste("datasets/","discrete/","discrete_test_2_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B), T = factor(ds$T))      
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_2_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    }
    pb$tick()
  }
  
  col_names <- c("A","B","C","D","T")  
  col_subsets <- subsets(col_names)
  pb <- progress_bar$new(total = 100)
  # v3 = A,B,C,D,T two values each
  for (test_n in 0:99){
    for(size in list_vals){
      d_name <- paste("datasets/","discrete/","discrete_test_3_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B),C = factor(ds$C), D = factor(ds$D), T = factor(ds$T))      
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_3_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    }
    pb$tick()
  }
}
# TEST 7 = test with discrete variables version 4
if(test_number==7){
  test_type <- "x2"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  list_vals <- c(50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000)
  
  subDir <- "IT_results/multiple/discrete/"
  col_names <- c("A","B","C","T")  
  col_subsets <- subsets(col_names)
  
  if (!file.exists(subDir)){
    dir.create(file.path(subDir))
  }
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    for(size in list_vals){
      d_name <- paste("datasets/","discrete/","discrete_test_4_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      #d_name <- paste("/Users/dariosimionato/Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B), C = factor(ds$C), T = factor(ds$T))
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_4_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    }
    pb$tick()
  }
}
# TEST 8 = test with discrete variables version 1-4 on args 2 number of variables
if(test_number==8){
  size <- as.integer(args[2])
  test_type <- "x2"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html

  subDir <- "IT_results/multiple/discrete/"
  col_names <- c("A","B","T")  
  col_subsets <- subsets(col_names)
  
  if (!file.exists(subDir)){
    dir.create(file.path(subDir))
  }
  # v1 = A,B,T two values each
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
      d_name <- paste("datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      #d_name <- paste("/Users/dariosimionato/Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B), T = factor(ds$T))
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
      pb$tick()
  }
  # v2 = A,B,T with more than two values each
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
      d_name <- paste("datasets/","discrete/","discrete_test_2_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B), T = factor(ds$T))      
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_2_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    
      pb$tick()
  }
  
  col_names <- c("A","B","C","D","T")  
  col_subsets <- subsets(col_names)
  pb <- progress_bar$new(total = 100)
  # v3 = A,B,C,D,T two values each
  for (test_n in 0:99){
      d_name <- paste("datasets/","discrete/","discrete_test_3_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B),C = factor(ds$C), D = factor(ds$D), T = factor(ds$T))      
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_3_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
      pb$tick()
  }
  
  col_names <- c("A","B","C","T")  
  col_subsets <- subsets(col_names)
  pb <- progress_bar$new(total = 100)
  # v4 = A1 B1 C1 and T experiment (A1 = A, B1 = B, C1 = C)
  for (test_n in 0:99){
      d_name <- paste("datasets/","discrete/","discrete_test_4_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      #d_name <- paste("/Users/dariosimionato/Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A = factor(ds$A), B = factor(ds$B), C = factor(ds$C), T = factor(ds$T))
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_4_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    
      pb$tick()
  }
}
# TEST 9 = test with discrete variables version 5 and 6
if(test_number==9){
  test_type <- "x2"  # https://www.bnlearn.com/documentation/man/conditional.independence.tests.html
  list_vals <- c(50,100,150,200,250,300,400,500,750,1000,2500,5000,10000,25000,50000,75000,100000)
  
  subDir <- "IT_results/multiple/discrete/"
  col_names <- c("A1","B1","C1","A2","B2","C2","T")  
  col_subsets <- subsets(col_names)
  
  if (!file.exists(subDir)){
    dir.create(file.path(subDir))
  }
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    for(size in list_vals){
      d_name <- paste("datasets/","discrete/","discrete_test_5_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      #d_name <- paste("/Users/dariosimionato/Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A1 = factor(ds$A1), B1 = factor(ds$B1), C1 = factor(ds$C1), A2 = factor(ds$A2), B2 = factor(ds$B2), C2 = factor(ds$C2), T = factor(ds$T))
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_5_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    }
    pb$tick()
  }
  pb <- progress_bar$new(total = 100)
  for (test_n in 0:99){
    for(size in list_vals){
      d_name <- paste("datasets/","discrete/","discrete_test_6_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      #d_name <- paste("/Users/dariosimionato/Desktop/PhD/PhD tests/Shapley_summands/estimation_of_shapley_summands/code post AAAI 22/datasets/","discrete/","discrete_test_1_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
      ds <- read_csv(d_name)
      dataset <- data.frame(A1 = factor(ds$A1), B1 = factor(ds$B1), C1 = factor(ds$C1), A2 = factor(ds$A2), B2 = factor(ds$B2), C2 = factor(ds$C2), T = factor(ds$T))
      first_item <- list()
      second_item <- list()
      cond_set <- list()
      p_value <- list()
      cor <- list()
      lb <- list()
      for (c1 in col_names){
        for (c2 in col_names){
          for (s in col_subsets){
            if (!c1 %in% s & !c2 %in% s & c1 != c2){
              # perform the test and add it to the dataframe
              if (!s[1]=="()"){
                t <- ci.test(c1, c2, c(s), data = dataset, test = test_type)
              }else{
                t <- ci.test(c1, c2, data = dataset, test = test_type)
              }
              first_item <- append(first_item,c1)
              second_item <- append(second_item,c2)
              cond_set <- append(cond_set, paste(s, collapse = ''))
              p_value <- append(p_value,t$p.value)
              cor <- append(cor,t$statistic[[1]])
              lb <- append(lb,-1)
            }
          }
        }
        df <- cbind(first_item,second_item,cond_set,p_value,cor,lb)
        
        res_name <- paste(test_type,"_test_6_s",format(size, scientific = FALSE),"_t",test_n,".csv", sep = "")
        
        write.table(df,paste(subDir,res_name, sep = ""), 
                    row.names = FALSE,
                    col.names = c("first_term","second_term","cond_set","p_value","statistic","lb"),
                    sep = ";")
      }
    }
    pb$tick()
  }
}
