import numpy as np
import os
import sys
from tqdm import tqdm
import pickle5 as pickle
import multiprocess as mp
import itertools
from itertools import chain, combinations
from calculate_bounds import create_S_matrix, calculate_bound_per_variable, calculate_bound_total, create_summand_S
from ds_creator_bound_test import calculate_and_save_R2_values

import time
import argparse
import warnings
from sklearn.exceptions import ConvergenceWarning
warnings.filterwarnings("ignore", category=ConvergenceWarning)
from threadpoolctl import threadpool_limits


class ResultDict():
    # dictionary container of the results on each trial

    def __init__(self):
        self.results = dict()
        for na in n_add_variables:
            self.results = dict()
            self.results["single"] = []
            self.results["multiple_min"] = []
            self.results["multiple_max"] = []

    def add_values(self, a, b, c):
        self.results["single"].append(a)
        self.results["multiple_min"].append(b)
        self.results["multiple_max"].append(c)


def powerset(iterable):
    "powerset([1,2,3]) --> (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(1,len(s)+1)) # range starts from 1 so I don't have the empty set

def loop_calculation(*args):
    # Given a set of parameters, it loads the corrispondent dictionary and then it returns the informativeness
    # stats for each variable
    params = args[0]
    n_el_tr = params[0]
    n_el_val = params[1]
    n_v_sets = params[2]
    i = params[3]
    ps = params[4]
    min_val = params[5]
    var_set = params[6]

    calculate_and_save_R2_values(n_trs_els = n_el_tr, n_val_els = n_el_val, n_val_sets = n_v_sets, i = i, var_set = var_set)

    delta = 0.05
    n = 1000
    m = n_v_sets
    sigma = np.random.rand(n,m) - 0.5
    sigma[sigma>=0] = 1
    sigma[sigma<0] = -1 

    with open(os.path.join('.',"data_bound",'test'+str(i)+"_"+str(n_trs_els)+"ntels"+str(n_val_els)+"nvels"+str(n_val_sets)+'sets','R2_values_TRel'+str(n_el_tr)+'_VALel'+str(n_el_val)+'_on'+str(n_v_sets)+'sets.pickle'), 'rb') as handle:
        R2_dict = pickle.load(handle)
    
    naive_val = 0
    S, new_ps = create_S_matrix(R2_dict =  R2_dict, ps = ps, min_val = min_val, n_v_sets = n_v_sets, variable_set = var_set, naive_score = naive_val)
    corrected_delta = delta / len(new_ps)  # correction for multiple hypotesis testing
    z = max(1,abs(1-min_val))   # since the minimum value of the summand V1-V2 may be -2 in the case of V1 = 1 and V2 = -1
    c = abs(1 - abs(1-min_val)) # for the same reason of above

    sd_bounds_mb = calculate_bound_per_variable(S, n=n, sigma = sigma, delta=corrected_delta, z = z, c = c, get_splitted_values = False)
    sd_bounds_sb = calculate_bound_total(n=n, variable_set = "ABCS", S = S, S_names=new_ps, sigma = sigma, delta = delta, z=z, c=c, get_splitted_values = False)
    
    return sd_bounds_sb, min(sd_bounds_mb), max(sd_bounds_mb)



if __name__ == '__main__':
    ps = [str(t) for t in powerset("ABCS")]
    ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]

    parser = argparse.ArgumentParser('main')
    parser.add_argument('-r', '--runs', type=int, default= 10)

    parser.add_argument('-a', '--trainel', type=int, default= 10000)
    parser.add_argument('-b', '--testel', type=int, default= 1)
    parser.add_argument('-c', '--testsets', type=int, default= 100000)
    parser.add_argument('-p', '--pari', type=bool, default= False)
    parser.add_argument('-v', '--addvariables', type=int, default= -1) # 22 max
    
    args = parser.parse_args()
    n_tests = args.runs

    n_trs_els = args.trainel
    n_val_els = args.testel
    n_val_sets = args.testsets
    n_add_variables = args.addvariables
    p = args.pari
    print(p)
    if n_add_variables == -1:
        n_add_variables = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
        if p:
            n_add_variables = [v for v in n_add_variables if v%2==0]
        else:
            n_add_variables = [v for v in n_add_variables if v%2!=0]
    else:
        n_add_variables = [n_add_variables]
    
    args = parser.parse_args()
    process_number = n_tests

    # process_number = args.processors
    # n_tests = args.runs
    # output_time = args.time

    # n_trs_els  = [10,25,50,100,500,1000,5000,10000]
    # n_val_els  = [1, 2, 5, 10, 100]
    # n_val_sets = [1, 10, 100, 1000, 10000, 100000]

    min_val = -1  
    # thr_prior_kn   = (7/12-9/16)/2   # for this experiment scope, it's half the value of one of the lowest summands (AS - S)
    seed = 120395
    np.random.seed(seed=seed)
    # if process_number < 1:
    #     process_number = len(n_trs_els)*len(n_val_els)*len(n_val_sets)
    # if process_number > 72:
    #     print("Limiting process number to 72")
    #     process_number = 72

    # initialize result dict structure

    print("process_number",process_number)
    print("n_tests",n_tests)

        
    with threadpool_limits(limits=1, user_api='blas'):
        t1 = time.time()
        for n_add in n_add_variables:
            extra_vars = "DEFGHIJKLMNOPQRTUVWXYZ"
            var_set = "ABCS"+extra_vars[:n_add]

            ps = [str(t) for t in powerset(var_set)]
            len(ps)
            ps = [''.join(sorted(x for x in t if x.isalpha())) for t in ps]

            print("using",len(var_set),"variables:",var_set)
            print("ps len",len(ps))
            
            res_container = ResultDict()
            p = mp.Pool(process_number)

            input = ((n_el_tr, n_el_val, n_v_sets, i, ps, min_val, var_set) 
                        for n_el_tr, n_el_val, n_v_sets, i, ps, min_val, var_set
                        in list(itertools.product([n_trs_els], [n_val_els], [n_val_sets], [i for i in range(n_tests)],
                            [ps], [min_val], [var_set])))
            results = p.map(loop_calculation, input)   

            p.close()
            p.join()
            
            for res_single,res_multiple_min,res_multiple_max in results:
                res_container.add_values(res_single,res_multiple_min,res_multiple_max)
                
            with open('count_results_with'+str(n_add)+'add'+str(n_trs_els)+"ntels"+str(n_val_els)+"nvels"+str(n_val_sets)+'sets.pickle', 'wb') as handle:
                pickle.dump(res_container.results, handle, protocol=pickle.HIGHEST_PROTOCOL)
            
            print((time.time()-t1)/60,"minutes elapsed for",n_tests,"runs")