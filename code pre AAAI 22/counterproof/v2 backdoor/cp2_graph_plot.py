import networkx as nx
import matplotlib.pyplot as plt

dg = nx.DiGraph()
for el in "ABCST":
# for el in "ABCDST":
    dg.add_node(el)

dg.add_edge("A", "S")
dg.add_edge("A", "T")
dg.add_edge("B", "T")
dg.add_edge("T","C")
dg.add_edge("S","C")
# dg.add_edge("S","D")
# dg.add_edge("T","D")

pos = {"S" : (-1,0),"A" : (-1,1),"B" : (0,1),"C" : (0,-1),"T" : (0,0)}
# pos = {"S" : (-1,0),"A" : (-1,1),"B" : (0,1),"C" : (0,-1),"D" : (-1,-1),"T" : (0,0)}

plt.subplot()
nx.draw(dg, pos, node_size=1500, with_labels=True, font_weight='bold', node_color="grey", edge_color="black")
# plt.show()

plt.savefig('cp2_graph.pdf')
# plt.savefig('cp2_graphonemoreel.pdf')